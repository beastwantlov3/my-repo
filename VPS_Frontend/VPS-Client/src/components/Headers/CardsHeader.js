import React, { useEffect, useState } from "react";
// nodejs library to set properties for components
import PropTypes from "prop-types";
// reactstrap components
import {
  Breadcrumb,
  BreadcrumbItem,
  Button,
  Card,
  CardBody,
  CardTitle,
  Container,
  Row,
  Col,
  FormGroup,
  Form,
} from "reactstrap";
import axios from "axios";
import ReactDatetime from "react-datetime";

const CardsHeader = ({ name, parentName }) => {
  const [revenueData, setRevenueData] = useState(0);
  const [customerdata, setCustomerData] = useState(0);
  const [orderData, setOrderData] = useState(0);
  const [blogData, setBlogData] = useState(0);
  const [startDate, setStartDate] = React.useState(null);
  const [endDate, setEndDate] = React.useState(null);


  const handleReactDatetimeChange = (who, date) => {
    if (who === "startDate") {
      setStartDate(date);
    } else if (who === "endDate") {
      setEndDate(date);
    }
  };

  const getClassNameReactDatetimeDays = (date) => {
    if (startDate && endDate) {
    }
    if (startDate && endDate && startDate._d + "" !== endDate._d + "") {
      if (
        new Date(endDate._d + "") > new Date(date._d + "") &&
        new Date(startDate._d + "") < new Date(date._d + "")
      ) {
        return " middle-date";
      }
      if (endDate._d + "" === date._d + "") {
        return " end-date";
      }
      if (startDate._d + "" === date._d + "") {
        return " start-date";
      }
    }
    return "";
  };

  // const fetchRevenueData = async () => {
  //   try {
  //     const response = await axios.get(
  //       "https://localhost:7050/api/OrderDetail/totalprice"
  //     );
  //     setRevenueData(response.data);
  //   } catch (error) {
  //     console.error("Error fetching data:", error);
  //   }
  // };
  const fetchCustomerByDate = async () => {
    try {
      if (!startDate || !endDate) {
        return; // Exit early if either startDate or endDate is missing
      }

      const startDateString = startDate.format("YYYY-MM-DD");
      const endDateString = endDate.format("YYYY-MM-DD");

      const response = await axios.get(
        `https://localhost:7050/api/Account/AccountByDate?start=${startDateString}&end=${endDateString}`
      );

      // Assuming the API returns the revenue data in the format { totalRevenue: number }response.data;
      setCustomerData(response.data);
      console.log(response.data);
    } catch (error) {
      console.error("Error fetching revenue data:", error);
    }
  };
  const fetchOrderByDate = async () => {
    try {
      if (!startDate || !endDate) {
        return; // Exit early if either startDate or endDate is missing
      }

      const startDateString = startDate.format("YYYY-MM-DD");
      const endDateString = endDate.format("YYYY-MM-DD");

      const response = await axios.get(
        `https://localhost:7050/api/Order/TotalByDate?start=${startDateString}&end=${endDateString}`
      );

      // Assuming the API returns the revenue data in the format { totalRevenue: number }response.data;
      setOrderData(response.data);
      console.log(response.data);
    } catch (error) {
      console.error("Error fetching revenue data:", error);
    }
  };
  const fetchBlogByDate = async () => {
    try {
      if (!startDate || !endDate) {
        return; // Exit early if either startDate or endDate is missing
      }

      const startDateString = startDate.format("YYYY-MM-DD");
      const endDateString = endDate.format("YYYY-MM-DD");

      const response = await axios.get(
        `https://localhost:7050/api/Blog/BlogByDate?start=${startDateString}&end=${endDateString}`
      );

      // Assuming the API returns the revenue data in the format { totalRevenue: number }response.data;
      setBlogData(response.data);
      console.log(response.data);
    } catch (error) {
      console.error("Error fetching revenue data:", error);
    }
  };

  const fetchRevenueByDate = async () => {
    try {
      if (!startDate || !endDate) {
        return; // Exit early if either startDate or endDate is missing
      }

      const startDateString = startDate.format("YYYY-MM-DD");
      const endDateString = endDate.format("YYYY-MM-DD");

      const response = await axios.get(
        `https://localhost:7050/api/Order/MoneyByDate?start=${startDateString}&end=${endDateString}`
      );

      // Assuming the API returns the revenue data in the format { totalRevenue: number }response.data;
      setRevenueData(response.data);
      console.log(response.data);
    } catch (error) {
      console.error("Error fetching revenue data:", error);
    }
  };

  // Call fetchRevenueByDate whenever the startDate or endDate changes
  useEffect(() => {
    fetchRevenueByDate();
    fetchCustomerByDate()
    fetchOrderByDate()
    fetchBlogByDate()
  }, [startDate, endDate]);

  const VND = new Intl.NumberFormat('vi-VN', {
    style: 'currency',
    currency: 'VND',
  });

  return (
    <>
      <div className="header bg-info pb-6">
        <Container fluid>
          <div className="header-body">
            <Row className="align-items-center py-4">
              <Col lg="6" xs="7">
                <h6 className="h2 text-white d-inline-block mb-0">{name}</h6>{" "}
                <Breadcrumb
                  className="d-none d-md-inline-block ml-md-4"
                  listClassName="breadcrumb-links breadcrumb-dark"
                >
                  <BreadcrumbItem>
                    <a href="#pablo" onClick={(e) => e.preventDefault()}>
                      <i className="fas fa-home" />
                    </a>
                  </BreadcrumbItem>
                  <BreadcrumbItem>
                    <a href="#pablo" onClick={(e) => e.preventDefault()}>
                      {parentName}
                    </a>
                  </BreadcrumbItem>
                  <BreadcrumbItem aria-current="page" className="active">
                    {name}
                  </BreadcrumbItem>
                </Breadcrumb>
              </Col>
              {/* <Col className="text-right" lg="6" xs="5">
                <Button
                  className="btn-neutral"
                  color="default"
                  href="#pablo"
                  onClick={(e) => e.preventDefault()}
                  size="sm"
                >
                  New
                </Button>
                <Button
                  className="btn-neutral"
                  color="default"
                  href="#pablo"
                  onClick={(e) => e.preventDefault()}
                  size="sm"
                >
                  Filters
                </Button>
              </Col> */}
            </Row>
            <Form onSubmit={fetchRevenueByDate}>
              <Row className="input-daterange datepicker align-items-center justify-content-start mb-2">
                <Col xs={2} sm={2}>
                  <label className=" form-control-label text-dark">
                    Ngày bắt đầu
                  </label>
                  <FormGroup>
                    <ReactDatetime
                      inputProps={{
                        placeholder: "Chọn ngày bắt đầu",
                      }}
                      value={startDate}
                      timeFormat={false}
                      onChange={(date) =>
                        handleReactDatetimeChange("startDate", date)
                      } // Use the correct ReactDatetime component and pass the date object
                      renderDay={(props, currentDate, selectedDate) => {
                        let classes = props.className;
                        classes += getClassNameReactDatetimeDays(currentDate);
                        return (
                          <td {...props} className={classes}>
                            {currentDate.date()}
                          </td>
                        );
                      }}
                    />
                  </FormGroup>
                </Col>
                <Col xs={2} sm={2}>
                  <FormGroup>
                    <label className=" form-control-label text-dark">
                      Ngày kết thúc
                    </label>
                    <ReactDatetime
                      inputProps={{
                        placeholder: "Chọn ngày kết thúc",
                      }}
                      value={endDate}
                      timeFormat={false}
                      onChange={(date) =>
                        handleReactDatetimeChange("endDate", date)
                      } // Use the correct ReactDatetime component and pass the date object
                      renderDay={(props, currentDate, selectedDate) => {
                        let classes = props.className;
                        classes += getClassNameReactDatetimeDays(currentDate);
                        return (
                          <td {...props} className={classes}>
                            {currentDate.date()}
                          </td>
                        );
                      }}
                    />
                  </FormGroup>
                </Col>
              </Row>
            </Form>
            <Row>
              <Col md="6" xl="3">
                <Card className="card-stats">
                  <CardBody>
                    <Row>
                      <div className="col">
                        <CardTitle
                          tag="h5"
                          className="text-uppercase text-muted mb-0"
                        >
                          Tổng doanh thu
                        </CardTitle>
                        <span className="h2 font-weight-bold mb-0">
                          {VND.format(revenueData)=== null ? "Loading..." : VND.format(revenueData)} 
                        </span>
                      </div>
                      <Col className="col-auto">
                        <div className="icon icon-shape bg-gradient-primary text-white rounded-circle shadow">
                          <i className="fa-solid fa-dollar-sign"></i>
                        </div>
                      </Col>
                    </Row>
                    <p className="mt-3 mb-0 text-sm">
                      <span className="text-success mr-2">
                        <i className="fa fa-arrow-up" /> Tính từ ngày bắt đầu
                      </span>{" "}
                    </p>
                  </CardBody>
                </Card>
              </Col>

              <Col md="6" xl="3">
                <Card className="card-stats">
                  <CardBody>
                    <Row>
                      <div className="col">
                        <CardTitle
                          tag="h5"
                          className="text-uppercase text-muted mb-0"
                        >
                          Số lượng người dùng
                        </CardTitle>
                        <span className="h2 font-weight-bold mb-0">
                          {customerdata}
                        </span>
                      </div>
                      <Col className="col-auto">
                        <div className="icon icon-shape bg-gradient-orange text-white rounded-circle shadow">
                          <i className="ni ni-single-02" />
                        </div>
                      </Col>
                    </Row>
                    <p className="mt-3 mb-0 text-sm">
                      <span className="text-success mr-2">
                        <i className="fa fa-arrow-up" /> Tính từ ngày bắt đầu
                      </span>{" "}
                    </p>
                  </CardBody>
                </Card>
              </Col>
              <Col md="6" xl="3">
                <Card className="card-stats">
                  <CardBody>
                    <Row>
                      <div className="col">
                        <CardTitle
                          tag="h5"
                          className="text-uppercase text-muted mb-0"
                        >
                          Số lượng đơn hàng
                        </CardTitle>
                        <span className="h2 font-weight-bold mb-0">
                          {orderData}
                        </span>
                      </div>
                      <Col className="col-auto">
                        <div className="icon icon-shape bg-gradient-green text-white rounded-circle shadow">
                          <i className="ni ni-collection" />
                        </div>
                      </Col>
                    </Row>
                    <p className="mt-3 mb-0 text-sm">
                      <span className="text-success mr-2">
                        <i className="fa fa-arrow-up" /> Tính từ ngày bắt đầu
                      </span>{" "}
                    </p>
                  </CardBody>
                </Card>
              </Col>
              <Col md="6" xl="3">
                <Card className="card-stats">
                  <CardBody>
                    <Row>
                      <div className="col">
                        <CardTitle
                          tag="h5"
                          className="text-uppercase text-muted mb-0"
                        >
                          Tổng số bài đăng
                        </CardTitle>
                        <span className="h2 font-weight-bold mb-0">
                          {blogData}
                        </span>
                      </div>
                      <Col className="col-auto">
                        <div className="icon icon-shape bg-gradient-red text-white rounded-circle shadow">
                          <i className="ni ni-active-40" />
                        </div>
                      </Col>
                    </Row>
                    <p className="mt-3 mb-0 text-sm">
                      <span className="text-success mr-2">
                        <i className="fa fa-arrow-up" /> Tính từ ngày bắt đầu
                      </span>{" "}
                    </p>
                  </CardBody>
                </Card>
              </Col>
            </Row>
          </div>
        </Container>
      </div>
    </>
  );
};

CardsHeader.propTypes = {
  name: PropTypes.string,
  parentName: PropTypes.string,
};

export default CardsHeader;

import React, { useState, useEffect, useRef } from "react";
// nodejs library that concatenates classes
import classnames from "classnames";
// nodejs library to set properties for components
import PropTypes from "prop-types";
import axios from "axios";
import cogoToast from "cogo-toast";
// reactstrap components
import {
  Collapse,
  DropdownMenu,
  DropdownItem,
  UncontrolledDropdown,
  DropdownToggle,
  Form,
  ListGroupItem,
  ListGroup,
  Media,
  Navbar,
  NavItem,
  NavLink,
  Nav,
  Container,
  Row,
  Col,
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  FormGroup,
} from "reactstrap";
import NotificationAlert from "react-notification-alert";

const AdminNavbar = ({ theme, sidenavOpen, toggleSidenav }) => {
  const [messageData, setMessageData] = useState([]);
  const [totalAlerts, setTotalAlerts] = useState(0); // State to store the total number of alerts
  const [messageManagerData, setMessageManagerData] = useState([]);
  const [messageSalerData, setMessageSalerData] = useState([]);
  const [totalManagerAlerts, setTotalManagerAlerts] = useState(0);
  const [totalSalerAlerts, setTotalSalerAlerts] = useState(0);
  const [showAllAlerts, setShowAllAlerts] = useState(false);
  const [showModal, setShowModal] = useState(false);
  const [selectedMessage, setSelectedMessage] = useState(null);
  const notificationAlertRef = useRef(null);

  const notifySuccess = () => {
    let options = {
      place: "tc",
      message: (
        <div className="alert-text">
          <span className="alert-title" data-notify="title">
            {" "}
            Xác nhận nhập sản phẩm thành công
          </span>
        </div>
      ),
      type: "success",
      icon: "ni ni-bell-55",
      autoDismiss: 7,
    };
    notificationAlertRef.current.notificationAlert(options);
  };

  const notifyDanger = () => {
    let options = {
      place: "tc",
      message: (
        <div className="alert-text">
          <span className="alert-title" data-notify="title">
            {" "}
            Xác nhận nhập sản phẩm thất bại!
          </span>
          <span data-notify="message">
            Số lượng vượt quá số lượng tồn kho.{" "}
          </span>
        </div>
      ),
      type: "danger",
      icon: "ni ni-bell-55",
      autoDismiss: 7,
    };
    notificationAlertRef.current.notificationAlert(options);
  };

  const toggleModal = () => {
    setShowModal(!showModal);
  };
  const handleToggleModal = (message) => {
    setSelectedMessage(message);
    setShowModal(!showModal);
  };
  // function that on mobile devices makes the search open
  const openSearch = () => {
    document.body.classList.add("g-navbar-search-showing");
    setTimeout(function () {
      document.body.classList.remove("g-navbar-search-showing");
      document.body.classList.add("g-navbar-search-show");
    }, 150);
    setTimeout(function () {
      document.body.classList.add("g-navbar-search-shown");
    }, 300);
  };
  // function that on mobile devices makes the search close
  const closeSearch = () => {
    document.body.classList.remove("g-navbar-search-shown");
    setTimeout(function () {
      document.body.classList.remove("g-navbar-search-show");
      document.body.classList.add("g-navbar-search-hiding");
    }, 150);
    setTimeout(function () {
      document.body.classList.remove("g-navbar-search-hiding");
      document.body.classList.add("g-navbar-search-hidden");
    }, 300);
    setTimeout(function () {
      document.body.classList.remove("g-navbar-search-hidden");
    }, 500);
  };

  const handleMyProfileClick = (e) => {
    e.preventDefault();
    window.location.href = process.env.PUBLIC_URL + "/admin/profile";
  };

  const [user, setUser] = useState(null);
  useEffect(() => {
    // Truy cập thông tin đăng nhập từ localStorage
    const storedUsername = sessionStorage.getItem("username");
    setUser(storedUsername);
  }, []);

  //Lấy thông tin data UserProfile
  useEffect(() => {
    const fetchUserData = async () => {
      try {
        const token = sessionStorage.getItem("token");
        const config = {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        };
        const response = await axios.get(
          "https://localhost:7050/api/Account/get-user",
          config
        );
        setUser(response.data);
      } catch (error) {
        console.error(error);
      }
    };

    fetchUserData();
  }, []);

  const handleLogout = async () => {
    try {
      const response = await axios.post(
        "https://localhost:7050/api/Auth/logout"
      );
      if (response.status === 200) {
        // Xóa thông tin đăng nhập từ cookie hoặc localStorage
        // Ví dụ: Xóa thông tin đăng nhập từ localStorage
        sessionStorage.removeItem("token");
        // Xóa thông tin đăng nhập đã lưu trữ (nếu có) trong sessionStorage hoặc Redux
        sessionStorage.removeItem("loggedIn");
        sessionStorage.removeItem("username");
        sessionStorage.removeItem("roleName");
        // setUser(false);
        // Chuyển hướng đến trang đăng nhập (Login page)
        cogoToast.success("Đăng xuất thành công", { position: "bottom-left" });
        window.location.href = "/login-register";
      }
    } catch (error) {
      console.error(error);
      // Xử lý lỗi đăng xuất
    }
  };

  useEffect(() => {
    // Function to fetch the message data
    const fetchManagerAlertData = async () => {
      try {
        const response = await axios.get(
          "https://localhost:7050/api/Alert/list"
        );
        const messages = response.data.sort((a, b) => b.alertId - a.alertId);

        const salerMessages = [];
        const managerMessages = [];

        messages.forEach((message) => {
          if (message.customer !== null) {
            salerMessages.push(message);
          } else {
            managerMessages.push(message);
          }
        });

        setMessageSalerData(salerMessages);
        setMessageManagerData(managerMessages);
        setTotalManagerAlerts(managerMessages.length);
        setTotalSalerAlerts(salerMessages.length);

        console.log("Total Messages:", messages.length);
        console.log("Total Saler Messages:", salerMessages.length);
        console.log("Total Manager Messages:", managerMessages.length);
      } catch (error) {
        console.error("Error fetching order data:", error);
      }
    };

    // Fetch the data when the component mounts
    fetchManagerAlertData();
  }, []);

  useEffect(() => {
    // Function to fetch the message data
    const fetchAlertData = async () => {
      try {
        const token = sessionStorage.getItem("token");
        const config = {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        };
        const response = await axios.get(
          "https://localhost:7050/api/SupplierAlert/listbysupplierid",
          config
        );
        // Sort the data by id in descending order to get the newest messages first
        // const sortedData = response.data.sort((a, b) => b.alertId - a.alertId);
        // console.log(sortedData);
        // Sort the data by id in descending order to get the newest messages first
        const messages = response.data.sort((a, b) => b.sid - a.sid);
        const numberOfMessages = messages.length;
        setMessageData(messages);
        setTotalAlerts(numberOfMessages);
      } catch (error) {
        console.error("Error fetching order data:", error);
      }
    };

    // Fetch the data when the component mounts
    fetchAlertData();
  }, []);

  const formatDate = (dateString) => {
    const dateOptions = {
      day: "numeric",
      month: "long",
      year: "numeric",
    };

    const timeOptions = {
      hour: "numeric",
      minute: "numeric",
      hour12: true, // Use 12-hour clock format (AM/PM)
    };

    // Convert the date to the desired format
    const date = new Date(dateString);
    const formattedDate = date.toLocaleDateString("vi-VN", dateOptions);
    const formattedTime = date.toLocaleTimeString("vi-VN", timeOptions);

    // Return the formatted date string
    return `${formattedDate} vào ${formattedTime}`;
  };

  const handleToggleAlerts = () => {
    setShowAllAlerts(!showAllAlerts);
  };

  const handleFormSubmit = async (e) => {
    e.preventDefault();
    const form = e.target;
    if (form.checkValidity()) {
      try {
        const parsedQuantity = parseInt(selectedMessage.quantity, 10);
        const supplierProductId = selectedMessage.sproductId;
        const responseProduct = await axios.get(
          `https://localhost:7050/api/Product`
        );

        const responseBySupplierProduct = await axios.get(
          `https://localhost:7050/api/SupplierProduct/${supplierProductId}`
        );
        console.log(selectedMessage);

        if (parsedQuantity > responseBySupplierProduct.data.unitInStock) {
          notifyDanger();
          return;
        }

        // Check if the entered quantity exceeds the available unitInStock

        const existingProduct = responseProduct.data.find(
          (product) =>
            product.productName === responseBySupplierProduct.data.productName
        );

        if (existingProduct) {
          // Update the existing product's quantity
          const updatedExistingProduct = {
            ...existingProduct,
            unitInStock: existingProduct.unitInStock + parsedQuantity,
          };

          // Make a PUT request to update the existing product's quantity
          await axios.put(
            `https://localhost:7050/api/Product/${existingProduct.productId}`,
            updatedExistingProduct
          );
        } else {
          const product = {
            productName: responseBySupplierProduct.data.productName,
            categoryId: responseBySupplierProduct.data.categoryId, // Get the category value from the defaultValue prop of Select2
            brand: responseBySupplierProduct.data.brand,
            unitPrice: responseBySupplierProduct.data.unitPrice,
            unitInStock: parsedQuantity,
            statusId: responseBySupplierProduct.data.statusId,
            description: responseBySupplierProduct.data.description,
            quality: responseBySupplierProduct.data.quality,
            image: responseBySupplierProduct.data.image,
            // Add other fields as needed
          };
          // Make a POST request to the API to add the product
          await axios.post("https://localhost:7050/api/Product", product);
        }
        // Build the product object with the form data

        const responseProductSupplier = await axios.get(
          `https://localhost:7050/api/SupplierProduct/${supplierProductId}`
        );

        const updatedData = {
          ...responseBySupplierProduct.data,
          unitInStock:
            responseProductSupplier.data.unitInStock - parsedQuantity,
        };

        await axios.put(
          `https://localhost:7050/api/SupplierProduct/${selectedMessage.sproductId}`,
          updatedData
        );
        // Delete the alert
        await axios.delete(
          `https://localhost:7050/api/SupplierAlert/${selectedMessage.sid}`
        );

        // Remove the selected message from messageData
        setMessageData((prevMessageData) =>
          prevMessageData.filter(
            (message) => message.sid !== selectedMessage.sid
          )
        );

        // Show success alert
        notifySuccess();
        // Reset the form fields or redirect to another page
      } catch (error) {
        notifyDanger();
        console.error("Error adding product:", error);
      }
    }
  };
  const handleOrderToGet = async (orderId) => {
    try {
      const getorderData = await axios.get(
        `https://localhost:7050/api/Order/${orderId}`
      );

      const imageUrl = getorderData.data.currentImage;
      const extractedPart = imageUrl.match(/\/([^/]+)\?/)[1];
      const formattedPart = "product/" + extractedPart;

      const updatedOrderData = {
        ...getorderData.data,
        currentImage: formattedPart,
        statusId: 9,
      };
      await axios.put(
        `https://localhost:7050/api/Order/${orderId}`,
        updatedOrderData
      );
      notifySuccess();
      // Optionally, you can also include a refetch of the order data or perform any other necessary actions
    } catch (error) {
      notifyDanger();
      console.error(error);
    }
  };

  const handleReturnOrder = async (orderId, orderDetailId) => {
    try {
      const getorderData = await axios.get(
        `https://localhost:7050/api/Order/${orderId}`
      );

      const imageUrl = getorderData.data.currentImage;
      const extractedPart = imageUrl.match(/\/([^/]+)\?/)[1];
      const formattedPart = "product/" + extractedPart;

      const updatedOrderData = {
        ...getorderData.data,
        currentImage: formattedPart,
        statusId: 8,
      };

      await axios.put(
        `https://localhost:7050/api/Order/${orderId}`,
        updatedOrderData
      );

      const getorderDetailData = await axios.get(
        `https://localhost:7050/api/OrderDetail/${orderDetailId}`
      );

      const updatedOrderDetailData = {
        ...getorderDetailData.data,
        status: 2
      };
      await axios.put(
        `https://localhost:7050/api/OrderDetail/${orderDetailId}`,
        updatedOrderDetailData
      );
      notifySuccess();
    } catch (error) {
      notifyDanger();
      console.error(error);
    }
  };

  return (
    <>
      <NotificationAlert ref={notificationAlertRef} />
      <Navbar
        className={classnames(
          "navbar-top navbar-expand border-bottom",
          { "navbar-dark bg-info": theme === "dark" },
          { "navbar-light bg-secondary": theme === "light" }
        )}
      >
        {messageData ? (
          <Modal isOpen={showModal} toggle={toggleModal}>
            <ModalHeader toggle={toggleModal}>
              Xác nhận số lượng sản phẩm
            </ModalHeader>
            <ModalBody>
              <Form>
                <FormGroup>
                  <Row className="align-items-center">
                    <Col className="col-auto">
                      <img
                        alt="..."
                        className="avatar rounded-circle"
                        src={require("../../assets/img/theme/team-1.jpg")}
                      />
                    </Col>
                    <div className="col ml--2">
                      <div className="d-flex justify-content-between align-items-center">
                        <div>
                          <h4 className="mb-0 text-sm"></h4>
                          <small className="pt--1"></small>
                        </div>
                        <div className="text-right text-muted">
                          <small>
                            {selectedMessage
                              ? formatDate(selectedMessage.currentDate)
                              : ""}
                          </small>
                        </div>
                      </div>
                      <p className="text-sm mb-0">
                        {selectedMessage ? selectedMessage.message : ""}
                      </p>
                    </div>
                  </Row>
                </FormGroup>
              </Form>
            </ModalBody>
            <ModalFooter>
              <Button color="primary" onClick={handleFormSubmit}>
                Xác nhận
              </Button>{" "}
              <Button color="secondary" onClick={toggleModal}>
                Hủy
              </Button>
            </ModalFooter>
          </Modal>
        ) : null}
        <Container fluid>
          <Collapse navbar isOpen={true}>
            <Form
              className={classnames(
                "navbar-search form-inline mr-sm-3",
                { "navbar-search-light": theme === "dark" },
                { "navbar-search-dark": theme === "light" }
              )}
            >
              <button
                aria-label="Close"
                className="close"
                type="button"
                onClick={closeSearch}
              >
                <span aria-hidden={true}>×</span>
              </button>
            </Form>

            <Nav className="align-items-center ml-md-auto" navbar>
              <NavItem className="d-xl-none">
                <div
                  className={classnames(
                    "pr-3 sidenav-toggler",
                    { active: sidenavOpen },
                    { "sidenav-toggler-dark": theme === "dark" }
                  )}
                  onClick={toggleSidenav}
                >
                  <div className="sidenav-toggler-inner">
                    <i className="sidenav-toggler-line" />
                    <i className="sidenav-toggler-line" />
                    <i className="sidenav-toggler-line" />
                  </div>
                </div>
              </NavItem>
              <NavItem className="d-sm-none">
                <NavLink onClick={openSearch}>
                  <i className="ni ni-zoom-split-in" />
                </NavLink>
              </NavItem>
              {user &&
                (user.role === "Quản trị viên" ||
                  user.role === "Nhà cung cấp" ||
                  user.role === "Nhân viên bán hàng") && (
                  <UncontrolledDropdown nav>
                    <DropdownToggle className="nav-link" color="" tag="a">
                      <i className="ni ni-bell-55" />
                    </DropdownToggle>
                    {user.role === "Nhà cung cấp" && messageData.length > 0 ? (
                      <DropdownMenu
                        className="dropdown-menu-xl py-0 overflow-hidden"
                        right
                      >
                        <div className="px-3 py-3">
                          <h6 className="text-sm text-muted m-0">
                            Bạn có{" "}
                            <strong className="text-info">
                              {showAllAlerts
                                ? totalAlerts
                                : Math.min(totalAlerts, 4)}
                            </strong>{" "}
                            thông báo.
                          </h6>
                        </div>

                        {messageData
                          .slice(0, showAllAlerts ? messageData.length : 4)
                          .map((message, index) => {
                            return (
                              <ListGroup flush key={index}>
                                <ListGroupItem
                                  className="list-group-item-action"
                                  href="#pablo"
                                  onClick={(e) => {
                                    e.preventDefault();
                                    handleToggleModal(message); // Call the modified function
                                  }}
                                  tag="a"
                                >
                                  <Row
                                    className="align-items-center"
                                    onClick={() => {
                                      // setSelectedProductId(product.product.productId);
                                      toggleModal();
                                    }}
                                  >
                                    <Col className="col-auto">
                                      <img
                                        alt="..."
                                        className="avatar rounded-circle"
                                        src={require("../../assets/img/theme/team-1.jpg")}
                                      />
                                    </Col>
                                    <div className="col ml--2">
                                      <div className="d-flex justify-content-between align-items-center">
                                        <div>
                                          <h4 className="mb-0 text-sm">
                                            {/* {message.sale !== null
                                        ? message.sale.fullName
                                        : message.shipper.fullName} */}
                                          </h4>
                                          <small className="pt--1">
                                            {/* (
                                      {message.sale !== null
                                        ? "Saler"
                                        : "Shipper"}
                                      ) */}
                                          </small>
                                        </div>
                                        <div className="text-right text-muted">
                                          <small>
                                            {formatDate(message?.currentDate)}
                                          </small>
                                        </div>
                                      </div>
                                      <p className="text-sm mb-0">
                                        {message.message}
                                      </p>
                                    </div>
                                  </Row>
                                </ListGroupItem>
                              </ListGroup>
                            );
                          })}
                        <DropdownItem
                          className="text-center text-info font-weight-bold py-3"
                          href="#pablo"
                          onClick={(e) => {
                            e.preventDefault();
                            e.stopPropagation(); // Prevent click event propagation
                            handleToggleAlerts(); // Toggle the state when "View all" is clicked
                          }}
                        >
                          {showAllAlerts ? "Trở lại" : "Xem thêm"}
                        </DropdownItem>
                      </DropdownMenu>
                    ) : user.role === "Quản trị viên" &&
                      messageManagerData.length > 0 ? (
                      <DropdownMenu
                        className="dropdown-menu-xl py-0 overflow-hidden"
                        right
                      >
                        <div className="px-3 py-3">
                          <h6 className="text-sm text-muted m-0">
                            Bạn có{" "}
                            <strong className="text-info">
                              {showAllAlerts
                                ? totalManagerAlerts
                                : Math.min(totalManagerAlerts, 4)}
                            </strong>{" "}
                            thông báo.
                          </h6>
                        </div>

                        {messageManagerData
                          .slice(
                            0,
                            showAllAlerts ? messageManagerData.length : 4
                          )
                          .map((message, index) => {
                            return (
                              <ListGroup flush key={index}>
                                <ListGroupItem
                                  className="list-group-item-action"
                                  href="#pablo"
                                  onClick={(e) => {
                                    e.preventDefault();
                                  }}
                                  tag="a"
                                >
                                  <Row
                                    className="align-items-center"
                                    onClick={() => {
                                      // setSelectedProductId(product.product.productId);
                                    }}
                                  >
                                    <Col className="col-auto">
                                      <img
                                        alt="..."
                                        className="avatar rounded-circle"
                                        src={require("../../assets/img/theme/team-1.jpg")}
                                      />
                                    </Col>
                                    <div className="col ml--2">
                                      <div className="d-flex justify-content-between align-items-center">
                                        <div>
                                          <h4 className="mb-0 text-sm">
                                            {message?.sale !== null
                                              ? message?.sale?.fullName
                                              : message?.shipper?.fullName}
                                          </h4>
                                          <small className="pt--1">
                                            {message?.sale !== null
                                              ? "Saler"
                                              : "Shipper"}
                                          </small>
                                        </div>
                                        <div className="text-right text-muted">
                                          <small>
                                            {formatDate(message?.currentDate)}
                                          </small>
                                        </div>
                                      </div>
                                      <p className="text-sm mb-0">
                                        {message?.message}
                                      </p>
                                    </div>
                                  </Row>
                                </ListGroupItem>
                              </ListGroup>
                            );
                          })}
                        <DropdownItem
                          className="text-center text-info font-weight-bold py-3"
                          href="#pablo"
                          onClick={(e) => {
                            e.preventDefault();
                            e.stopPropagation(); // Prevent click event propagation
                            handleToggleAlerts(); // Toggle the state when "View all" is clicked
                          }}
                        >
                          {showAllAlerts ? "Trở lại" : "Xem thêm"}
                        </DropdownItem>
                      </DropdownMenu>
                    ) : user.role === "Nhân viên bán hàng" &&
                      messageSalerData.length > 0 ? (
                      <DropdownMenu
                        className="dropdown-menu-xl py-0 overflow-hidden"
                        right
                      >
                        <div className="px-3 py-3">
                          <h6 className="text-sm text-muted m-0">
                            Bạn có{" "}
                            <strong className="text-info">
                              {showAllAlerts
                                ? totalSalerAlerts
                                : Math.min(totalSalerAlerts, 4)}
                            </strong>{" "}
                            thông báo.
                          </h6>
                        </div>

                        {messageSalerData
                          .slice(0, showAllAlerts ? messageSalerData.length : 4)
                          .map((message, index) => {
                            return (
                              <ListGroup flush key={index}>
                                <ListGroupItem
                                  className="list-group-item-action"
                                  href="#pablo"
                                  onClick={(e) => {
                                    e.preventDefault();
                                  }}
                                  tag="a"
                                >
                                  <Row
                                    className="align-items-center"
                                    onClick={() => {
                                      // setSelectedProductId(product.product.productId);
                                    }}
                                  >
                                    <Col className="col-auto">
                                      <img
                                        alt="..."
                                        className="avatar rounded-circle"
                                        src={require("../../assets/img/theme/team-1.jpg")}
                                      />
                                    </Col>
                                    <div className="col ml--2">
                                      <div className="d-flex justify-content-between align-items-center">
                                        <div>
                                          <h4 className="mb-0 text-sm">
                                            {message?.customer?.fullName}
                                          </h4>
                                          <small className="pt--1">
                                            {"Customer"}
                                          </small>
                                        </div>

                                        <div className="text-right text-muted">
                                          <small>
                                            {formatDate(message?.currentDate)}
                                          </small>
                                        </div>
                                      </div>
                                      <p className="text-sm mb-0">
                                        {message?.message}
                                      </p>
                                      {message?.message.startsWith(
                                        "Khách hàng đã sử dụng xong đơn hàng"
                                      ) ? (
                                        <button
                                          className="btn btn-sm btn-primary mt-2"
                                          onClick={() => {
                                            handleOrderToGet(message.orderId);
                                          }}
                                        >
                                          Xác nhận
                                        </button>
                                      ) : message?.message.startsWith(
                                          "Lý do hoàn đơn hàng"
                                        ) ? (
                                        <button
                                          className="btn btn-sm btn-danger mt-2"
                                          onClick={() => {
                                            handleReturnOrder(
                                              message.orderId,
                                              message.orderDetailId
                                            );
                                          }}
                                        >
                                          Hoàn đơn
                                        </button>
                                      ) : null}
                                    </div>
                                  </Row>
                                </ListGroupItem>
                              </ListGroup>
                            );
                          })}
                        <DropdownItem
                          className="text-center text-info font-weight-bold py-3"
                          href="#pablo"
                          onClick={(e) => {
                            e.preventDefault();
                            e.stopPropagation(); // Prevent click event propagation
                            handleToggleAlerts(); // Toggle the state when "View all" is clicked
                          }}
                        >
                          {showAllAlerts ? "Trở lại" : "Xem thêm"}
                        </DropdownItem>
                      </DropdownMenu>
                    ) : null}
                  </UncontrolledDropdown>
                )}
            </Nav>
            {user ? (
              <Nav className="align-items-center ml-auto ml-md-0" navbar>
                <UncontrolledDropdown nav>
                  <DropdownToggle className="nav-link pr-0" color="" tag="a">
                    <Media className="align-items-center">
                      <span className="avatar avatar-sm rounded-circle">
                        <img alt="..." src={user.avatar} />
                      </span>
                      <Media className="ml-2 d-none d-lg-block">
                        <span className="mb-0 text-sm font-weight-bold">
                          {user.fullName}
                        </span>
                      </Media>
                    </Media>
                  </DropdownToggle>
                  <DropdownMenu right>
                    <DropdownItem className="noti-title" header tag="div">
                      <h6 className="text-overflow m-0">
                        Welcome, {user.fullName}!
                      </h6>
                    </DropdownItem>
                    <DropdownItem href="#pablo" onClick={handleMyProfileClick}>
                      <i className="ni ni-single-02" />
                      <span>My profile</span>
                    </DropdownItem>
                    {/* <DropdownItem
                      href="#pablo"
                      onClick={(e) => e.preventDefault()}
                    >
                      <i className="ni ni-settings-gear-65" />
                      <span>Settings</span>
                    </DropdownItem>
                    <DropdownItem
                      href="#pablo"
                      onClick={(e) => e.preventDefault()}
                    >
                      <i className="ni ni-calendar-grid-58" />
                      <span>Activity</span>
                    </DropdownItem>
                    <DropdownItem
                      href="#pablo"
                      onClick={(e) => e.preventDefault()}
                    >
                      <i className="ni ni-support-16" />
                      <span>Support</span>
                    </DropdownItem> */}
                    <DropdownItem divider />
                    <DropdownItem
                      // href="#pablo"
                      onClick={handleLogout}
                    >
                      <i className="ni ni-user-run" />
                      <span>Logout</span>
                    </DropdownItem>
                  </DropdownMenu>
                </UncontrolledDropdown>
              </Nav>
            ) : null}
          </Collapse>
        </Container>
      </Navbar>
    </>
  );
};

AdminNavbar.defaultProps = {
  toggleSidenav: () => {},
  sidenavOpen: false,
  theme: "dark",
};
AdminNavbar.propTypes = {
  toggleSidenav: PropTypes.func,
  sidenavOpen: PropTypes.bool,
  theme: PropTypes.oneOf(["dark", "light"]),
};

export default AdminNavbar;

import PropTypes from "prop-types";
import React, { useState, useEffect } from "react";
import { useHistory, Link } from "react-router-dom";
import { useSelector } from "react-redux";
import clsx from "clsx";
import MenuCart from "./sub-components/MenuCart";
import axios from "axios";
import cogoToast from "cogo-toast";
import { useDispatch } from "react-redux";
import { clearWishlist } from "../../store/slices/wishlist-slice";
import { clearCompare } from "../../store/slices/compare-slice";
import { setCartItemsForUsername } from "../../store/slices/cart-slice";

const IconGroup = ({ iconWhiteClass }) => {
  const dispatch = useDispatch();
  const handleClick = (e) => {
    e.currentTarget.nextSibling.classList.toggle("active");
  };

  const triggerMobileMenu = () => {
    const offcanvasMobileMenu = document.querySelector(
      "#offcanvas-mobile-menu"
    );
    offcanvasMobileMenu.classList.add("active");
  };
  const { compareItems } = useSelector((state) => state.compare);
  const { wishlistItems } = useSelector((state) => state.wishlist);
  const { cartItems } = useSelector((state) => state.cart);
  const isLoggedIn = sessionStorage.getItem("loggedIn");
  const history = useHistory();

  const [user, setUser] = useState(null);
  useEffect(() => {
    // Truy cập thông tin đăng nhập từ localStorage
    const storedUsername = sessionStorage.getItem("username");
    setUser(storedUsername);
  }, []);

  const avatarStyle = {
    width: "30px", // Điều chỉnh kích thước avatar tùy ý
    height: "30px",
    borderRadius: "50%", // Góc bo tròn là 50% của chiều rộng (hoặc chiều cao), tức là làm tròn thành hình tròn
    overflow: "hidden", // Đảm bảo hình ảnh không bị tràn ra ngoài phần bo tròn
    objectFit: "cover", // Đảm bảo hình ảnh không bị biến dạng
    // marginRight: '10px', // Tùy chỉnh khoảng cách giữa hình ảnh và nút
  };

  //Lấy thông tin data UserProfile
  useEffect(() => {
    const fetchUserData = async () => {
      try {
        const token = sessionStorage.getItem("token");
        const config = {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        };
        const response = await axios.get(
          "https://localhost:7050/api/Account/get-user",
          config
        );
        setUser(response.data);
      } catch (error) {
        console.error(error);
      }
    };

    fetchUserData();
  }, []);

  const handleLogout = async () => {
    try {
      const response = await axios.post(
        "https://localhost:7050/api/Auth/logout"
      );
      if (response.status === 200) {
        // Xóa thông tin đăng nhập từ cookie hoặc localStorage
        // Ví dụ: Xóa thông tin đăng nhập từ localStorage
        // Xóa token đã lưu trữ trong sessionStorage
        // dispatch(clearCartForUsername(username));

        // saveCartToStorage(username,cartItems)
        sessionStorage.removeItem("token");
        // Xóa thông tin đăng nhập đã lưu trữ (nếu có) trong sessionStorage hoặc Redux
        sessionStorage.removeItem("loggedIn");
        sessionStorage.removeItem("username");
        sessionStorage.removeItem("roleName");
        sessionStorage.removeItem("cartItems");
        sessionStorage.removeItem("compareItems");
        sessionStorage.removeItem("wishlistItems");
        // Cập nhật lại các biến state về trạng thái rỗng hoặc 0
        // dispatch(clearCartForUsername(username));
        dispatch({ type: "cart" });
        dispatch({ type: "compare" });
        dispatch({ type: "wishlist" });
        setUser(false);
        dispatch(clearWishlist());
        dispatch(clearCompare());
        // Chuyển hướng đến trang đăng nhập (Login page)
        cogoToast.success("Đăng xuất thành công", { position: "bottom-left" });
        window.location.href = "/login-register";
      }
    } catch (error) {
      console.error(error);
      // Xử lý lỗi đăng xuất
    }
  };

  const handleCompareClick = () => {
    if (!isLoggedIn) {
      window.location.href = "/login-register";
      cogoToast.error("Bạn cần phải đăng nhập", { position: "bottom-left" });
    } else {
      // Thực hiện hành động tiếp theo khi người dùng bấm vào nút Compare
      // Ví dụ: Chuyển hướng đến trang so sánh sản phẩm
      history.push("/compare");
    }
  };

  const handleWishlistClick = () => {
    if (!isLoggedIn) {
      history.push("/login-register");
      cogoToast.error("Bạn cần phải đăng nhập", { position: "bottom-left" });
    } else {
      // Thực hiện hành động tiếp theo khi người dùng bấm vào nút Wishlist
      history.push("/wishlist");
    }
  };

  const handleCartClick = () => {
    if (!isLoggedIn) {
      window.location.href = "/login-register";
      cogoToast.error("Bạn cần phải đăng nhập", { position: "bottom-left" });
    } else {
      // Thực hiện hành động tiếp theo khi người dùng bấm vào nút Cart
      history.push("/cart");
    }
  };

  return (
    <div className={clsx("header-right-wrap", iconWhiteClass)}>
      <div className="same-style header-search d-none d-lg-block">
        <button className="search-active" onClick={(e) => handleClick(e)}>
          <i className="pe-7s-search" />
        </button>
        <div className="search-content">
          <form action="#">
            <input type="text" placeholder="Search" />
            <button className="button-search">
              <i className="pe-7s-search" />
            </button>
          </form>
        </div>
      </div>
      <div className="same-style account-setting d-none d-lg-block">
        {user ? (
          <button
            className="account-setting-active"
            onClick={(e) => handleClick(e)}
          >
            <img
              src={user.avatar}
              alt="Avatar"
              style={avatarStyle}
              onClick={(e) => handleClick(e)}
            />
          </button>
        ) : (
          <button
            className="account-setting-active"
            onClick={(e) => handleClick(e)}
          >
            <i className="pe-7s-user-female" />
          </button>
        )}
        {user ? (
          <div className="account-dropdown">
            <ul>
              <li>Welcome, {user.fullName}</li>
              <li style={{ fontSize: "15px" }}>
                <Link to={process.env.PUBLIC_URL + "/my-account"}>
                  Tài khoản của tôi
                </Link>
              </li>
              <li style={{ fontSize: "15px" }}>
                <Link to={process.env.PUBLIC_URL + "/view-list-orders"}>
                  Xem đơn hàng
                </Link>
              </li>
              <li>
                <Link>
                  <span
                    className="logout-link"
                    onClick={handleLogout}
                    style={{ cursor: "pointer" }}
                  >
                    Đăng xuất
                  </span>
                </Link>
              </li>
              
            </ul>
          </div>
        ) : (
          <div className="account-dropdown">
            <ul>
              <li>
                <Link to={process.env.PUBLIC_URL + "/login-register"}>
                  Login
                </Link>
              </li>
              <li>
                <Link to={process.env.PUBLIC_URL + "/login-register"}>
                  Register
                </Link>
              </li>
            </ul>
          </div>
        )}
      </div>

      <div className="same-style header-compare">
        <Link
          to={
            isLoggedIn
              ? process.env.PUBLIC_URL + "/compare"
              : process.env.PUBLIC_URL + "/login-register"
          }
          onClick={handleCompareClick}
        >
          <i className="pe-7s-shuffle" />
          <span className="count-style">
            {isLoggedIn ? compareItems.length : 0}
          </span>
        </Link>
      </div>
      <div className="same-style header-wishlist">
        <Link
          to={
            isLoggedIn
              ? process.env.PUBLIC_URL + "/wishlist"
              : process.env.PUBLIC_URL + "/login-register"
          }
          onClick={handleWishlistClick}
        >
          <i className="pe-7s-like" />
          <span className="count-style">
            {isLoggedIn ? wishlistItems.length : 0}
          </span>
        </Link>
      </div>
      <div className="same-style cart-wrap d-none d-lg-block">
        <button className="icon-cart" onClick={handleCartClick}>
          <i className="pe-7s-shopbag" />
          <span className="count-style">
            {isLoggedIn ? cartItems.length : 0}
          </span>
        </button>
        {/* menu cart */}
        <MenuCart />
      </div>
      <div className="same-style cart-wrap d-block d-lg-none">
        <Link
          className="icon-cart"
          to={
            isLoggedIn
              ? process.env.PUBLIC_URL + "/cart"
              : process.env.PUBLIC_URL + "/login-register"
          }
          onClick={handleCartClick}
        >
          <i className="pe-7s-shopbag" />
          <span className="count-style">
            {isLoggedIn ? cartItems.length : 0}
          </span>
        </Link>
      </div>
      <div className="same-style mobile-off-canvas d-block d-lg-none">
        <button
          className="mobile-aside-button"
          onClick={() => triggerMobileMenu()}
        >
          <i className="pe-7s-menu" />
        </button>
      </div>
    </div>
  );
};

IconGroup.propTypes = {
  iconWhiteClass: PropTypes.string,
};

export default IconGroup;

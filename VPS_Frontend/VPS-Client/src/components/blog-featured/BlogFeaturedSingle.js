import PropTypes from "prop-types";
import { Link } from "react-router-dom";

const BlogFeaturedSingle = ({ singlePost }) => {
  // Add a conditional check for singlePost.category
  const categories =
    singlePost.category && Array.isArray(singlePost.category)
      ? singlePost.category
      : [];
  const formatDate = (dateString) => {
    const dateOptions = {
      day: "numeric",
      month: "long",
      year: "numeric",
    };

    const date = new Date(dateString);
    const formattedDate = date.toLocaleDateString("vi-VN", dateOptions);

    return `${formattedDate}`;
  };
  return (
    <div className="blog-wrap mb-30 scroll-zoom">
      <div className="blog-img" style={{
                width: "350px", 
                height: "270px",
              }}>
        <Link
          to={
            process.env.PUBLIC_URL + "/blog-details-standard/" + singlePost.id
          }
        >
          <img src={process.env.PUBLIC_URL + singlePost.image} alt="" />
        </Link>
        <div className="blog-category-names">
          {/* Use the categories variable instead of singlePost.category */}
          {categories.map((singleCategory, key) => {
            return (
              <span className="purple" key={key}>
                {singleCategory}
              </span>
            );
          })}
        </div>
      </div>
      <div className="blog-content-wrap">
        <div className="blog-content text-center" style={{
                width: "320px", 
                height: "140px",
              }}>
          <h3>
            <Link
              to={
                process.env.PUBLIC_URL +
                "/blog-details-standard/" +
                singlePost.id
              }
            >
              {singlePost.title}
            </Link>
          </h3>
          <span>
            Ngày đăng bài:{" "}
            <Link
              to={
                process.env.PUBLIC_URL +
                "/blog-details-standard/" +
                singlePost.id
              }
            >
              {formatDate(singlePost.publishDate)}
            </Link>
          </span>
        </div>
      </div>
    </div>
  );
};

BlogFeaturedSingle.propTypes = {
  singlePost: PropTypes.shape({}),
};

export default BlogFeaturedSingle;

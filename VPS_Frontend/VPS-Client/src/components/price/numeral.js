import React from 'react';
import numeral from 'numeral';

const AmountFormatted = ({ amount }) => {
  const formattedAmount = numeral(amount).format('0,0') + ' VND';

  return <span>{formattedAmount}</span>;
};

export default AmountFormatted;

import PropTypes from "prop-types";
import { Fragment, useState } from "react";
import { useDispatch } from "react-redux";
import { useHistory, Link } from "react-router-dom";
import clsx from "clsx";
import { getDiscountPrice } from "../../helpers/product";
import Rating from "./sub-components/ProductRating";
import ProductModal from "./ProductModal";
import { addToCart } from "../../store/slices/cart-slice";
import { addToWishlist } from "../../store/slices/wishlist-slice";
import { addToCompare } from "../../store/slices/compare-slice";
import AmountFormatted from "../../../src/components/price/numeral";

const ProductGridListSingle = ({
  key,
  product,
  currency,
  cartItem,
  wishlistItem,
  compareItem,
  spaceBottomClass,
}) => {
  const [modalShow, setModalShow] = useState(false);
  const discountedPrice = getDiscountPrice(product.unitPrice, product.discount);
  const finalProductPrice = +(
    product.unitPrice * currency.currencyRate
  ).toFixed(3);
  const finalDiscountedPrice = +(
    discountedPrice * currency.currencyRate
  ).toFixed(3);
  const dispatch = useDispatch();
  const username = sessionStorage.getItem("username");
  const history = useHistory();

  const handleAddToCart = () => {
    if (!username) {
      // Chưa đăng nhập, chuyển hướng về trang home
      history.push("/login-register");
      return;
    }

    console.log("dispatching addToCart action"); // Debugging statement
    dispatch(addToCart(product));
  };

  const handleAddToWishlist = () => {
    if (!username) {
      // Chưa đăng nhập, chuyển hướng về trang login
      history.push("/login-register");
      return;
    }

    // Đã đăng nhập, thực hiện xử lý thêm sản phẩm vào danh sách yêu thích
    dispatch(addToWishlist(product));
  };

  const handleAddToCompare = () => {
    if (!username) {
      // Chưa đăng nhập, chuyển hướng về trang login
      history.push("/login-register");
      return;
    }

    // Đã đăng nhập, thực hiện xử lý thêm sản phẩm vào danh sách so sánh
    dispatch(addToCompare(product));
  };
  console.log("PUBLIC_URL:", process.env.PUBLIC_URL);

  return (
    <Fragment>
      <div className={clsx("product-wrap", spaceBottomClass)} key={key}>
        <div className="product-img d-block" key={key}>
          <Link to={process.env.PUBLIC_URL + "/product/" + product.productId}>
            <img
              className="default-img"
              src={process.env.PUBLIC_URL + product.image}
              alt=""
              style={{
                width: "450px", // Đặt chiều rộng ảnh là 100%
                height: "250px", // Giữ tỷ lệ khung hình
                /* Các thuộc tính CSS khác tùy theo nhu cầu */
              }}
            />
            <img
              className="hover-img"
              src={process.env.PUBLIC_URL + product.image}
              alt=""
              style={{
                width: "100%", // Đặt chiều rộng ảnh là 100%
                height: "auto", // Giữ tỷ lệ khung hình
                /* Các thuộc tính CSS khác tùy theo nhu cầu */
              }}
            />
          </Link>
          {product.discount || product.new ? (
            <div className="product-img-badges">
              {product.discount ? (
                <span className="pink">-{product.discount}%</span>
              ) : (
                ""
              )}
            </div>
          ) : (
            ""
          )}

          <div className="product-action" key={key}>
            <div className="pro-same-action pro-wishlist">
              <button
                className={wishlistItem !== undefined ? "active" : ""}
                disabled={wishlistItem !== undefined}
                title={
                  wishlistItem !== undefined
                    ? "Added to wishlist"
                    : "Add to wishlist"
                }
                onClick={() => dispatch(addToWishlist(product))}
              >
                <i className="pe-7s-like" />
              </button>
            </div>
            <div className="pro-same-action pro-cart">
              {product.affiliateLink ? (
                <a
                  href={product.affiliateLink}
                  rel="noopener noreferrer"
                  target="_blank"
                >
                  {" "}
                  Buy now{" "}
                </a>
              ) : product.variation && product.variation.length >= 1 ? (
                <Link
                  to={`${process.env.PUBLIC_URL}/product/${product.productId}`}
                >
                  Select Option
                </Link>
              ) : product.unitInStock && product.unitInStock > 0 ? (
                <button
                  onClick={handleAddToCart}
                  className={
                    cartItem !== undefined && cartItem.quantity > 0
                      ? "active"
                      : ""
                  }
                  disabled={cartItem !== undefined && cartItem.quantity > 0}
                  title={
                    cartItem !== undefined ? "Added to cart" : "Add to cart"
                  }
                >
                  {" "}
                  <i className="pe-7s-cart"></i>{" "}
                  {cartItem !== undefined && cartItem.quantity > 0
                    ? "Added"
                    : "Add to cart"}
                </button>
              ) : (
                <button disabled className="active">
                  Out of Stock
                </button>
              )}
            </div>
            <div className="pro-same-action pro-quickview">
              <button onClick={() => setModalShow(true)} title="Quick View">
                <i className="pe-7s-look" />
              </button>
            </div>
          </div>
        </div>
        <div className="product-content text-center" key={key}>
          <h3>
            <Link className="mb-4" to={process.env.PUBLIC_URL + "/product/" + product.productId}>
              {product.productName}
            </Link>
          </h3>
          {product.quality && product.quality > 0 ? (
            <div className="product-rating">
              <Rating ratingValue={product.quality} />
            </div>
          ) : (
            ""
          )}
          <div className="product-price">
            {discountedPrice !== null ? (
              <Fragment>
                <span>
                  <AmountFormatted
                    amount={finalDiscountedPrice + " " + currency.currencySymbol}
                  />
                  
                </span>{" "}
                <span className="old">
                <AmountFormatted
                    amount={finalProductPrice + " " + currency.currencySymbol}
                  />
                  
                </span>
              </Fragment>
            ) : (
              <span>
                <AmountFormatted
                    amount={finalProductPrice + " " + currency.currencySymbol}
                  /> </span>
            )}
          </div>
        </div>
      </div>
      <div className="shop-list-wrap mb-30" key={key}>
        <div className="row">
          <div className="col-xl-4 col-md-5 col-sm-6">
            <div className="product-list-image-wrap">
              <div className="product-img">
                <Link
                  to={process.env.PUBLIC_URL + "/product/" + product.productId}
                >
                  <img
                    className="default-img img-fluid"
                    src={process.env.PUBLIC_URL + product.image}
                    alt=""
                  />
                  <img
                    className="hover-img img-fluid"
                    src={process.env.PUBLIC_URL + product.image}
                    alt=""
                  />
                </Link>
                {product.discount || product.new ? (
                  <div className="product-img-badges">
                    {product.discount ? (
                      <span className="pink">-{product.discount}%</span>
                    ) : (
                      ""
                    )}
                    {/* {product.new ? <span className="purple">New</span> : ""} */}
                  </div>
                ) : (
                  ""
                )}
              </div>
            </div>
          </div>
          <div className="col-xl-8 col-md-7 col-sm-6">
            <div className="shop-list-content">
              <h3>
                <Link
                  to={process.env.PUBLIC_URL + "/product/" + product.productId}
                >
                  {product.productName}
                </Link>
              </h3>
              <div className="product-list-price">
                {discountedPrice !== null ? (
                  <Fragment>
                    <span>
                      <AmountFormatted
                        amount={
                          finalDiscountedPrice + " " + currency.currencySymbol
                        }
                      />
                    </span>{" "}
                    <span className="old">
                      <AmountFormatted
                        amount={
                          finalProductPrice + " " + currency.currencySymbol
                        }
                      />
                    </span>
                  </Fragment>
                ) : (
                  <span>
                    {finalProductPrice + " " + currency.currencySymbol}{" "}
                  </span>
                )}
              </div>
              {product.quality && product.quality > 0 ? (
                <div className="rating-review">
                  <div className="product-list-rating">
                    <Rating ratingValue={product.quality} />
                  </div>
                </div>
              ) : (
                ""
              )}
              {product.description ? <p>{product.description}</p> : ""}

              <div className="shop-list-actions d-flex align-items-center">
                <div className="shop-list-btn btn-hover">
                  {product.affiliateLink ? (
                    <a
                      href={product.affiliateLink}
                      rel="noopener noreferrer"
                      target="_blank"
                    >
                      Buy now
                    </a>
                  ) : product.variation && product.variation.length >= 1 ? (
                    <Link
                      to={`${process.env.PUBLIC_URL}/product/${product.productId}`}
                    >
                      Select Option
                    </Link>
                  ) : product.unitInStock && product.unitInStock > 0 ? (
                    <button
                      onClick={handleAddToCart}
                      className={
                        cartItem !== undefined && cartItem.quantity > 0
                          ? "active"
                          : ""
                      }
                      disabled={cartItem !== undefined && cartItem.quantity > 0}
                      title={
                        cartItem !== undefined ? "Added to cart" : "Add to cart"
                      }
                    >
                      <i className="pe-7s-cart" />{" "}
                      {cartItem !== undefined && cartItem.quantity > 0
                        ? "Added"
                        : "Add to cart"}
                    </button>
                  ) : (
                    <button disabled className="active">
                      Out of Stock
                    </button>
                  )}
                </div>

                <div className="shop-list-wishlist ml-10">
                  <button
                    className={wishlistItem !== undefined ? "active" : ""}
                    disabled={wishlistItem !== undefined}
                    title={
                      wishlistItem !== undefined
                        ? "Added to wishlist"
                        : "Add to wishlist"
                    }
                    onClick={handleAddToWishlist}
                  >
                    <i className="pe-7s-like" />
                  </button>
                </div>

                <div className="shop-list-compare ml-10">
                  <button
                    className={compareItem !== undefined ? "active" : ""}
                    disabled={compareItem !== undefined}
                    title={
                      compareItem !== undefined
                        ? "Added to compare"
                        : "Add to compare"
                    }
                    onClick={handleAddToCompare}
                  >
                    <i className="pe-7s-shuffle" />
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <ProductModal
        show={modalShow}
        onHide={() => setModalShow(false)}
        product={product}
        currency={currency}
        discountedPrice={discountedPrice}
        finalProductPrice={finalProductPrice}
        finalDiscountedPrice={finalDiscountedPrice}
        wishlistItem={wishlistItem}
        compareItem={compareItem}
      />
    </Fragment>
  );
};

ProductGridListSingle.propTypes = {
  cartItem: PropTypes.shape({}),
  compareItem: PropTypes.shape({}),
  currency: PropTypes.shape({}),
  product: PropTypes.shape({}),
  spaceBottomClass: PropTypes.string,
  wishlistItem: PropTypes.shape({}),
};

export default ProductGridListSingle;

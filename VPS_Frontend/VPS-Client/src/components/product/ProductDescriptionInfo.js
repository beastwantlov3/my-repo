import PropTypes from "prop-types";
import React, { Fragment, useState } from "react";
import { Link } from "react-router-dom";
import { useDispatch } from "react-redux";
import { getProductCartQuantity } from "../../helpers/product";
import Rating from "./sub-components/ProductRating";
import { addToCart } from "../../store/slices/cart-slice";
import { addToWishlist } from "../../store/slices/wishlist-slice";
import { addToCompare } from "../../store/slices/compare-slice";
import { useHistory } from "react-router-dom";
import AmountFormatted from "../../../src/components/price/numeral";
import { useEffect } from "react";
import axios from "axios";

const ProductDescriptionInfo = ({
  product,
  discountedPrice,
  currency,
  finalDiscountedPrice,
  finalProductPrice,
  cartItems,
  wishlistItem,
  compareItem,
}) => {
  const dispatch = useDispatch();
  const [productStock, setProductStock] = useState(
    product.variation ? product.variation[0].size[0].stock : product.unitInStock
  );
  const [quantityCount, setQuantityCount] = useState(1);
  const [qualityPercentages, setQualityPercentages] = useState([]);
  const [selectedQuality, setSelectedQuality] = useState(null);
  const [finalDiscountedPrices, setFinalDiscountedPrices] =
    useState(finalDiscountedPrice);

  const productCartQty = getProductCartQuantity(cartItems, product);
  const username = sessionStorage.getItem("username");
  const navigate = useHistory();

  const handleAddToCart = () => {
    if (!username) {
      // Chưa đăng nhập, chuyển hướng về trang home
      navigate("/login-register");
      return;
    }

    const selectedCartItem = cartItems.find(
      (item) =>
        item.productId === product.productId &&
        item.selectedQuality === selectedQuality
    );

    if (selectedCartItem) {
      const updatedProduct = {
        ...selectedCartItem,
        quantity:
          selectedCartItem.quantity >= productStock
            ? productStock
            : selectedCartItem.quantity + 1,
      };
      dispatch(addToCart(updatedProduct));
    } else {
      const uniqueCartItemId = `${product.cartItemId}-${selectedQuality}`;
      // Đã đăng nhập, thực hiện xử lý thêm sản phẩm vào giỏ hàng như trước
      const newCartItem = {
        ...product,
        selectedQuality: selectedQuality, // Include the selected quality
        cartItemId: uniqueCartItemId, // Use the unique cartItemId
        quantity:
          productCartQty >= productStock ? productStock : productCartQty + 1,
      };
      dispatch(addToCart(newCartItem));
    }
  };

  // Hàm để xử lý khi nhấn nút "Add to wishlist"
  const handleAddToWishlist = () => {
    if (username) {
      // Gọi hàm addToWishlist nếu đã đăng nhập
      dispatch(addToWishlist(product));
    } else {
      // Nếu chưa đăng nhập, chuyển hướng đến trang đăng nhập
      navigate("/login-register"); // Thay đổi "/login" bằng đường dẫn của trang đăng nhập của bạn
    }
  };

  // Hàm để xử lý khi nhấn nút "Add to compare"
  const handleAddToCompare = () => {
    if (username) {
      // Gọi hàm addToCompare nếu đã đăng nhập
      dispatch(addToCompare(product));
    } else {
      // Nếu chưa đăng nhập, chuyển hướng đến trang đăng nhập
      navigate("/login-register");
    }
  };

  useEffect(() => {
    const fetchQualityData = async () => {
      try {
        const response = await axios.get(
          `https://localhost:7050/api/Product/${product.productId}`
        );
        setQualityPercentages(response.data);
      } catch (error) {
        console.error(error);
      }
    };

    fetchQualityData();
  }, []);

  console.log("qualityPercentages:", qualityPercentages);
  console.log("selectedQuality:", selectedQuality);

  const handleQualityChange = (event, quality) => {
    setSelectedQuality(quality);
    // Perform any other actions related to quality selection
    const newFinalDiscountedPrice = finalDiscountedPrice * (quality / 100);
    // Update the finalDiscountedPrice state
    setFinalDiscountedPrices(newFinalDiscountedPrice);
  };

  return (
    <div className="product-details-content ml-70">
      <h2>{product.productName}</h2>
      <div className="product-details-price">
        {discountedPrice !== null ? (
          <Fragment>
            <span>
              <b>
                <AmountFormatted
                  amount={finalDiscountedPrices + " " + currency.currencySymbol}
                />
              </b>
            </span>{" "}
            <span className="old">
              <AmountFormatted
                amount={finalProductPrice + " " + currency.currencySymbol}
              />
            </span>
          </Fragment>
        ) : (
          <span>
            <AmountFormatted
              amount={finalProductPrice + " " + currency.currencySymbol}
            />
          </span>
        )}
      </div>
      {product.quality && product.quality > 0 ? (
        <div className="pro-details-rating-wrap">
          <div className="pro-details-rating">
            <Rating ratingValue={product.quality} />
          </div>
        </div>
      ) : (
        ""
      )}
      <div className="pro-details-list">
        <p>{product.description}</p>
      </div>

      <div className="pro-details-size">
  <span>Quality </span>
  <div className="pro-details-size-content">
    {qualityPercentages.length === 0 ? (
      <label className={`pro-details-size-content--single`}>
        <input
          type="radio"
          value={100}
          checked={selectedQuality === 100}
          onChange={(e) => handleQualityChange(e, 100)}
        />
        <span className="">100%</span>
      </label>
    ) : (
      Array.from(new Set(qualityPercentages.map((single) => single.qualityPercentage))).map((quality, key) => (
        <label className={`pro-details-size-content--single`} key={key}>
          <input
            type="radio"
            value={quality}
            checked={selectedQuality === quality}
            onChange={(e) =>
              handleQualityChange(e, quality)
            }
          />
          <span className="">{quality}%</span>
        </label>
      ))
    )}
  </div>
</div>


      {product.affiliateLink ? (
        <div className="pro-details-quality">
          <div className="pro-details-cart btn-hover ml-0">
            <a
              href={product.affiliateLink}
              rel="noopener noreferrer"
              target="_blank"
            >
              Buy Now
            </a>
          </div>
        </div>
      ) : (
        <div className="pro-details-quality">
          <div className="cart-plus-minus">
            <button
              onClick={() =>
                setQuantityCount(quantityCount > 1 ? quantityCount - 1 : 1)
              }
              className="dec qtybutton"
            >
              -
            </button>
            <input
              className="cart-plus-minus-box"
              type="text"
              value={quantityCount}
              readOnly
            />
            <button
              onClick={() =>
                setQuantityCount(
                  quantityCount < productStock - productCartQty
                    ? quantityCount + 1
                    : quantityCount
                )
              }
              className="inc qtybutton"
            >
              +
            </button>
          </div>
          <div className="pro-details-cart btn-hover">
            {productStock && productStock > 0 ? (
              <button
                onClick={handleAddToCart}
                disabled={productCartQty >= productStock}
              >
                {" "}
                Add To Cart{" "}
              </button>
            ) : (
              <button disabled>Out of Stock</button>
            )}
          </div>
          <div className="pro-details-wishlist">
            <button
              className={wishlistItem !== undefined ? "active" : ""}
              disabled={wishlistItem !== undefined}
              title={
                wishlistItem !== undefined
                  ? "Added to wishlist"
                  : "Add to wishlist"
              }
              onClick={handleAddToWishlist}
            >
              <i className="pe-7s-like" />
            </button>
          </div>
          <div className="pro-details-compare">
            <button
              className={compareItem !== undefined ? "active" : ""}
              disabled={compareItem !== undefined}
              title={
                compareItem !== undefined
                  ? "Added to compare"
                  : "Add to compare"
              }
              onClick={handleAddToCompare}
            >
              <i className="pe-7s-shuffle" />
            </button>
          </div>
        </div>
      )}
      {product.category.categoryName ? (
        <div className="pro-details-meta">
          <span>Categories :</span>
          <ul style={{ backgroundColor: "black", borderRadius: "10px" }}>
            <li>
              <Link
                style={{ color: "white" }}
                to={
                  process.env.PUBLIC_URL +
                  "/shop-grid-standard/" +
                  product.category.categoryId
                }
              >
                {product.category.categoryName}
              </Link>
            </li>
          </ul>
        </div>
      ) : (
        ""
      )}

      <div className="pro-details-meta">
        <span>Brand :</span>
        <ul style={{ backgroundColor: "black", borderRadius: "10px" }}>
          <li>
            <div style={{ color: "white" }}>{product.brand}</div>
          </li>
        </ul>
      </div>

      <div className="pro-details-social">
        <ul>
          <li>
            <a href="//facebook.com">
              <i className="fa fa-facebook" />
            </a>
          </li>
          <li>
            <a href="//dribbble.com">
              <i className="fa fa-dribbble" />
            </a>
          </li>
          <li>
            <a href="//pinterest.com">
              <i className="fa fa-pinterest-p" />
            </a>
          </li>
          <li>
            <a href="//twitter.com">
              <i className="fa fa-twitter" />
            </a>
          </li>
          <li>
            <a href="//linkedin.com">
              <i className="fa fa-linkedin" />
            </a>
          </li>
        </ul>
      </div>
    </div>
  );
};

ProductDescriptionInfo.propTypes = {
  cartItems: PropTypes.array,
  compareItem: PropTypes.shape({}),
  currency: PropTypes.shape({}),
  discountedPrice: PropTypes.number,
  finalDiscountedPrice: PropTypes.number,
  finalProductPrice: PropTypes.number,
  product: PropTypes.shape({}),
  wishlistItem: PropTypes.shape({}),
};

export default ProductDescriptionInfo;

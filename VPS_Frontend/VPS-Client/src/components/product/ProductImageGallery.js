import React, { Fragment, useState } from "react";
import PropTypes from "prop-types";
import { EffectFade, Thumbs } from "swiper";
import AnotherLightbox from "yet-another-react-lightbox";
import Thumbnails from "yet-another-react-lightbox/plugins/thumbnails";
import Zoom from "yet-another-react-lightbox/plugins/zoom";
import Fullscreen from "yet-another-react-lightbox/plugins/fullscreen";
import Swiper, { SwiperSlide } from "../../components/swiper";
import { useEffect } from "react";
import axios from "axios";

const ProductImageGallery = ({ product }) => {
  const [thumbsSwiper, setThumbsSwiper] = useState(null);
  const [index, setIndex] = useState(-1);
  const [additionalImages, setAdditionalImages] = useState([]); // New state to hold additional images

  const slides = additionalImages.map((img, i) => ({
      src: process.env.PUBLIC_URL + img,
      key: i,
  }));

  // swiper slider settings
  const gallerySwiperParams = {
    spaceBetween: 10,
    loop: true,
    effect: "fade",
    fadeEffect: {
      crossFade: true,
    },
    thumbs: { swiper: thumbsSwiper },
    modules: [EffectFade, Thumbs],
  };

  const thumbnailSwiperParams = {
    onSwiper: setThumbsSwiper,
    spaceBetween: 10,
    slidesPerView: 4,
    touchRatio: 0.2,
    freeMode: true,
    loop: true,
    slideToClickedSlide: true,
    navigation: true,
  };

  useEffect(() => {
    // Fetch additional images from the API using the productId
    const fetchAdditionalImages = async () => {
      try {
        const response = await axios.get(
          `https://localhost:7050/api/ProductDetail/GetProductDetailByProductId/${product.productId}`
        );
        const data = response.data;
        // Extract the coreImage URLs from the response
        const imageUrls = data.map((item) => item.coreImage);
        console.log(imageUrls);
        setAdditionalImages(data);
      } catch (error) {
        console.error("Error fetching additional images:", error);
      }
    };

    fetchAdditionalImages();
  }, [product.productId]);

  return (
    <Fragment>
      <div className="product-large-image-wrapper">
        {product.discount || product.new ? (
          <div className="product-img-badges">
            {product.discount ? (
              <span className="pink">-{product.discount}%</span>
            ) : (
              ""
            )}
            {product.new ? <span className="purple">New</span> : ""}
          </div>
        ) : (
          ""
        )}
        {additionalImages.length ? (
          <Swiper options={gallerySwiperParams}>
            {additionalImages.map((single, key) => (
              <SwiperSlide key={key}>
                <button
                  className="lightgallery-button"
                  onClick={() => setIndex(key)}
                >
                  <i className="pe-7s-expand1"></i>
                </button>
                <div className="single-image" style={{
                width: "540px", 
                height: "300px",
              }}>
                  <img
                    src={process.env.PUBLIC_URL + single.coreImage} // Use single.coreImage
                    className="img-fluid"
                    alt=""
                  />
                </div>
              </SwiperSlide>
            ))}
            <AnotherLightbox
              open={index >= 0}
              index={index}
              close={() => setIndex(-1)}
              slides={slides}
              plugins={[Thumbnails, Zoom, Fullscreen]}
            />
          </Swiper>
        ) : null}
      </div>
      <div className="product-small-image-wrapper mt-15">
        {additionalImages.length ? (
          <Swiper options={thumbnailSwiperParams}>
            {additionalImages.map((single, key) => (
              <SwiperSlide key={key}>
                <div className="single-image" style={{
                width: "120px", 
                height: "50px",
              }}>
                  <img
                    src={process.env.PUBLIC_URL + single.coreImage} // Use single.coreImage
                  />
                </div>
              </SwiperSlide>
            ))}
          </Swiper>
        ) : null}
      </div>
    </Fragment>
  );
};

ProductImageGallery.propTypes = {
  product: PropTypes.shape({}),
};

export default ProductImageGallery;

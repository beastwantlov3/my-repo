import PropTypes from "prop-types";
import axios from "axios";
import { setActiveSort } from "../../helpers/product";
import { useEffect, useState } from "react";
import { Link } from "react-router-dom";

const ShopCategories = ({
  products,
  getSortParams,
  filterByCategory
}) => {
  // No need to use the state variable category, use categories prop instead
  const [category, setCategory] = useState([]);

  useEffect(() => {
    fetchData();
  }, []);

  const fetchData = async () => {
    try {
      const response = await axios.get(
        "https://localhost:7050/api/Category/list"
      );
      setCategory(response.data);
    } catch (error) {
      console.error("Error fetching data:", error);
    }
  };

  return (
    <div className="sidebar-widget">
      <h4 className="pro-sidebar-title">Phân loại sản phẩm</h4>
      <div className="sidebar-widget-list mt-30">
        {category ? (
          <ul>
            <li>
            <div className="sidebar-widget-list-left">
                <Link
                  to={process.env.PUBLIC_URL + `/shop-grid-standard/0`}
                >
                  <button
                    onClick={(e) => {
                      filterByCategory(0); // 0 for "Tất cả" category
                      setActiveSort(e);
                    }}
                  >
                    <span className="checkmark" /> Tất cả
                  </button>
                </Link>
              </div>
            </li>
            {category.map((category) => {
              return (
                <li key={category.categoryId}>
                  <div className="sidebar-widget-list-left">
                    <Link
                      to={
                        process.env.PUBLIC_URL +
                        `/shop-grid-standard/${category.categoryId}`
                      }
                    >
                      <button
                        
                      >
                        <span className="checkmark" /> {category.categoryName}
                      </button>
                    </Link>
                  </div>
                </li>
              );
            })}
          </ul>
        ) : (
          "No categories found"
        )}
      </div>
    </div>
  );
};

ShopCategories.propTypes = {
  categories: PropTypes.array,
  getSortParams: PropTypes.func,
  filterByCategory: PropTypes.func,
};

export default ShopCategories;

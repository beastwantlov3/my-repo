// import Alternative from "views/pages/dashboards/Alternative.js";
import Buttons from ".././src/views/pages/components/Buttons.js";
import Cards from ".././src/views/pages/components/Cards.js";
import Components from ".././src/views/pages/forms/Components.js";
import Elements from ".././src/views/pages/forms/Elements.js";
import Grid from ".././src/views/pages/components/Grid.js";
import Icons from ".././src/views/pages/components/Icons.js";
import Notifications from ".././src/views/pages/components/Notifications.js";
import Typography from ".././src/views/pages/components/Typography.js";
import Validation from ".././src/views/pages/forms/Validation.js";

import Calendar from ".././src/views/pages/Calendar.js";
// import Charts from "views/pages/Charts.js";
import Dashboard from ".././src/views/pages/managers/Dashboard.js";
// import Google from "views/pages/maps/Google.js";
// import RTLSupport from "views/pages/examples/RTLSupport.js";
import Products from ".././src/views/pages/managers/products/Products.js";
import Orders from ".././src/views/pages/managers/orders/Orders.js";
import OrderDetails from ".././src/views/pages/managers/orders/OrderDetails";
// import Timeline from "views/pages/examples/Timeline.js";
import Vector from ".././src/views/pages/maps/Vector.js";
// import Widgets from "views/pages/Widgets.js";
import EditProduct from ".././src/views/pages/managers/products/EditProduct";
import AddProduct from ".././src/views/pages/managers/products/AddProduct";
import Chat from ".././src/views/pages/consultants/Chat";
import Blogs from ".././src/views/pages/blogs/Blogs";
import BlogDetail from ".././src/views/pages/blogs/BlogDetail";
import AddBlog from ".././src/views/pages/blogs/AddBlog";
import QrScanner from ".././src/views/pages/qrscanner/QrScanner";
// import Sales from "views/pages/sales/Sales";
import SalersManager from ".././src/views/pages/sales/SalersManager";
import SaleDetails from ".././src/views/pages/sales/SaleDetails";
import AddOrder from ".././src/views/pages/managers/orders/AddOrders";
import EditOrders from ".././src/views/pages/managers/orders/EditOrders";
import AddOrderDetail from ".././src/views/pages/managers/orders/AddOrderDetail";
import Shippers from ".././src/views/pages/admin/Shippers";
import Customer from ".././src/views/pages/admin/Customers";
import Saler from ".././src/views/pages/admin/Salers";
import AddShipper from ".././src/views/pages/admin/AddShipper";
import AddSaler from ".././src/views/pages/admin/AddSaler";
import ShippersManager from ".././src/views/pages/shippers/ShippersManager";
import ShipperDetails from ".././src/views/pages/shippers/ShipperDetails";
import DashboardAdmin from ".././src/views/pages/admin/DashboardAdmin";
import ProfileEmployee from ".././src/views/pages/managers/ProfileEmployee";
import ProductsSupplier from ".././src/views/pages/supplier/ProductsSupplier";
import Suppliers from ".././src/views/pages/admin/Supplier";
import AddSupplier from ".././src/views/pages/admin/AddSupplier";
import EditProductSupplier from ".././src/views/pages/supplier/EditProductSupplier";
import AddProductSupplier from ".././src/views/pages/supplier/AddProductSupplier";
import Bloggers from ".././src/views/pages/admin/Bloggers";
import AddBlogger from ".././src/views/pages/admin/AddBlogger";
import AddAccountBlogger from ".././src/views/pages/admin/AddAccountBlogger";
import ManagerSupplier from ".././src/views/pages/managers/ManagerSupplier";
import ManagerSaler from ".././src/views/pages/managers/ManagerSaler";
import AddAccountShipper from ".././src/views/pages/admin/AddAccountShipper";
import AddAccountSupplier from ".././src/views/pages/admin/AddAccountSupplier";
import AddAccountSaler from ".././src/views/pages/admin/AddAccountSaler";
import Profile from "./views/pages/account/profile.js";
import Accounts from "./views/pages/managers/Accounts.js";
import AccountConfirm from "./views/pages/managers/AccountConfirm.js";

const routes = [
  {
    collapse: true,
    name: "Admin",
    icon: "ni ni-diamond text-red",
    miniName: "S",
    state: "AdminCollapse",
    roleName: "Admin", 
    views: [
      {
        path: "/dashboard-admin",
        name: "Dashboard",
        miniName: "DA",
        component: DashboardAdmin,
        layout: "/admin",
      },
      {
        path: "/salers",
        name: "Salers",
        miniName: "Sa",
        component: Saler,
        layout: "/admin",
      },
      {
        path: "/shippers",
        name: "Shippers",
        miniName: "Sh",
        layout: "/admin",
        component: Shippers,
      },
      {
        path: "/suppliers",
        name: "Suppliers",
        miniName: "Su",
        layout: "/admin",
        component: Suppliers,
      },
      {
        path: "/bloggers",
        name: "Bloggers",
        miniName: "B",
        layout: "/admin",
        component: Bloggers,
      },
      {
        path: "/customers",
        name: "Customers",
        miniName: "C",
        component: Customer,
        layout: "/admin",
      },
      {
        path: "/add-shipper",
        component: AddShipper,
        layout: "/admin",
        hidden: true, // Exclude this route from navigation
        hideName: true,
      },
      {
        path: "/add-supplier",
        component: AddSupplier,
        layout: "/admin",
        hidden: true, // Exclude this route from navigation
        hideName: true,
      },
      {
        path: "/add-saler",
        component: AddSaler,
        layout: "/admin",
        hidden: true, // Exclude this route from navigation
        hideName: true,
      },
      {
        path: "/add-blogger",
        component: AddBlogger,
        layout: "/admin",
        hidden: true, // Exclude this route from navigation
        hideName: true,
      },
      {
        path: "/add-account-shipper",
        component: AddAccountShipper,
        layout: "/admin",
        hidden: true, // Exclude this route from navigation
        hideName: true,
      },
      {
        path: "/add-account-supplier",
        component: AddAccountSupplier,
        layout: "/admin",
        hidden: true, // Exclude this route from navigation
        hideName: true,
      },
      {
        path: "/add-account-saler",
        component: AddAccountSaler,
        layout: "/admin",
        hidden: true, // Exclude this route from navigation
        hideName: true,
      },
      {
        path: "/add-account-blogger",
        component: AddAccountBlogger,
        layout: "/admin",
        hidden: true, // Exclude this route from navigation
        hideName: true,
      },
    ],
  },
  {
    collapse: true,
    name: "Manager",
    icon: "ni ni-briefcase-24 text-blue",
    state: "tablesCollapse",
    roleName: "Manager", 
    views: [
      {
        path: "/manager",
        name: "Dashboard",
        miniName: "D",
        component: Dashboard,
        layout: "/admin",
      },
      {
        path: "/accounts-manager",
        name: "Account",
        miniName: "A",
        component: Accounts,
        layout: "/admin",
      },
      {
        path: "/account-confirm/:customerId",
        component: AccountConfirm,
        layout: "/admin",
        hidden: true, // Exclude this route from navigation
        hideName: true,
      },
      {
        path: "/orders",
        name: "Orders",
        miniName: "O",
        layout: "/admin",
        component: Orders,
      },
      {
        path: "/view-order/:orderId",
        // name: "Orders Details",
        component: OrderDetails,
        layout: "/admin",
        hidden: true, // Exclude this route from navigation
        hideName: true,
      },
      {
        path: "/add-order",
        // name: "Add Orders",
        component: AddOrder,
        layout: "/admin",
        hidden: true, // Exclude this route from navigation
        hideName: true,
      },
      {
        path: "/add-orderDetails",
        // name: "Add Orders Details",
        component: AddOrderDetail,
        layout: "/admin",
        hidden: true, // Exclude this route from navigation
        hideName: true,
      },
      {
        path: "/edit-order/:orderId",
        // name: "Edit Details",
        component: EditOrders,
        layout: "/admin",
        hidden: true, // Exclude this route from navigation
        hideName: true,
      },
      {
        // collapse: true,
        path: "/products",
        name: "Products",
        miniName: "P",
        layout: "/admin",
        component: Products,
      },
      {
        path: "/list-products",
        name: "All Products",
        component: Products,
        layout: "/admin",
        hidden: true, // Exclude this route from navigation
        hideName: true,
      },
      {
        path: "/edit-product/:productId",
        component: EditProduct,
        layout: "/admin",
        hidden: true, // Exclude this route from navigation
        hideName: true,
      },
      {
        path: "/add-product",
        // name: "Add Product",
        component: AddProduct,
        layout: "/admin",
        hidden: true, // Exclude this route from navigation
        hideName: true,
      },
      {
        path: "/profile-employee/:accountId",
        component: ProfileEmployee,
        layout: "/admin",
        hidden: true, // Exclude this route from navigation
        hideName: true,
      },
      {
        path: "/manager-supplier/:supplierId",
        component: ManagerSupplier,
        layout: "/admin",
        hidden: true, // Exclude this route from navigation
        hideName: true,
      },
      {
        path: "/manager-saler/:saleId",
        component: ManagerSaler,
        layout: "/admin",
        hidden: true, // Exclude this route from navigation
        hideName: true,
      },
    ],
  },
  {
    collapse: true,
    path: "/saler/",
    name: "Saler",
    icon: "ni ni-folder-17 text-yellow",
    miniName: "S",
    layout: "/admin",
    roleName: "Sale",
    state: "salerCollapse",
    views: [
      {
        path: "/list-sales/",
        name: "Salers Dashboard",
        miniName: "S",
        component: SalersManager,
        layout: "/admin",
      },
      {
        path: "/saler-details/:saleId",
        name: "Saler Details",
        component: SaleDetails,
        layout: "/admin",
        hidden: true, // Exclude this route from navigation
        hideName: true,
      },
    ],
  },
  {
    collapse: true,
    path: "/shipper/",
    name: "Shipper",
    icon: "ni ni-delivery-fast text-green",
    miniName: "S",
    layout: "/admin",
    roleName: "Shipper",
    state: "shippersCollapse",
    views: [
      {
        path: "/list-shippers",
        name: "Shipper Dashboard",
        miniName: "Sh",
        component: ShippersManager,
        layout: "/admin",
      },
      {
        path: "/order-details/:shipperId",
        component: ShipperDetails,
        layout: "/admin",
        hidden: true, // Exclude this route from navigation
        hideName: true,
      },
    ],
  },
  {
    collapse: true,
    name: "Account",
    icon: "ni ni-ungroup text-orange",
    roleName: "R",
    state: "examplesCollapse",
    views: [
      // {
      //   path: "/pricing",
      //   name: "Pricing",
      //   miniName: "P",
      //   component: Pricing,
      //   layout: "/auth",
      // },
      {
        path: "/profile",
        name: "Profile",
        miniName: "L",
        component: Profile,
        layout: "/admin",
      },
      // {
      //   path: "/timeline",
      //   name: "Timeline",
      //   miniName: "T",
      //   component: Timeline,
      //   layout: "/admin",
      // },

      // {
      //   path: "/rtl-support",
      //   name: "RTL Support",
      //   miniName: "RS",
      //   component: RTLSupport,
      //   layout: "/rtl",
      // },
    ],
  },
//   {
//     collapse: true,
//     name: "Components",
//     icon: "ni ni-ui-04 text-info",
//     state: "componentsCollapse",
//     views: [
//       {
//         path: "/buttons",
//         name: "Buttons",
//         miniName: "B",
//         component: Buttons,
//         layout: "/admin",
//       },
//       {
//         path: "/cards",
//         name: "Cards",
//         miniName: "C",
//         component: Cards,
//         layout: "/admin",
//       },
//       {
//         path: "/grid",
//         name: "Grid",
//         miniName: "G",
//         component: Grid,
//         layout: "/admin",
//       },
//       {
//         path: "/notifications",
//         name: "Notifications",
//         miniName: "N",
//         component: Notifications,
//         layout: "/admin",
//       },
//       {
//         path: "/icons",
//         name: "Icons",
//         miniName: "I",
//         component: Icons,
//         layout: "/admin",
//       },
//       {
//         path: "/typography",
//         name: "Typography",
//         miniName: "T",
//         component: Typography,
//         layout: "/admin",
//       },
//       {
//         collapse: true,
//         name: "Multi Level",
//         miniName: "M",
//         state: "multiCollapse",
//         views: [
//           {
//             path: "#pablo",
//             name: "Third level menu",
//             component: () => {},
//             layout: "/",
//           },
//           {
//             path: "#pablo",
//             name: "Just another link",
//             component: () => {},
//             layout: "/",
//           },
//           {
//             path: "#pablo",
//             name: "One last link",
//             component: () => {},
//             layout: "/",
//           },
//         ],
//       },
//     ],
//   },
  // {
  //   collapse: true,
  //   name: "Forms",
  //   icon: "ni ni-single-copy-04 text-pink",
  //   state: "formsCollapse",
  //   views: [
  //     {
  //       path: "/elements",
  //       name: "Elements",
  //       miniName: "E",
  //       component: Elements,
  //       layout: "/admin",
  //     },
  //     {
  //       path: "/components",
  //       name: "Components",
  //       miniName: "C",
  //       component: Components,
  //       layout: "/admin",
  //     },
  //     {
  //       path: "/validation",
  //       name: "Validation",
  //       miniName: "V",
  //       component: Validation,
  //       layout: "/admin",
  //     },
  //   ],
  // },
  {
    collapse: true,
    name: "Supplier",
    icon: "ni ni-building text-pink",
    state: "formsCollapse",
    roleName: "Supplier",
    views: [
      {
        path: "/list-suppliers/",
        name: "Products of Supplier",
        miniName: "PS",
        component: ProductsSupplier,
        layout: "/admin",
      },
      {
        path: "/edit-product-supplier/:productId",
        component: EditProductSupplier,
        layout: "/admin",
        hidden: true, // Exclude this route from navigation
        hideName: true,
      },
      {
        path: "/add-product-supplier",
        // name: "Add Product",
        component: AddProductSupplier,
        layout: "/admin",
        hidden: true, // Exclude this route from navigation
        hideName: true,
      },
    ],
  },
  // {
  //   collapse: true,
  //   name: "Maps",
  //   icon: "ni ni-map-big text-primary",
  //   state: "mapsCollapse",
  //   views: [
  //     // {
  //     //   path: "/google",
  //     //   name: "Google",
  //     //   miniName: "G",
  //     //   component: Google,
  //     //   layout: "/admin",
  //     // },
  //     {
  //       path: "/vector",
  //       name: "Vector",
  //       miniName: "V",
  //       component: Vector,
  //       layout: "/admin",
  //     },
  //   ],
  // },
  // {
  //   path: "/widgets",
  //   name: "Widgets",
  //   icon: "ni ni-archive-2 text-green",
  //   component: Widgets,
  //   layout: "/admin",
  // },
  // {
  //   path: "/list-sale",
  //   name: "Sales",
  //   icon: "ni ni-camera-compact text-info",
  //   component: Sales,
  //   layout: "/admin",
  // },
  // {
  //   path: "/qr-scanner",
  //   name: "QR Scanner",
  //   icon: "ni ni-camera-compact text-info",
  //   component: QrScanner,
  //   layout: "/admin",
  // },
  // {
  //   path: "/calendar",
  //   name: "Calendar",
  //   icon: "ni ni-calendar-grid-58 text-red",
  //   component: Calendar,
  //   layout: "/admin",
  // },
  // {
  //   path: "/chat",
  //   name: "Chat",
  //   icon: "ni ni-chat-round text-yellow",
  //   component: Chat,
  //   layout: "/admin",
  // },
  {
    collapse: true,
    path: "/blog",
    name: "Blog",
    icon: "ni ni-active-40 text-blue",
    layout: "/admin",
    roleName: "Blogger",
    views: [
      {
        path: "/list-blogs",
        name: "Blogs List",
        miniName: "Blogs",
        component: Blogs,
        layout: "/admin",
      },
      {
        path: "/blog-details/:blogId",
        miniName: "",
        component: BlogDetail,
        layout: "/admin",
        hidden: true, // Exclude this route from navigation
        hideName: true,
      },
      {
        path: "/add-blog",
        name: "Add Blog",
        component: AddBlog,
        miniName: "",
        layout: "/admin",
      },
    ],
  },
];

export default routes;

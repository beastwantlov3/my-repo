import React, { useEffect } from "react";
import { createRoot } from 'react-dom/client';
import { BrowserRouter, Route, Switch, Redirect } from "react-router-dom";
import { Provider } from "react-redux";
import App from "./App";
import { store } from "./store/store";
import PersistProvider from "./store/providers/persist-provider";
import { setProducts } from "./store/slices/product-slice";
import "animate.css";
import "swiper/swiper-bundle.min.css";
import "yet-another-react-lightbox/styles.css";
import "yet-another-react-lightbox/plugins/thumbnails.css";
import "./assets/scss/style.scss";
import "./i18n";
import { API_URL } from "./config/app.define";
import "react-notification-alert/dist/animate.css";
import "react-perfect-scrollbar/dist/css/styles.css";
import "@fullcalendar/common/main.min.css";
import "@fullcalendar/daygrid/main.min.css";
import "sweetalert2/dist/sweetalert2.min.css";
import "select2/dist/css/select2.min.css";
import "quill/dist/quill.core.css";
import "@fortawesome/fontawesome-free/css/all.min.css";
import "../../VPS-Client/src/assets/vendor/nucleo/css/nucleo.css";
import "../../VPS-Client/src/assets/scss/argon-dashboard-pro-react.scss";
import AdminLayout from "../../VPS-Client/src/layouts/Admin.js";
import AuthLayout from "../../VPS-Client/src/layouts/Auth.js";

const fetchProducts = async () => {
  try {
    const response = await fetch(`${API_URL}/Product`);
    const data = await response.json();
    store.dispatch(setProducts(data));
  } catch (error) {
    console.log("Error fetching products:", error);
  }
};

fetchProducts();

const container = document.getElementById("root");
const root = createRoot(container);

root.render(
  <Provider store={store}>
    <PersistProvider>
      <BrowserRouter>
        <Switch>
          <Route
            path="/admin"
            render={(props) => <AdminLayout {...props} />}
          />
          <Route path="/auth" render={(props) => <AuthLayout {...props} />} />
          <Redirect from="*" to="/" />
        </Switch>
      </BrowserRouter>
      <App />
    </PersistProvider>
  </Provider>
);


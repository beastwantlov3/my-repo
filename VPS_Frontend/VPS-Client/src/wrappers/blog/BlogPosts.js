import React, { Fragment } from "react";
import { Link } from "react-router-dom";

const BlogPosts = ({ data }) => {
  const formatDate = (dateString) => {
    const dateOptions = {
      day: "numeric",
      month: "long",
      year: "numeric",
    };

    const date = new Date(dateString);
    const formattedDate = date.toLocaleDateString("vi-VN", dateOptions);

    return `${formattedDate}`;
  };

  return (
    <Fragment>
      {data.length > 0 ? (
        data.map((blog) => (
          <div key={blog.id} className="col-lg-6 col-md-6 col-sm-12">
            <div className="blog-wrap-2 mb-30">
              <div className="blog-img-2" style={{
                width: "387px", 
                height: "250px",
              }}>
                <Link
                  to={
                    process.env.PUBLIC_URL + "/blog-details-standard/" + blog.id
                  }
                >
                  <img src={blog.image} alt="Image..." />
                </Link>
              </div>
              <div className="blog-content-2" style={{
                width: "387px", 
                height: "290px",
              }}>
                <div className="blog-meta-2">
                  <ul>
                    <li>{formatDate(blog.publishDate)}</li>
                  </ul>
                </div>
                <h4>
                  <Link
                    to={
                      process.env.PUBLIC_URL +
                      "/blog-details-standard/" +
                      blog.id
                    }
                  >
                    {blog.title}
                  </Link>
                </h4>
                <p>
                  <b>Người viết bài: </b>
                  {blog.blogger.fullName}
                </p>
                <div className="blog-share-comment">
                  <div className="blog-btn-2">
                    <Link
                      to={
                        process.env.PUBLIC_URL +
                        "/blog-details-standard/" +
                        blog.id
                      }
                    >
                      Đọc thêm
                    </Link>
                  </div>
                  <div className="blog-share">
                    <span>share :</span>
                    <div className="share-social">
                      <ul>
                        <li>
                          <a className="facebook" href="//facebook.com">
                            <i className="fa fa-facebook" />
                          </a>
                        </li>
                        <li>
                          <a className="twitter" href="//twitter.com">
                            <i className="fa fa-twitter" />
                          </a>
                        </li>
                        <li>
                          <a className="instagram" href="//instagram.com">
                            <i className="fa fa-instagram" />
                          </a>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        ))
      ) : (
        <p>Không tìm thấy blogs nào!</p>
      )}
    </Fragment>
  );
};

export default BlogPosts;

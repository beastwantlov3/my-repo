import React from "react";

const BlogPagination = ({ currentPage, totalPages, handlePageChange }) => {
  // Create an array of page numbers to display in the pagination
  const pageNumbers = [];
  for (let i = 1; i <= totalPages; i++) {
    pageNumbers.push(i);
  }

  return (
    <div className="pro-pagination-style text-center mt-20">
      <ul>
        {/* Previous button */}
        {currentPage > 1 && (
          <li>
            <button
              className="prev"
              onClick={() => handlePageChange(currentPage - 1)}
            >
              <i className="fa fa-angle-double-left" />
            </button>
          </li>
        )}

        {/* Page numbers */}
        {pageNumbers.map((number) => (
          <li key={number}>
            <button
              className={currentPage === number ? "active" : ""}
              onClick={() => handlePageChange(number)}
            >
              {number}
            </button>
          </li>
        ))}

        {/* Next button */}
        {currentPage < totalPages && (
          <li>
            <button
              className="next"
              onClick={() => handlePageChange(currentPage + 1)}
            >
              <i className="fa fa-angle-double-right" />
            </button>
          </li>
        )}
      </ul>
    </div>
  );
};

export default BlogPagination;

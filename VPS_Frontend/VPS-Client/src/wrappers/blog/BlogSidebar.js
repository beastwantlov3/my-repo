import axios from "axios";
import { useEffect } from "react";
import { useState } from "react";
import { Link } from "react-router-dom";

const BlogSidebar = ({ formSubmit, value, handleSearchKey, clearSearch }) => {
  const [data, setData] = useState([]);
  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await axios.get(
          "https://localhost:7050/api/Blog/GetTop3"
        );
        setData(response.data);
      } catch (error) {
        console.error("Error fetching data:", error);
      }
    };
    fetchData();
  }, []);

  const formatDate = (dateString) => {
    const dateOptions = {
      day: "numeric",
      month: "long",
      year: "numeric",
    };

    // Convert the date to the desired format
    const date = new Date(dateString);
    const formattedDate = date.toLocaleDateString("vi-VN", dateOptions);

    // Return the formatted date string
    return `${formattedDate}`;
  };

  return (
    <div className="sidebar-style">
      <div className="sidebar-widget">
        <h4 className="pro-sidebar-title">Tìm kiếm </h4>
        <div className="pro-sidebar mb-55 mt-25">
          <form onSubmit={formSubmit} className="pro-sidebar-search-form">
            <input
              type="text"
              placeholder="Tìm kiếm blog..."
              value={value}
              onChange={handleSearchKey}
            />
            {value && <span onClick={clearSearch}></span>}
          </form>
        </div>
      </div>
      <div className="sidebar-widget">
        <h4 className="pro-sidebar-title">Blogs gần đây </h4>
        <div className="sidebar-project-wrap mt-30">
          {data.map((blog) => (
            <div className="single-sidebar-blog" key={blog.id}>
              <div className="sidebar-blog-img">
                <Link
                  to={
                    process.env.PUBLIC_URL + "/blog-details-standard/" + blog.id
                  }
                >
                  <img src={blog.image} alt="" />
                </Link>
              </div>
              <div className="sidebar-blog-content">
                <h4 className="mt-2 mb-1" 
              style={{
                width: "350px", 
              }}>
                  <Link
                    to={
                      process.env.PUBLIC_URL +
                      "/blog-details-standard/" +
                      blog.id
                    }
                  >
                    {blog.title}
                  </Link>
                </h4>
                <span>{formatDate(blog.publishDate)}</span>
              </div>
            </div>
          ))}
        </div>
      </div>
      {/* <div className="sidebar-widget mt-35">
        <h4 className="pro-sidebar-title">Categories</h4>
        <div className="sidebar-widget-list sidebar-widget-list--blog mt-20">
          <ul>
            <li>
              <div className="sidebar-widget-list-left">
                <input type="checkbox" defaultValue />{" "}
                <Link to={process.env.PUBLIC_URL + "/blog-standard"}>
                  Women <span>4</span>{" "}
                </Link>
                <span className="checkmark" />
              </div>
            </li>
            <li>
              <div className="sidebar-widget-list-left">
                <input type="checkbox" defaultValue />{" "}
                <Link to={process.env.PUBLIC_URL + "/blog-standard"}>
                  Men <span>4</span>{" "}
                </Link>
                <span className="checkmark" />
              </div>
            </li>
            <li>
              <div className="sidebar-widget-list-left">
                <input type="checkbox" defaultValue />{" "}
                <Link to={process.env.PUBLIC_URL + "/blog-standard"}>
                  Bags <span>4</span>{" "}
                </Link>
                <span className="checkmark" />
              </div>
            </li>
            <li>
              <div className="sidebar-widget-list-left">
                <input type="checkbox" defaultValue />{" "}
                <Link to={process.env.PUBLIC_URL + "/blog-standard"}>
                  Accessories <span>4</span>{" "}
                </Link>
                <span className="checkmark" />
              </div>
            </li>
          </ul>
        </div>
      </div>
      <div className="sidebar-widget mt-50">
        <h4 className="pro-sidebar-title">Tag </h4>
        <div className="sidebar-widget-tag mt-25">
          <ul>
            <li>
              <Link to={process.env.PUBLIC_URL + "/blog-standard"}>
                Clothing
              </Link>
            </li>
            <li>
              <Link to={process.env.PUBLIC_URL + "/blog-standard"}>
                Accessories
              </Link>
            </li>
            <li>
              <Link to={process.env.PUBLIC_URL + "/blog-standard"}>
                For Men
              </Link>
            </li>
            <li>
              <Link to={process.env.PUBLIC_URL + "/blog-standard"}>Women</Link>
            </li>
            <li>
              <Link to={process.env.PUBLIC_URL + "/blog-standard"}>
                Fashion
              </Link>
            </li>
          </ul>
        </div>
      </div> */}
    </div>
  );
};

export default BlogSidebar;

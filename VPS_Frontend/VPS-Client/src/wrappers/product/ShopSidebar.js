import PropTypes from "prop-types";
import clsx from "clsx";
import ShopTag from "../../components/product/ShopTag";
import {
  getIndividualCategories,
  getIndividualTags,

} from "../../helpers/product";
import ShopCategories from "../../components/product/ShopCategories";

// import ShopTag from "../../components/product/ShopTag";

const ShopSidebar = ({ products, formSubmit, value, handleSearchKey, clearSearch, getSortParams, sideSpaceClass }) => {
  const uniqueCategories = getIndividualCategories(products);
  const uniqueTags = getIndividualTags(products);

  return (
    <div className={clsx("sidebar-style", sideSpaceClass)}>
      {/* shop search */}
      <div className="sidebar-widget">
        <h4 className="pro-sidebar-title">Tìm kiếm </h4>
        <div className="pro-sidebar mb-55 mt-25">
          <form onSubmit={formSubmit} className="pro-sidebar-search-form">
            <input
              type="text"
              placeholder="Tìm kiếm sản phẩm..."
              value={value}
              onChange={handleSearchKey}
            />
            {value && (
              <span onClick={clearSearch}>
              </span>
            )}
          </form>
        </div>
      </div>

      {/* filter by categories */}
      <ShopCategories categories={uniqueCategories} getSortParams={getSortParams} />


      {/* filter by tag */}
      {/* <ShopTag brand={uniqueTags} getSortParams={getSortParams} /> */}
    </div>
  );
};

ShopSidebar.propTypes = {
  getSortParams: PropTypes.func,
  products: PropTypes.array,
  sideSpaceClass: PropTypes.string,
};

export default ShopSidebar;

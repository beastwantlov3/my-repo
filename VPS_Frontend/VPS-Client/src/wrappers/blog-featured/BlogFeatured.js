import PropTypes from "prop-types";
import clsx from "clsx";
import blogFeaturedData from "../../data/blog-featured/blog-featured.json";
import BlogFeaturedSingle from "../../components/blog-featured/BlogFeaturedSingle";
import SectionTitle from "../../components/section-title/SectionTitle";
import { useState } from "react";
import { useEffect } from "react";
import axios from "axios";

const BlogFeatured = ({ spaceTopClass, spaceBottomClass }) => {
  const [data, setData] = useState([]);

  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await axios.get(
          "https://localhost:7050/api/Blog/GetTop3"
        );
        setData(response.data);
      } catch (error) {
        console.error("Error fetching data:", error);
      }
    };
    fetchData();
  }, []);

  return (
    <div className={clsx("blog-area", spaceTopClass, spaceBottomClass)}>
      <div className="container">
        <SectionTitle
          titleText="CÁC BÀI VIẾT"
          positionClass="text-center"
          spaceClass="mb-55"
        />
        <div className="row">
          {/* Use conditional rendering */}
          {data.length > 0 ? (
            data.map(singlePost => (
              <div className="col-lg-4 col-sm-6" key={singlePost.id}>
                <BlogFeaturedSingle singlePost={singlePost} />
              </div>
            ))
          ) : (
            <div>Loading...</div>
            // You can also return null here if you want to render nothing when data is empty
            // null
          )}
        </div>
      </div>
    </div>
  );
};

BlogFeatured.propTypes = {
  spaceBottomClass: PropTypes.string,
  spaceTopClass: PropTypes.string
};

export default BlogFeatured;

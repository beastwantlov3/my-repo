import cogoToast from "cogo-toast";
const { createSlice } = require("@reduxjs/toolkit");

const wishlistSlice = createSlice({
  name: "wishlist",
  initialState: {
    wishlistItems: [],
  },
  reducers: {
    addToWishlist(state, action) {
      const isInWishlist = state.wishlistItems.findIndex(
        (item) => item.productId === action.payload.productId
      );
      sessionStorage.setItem(
        "wishlistItems",
        JSON.stringify(state.wishlistItems)
      );
      if (isInWishlist > -1) {
        cogoToast.info("Product already in wishlist", {
          position: "bottom-left",
        });
      } else {
        state.wishlistItems.push(action.payload);
        cogoToast.success("Added To wishlist", { position: "bottom-left" });
      }
    },
    deleteFromWishlist(state, action) {
      state.wishlistItems = state.wishlistItems.filter(
        (item) => item.productId !== action.payload
      );
      // Sau khi xóa sản phẩm, lưu lại thông tin danh sách yêu thích vào sessionStorage
      sessionStorage.setItem(
        "wishlistItems",
        JSON.stringify(state.wishlistItems)
      );
      cogoToast.error("Removed From Wishlist", { position: "bottom-left" });
    },
    deleteAllFromWishlist(state) {
      state.wishlistItems = [];
      // Sau khi xóa toàn bộ sản phẩm trong danh sách yêu thích, lưu lại thông tin danh sách yêu thích vào sessionStorage
      sessionStorage.setItem(
        "wishlistItems",
        JSON.stringify(state.wishlistItems)
      );
    },

    clearWishlist: (state) => {
      // Clear wishlist items and username from the state
      state.wishlistItems = [];
      state.userName = "";
      // Clear wishlist items from sessionStorage
      sessionStorage.removeItem("wishlistItems");
      cogoToast.info("Cleared Wishlist", { position: "bottom-left" });
    },
  },
});

export const {
  addToWishlist,
  deleteFromWishlist,
  deleteAllFromWishlist,
  clearWishlist
} = wishlistSlice.actions;
export default wishlistSlice.reducer;

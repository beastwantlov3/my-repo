import cogoToast from "cogo-toast";
const { createSlice } = require("@reduxjs/toolkit");

const compareSlice = createSlice({
  name: "compare",
  initialState: {
    compareItems: [], // Lấy dữ liệu sản phẩm so sánh từ sessionStorage khi khởi tạo state
},
  reducers: {
    addToCompare(state, action) {
      state.compareItems.push(action.payload);
      sessionStorage.setItem(
        "compareItems",
        JSON.stringify(state.compareItems)
      ); // Lưu lại thông tin sản phẩm so sánh vào sessionStorage
      cogoToast.success("Added To compare", { position: "bottom-left" });
      
    },
    deleteFromCompare(state, action) {
      state.compareItems = state.compareItems.filter(
        (item) => item.id !== action.payload
      );
      sessionStorage.setItem(
        "compareItems",
        JSON.stringify(state.compareItems)
      ); // Lưu lại thông tin sản phẩm so sánh vào sessionStorage
      cogoToast.error("Removed From Compare", { position: "bottom-left" });
    },

    clearCompare: (state) => {
        // Clear wishlist items and username from the state
        state.compareItems = [];
        state.userName = "";
        // Clear wishlist items from sessionStorage
        sessionStorage.removeItem("compareItems");
        cogoToast.info("Cleared Compare", { position: "bottom-left" });
      },
  },
});

export const { addToCompare, deleteFromCompare, clearCompare } = compareSlice.actions;
export default compareSlice.reducer;

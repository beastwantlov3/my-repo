import { v4 as uuidv4 } from "uuid";
import cogoToast from "cogo-toast";
const { createSlice } = require("@reduxjs/toolkit");

const cartSlice = createSlice({
  name: "cart",
  initialState: {
    cartItems: [],
    userName: "",
    selectedDurations: [], // Khởi tạo là một mảng rỗng
    totalPrice: 0,
  },
  reducers: {
    addToCart: (state, action) => {
      const product = action.payload;

      if (!product) {
        const cartItem = state.cartItems.find(
          (item) => item.productId === product.productId
        );

        if (!cartItem) {
          state.cartItems.push({
            ...product,
            quantity: product.quantity ? product.quantity : 1,
            cartItemId: uuidv4(),
            selectedQuality: product.selectedQuality, // Add this line
          });
        } else {
          state.cartItems = state.cartItems.map((item) => {
            if (item.cartItemId === cartItem.cartItemId) {
              return {
                ...item,
                quantity: product.quantity
                  ? item.quantity + product.quantity
                  : item.quantity + 1,
              };
            }
            return item;
          });
        }
      } else {
        const cartItem = state.cartItems.find(
          (item) =>
            item.productId === product.productId &&
            (product.cartItemId ? product.cartItemId === item.cartItemId : true)
        );

        if (!cartItem) {
          state.cartItems.push({
            ...product,
            quantity: product.quantity ? product.quantity : 1,
            cartItemId: uuidv4(),
          });
        } else {
          state.cartItems = state.cartItems.map((item) => {
            if (item.cartItemId === cartItem.cartItemId) {
              return {
                ...item,
                quantity: product.quantity
                  ? item.quantity + product.quantity
                  : item.quantity + 1,
              };
            }
            return item;
          });
        }
      }
      // Sau khi thêm sản phẩm, lưu lại thông tin giỏ hàng vào sessionStorage
      const username = sessionStorage.getItem("username");
      if (username) {
        sessionStorage.setItem(username, JSON.stringify(state.cartItems));
      }
      cogoToast.success("Added To Cart", { position: "bottom-left" });
    },

    deleteFromCart(state, action) {
      state.cartItems = state.cartItems.filter(
        (item) => item.cartItemId !== action.payload
      );
      // Sau khi xóa sản phẩm, lưu lại thông tin giỏ hàng vào sessionStorage
      const username = sessionStorage.getItem("username");
      if (username) {
        sessionStorage.setItem(username, JSON.stringify(state.cartItems));
      }
      cogoToast.error("Removed From Cart", { position: "bottom-left" });
    },
    decreaseQuantity(state, action) {
      const product = action.payload;
      if (product.quantity === 1) {
        state.cartItems = state.cartItems.filter(
          (item) => item.cartItemId !== product.cartItemId
        );
        const username = sessionStorage.getItem("username");
        if (username) {
          sessionStorage.setItem(username, JSON.stringify(state.cartItems));
        }
        cogoToast.error("Removed From Cart", { position: "bottom-left" });
      } else {
        state.cartItems = state.cartItems.map((item) =>
          item.cartItemId === product.cartItemId
            ? { ...item, quantity: item.quantity - 1 }
            : item
        );
        const username = sessionStorage.getItem("username");
        if (username) {
          sessionStorage.setItem(username, JSON.stringify(state.cartItems));
        }
        cogoToast.warn("Item Decremented From Cart", {
          position: "bottom-left",
        });
      }
      // Sau khi giảm số lượng sản phẩm, lưu lại thông tin giỏ hàng vào sessionStorage
      const username = sessionStorage.getItem("username");
      if (username) {
        const cartItems = JSON.parse(sessionStorage.getItem("cartItems")) || [];
        // Cập nhật thông tin giỏ hàng của người dùng đã đăng nhập
        sessionStorage.setItem("cartItems", JSON.stringify(cartItems));
      }
    },
    deleteAllFromCart(state) {
      state.cartItems = [];
      // Sau khi xóa toàn bộ sản phẩm trong giỏ hàng, lưu lại thông tin giỏ hàng vào sessionStorage
      const username = sessionStorage.getItem("username");
      if (username) {
        sessionStorage.setItem(username, JSON.stringify(state.cartItems));
      }
    },

    saveCartToStorage: (state, action) => {
      const { userName, cartItems } = action.payload;
      sessionStorage.setItem(userName, JSON.stringify(cartItems));
    },

    clearCartForUsername: (state, action) => {
      const username = action.payload;
      state.cartItems = state.cartItems.filter(
        (item) => item.userName !== username
      );
    },
    setCartItemsForUsername: (state, action) => {
      const { userName, cartItems } = action.payload;
      state.cartItems = cartItems;
      state.userName = userName;
    },

    clearCart: (state) => {
      const username = state.userName;
      if (username) {
        sessionStorage.removeItem(username); // Remove cartItems for this username
      }
      state.cartItems = [];
    },

    addUserNameToCart: (state, action) => {
      state.userName = action.payload;
    },
    updateSelectedDuration: (state, action) => {
      const { cartItemId, duration } = action.payload;
      state.selectedDurations[cartItemId] = duration;
    },

    setTotalPrice: (state, action) => {
      state.totalPrice = action.payload;
    },
  },
});

export const {
  addToCart,
  decreaseQuantity,
  deleteFromCart,
  deleteAllFromCart,
  saveCartToStorage,
  clearCart,
  addUserNameToCart,
  clearCartForUsername,
  setCartItemsForUsername,
  updateSelectedDurations,
  setTotalPrice
} = cartSlice.actions;
export default cartSlice.reducer;

import { Suspense, lazy } from "react";
import ScrollToTop from "./helpers/scroll-top";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import routes from "./routes";
import AdminLayout from "../../VPS-Client/src/layouts/Admin.js";
import ViewOrderCustomer from "./pages/other/ViewOrderCustomer";
import ListOrdersCustomer from "./pages/other/ListOrdersCustomer";

// home pages
const HomeFashion = lazy(() => import("./pages/home/HomeFashion"));

const ShopGridStandard = lazy(() => import("./pages/shop/ShopGridStandard"));

// product pages
const Product = lazy(() => import("./pages/shop-product/Product"));
const ProductTabLeft = lazy(() =>
  import("./pages/shop-product/ProductTabLeft")
);
const ProductTabRight = lazy(() =>
  import("./pages/shop-product/ProductTabRight")
);
const ProductSticky = lazy(() => import("./pages/shop-product/ProductSticky"));
const ProductSlider = lazy(() => import("./pages/shop-product/ProductSlider"));
const ProductFixedImage = lazy(() =>
  import("./pages/shop-product/ProductFixedImage")
);

// blog pages
const BlogStandard = lazy(() => import("./pages/blog/BlogStandard"));
const BlogDetailsStandard = lazy(() =>
  import("./pages/blog/BlogDetailsStandard")
);

// other pages
const About = lazy(() => import("./pages/other/About"));
const Contact = lazy(() => import("./pages/other/Contact"));
const MyAccount = lazy(() => import("./pages/other/MyAccount"));
const LoginRegister = lazy(() => import("./pages/other/LoginRegister"));
const ForgotPassword = lazy(() => import("./pages/other/ForgotPassword"));

const Cart = lazy(() => import("./pages/other/Cart"));
const Wishlist = lazy(() => import("./pages/other/Wishlist"));
const Compare = lazy(() => import("./pages/other/Compare"));
const Checkout = lazy(() => import("./pages/other/Checkout"));

const App = () => {

  return (
    <Router>
      <ScrollToTop>
        <Suspense
          fallback={
            <div className="flone-preloader-wrapper">
              <div className="flone-preloader">
                <span></span>
                <span></span>
              </div>
            </div>
          }
        >
          <Switch>
          <Route exact path="/" component={HomeFashion} /> {/* Use component prop */}


            {/* Homepages */}
            <Route
              path={process.env.PUBLIC_URL + "/home-fashion"}
              component={HomeFashion}
            />
            <Route
              path={process.env.PUBLIC_URL + "/shop-grid-standard/:categoryId"}
              component={ShopGridStandard}
            />
            <Route
              path={process.env.PUBLIC_URL + "/product/:id"}
              component={Product}
            />
            <Route
              path={process.env.PUBLIC_URL + "/product-tab-left/:id"}
              component={ProductTabLeft}
            />
            <Route
              path={process.env.PUBLIC_URL + "/product-tab-right/:id"}
              component={ProductTabRight}
            />
            <Route
              path={process.env.PUBLIC_URL + "/product-sticky/:id"}
              component={ProductSticky}
            />
            <Route
              path={process.env.PUBLIC_URL + "/product-slider/:id"}
              component={ProductSlider}
            />
            <Route
              path={process.env.PUBLIC_URL + "/product-fixed-image/:id"}
              component={ProductFixedImage}
            />

            {/* Blog pages */}
            <Route
              path={process.env.PUBLIC_URL + "/blog-standard"}
              component={BlogStandard}
            />
            <Route
              path={process.env.PUBLIC_URL + "/blog-details-standard/:blogId"}
              component={BlogDetailsStandard}
            />

            {/* Other pages */}
            <Route
              path={process.env.PUBLIC_URL + "/about"}
              component={About}
            />
            <Route
              path={process.env.PUBLIC_URL + "/contact"}
              component={Contact}
            />
            <Route
              path={process.env.PUBLIC_URL + "/my-account"}
              component={MyAccount}
            />
            <Route
              path={process.env.PUBLIC_URL + "/view-list-orders"}
              component={ListOrdersCustomer}
            />
            <Route
              path={process.env.PUBLIC_URL + "/view-order-detail/:orderId"}
              component={ViewOrderCustomer}
            />
            <Route
              path={process.env.PUBLIC_URL + "/login-register"}
              component={LoginRegister}
            />
            <Route
              path={process.env.PUBLIC_URL + "/forgot-password"}
              component={ForgotPassword}
            />
            <Route path={process.env.PUBLIC_URL + "/cart"} component={Cart} />
            <Route
              path={process.env.PUBLIC_URL + "/wishlist"}
              component={Wishlist}
            />
            <Route
              path={process.env.PUBLIC_URL + "/compare"}
              component={Compare}
            />
            <Route
              path={process.env.PUBLIC_URL + "/checkout"}
              component={Checkout}
            />
            {routes.map((route, index) => (
              <Route
                key={index}
                path={route.path}
                component={route.component}
                layout={route.layout}
              />
            ))}
            <Route
            path="/admin"
            render={(props) => <AdminLayout {...props} />}
          />
          </Switch>
        </Suspense>
      </ScrollToTop>
    </Router>
  );
};

export default App;

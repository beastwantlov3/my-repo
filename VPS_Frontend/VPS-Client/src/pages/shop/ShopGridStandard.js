import { Fragment, useState, useEffect, useCallback } from "react";
import { useLocation, useParams } from "react-router-dom";
import { getSortedProducts } from "../../helpers/product";
import SEO from "../../components/seo";
import LayoutOne from "../../layouts/LayoutOne";
import Breadcrumb from "../../wrappers/breadcrumb/Breadcrumb";
import ShopSidebar from "../../wrappers/product/ShopSidebar";
import ShopTopbar from "../../wrappers/product/ShopTopbar";
import ShopProducts from "../../wrappers/product/ShopProducts";
import axios from "axios";
import ProductPagination from "../../wrappers/product/ProductPagination";

const ShopGridStandard = () => {
  const [layout, setLayout] = useState("grid three-column");
  const [sortType, setSortType] = useState("");
  const [sortValue, setSortValue] = useState("");
  const [filterSortType, setFilterSortType] = useState("");
  const [filterSortValue, setFilterSortValue] = useState("");
  const [offset, setOffset] = useState(0);
  const [currentPage, setCurrentPage] = useState(1);
  const [currentData, setCurrentData] = useState([]);
  const [products, setProducts] = useState([]);
  const [originalProducts, setOriginalProducts] = useState([]); // Store the original fetched products
  const { categoryId } = useParams();
  const itemsPerPage = 6;
  const [searchKey, setSearchKey] = useState("");

  let { pathname } = useLocation();

  const getLayout = (layout) => {
    setLayout(layout);
  };

  const getSortParams = (sortType, sortValue) => {
    setSortType(sortType);
    setSortValue(sortValue);
  };

  const getFilterSortParams = (sortType, sortValue) => {
    setFilterSortType(sortType);
    setFilterSortValue(sortValue);
  };

  const fetchData = useCallback(async () => {
    try {
      const response = await axios.get(
        `https://localhost:7050/api/Product/GetProductByCateId/${categoryId}`
      );
      const data = response.data;
      setProducts(data);
      setOriginalProducts(data); // Store the original fetched products
    } catch (error) {
      console.error("Error fetching products:", error);
    }
  }, [categoryId]);

  useEffect(() => {
    fetchData();
  }, [fetchData]);

  useEffect(() => {
    let sortedProducts = getSortedProducts(products, sortType, sortValue);
    const filterSortedProducts = getSortedProducts(
      sortedProducts,
      filterSortType,
      filterSortValue
    );
    sortedProducts = filterSortedProducts;

    // Calculate the offset and limit for pagination
    const startIndex = (currentPage - 1) * itemsPerPage;
    const endIndex = startIndex + itemsPerPage;

    // Slice the products array based on the calculated offset and limit
    const paginatedProducts = sortedProducts.slice(startIndex, endIndex);

    // Update the currentData state with the paginated products
    setCurrentData(paginatedProducts);
  }, [
    currentPage,
    products,
    sortType,
    sortValue,
    filterSortType,
    filterSortValue,
  ]);

  const handlePageChange = (pageNumber) => {
    setCurrentPage(pageNumber);
    setOffset((pageNumber - 1) * itemsPerPage);
  };

  const handleSearch = (searchKeyword) => {
    // If the search keyword is empty, reset the products to the original list
    if (searchKeyword.trim() === "") {
      setProducts(originalProducts);
      setCurrentPage(1); // Reset pagination to the first page
    } else {
      // Filter the products based on the search keyword
      const filteredProducts = originalProducts.filter((product) =>
        product.title.toLowerCase().includes(searchKeyword.toLowerCase())
      );
      setProducts(filteredProducts);
      setCurrentPage(1); // Reset pagination to the first page
    }
  };

  const handleSearchBar = (e) => {
    e.preventDefault();
    handleSearchResults();
  };

  const handleSearchResults = () => {
    const filteredProdcuts = originalProducts.filter((product) =>
      product.productName.toLowerCase().includes(searchKey.toLowerCase().trim())
    );
    setProducts(filteredProdcuts);
  };

  const handleClearSearch = () => {
    setProducts(originalProducts);
    setSearchKey("");
  };

  const filterByCategory = useCallback((categoryId) => {
    if (categoryId === 0) {
      setProducts(originalProducts); // Show all products when categoryId is 0
    } else {
      const filteredProducts = originalProducts.filter(
        (product) => product.categoryId === categoryId
      );
      setProducts(filteredProducts);
    }
    setCurrentPage(1); // Reset pagination to the first page
  }, [originalProducts]);

  return (
    <Fragment>
      <SEO
        titleTemplate="Shop Page"
        description="Shop page of flone react minimalist eCommerce template."
      />

      <LayoutOne headerTop="visible">
        {/* breadcrumb */}
        <Breadcrumb
          pages={[
            { label: "Home", path: process.env.PUBLIC_URL + "/" },
            {
              label: "Shop",
              path: process.env.PUBLIC_URL + pathname,
            },
          ]}
        />

        <div className="shop-area pt-95 pb-100">
          <div className="container">
            <div className="row">
              <div className="col-lg-3 order-2 order-lg-1">
                {/* shop sidebar */}
                <ShopSidebar
                  products={products}
                  handleSearch={handleSearch} // Pass the handleSearch function to ShopSidebar
                  getSortParams={getSortParams}
                  sideSpaceClass="mr-30"
                  value={searchKey}
                  clearSearch={handleClearSearch}
                  formSubmit={handleSearchBar}
                  handleSearchKey={(e) => setSearchKey(e.target.value)}
                  filterByCategory={filterByCategory}
                />
              </div>
              <div className="col-lg-9 order-1 order-lg-2">
                {/* shop topbar default */}
                <ShopTopbar
                  getLayout={getLayout}
                  getFilterSortParams={getFilterSortParams}
                  productCount={products.length}
                  sortedProductCount={currentData.length}
                />

                {/* shop page content default */}
                <ShopProducts layout={layout} currentData={currentData} />

                {/* shop product pagination */}
                <div className="pro-pagination-style text-center mt-30">
                  <ProductPagination
                    currentPage={currentPage}
                    totalPages={Math.ceil(products.length / itemsPerPage)}
                    handlePageChange={handlePageChange}
                  />
                </div>
              </div>
            </div>
          </div>
        </div>
      </LayoutOne>
    </Fragment>
  );
};

export default ShopGridStandard;

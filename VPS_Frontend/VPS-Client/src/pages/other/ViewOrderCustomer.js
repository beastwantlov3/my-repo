import axios from "axios";
import React, { useEffect, useState } from "react";
import { Link, useParams } from "react-router-dom/cjs/react-router-dom.min";
import SEO from "../../components/seo.jsx";
import LayoutOne from "../../layouts/LayoutOne.js";
import Breadcrumb from "../../wrappers/breadcrumb/Breadcrumb";
import { useLocation } from "react-router-dom/cjs/react-router-dom.min.js";
import NotificationAlert from "react-notification-alert";
import { Button, Modal, ModalBody, ModalFooter, ModalHeader } from "reactstrap";

const ViewOrderCustomer = () => {
  const { orderId } = useParams();
  const [orderDetailData, setOrderDetailData] = useState([]);
  const [orderData, setOrderData] = useState([]);
  const [customerData, setCustomerData] = useState([]);
  const [totalAmount, setTotalAmount] = useState(0);
  let { pathname } = useLocation();
  const notificationAlertRef = React.useRef(null);
  const [cancellationReason, setCancellationReason] = useState("");
  const [cancelModal, setCancelModal] = useState(false);
  const [evaluateModal, setEvaluateModal] = useState(false);
  const [showFeedback, setShowFeedback] = useState(false);
  const [feedbackContent, setFeedbackContent] = useState({});
  const [evaluateContent, setEvaluateContent] = useState();
  const [feedbackVisibility, setFeedbackVisibility] = useState({});
  const [feedbackSubmitted, setFeedbackSubmitted] = useState(
    localStorage.getItem("feedbackSubmitted") === "true" ? true : false
  );

  const notifySuccess = () => {
    let options = {
      place: "tc",
      message: (
        <div className="alert-text">
          <span className="alert-title" data-notify="title">
            {" "}
            Xử lý đơn hàng thành công
          </span>
          <span data-notify="success">Chờ nhân viên xác nhận</span>
        </div>
      ),
      type: "success",
      icon: "ni ni-bell-55",
      autoDismiss: 7,
    };
    notificationAlertRef.current.notificationAlert(options);
  };

  const notifyDanger = () => {
    let options = {
      place: "tc",
      message: (
        <div className="alert-text">
          <span className="alert-title" data-notify="title">
            {" "}
            Xử lý đơn hàng thất bại
          </span>
          <span data-notify="message">Vui lòng kiểm tra lại đường truyền</span>
        </div>
      ),
      type: "danger",
      icon: "ni ni-bell-55",
      autoDismiss: 7,
    };
    notificationAlertRef.current.notificationAlert(options);
  };

  const fetchCustomerData = async () => {
    // if (orderDetailData.length === 0 || orderData.length === 0) return;

    try {
      const response = await axios.get(
        `https://localhost:7050/api/Customer/${orderData.customerId}`
      );
      setCustomerData(response.data);
    } catch (error) {
      console.error("Error fetching customer data:", error);
    }
  };

  useEffect(() => {
    const fetchOrderDetailData = async () => {
      try {
        const response = await axios.get(
          `https://localhost:7050/api/OrderDetail/listbyod?id=${orderId}`
        );

        setOrderDetailData(response.data);
      } catch (error) {
        console.error("Error fetching order detail data:", error);
      }
    };

    const fetchOrderData = async () => {
      try {
        const response = await axios.get(
          `https://localhost:7050/api/Order/${orderId}`
        );
        const totalPrice = response.data.price;
        const shippingCost = response.data.shipCost;
        const calculatedTotal = totalPrice - shippingCost;
        setTotalAmount(calculatedTotal);
        setOrderData(response.data);
      } catch (error) {
        console.error("Error fetching order data:", error);
      }
    };

    fetchOrderDetailData();
    fetchOrderData();
  }, [orderId]);

  useEffect(() => {
    fetchCustomerData();
  }, [orderDetailData, orderData]);

  const formatDate = (dateString) => {
    const dateOptions = {
      weekday: "long",
      day: "numeric",
      month: "long",
      year: "numeric",
    };

    const timeOptions = {
      hour: "numeric",
      minute: "numeric",
      hour12: true, // Use 12-hour clock format (AM/PM)
    };

    // Convert the date to the desired format
    const date = new Date(dateString);
    const formattedDate = date.toLocaleDateString("vi-VN", dateOptions);
    const formattedTime = date.toLocaleTimeString("vi-VN", timeOptions);

    // Return the formatted date string
    return `${formattedDate} vào ${formattedTime}`;
  };

  const fetchOrderData = async () => {
    try {
      const response = await axios.get(
        `https://localhost:7050/api/Order/${orderId}`
      );
      const totalPrice = response.data.price;
      const shippingCost = response.data.shipCost;
      const calculatedTotal = totalPrice - shippingCost;
      setTotalAmount(calculatedTotal);
      setOrderData(response.data);
      // console.log(response.data);
    } catch (error) {
      console.error("Error fetching order data:", error);
    }
  };

  const handleCancelOrderClick = () => {
    setCancelModal(true);
  };

  // Function to close the cancellation modal
  const toggleEvaluateModal = () => {
    setEvaluateModal(false);
  };

  const handleEvaluateOrderClick = () => {
    setEvaluateModal(true);
  };

  // Function to close the cancellation modal
  const toggleCancelModal = () => {
    setCancelModal(false);
  };

  const handleConfirmOrder = async () => {
    try {
      const currentDate = new Date();
      const year = currentDate.getFullYear();
      const month = String(currentDate.getMonth() + 1).padStart(2, "0");
      const day = String(currentDate.getDate()).padStart(2, "0");
      const hours = String(currentDate.getHours()).padStart(2, "0");
      const minutes = String(currentDate.getMinutes()).padStart(2, "0");
      const seconds = String(currentDate.getSeconds()).padStart(2, "0");
      const milliseconds = String(currentDate.getMilliseconds()).padStart(
        3,
        "0"
      );

      const formattedDate = `${year}-${month}-${day}T${hours}:${minutes}:${seconds}.${milliseconds}Z`;

      const imageUrl = orderData.currentImage;
      const extractedPart = imageUrl.match(/\/([^/]+)\?/)[1];
      const formattedPart = "product/" + extractedPart;

      const updatedOrderData = {
        ...orderData,
        currentImage: formattedPart,
        statusId: 6,
        customerConfirm: formattedDate,
      };
      await axios.put(
        `https://localhost:7050/api/Order/${orderId}`,
        updatedOrderData
      );
      // Optionally, you can also refetch the updated order data
      fetchOrderData();
      notifySuccess();
    } catch (error) {
      console.log("Error updating order status:", error);
      notifyDanger();
    }
  };
  const handleCancelledOrder = async () => {
    try {
      // Fetch product data for each order detail
      const productPromises = orderDetailData.map(async (orderDetail) => {
        try {
          const responseProduct = await axios.get(
            `https://localhost:7050/api/Product/GetProductById/${orderDetail.product.productId}`
          );
          console.log(
            "Product Response for productId",
            orderDetail.product.productId,
            ":",
            responseProduct.data
          );
          return responseProduct.data; // Assuming your API returns the product data
        } catch (error) {
          console.error("Error fetching product data:", error);
          return null;
        }
      });

      const productsData = await Promise.all(productPromises);
      console.log("productdata", productsData);

      // Get current date and time
      const currentDate = new Date();
      const year = currentDate.getFullYear();
      const month = String(currentDate.getMonth() + 1).padStart(2, "0");
      const day = String(currentDate.getDate()).padStart(2, "0");
      const hours = String(currentDate.getHours()).padStart(2, "0");
      const minutes = String(currentDate.getMinutes()).padStart(2, "0");
      const seconds = String(currentDate.getSeconds()).padStart(2, "0");
      const milliseconds = String(currentDate.getMilliseconds()).padStart(
        3,
        "0"
      );

      // Format the date and time
      const formattedDate = `${year}-${month}-${day}T${hours}:${minutes}:${seconds}.${milliseconds}Z`;

      // Update order data with status and cancellation date

      const imageUrl = orderData.currentImage;
      const extractedPart = imageUrl.match(/\/([^/]+)\?/)[1];
      const formattedPart = "product/" + extractedPart;

      const updatedOrderData = {
        ...orderData,
        currentImage: formattedPart,
        statusId: 7,
        customerCancelled: formattedDate,
      };
      await axios.put(
        `https://localhost:7050/api/Order/${orderId}`,
        updatedOrderData
      );

      // Create an alert entry for the cancellation
      const updateAlertData = {
        customerId: orderData.customerId,
        message: `Lý do không nhận đơn #${orderId}: ${cancellationReason}`,
        orderId: orderId,
        currentDate: formattedDate,
      };
      await axios.post(`https://localhost:7050/api/Alert`, updateAlertData);

      // Update product quantities
      for (let i = 0; i < orderDetailData.length; i++) {
        const orderDetail = orderDetailData[i];
        const productData = productsData[i]; // Corresponding product data

        if (productData) {
          const updateProduct = {
            ...productData,
            unitInStock: productData.unitInStock + orderDetail.quantity,
          };

          await axios.put(
            `https://localhost:7050/api/Product/${orderDetail.product.productId}`,
            updateProduct
          );
        } else {
          console.log(
            `No product data found for order detail ${orderDetail.product.productId}`
          );
        }
      }

      toggleCancelModal();
      // Optionally, you can also refetch the updated order data
      fetchOrderData();
      notifySuccess();
    } catch (error) {
      console.log("Error updating order status:", error);
      notifyDanger();
    }
  };

  const handleReturnOrderClick = async (orderDetailId, feedbackValue) => {
    try {
      const currentDate = new Date();
      const year = currentDate.getFullYear();
      const month = String(currentDate.getMonth() + 1).padStart(2, "0");
      const day = String(currentDate.getDate()).padStart(2, "0");
      const hours = String(currentDate.getHours()).padStart(2, "0");
      const minutes = String(currentDate.getMinutes()).padStart(2, "0");
      const seconds = String(currentDate.getSeconds()).padStart(2, "0");
      const milliseconds = String(currentDate.getMilliseconds()).padStart(
        3,
        "0"
      );

      const formattedDate = `${year}-${month}-${day}T${hours}:${minutes}:${seconds}.${milliseconds}Z`;
      const updateAlertData = {
        customerId: orderData.customerId,
        message: `Lý do hoàn đơn hàng #${orderDetailId}: ${feedbackValue}`,
        orderId: orderId,
        shipperId: orderData.shipId,
        saleId: orderData.saleId,
        currentDate: formattedDate,
        orderDetailId: orderDetailId,
      };
      await axios.post(`https://localhost:7050/api/Alert`, updateAlertData);
      setFeedbackVisibility((prevVisibility) => ({
        ...prevVisibility,
        [orderDetailId]: false,
      }));
      notifySuccess();
    } catch (error) {
      console.error("Error submitting feedback:", error);
      notifyDanger();
    }
  };

  const toggleFeedback = () => {
    setShowFeedback(!showFeedback);
    setFeedbackContent(""); // Clear the feedback content when toggling
    setEvaluateContent(""); // Clear the feedback content when toggling
  };

  const submitFeedback = async () => {
    try {
      const currentDate = new Date();
      const year = currentDate.getFullYear();
      const month = String(currentDate.getMonth() + 1).padStart(2, "0");
      const day = String(currentDate.getDate()).padStart(2, "0");
      const hours = String(currentDate.getHours()).padStart(2, "0");
      const minutes = String(currentDate.getMinutes()).padStart(2, "0");
      const seconds = String(currentDate.getSeconds()).padStart(2, "0");
      const milliseconds = String(currentDate.getMilliseconds()).padStart(
        3,
        "0"
      );
      const formattedDate = `${year}-${month}-${day}T${hours}:${minutes}:${seconds}.${milliseconds}Z`;
      // Send the feedback content to the API
      const response = await axios.post("https://localhost:7050/api/Alert", {
        customerId: orderData.customerId,
        message: `Đánh giá sản phẩm: ${evaluateContent}`,
        orderId: orderData.orderId,
        currentDate: formattedDate,
        saleId: orderData.saleId,
        shipperId: orderData.shipId,
      });
      setEvaluateModal(false);
      setFeedbackSubmitted(true);
      localStorage.setItem("feedbackSubmitted", "true");
      setShowFeedback(false);
      // Handle the response if needed
      console.log("Feedback submitted successfully:", response.data);
      notifySuccess();
      toggleFeedback(); // Close the feedback textarea
    } catch (error) {
      console.error("Error submitting feedback:", error);
    }
  };

  const handleDoneOrder = async () => {
    try {
      // Send the feedback content to the API

      const imageUrl = orderData.currentImage;
      const extractedPart = imageUrl.match(/\/([^/]+)\?/)[1];
      const formattedPart = "product/" + extractedPart;

      await axios.put(`https://localhost:7050/api/Order/${orderData.orderId}`, {
        ...orderData,
        currentImage: formattedPart,
        statusId: 8,
      });

      const currentDate = new Date();
      const year = currentDate.getFullYear();
      const month = String(currentDate.getMonth() + 1).padStart(2, "0");
      const day = String(currentDate.getDate()).padStart(2, "0");
      const hours = String(currentDate.getHours()).padStart(2, "0");
      const minutes = String(currentDate.getMinutes()).padStart(2, "0");
      const seconds = String(currentDate.getSeconds()).padStart(2, "0");
      const milliseconds = String(currentDate.getMilliseconds()).padStart(
        3,
        "0"
      );
      const formattedDate = `${year}-${month}-${day}T${hours}:${minutes}:${seconds}.${milliseconds}Z`;
      // Send the feedback content to the API
      const response = await axios.post("https://localhost:7050/api/Alert", {
        customerId: orderData.customerId,
        message: `Khách hàng đã sử dụng xong đơn hàng #${orderData.orderId}. Vui lòng xác nhận.`,
        orderId: orderData.orderId,
        currentDate: formattedDate,
        saleId: orderData.saleId,
        shipperId: orderData.shipId,
      });
      console.log("Feedback submitted successfully:", response.data);

      notifySuccess();
    } catch (error) {
      console.error("Error submitting feedback:", error);
    }
  };

  const VND = new Intl.NumberFormat("vi-VN", {
    style: "currency",
    currency: "VND",
  });

  return (
    <>
      <SEO
        titleTemplate="Order"
        description="Order page of flone react minimalist eCommerce template."
      />

      <LayoutOne headerTop="visible">
        <Breadcrumb
          pages={[
            { label: "Home", path: process.env.PUBLIC_URL + "/" },
            { label: "Order", path: process.env.PUBLIC_URL + pathname },
          ]}
        />
        <div className="rna-wrapper">
          <NotificationAlert ref={notificationAlertRef} />
        </div>
        {/* Hủy đơn hàng ------------------------ */}
        <Modal isOpen={cancelModal} toggle={toggleCancelModal}>
          <ModalHeader toggle={toggleCancelModal}>Hủy đơn hàng</ModalHeader>
          <ModalBody>
            <div className="form-group">
              <label htmlFor="cancellationReason">Lý do hủy đơn hàng:</label>
              <textarea
                required
                className="form-control"
                id="cancellationReason"
                rows="4"
                value={cancellationReason}
                onChange={(e) => setCancellationReason(e.target.value)}
              />
            </div>
          </ModalBody>
          <ModalFooter>
            <Button color="danger" onClick={handleCancelledOrder}>
              Xác nhận hủy đơn
            </Button>{" "}
            <Button color="secondary" onClick={toggleCancelModal}>
              Hủy
            </Button>
          </ModalFooter>
        </Modal>
        {/* Đánh giá đơn hàng ------------------------ */}
        <Modal isOpen={evaluateModal} toggle={toggleEvaluateModal}>
          <ModalHeader toggle={toggleEvaluateModal}>
            Đánh giá đơn hàng
          </ModalHeader>
          <ModalBody>
            <div className="form-group">
              <label htmlFor="cancellationReason">Đánh giá đơn hàng:</label>
              <textarea
                className="form-control"
                id="cancellationReason"
                rows="4"
                value={evaluateContent}
                onChange={(e) => setEvaluateContent(e.target.value)}
              />
            </div>
          </ModalBody>
          <ModalFooter>
            <Button color="danger" onClick={submitFeedback}>
              Gửi đánh giá
            </Button>{" "}
            <Button color="secondary" onClick={toggleEvaluateModal}>
              Hủy
            </Button>
          </ModalFooter>
        </Modal>
        <div className="col-lg-10 mt--4 mx-auto">
          <div className="card my-5">
            <div className="card-header pb-0">
              <div className="d-flex justify-content-between align-items-center">
                <div>
                  <h3>Chi tiết đơn hàng</h3>
                  <p className="text-sm mb-0">
                    Order no. <b>#{orderData.orderId}</b> from{" "}
                    <b>
                      {new Date(orderData.orderDate).toLocaleDateString(
                        "en-US",
                        {
                          month: "2-digit",
                          day: "2-digit",
                          year: "numeric",
                        }
                      )}
                    </b>
                  </p>
                  <p className="text-sm">
                    Ngày dự kiến giao hàng:{" "}
                    <b>
                      {orderData.shippedDate !== null
                        ? new Date(orderData.shippedDate).toLocaleDateString(
                            "en-US",
                            {
                              month: "2-digit",
                              day: "2-digit",
                              year: "numeric",
                            }
                          )
                        : "Chưa xác định"}
                    </b>
                  </p>
                </div>
                {orderData.shipConfirm !== null ? (
                  <div>
                    {feedbackSubmitted && orderData.statusId === 4 ? (
                      <a
                        href={process.env.PUBLIC_URL + "/"}
                        className="btn ms-auto mb-0 bg-gradient-success"
                      >
                        Quay về Home
                      </a>
                    ) : (
                      <div>
                        <button
                          className="btn ms-auto mb-0 bg-gradient-success"
                          onClick={() => {
                            if (
                              orderData.customerConfirm === null &&
                              orderData.customerCancelled === null &&
                              orderData.shipSuccessConfirm !== null
                            ) {
                              handleConfirmOrder();
                            } else if (
                              orderData.shipSuccessConfirm !== null &&
                              orderData.statusId !== 8 &&
                              !showFeedback
                            ) {
                              handleEvaluateOrderClick();
                            } else if (
                              orderData.customerConfirm !== null &&
                              orderData.statusId !== 8
                            ) {
                              handleDoneOrder();
                            }
                          }}
                        >
                          {orderData.customerConfirm === null &&
                          orderData.customerCancelled === null &&
                          orderData.shipSuccessConfirm !== null ? (
                            "Xác nhận đơn hàng"
                          ) : orderData.shipSuccessConfirm !== null &&
                            orderData.statusId !== 8 &&
                            !showFeedback ? (
                            "Đánh giá đơn hàng"
                          ) : orderData.customerConfirm !== null &&
                            orderData.statusId !== 8 ? (
                            "Xác nhận dùng xong"
                          ) : (
                            <a
                              href={process.env.PUBLIC_URL + "/"}
                              className="bg-gradient-success text-white"
                            >
                              Quay về Home
                            </a>
                          )}
                        </button>
                      </div>
                    )}
                  </div>
                ) : orderData.shipConfirm === null ||
                  orderData.customerCancelled !== null ? (
                  orderData.customerCancelled !== null ? null : (
                    <button
                      className="btn ms-auto mb-0 bg-gradient-success"
                      onClick={handleCancelOrderClick}
                    >
                      Hủy đơn hàng
                    </button>
                  )
                ) : null}
              </div>
            </div>
            <div className="card-body p-3 pt-0">
              <div className="row d-flex justify-content-between mb-4 mt-2">
                <div className="col-lg-5 col-md-6 col-12 ml-2">
                  <h3 className="mb-3">Theo dõi đơn hàng</h3>
                  <div className="timeline timeline-one-side">
                    <div className="timeline-block mb-2">
                      <span className="timeline-step">
                        <i className="ni ni-cart text-orange"></i>
                      </span>
                      <div className="timeline-content">
                        <h6 className="text-dark text-sm font-weight-bold mb-0">
                          Ngày tạo đơn hàng
                        </h6>
                        <p className="text-dark font-weight-bold text-xs mt-1 mb-0 ">
                          {orderData.orderDate ? (
                            <p className="text-dark font-weight-bold text-xs mt-1 mb-0">
                              {formatDate(orderData.orderDate)}
                            </p>
                          ) : (
                            <p className="text-danger font-weight-bold text-xs mt-1 mb-0">
                              Chưa xác nhận
                            </p>
                          )}
                        </p>
                      </div>
                    </div>
                    <div className="timeline-block mb-2">
                      <span className="timeline-step">
                        <i className="ni ni-briefcase-24 text-blue" />
                      </span>
                      <div className="timeline-content">
                        <h6 className="text-dark text-sm font-weight-bold mb-0">
                          Quản lý
                        </h6>
                        <p className="text-dark font-weight-bold text-xs mt-1 mb-0 ">
                          {orderData.managerConfirm ? (
                            <p className="text-dark font-weight-bold text-xs mt-1 mb-0">
                              Đã xác nhận:{" "}
                              {formatDate(orderData.managerConfirm)}
                            </p>
                          ) : orderData.managerViewed ? (
                            <p className="text-dark font-weight-bold text-xs mt-1 mb-0">
                              Đã xem: {formatDate(orderData.managerViewed)}
                            </p>
                          ) : orderData.managerCancelled ? (
                            <p className="text-danger font-weight-bold text-xs mt-1 mb-0">
                              Đã hủy đơn:{" "}
                              {formatDate(orderData.managerCancelled)}
                            </p>
                          ) : (
                            <p className="text-dark font-weight-bold text-xs mt-1 mb-0">
                              Đang chờ xác nhận...
                            </p>
                          )}
                        </p>
                      </div>
                    </div>
                    <div className="timeline-block mb-2">
                      <span className="timeline-step">
                        <i className="ni ni-folder-17 text-yellow"></i>
                      </span>
                      <div className="timeline-content">
                        <h6 className="text-dark text-sm font-weight-bold mb-0">
                          Nhân viên
                        </h6>
                        <p className="text-dark font-weight-bold text-xs mt-1 mb-0">
                          {orderData.saleConfirm ? (
                            <p className="text-dark font-weight-bold text-xs mt-1 mb-0">
                              Đã xác nhận: {formatDate(orderData.saleConfirm)}
                            </p>
                          ) : orderData.salerCancelled ? (
                            <p className="text-danger font-weight-bold text-xs mt-1 mb-0">
                              Đã hủy: {formatDate(orderData.salerCancelled)}
                            </p>
                          ) : (
                            <p className="text-danger font-weight-bold text-xs mt-1 mb-0">
                              Chưa xác nhận
                            </p>
                          )}
                        </p>
                      </div>
                    </div>
                    <div className="timeline-block mb-2">
                      <span className="timeline-step">
                        <i className="ni ni-delivery-fast text-success"></i>
                      </span>
                      <div className="timeline-content">
                        <h6 className="text-dark text-sm font-weight-bold mb-0">
                          Người giao hàng nhận đơn{" "}
                        </h6>
                        <p className="text-dark font-weight-bold text-xs mt-1 mb-0">
                          {orderData.shipConfirm ? (
                            <p className="text-dark font-weight-bold text-xs mt-1 mb-0">
                              Đã xác nhận: {formatDate(orderData.shipConfirm)}
                            </p>
                          ) : orderData.shipperCancelled ? (
                            <p className="text-danger font-weight-bold text-xs mt-1 mb-0">
                              Đã hủy: {formatDate(orderData.shipperCancelled)}
                            </p>
                          ) : (
                            <p className="text-danger font-weight-bold text-xs mt-1 mb-0">
                              Chưa xác nhận
                            </p>
                          )}
                        </p>
                      </div>
                    </div>
                    <div className="timeline-block mb-2">
                      <span className="timeline-step">
                        <i className="ni ni-square-pin text-danger"></i>
                      </span>
                      <div className="timeline-content">
                        <h6 className="text-dark text-sm font-weight-bold mb-0">
                          Giao hàng thành công
                        </h6>
                        <p className="text-dark font-weight-bold text-xs mt-1 mb-0">
                          {orderData.shipSuccessConfirm ? (
                            <p className="text-dark font-weight-bold text-xs mt-1 mb-0">
                              {formatDate(orderData.shipSuccessConfirm)}
                            </p>
                          ) : (
                            <p className="text-danger font-weight-bold text-xs mt-1 mb-0">
                              Chưa xác nhận
                            </p>
                          )}
                        </p>
                      </div>
                    </div>
                    <div className="timeline-block mb-2">
                      <span className="timeline-step">
                        <i className="ni ni-check-bold text-success"></i>
                      </span>
                      <div className="timeline-content">
                        <h6 className="text-dark text-sm font-weight-bold mb-0">
                          Khách hàng xác nhận
                        </h6>
                        <p className="text-dark font-weight-bold text-xs mt-1 mb-0">
                          {orderData.customerConfirm ? (
                            <p className="text-dark font-weight-bold text-xs mt-1 mb-0">
                              {formatDate(orderData.customerConfirm)}
                            </p>
                          ) : orderData.customerCancelled ? (
                            <p className="text-danger font-weight-bold text-xs mt-1 mb-0">
                              Đã hủy: {formatDate(orderData.customerCancelled)}
                            </p>
                          ) : (
                            <p className="text-danger font-weight-bold text-xs mt-1 mb-0">
                              Chưa xác nhận
                            </p>
                          )}
                        </p>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="col-lg-6 col-md-6 col-12">
                  <h3 className="mb-3">Thông tin của đơn hàng</h3>

                  <ul className="list-group">
                    <li className="list-group-item border-0 d-flex mb-2 bg-gradient-success border-radius-lg">
                      <div className="d-flex flex-column text-white">
                        <h3 className="mb-3 text-white">
                          {customerData.fullName}
                        </h3>
                        <span className="mb-2 text-sm text-white">
                          Địa chỉ:{" "}
                          <span className="text-white ms-2 font-weight-bold">
                            {customerData.address}
                          </span>
                        </span>
                        <span className="mb-2 text-sm text-white">
                          Số dư:{" "}
                          <span className="text-white ms-2 font-weight-bold">
                            {VND.format(customerData.balance)}
                          </span>
                        </span>
                        <span className="text-sm">
                          Số điện thoại:{" "}
                          <span className="text-white ms-2 font-weight-bold">
                            {customerData.phone}
                          </span>
                        </span>
                      </div>
                    </li>
                  </ul>
                </div>
              </div>
              <hr className="horizontal dark my-1" />
              {orderDetailData.map((orderDetail, index) => (
                <React.Fragment key={orderDetail.orderDetailId}>
                  <div className="row d-flex justify-content-between mb-4 mt-2">
                    <div className="col-lg-6 col-md-6 col-12">
                      <div>
                        <img
                          src={orderDetail.product.image}
                          className="avatar avatar-xl me-3"
                          alt="product image"
                        ></img>
                      </div>
                      <div>
                        <h6 className="text-lg mb-0 mt-2">
                          {orderDetail.product.productName}
                        </h6>
                        <p className="text-sm mb-3">
                          <b>Hãng: </b>
                          {orderDetail.product.brand}
                        </p>
                      </div>
                    </div>

                    <div className="col-lg-4 col-md-4 col-12">
                      <h3 className="mb-3">Chi tiết</h3>
                      <div className="d-flex justify-content-between">
                        <span className="mb-2 text-sm">Giá sản phẩm:</span>
                        <span className="text-dark font-weight-bold ms-2">
                          {VND.format(orderDetail.unitPrice)}
                        </span>
                      </div>
                      <div className="d-flex justify-content-between">
                        <span className="mb-2 text-sm">Số lượng:</span>
                        <span className="text-dark font-weight-bold ms-2">
                          {orderDetail.quantity}
                        </span>
                      </div>
                      <div className="d-flex justify-content-between">
                        <span className="text-sm">Khuyến mãi:</span>
                        <span className="text-dark ms-2 font-weight-bold">
                          {orderDetail.discount}
                        </span>
                      </div>
                    </div>
                    <div className="col-lg-2 col-md-2 col-12 mt-2">
                      {orderData.shipSuccessConfirm !== null &&
                      orderData.statusId === 5 &&
                      orderDetail.status !== 2 ? (
                        <div>
                          <a
                            className="btn ms-auto mb-0 bg-gradient-danger text-white mt-5 ml-2"
                            onClick={() =>
                              setFeedbackVisibility((prevVisibility) => ({
                                ...prevVisibility,
                                [orderDetail.orderDetailId]: true,
                              }))
                            }
                          >
                            Hoàn trả đơn hàng
                          </a>
                          {feedbackVisibility[orderDetail.orderDetailId] && (
                            <div className="mt-2">
                              <textarea
                                rows="5"
                                placeholder="Lý do hoàn đơn của bạn..."
                                value={
                                  feedbackContent[orderDetail.orderDetailId] ||
                                  ""
                                }
                                onChange={(e) =>
                                  setFeedbackContent((prevFeedback) => ({
                                    ...prevFeedback,
                                    [orderDetail.orderDetailId]: e.target.value,
                                  }))
                                }
                              />
                              <button
                                className="btn btn-danger mt-2"
                                onClick={() => {
                                  handleReturnOrderClick(
                                    orderDetail.orderDetailId,
                                    feedbackContent[orderDetail.orderDetailId]
                                  );
                                  setFeedbackVisibility((prevVisibility) => ({
                                    ...prevVisibility,
                                    [orderDetail.orderDetailId]: false,
                                  }));
                                }}
                              >
                                Gửi
                              </button>
                            </div>
                          )}
                        </div>
                      ) : orderDetail.status === 2?(
                        <h6 className="btn mb-0 bg-gradient-success text-white mt-5 ml-2">
                        Đã xác nhận hoàn trả
                      </h6>
                      ): null}
                    </div>
                  </div>
                  {index !== orderData.length - 1 && (
                    <hr className="horizontal dark my-1" />
                  )}
                </React.Fragment>
              ))}
              {/* <hr className="horizontal dark my-1"></hr> */}
              <div className="row mt-4 ml-2">
                <div className="col-lg-12 col-12 ms-auto mr-4">
                  <h3 className="mb-3">Thanh toán</h3>
                  <div className="d-flex justify-content-between mb-4">
                    <span className="text-sm">Tổng tiền hàng: </span>
                    <span className="text-dark ms-2 font-weight-bold">
                      {VND.format(totalAmount - orderData.shipCost)}
                    </span>
                  </div>
                  <div className="d-flex justify-content-between">
                    <span className="text-sm">Phí vận chuyển:</span>
                    <span className="text-dark ms-2 font-weight-bold">
                      {orderData.shipCost === null
                        ? "0"
                        : `${VND.format(orderData.shipCost)}`}{" "}
                    </span>
                  </div>
                  <div className="d-flex justify-content-between mt-4">
                    <span className="mb-2 text-lg">Tổng thanh toán:</span>
                    <span className="text-dark text-lg ms-2 font-weight-bold">
                      {VND.format(totalAmount)}
                    </span>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </LayoutOne>
    </>
  );
};

export default ViewOrderCustomer;

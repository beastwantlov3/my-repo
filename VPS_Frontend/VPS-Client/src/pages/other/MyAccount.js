import { useCallback, Fragment, useEffect, useState, useRef } from "react";
import axios from "axios";
import { useLocation } from "react-router-dom";
import Accordion from "react-bootstrap/Accordion";
import SEO from "../../components/seo";
import LayoutOne from "../../layouts/LayoutOne";
import Breadcrumb from "../../wrappers/breadcrumb/Breadcrumb";
import { useHistory } from "react-router-dom";
import cogoToast from "cogo-toast";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faEye, faEyeSlash } from "@fortawesome/free-solid-svg-icons";
import moment from "moment";
import "bootstrap/dist/css/bootstrap.min.css";
import { ClipLoader } from "react-spinners";

const MyAccount = () => {
  const containerStyle = {
    width: "150px",
    height: "150px",
    border: "1px solid #ccc",
    borderRadius: "50%",
    overflow: "hidden",
  };

  const imageStyle = {
    width: "100%",
    height: "100%",
    objectFit: "cover",
  };

  axios.defaults.withCredentials = true;
  let { pathname } = useLocation();
  const [userData, setUserData] = useState(null);
  const [identityData, setIdentityData] = useState(null);
  const [currentPassword, setCurrentPassword] = useState("");
  const [newPassword, setNewPassword] = useState("");
  const [confirmPassword, setConfirmPassword] = useState("");
  const [showCurrentPassword, setShowCurrentPassword] = useState(false);
  const [showNewPassword, setShowNewPassword] = useState(false);
  const [showConfirmPassword, setShowConfirmPassword] = useState(false);
  const history = useHistory();

  const [fullName, setFullName] = useState("");
  const [phone, setPhone] = useState("");
  const [address, setAddress] = useState("");
  const [dob, setDob] = useState("");
  const [profile, setProfile] = useState("");

  const [identityCardImage, setIdentityCardImage] = useState("");
  const [backIdentityCard, setBackIdentityCard] = useState("");

  //Hiển thị thông báo lỗi
  const [fullNameError, setFullNameError] = useState("");
  const [phoneError, setPhoneError] = useState("");
  // const [addressError, setAddressError] = useState("");
  // const [dobError, setDobError] = useState("");
  // const [profileError, setProfileError] = useState("");

  const [isFormEdited, setFormEdited] = useState(false);

  //Hiển thị lỗi đổi mật khẩu
  const [currentPasswordError, setCurrentPasswordError] = useState("");
  const [newPasswordError, setNewPasswordError] = useState("");
  const [confirmPasswordError, setConfirmPasswordError] = useState("");

  const [selectedFile, setSelectedFile] = useState(null);
  const [avatar, setAvatar] = useState("");
  const videoRef = useRef(null);
  const [imageData, setImageData] = useState("");
  const handleFileChange = (event) => {
    setSelectedFile(event.target.files[0]);
  };

  //Hiển thị mật khẩu
  const handleCurrentPasswordToggle = () => {
    setShowCurrentPassword(!showCurrentPassword);
  };
  const handleNewPasswordToggle = () => {
    setShowNewPassword(!showNewPassword);
  };
  const handleConfirmPasswordToggle = () => {
    setShowConfirmPassword(!showConfirmPassword);
  };

  //Lấy thông tin data UserProfile
  useEffect(() => {
    const fetchUserData = async () => {
      try {
        const token = sessionStorage.getItem("token");
        const config = {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        };
        const response = await axios.get(
          "https://localhost:7050/api/Account/get-user",
          config
        );
        setUserData(response.data);
        // Populate the form fields if user data is available
        if (response.data) {
          const { fullName, phone, address, dob, profile, avatar } =
            response.data;
          setFullName(fullName);
          setPhone(phone);
          setAddress(address);
          setDob(dob);
          setProfile(profile);
          setAvatar(avatar);
        }

        const isDirty =
          fullName !== response.data.fullName ||
          phone !== response.data.phone ||
          dob !== response.data.dob ||
          address !== response.data.address ||
          profile !== response.data.profile;
        setFormEdited(isDirty);
      } catch (error) {
        console.error(error);
      }
    };

    fetchUserData();
  }, []);
  //state button camera, chụp
  const [showCaptureButton, setShowCaptureButton] = useState(true);
  const [showSaveButton, setShowSaveButton] = useState(false);
  //mở camera
  const handleCapture = async () => {
    const constraints = { video: true };

    try {
      const stream = await navigator.mediaDevices.getUserMedia(constraints);
      videoRef.current.srcObject = stream;
      setShowCaptureButton(false);
      setShowSaveButton(true);
    } catch (err) {
      console.error("Error accessing camera:", err);
    }
  };

  //Chụp ảnh
  const handleTakePhoto = () => {
    const canvas = document.createElement("canvas");
    const video = videoRef.current;
    canvas.width = video.videoWidth;
    canvas.height = video.videoHeight;
    const ctx = canvas.getContext("2d");
    ctx.drawImage(video, 0, 0, canvas.width, canvas.height);

    // Convert the canvas image to base64 data URL in JPEG format
    const dataURL = canvas.toDataURL("image/jpeg"); // Thay đổi thành 'image/jpeg'
    setImageData(dataURL);
  };

  //tắt camera
  const handleCancelCapture = () => {
    // Thực hiện các xử lý khi người dùng bấm nút tắt camera
    // Cập nhật giá trị của các biến trạng thái để ẩn button "Lưu ảnh" và hiển thị lại button "Chụp ảnh"
    setShowCaptureButton(true);
    setShowSaveButton(false);
    // Tắt video bằng cách dừng stream và hủy kết nối video
    const videoElement = videoRef.current;
    if (videoElement && videoElement.srcObject) {
      const stream = videoElement.srcObject;
      const tracks = stream.getTracks();
      tracks.forEach((track) => track.stop());
      videoElement.srcObject = null;
    }
  };

  //upload ảnh
  //upload ảnh
  const handleUpload = useCallback(async () => {
    if (imageData) {
      // Nếu có ảnh từ camera được chụp, tải lên ảnh này
      // const formData = new FormData();
      // const base64Image = imageData.split(",")[1];
      // const imageBlob = new Blob([atob(base64Image)], { type: "image/jpeg" }); // Thay đổi thành 'image/jpeg'
      // formData.append("file", imageBlob);
      const formData = new FormData();
      formData.append("file", dataURItoBlob(imageData));
      try {
        const token = sessionStorage.getItem("token");
        const config = {
          headers: {
            Authorization: `Bearer ${token}`,
            "Content-Type": "image/jpeg", // Đặt Content-Type là image/jpeg
          },
        };
        await axios.post(
          "https://localhost:7050/api/Account/update-avatar",
          formData,
          config
        );
        cogoToast.success("Tải ảnh avatar thành công", {
          position: "bottom-left",
        });
      } catch (error) {
        cogoToast.error("Tải ảnh avatar thất bại", { position: "bottom-left" });
        console.error(error);
      }
    } else if (selectedFile) {
      // Nếu không có ảnh từ camera, nhưng có ảnh từ file input, tải lên ảnh này
      const formData = new FormData();
      formData.append("file", selectedFile);
      const token = sessionStorage.getItem("token");
      const config = {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      };
      try {
        await axios.post(
          "https://localhost:7050/api/Account/update-avatar",
          formData,
          config
        );

        cogoToast.success("Tải ảnh avatar thành công", {
          position: "bottom-left",
        });
      } catch (error) {
        cogoToast.error("Tải ảnh avatar thất bại", { position: "bottom-left" });
        console.error(error);
      }
    }
  }, [imageData, selectedFile]);

  const dataURItoBlob = (dataURI) => {
    const byteString = atob(dataURI.split(",")[1]);
    const mimeString = dataURI.split(",")[0].split(":")[1].split(";")[0];
    const ab = new ArrayBuffer(byteString.length);
    const ia = new Uint8Array(ab);
    for (let i = 0; i < byteString.length; i++) {
      ia[i] = byteString.charCodeAt(i);
    }
    return new Blob([ab], { type: mimeString });
  };

  //Chỉnh sửa thông tin profile
  const handleUpdateProfile = async (e) => {
    e.preventDefault();
    try {
      setFullNameError("");
      setPhoneError("");

      if (fullName.length > 30) {
        setFullNameError("Họ và tên không được vượt quá 30 ký tự.");
      } else if (fullName.indexOf(" ") === -1) {
        setFullNameError(
          "Họ và tên không hợp lệ. Phải có ít nhất 1 khoảng trắng."
        );
      }

      if (!/^\d{10,}$/.test(phone)) {
        setPhoneError(
          "Số điện thoại không hợp lệ. Vui lòng nhập ít nhất 10 số."
        );
      }

      const updateData = {
        fullName: fullName,
        phone: phone,
        address: address,
        dob: dob,
        profile: profile,
      };

      const token = sessionStorage.getItem("token");
      const config = {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      };

      await axios.put(
        "https://localhost:7050/api/Account/update-profile",
        updateData,
        config
      );

      // Xử lý khi cập nhật thành công
      cogoToast.success("Cập nhật thông tin thành công", {
        position: "bottom-left",
      });
      setFormEdited(false);
      // Các xử lý tiếp theo sau khi cập nhật thành công nếu cần
    } catch (error) {
      // Xử lý khi gặp lỗi, có thể kiểm tra error.response để xác định lỗi cụ thể từ server
      cogoToast.error("Cập nhật thông tin thất bại", {
        position: "bottom-left",
      });
    }
  };

  //Đổi mật khẩu user
  const handleChangePassword = async (e) => {
    e.preventDefault();
    try {
      setCurrentPasswordError("");
      setNewPasswordError("");
      setConfirmPasswordError("");
      if (!currentPassword) {
        setCurrentPasswordError("Vui lòng nhập mật khẩu hiện tại.");
      }
      if (!newPassword) {
        setNewPasswordError("Vui lòng nhập mật khẩu mới.");
      }
      if (!confirmPassword) {
        setConfirmPasswordError("Vui lòng nhập xác nhận mật khẩu.");
      }
      const token = sessionStorage.getItem("token");
      const config = {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      };

      // Gửi yêu cầu xác thực mã nhập vào đến API
      const params = new URLSearchParams();
      params.append("currentPassword", currentPassword);
      params.append("newPassword", newPassword);
      params.append("confirmPassword", confirmPassword);
      await axios.put(
        `https://localhost:7050/api/Auth/change-password`,
        params,
        config
      );
      // Xử lý phản hồi từ API và hiển thị thông báo tương ứng
      // Xác thực thành công
      // Hiển thị thông báo thành công
      // if (!currentPasswordError && !newPasswordError && !confirmPasswordError) {
      cogoToast.success("Đổi mật khẩu thành công", {
        position: "bottom-left",
      });
      // navigate.push("/my-account");
      window.location.href = "http://localhost:3000/my-account";
      // } else {
      //   cogoToast.error("Đổi mật khẩu thất bại", { position: "bottom-left" });
      // }
    } catch (error) {
      if (error.response && error.response.data) {
        const errorMessage = error.response.data.error;
        if (
          errorMessage ===
          "Mật khẩu hiện tại không đúng. Vui lòng nhập lại mật khẩu!"
        ) {
          setCurrentPasswordError(errorMessage);
        } else if (
          errorMessage ===
          "Mật khẩu không hợp lệ. Mật khẩu yêu cầu ít nhất 1 kí tự viết hoa và 1 kí tự đặc biệt và 1 kí tự số và không được quá 14 kí tự"
        ) {
          setNewPasswordError(errorMessage);
        } else if (
          errorMessage === "Xác nhận mật khẩu không khớp với mật khẩu."
        ) {
          setConfirmPasswordError(errorMessage);
        } else {
          console.error("Unknown error:", errorMessage);
          cogoToast.error("Đổi mật khẩu thất bại", { position: "bottom-left" });
        }
      } else {
        console.error("Registration failed:", error);
        cogoToast.error("Đổi mật khẩu thất bại", { position: "bottom-left" });
      }
    }
  };
  const [isValid, setIsValid] = useState(null);

  const [fullname, setfullname] = useState("");
  const [cardNumber, setCardNumber] = useState("");
  const [dateOfBirth, setDateOfBirth] = useState("");
  const [gender, setGender] = useState("");
  const [nationality, setNationality] = useState("");
  const [placeOfOrigin, setPlaceOfOrigin] = useState("");
  //Lấy thông tin data UserProfile
  useEffect(() => {
    const fetchIdentityData = async () => {
      try {
        const token = sessionStorage.getItem("token");
        const config = {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        };
        const response = await axios.get(
          "https://localhost:7050/api/Account/get-identity",
          config
        );
        setIdentityData(response.data);
        // Populate the form fields if user data is available
        if (response.data) {
          const {
            identityCardImage,
            backIdentityCard,
            isValid,
            fullname,
            cardNumber,
            dateOfBirth,
            gender,
            nationality,
            placeOfOrigin,
          } = response.data;
          setIdentityCardImage(identityCardImage);
          setBackIdentityCard(backIdentityCard);
          setfullname(fullname);
          setCardNumber(cardNumber);
          setDateOfBirth(dateOfBirth);
          setGender(gender);
          setNationality(nationality);
          setPlaceOfOrigin(placeOfOrigin);
          setIsValid(isValid);
        }
      } catch (error) {
        console.error(error);
      }
    };

    fetchIdentityData();
  }, []);

  const [file, setFile] = useState(null);
  const [file1, setFile1] = useState(null);
  const [previewImage, setPreviewImage] = useState(null);
  const [previewImage1, setPreviewImage1] = useState(null);
  const handleFileIdChange = (event) => {
    const file = event.target.files[0];
    setFile(file);
    // Hiển thị ảnh trước khi gửi lên server
    const reader = new FileReader();
    reader.onloadend = () => {
      setPreviewImage(reader.result);
    };
    reader.readAsDataURL(file);
  };
  const handleFileIdChange1 = (event) => {
    const file1 = event.target.files[0];
    setFile1(file1);
    // Hiển thị ảnh trước khi gửi lên server
    const reader = new FileReader();
    reader.onloadend = () => {
      setPreviewImage1(reader.result);
    };
    reader.readAsDataURL(file1);
  };
  const [loading, setLoading] = useState(false);
  const handleRecognition = () => {
    if (loading) {
      return; // Không thực hiện hàm khi đang trong quá trình tải
    }
    setLoading(true);
    const formData = new FormData();
    formData.append("identityCardImage", file);
    formData.append("backIdentityCard", file1);
    const token = sessionStorage.getItem("token");
    const config = {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    };
    axios
      .post("https://localhost:7050/api/Account/recognize", formData, config)
      .then((response) => {
        cogoToast.success("Tải ảnh thông tin căn cước thành công", {
          position: "bottom-left",
        });
        setLoading(false);
        window.location.reload(); // Tải lại trang
      })
      .catch((error) => {
        cogoToast.error("Tải ảnh thông tin căn cước thất bại", {
          position: "bottom-left",
        });
        console.error(error);
        setLoading(false);
        window.location.reload(); // Tải lại trang
      });
  };
  return (
    <Fragment>
      <SEO
        titleTemplate="My Account"
        description="My Account page of flone react minimalist eCommerce template."
      />
      <LayoutOne headerTop="visible">
        {/* breadcrumb */}
        <Breadcrumb
          pages={[
            { label: "Home", path: process.env.PUBLIC_URL + "/" },
            { label: "My Account", path: process.env.PUBLIC_URL + pathname },
          ]}
        />

        <div className="myaccount-area pb-80 pt-100">
          <div className="container">
            <div className="row d-flex justify-content-center">
              <div className="col-lg-9 col-md-12 ms-auto me-auto">
                <div className="myaccount-wrapper">
                  <Accordion defaultActiveKey="0">
                    <Accordion.Item
                      eventKey="0"
                      className="single-my-account mb-20"
                    >
                      <Accordion.Header className="panel-heading mb-0">
                        <span>1 .</span> Chỉnh sửa thông tin cá nhân{" "}
                      </Accordion.Header>
                      {userData ? (
                        <Accordion.Body>
                          <div className="myaccount-info-wrapper">
                            <div className="account-info-wrapper row">
                              <h2>Thông tin tài khoản của bạn</h2>
                              <div className="col-lg-6 col-md-6">
                                <h5>Tài khoản: {userData.userName}</h5>
                                <h5>Email: {userData.email}</h5>
                                <h5>
                                  Số dư tài khoản:{" "}
                                  <span style={{ color: "red" }}>
                                    {userData.balance}
                                  </span>
                                </h5>
                                <h5>
                                  Ngày tạo tài khoản:{" "}
                                  <span style={{ color: "red" }}>
                                    {userData.createdDate}
                                  </span>
                                </h5>
                              </div>
                              <div className="col-lg-6 col-md-6">
                                <div className="row justify-content-center">
                                  <div className="col-sm-auto col-4">
                                    <div style={containerStyle}>
                                      {imageData ? ( // Kiểm tra imageData có dữ liệu không
                                        <img
                                          src={imageData}
                                          alt="Captured"
                                          style={imageStyle}
                                        />
                                      ) : selectedFile ? ( // Kiểm tra selectedFile có dữ liệu không
                                        <img
                                          src={URL.createObjectURL(
                                            selectedFile
                                          )}
                                          alt="Avatar"
                                          style={imageStyle}
                                        />
                                      ) : avatar ? ( // Kiểm tra avatar có dữ liệu không
                                        <img
                                          src={avatar}
                                          alt="Avatar"
                                          style={imageStyle}
                                        />
                                      ) : (
                                        <img
                                          src={avatar}
                                          alt="Avatar"
                                          style={imageStyle}
                                        />
                                      )}
                                    </div>
                                  </div>
                                  <div className="col-sm-auto col-8">
                                    <div className="input-group justify-content-center">
                                      <div className="custom-file  mt-2">
                                        <input
                                          type="file"
                                          className="custom-file-input"
                                          id="upload-avatar"
                                          onChange={handleFileChange}
                                        />
                                      </div>
                                      <div className="input-group-append justify-content-center m-lg-2 mt-3">
                                        <button
                                          className="btn btn-outline-success"
                                          type="button"
                                          onClick={handleUpload}
                                        >
                                          Tải lên ảnh
                                        </button>
                                      </div>
                                      <div className="input-group-append justify-content-center mt-2">
                                        {showCaptureButton && (
                                          <button
                                            className="btn btn-primary"
                                            type="button"
                                            onClick={handleCapture}
                                          >
                                            Chụp ảnh
                                          </button>
                                        )}
                                        {showSaveButton && (
                                          <button
                                            className="btn btn-primary"
                                            type="button"
                                            onClick={handleTakePhoto}
                                          >
                                            <i className="fa fa-camera"></i>
                                          </button>
                                        )}
                                        {showSaveButton && (
                                          <button
                                            className="btn btn-danger"
                                            type="button"
                                            onClick={handleCancelCapture}
                                          >
                                            X
                                          </button>
                                        )}
                                      </div>
                                      <div className="d-flex justify-content-center">
                                        <video
                                          ref={videoRef}
                                          style={{
                                            display: "block",
                                            width: "50%",
                                          }}
                                          className="justify-content-center"
                                          autoPlay
                                          muted
                                        />
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div className="row">
                              <div className="col-lg-6 col-md-6">
                                <div className="billing-info">
                                  <label>Họ và tên</label>
                                  <input
                                    type="text"
                                    value={fullName}
                                    onChange={(e) => {
                                      setFullName(e.target.value);
                                      setFormEdited(true);
                                    }}
                                  />
                                </div>
                                <span className="text-center text-danger font-weight-900 mt--8 m-lg-2">
                                  {fullNameError}
                                </span>
                              </div>
                              <div className="col-lg-6 col-md-6">
                                <div className="billing-info">
                                  <label>Số điện thoại</label>
                                  <input
                                    type="text"
                                    value={phone}
                                    onChange={(e) => {
                                      setPhone(e.target.value);
                                      setFormEdited(true);
                                    }}
                                  />
                                </div>
                                <span className="text-center text-danger font-weight-900 mt--8 m-lg-2">
                                  {phoneError}
                                </span>
                              </div>
                              {/* <div className="col-lg-12 col-md-12">
                                <div className="billing-info">
                                  <label>Email Address</label>
                                  <input type="email" value={userData.email} />
                                </div>
                              </div> */}
                              <div className="col-lg-6 col-md-6">
                                <div className="billing-info">
                                  <label>Ngày sinh</label>
                                  <input
                                    type="date"
                                    value={moment(dob, "DD/MM/YYYY").format(
                                      "YYYY-MM-DD"
                                    )}
                                    onChange={(e) => {
                                      setDob(
                                        moment(
                                          e.target.value,
                                          "YYYY-MM-DD"
                                        ).format("DD/MM/YYYY")
                                      );
                                      setFormEdited(true);
                                    }}
                                    timeformat="false"
                                  />
                                </div>
                              </div>
                              {/* <div className="col-lg-6 col-md-6">
                                <div className="billing-info">
                                  <label>Ngày tạo tài khoản</label>
                                  <input
                                    type="text"
                                    disabled
                                    value={userData.createdDate}
                                  />
                                </div>
                              </div> */}
                              <div className="col-lg-12 col-md-12">
                                <div className="billing-info">
                                  <label>Địa chỉ</label>
                                  <textarea
                                    type="text"
                                    value={address}
                                    onChange={(e) => {
                                      setAddress(e.target.value);
                                      setFormEdited(true);
                                    }}
                                  />
                                </div>
                              </div>
                              <div className="col-lg-12 col-md-12">
                                <div className="billing-info">
                                  <label>Thông tin tiểu sử</label>
                                  <textarea
                                    type="text"
                                    value={profile}
                                    onChange={(e) => {
                                      setProfile(e.target.value);
                                      setFormEdited(true);
                                    }}
                                  />
                                </div>
                              </div>
                            </div>
                            <div className="billing-back-btn">
                              <div className="billing-btn">
                                {isFormEdited && (
                                  <button
                                    type="submit"
                                    onClick={handleUpdateProfile}
                                  >
                                    Chỉnh sửa thông tin
                                  </button>
                                )}
                              </div>
                            </div>
                          </div>
                        </Accordion.Body>
                      ) : (
                        <p>Loading...</p>
                      )}
                    </Accordion.Item>

                    <Accordion.Item
                      eventKey="1"
                      className="single-my-account mb-20"
                    >
                      <Accordion.Header className="panel-heading">
                        <span>2 .</span> Đổi mật khẩu của bạn
                      </Accordion.Header>
                      <Accordion.Body>
                        <div className="myaccount-info-wrapper">
                          <div className="account-info-wrapper">
                            <h2>ĐỔI MẬT KHẨU</h2>
                          </div>
                          <div className="row">
                            <div className="col-lg-12 col-md-12">
                              <div className="billing-info">
                                <label>Mật khẩu hiện tại</label>
                                <div style={{ position: "relative" }}>
                                  <input
                                    type={
                                      showCurrentPassword ? "text" : "password"
                                    }
                                    value={currentPassword}
                                    onChange={(e) =>
                                      setCurrentPassword(e.target.value)
                                    }
                                    name="user-password"
                                    placeholder="Mật Khẩu"
                                    style={{ paddingRight: "2.5em" }}
                                  />
                                  <FontAwesomeIcon
                                    icon={
                                      showCurrentPassword ? faEyeSlash : faEye
                                    }
                                    onClick={handleCurrentPasswordToggle}
                                    style={{
                                      position: "absolute",
                                      right: "0.3em",
                                      top: "30%",
                                      transform: "translateY(-30%)",
                                      cursor: "pointer",
                                    }}
                                  />
                                </div>
                                <span className="text-center text-danger font-weight-900 mt--8 m-lg-2">
                                  {currentPasswordError}
                                </span>
                              </div>
                            </div>
                            <div className="col-lg-12 col-md-12">
                              <div className="billing-info">
                                <label>Mật khẩu mới</label>
                                <div style={{ position: "relative" }}>
                                  <input
                                    type={showNewPassword ? "text" : "password"}
                                    value={newPassword}
                                    onChange={(e) =>
                                      setNewPassword(e.target.value)
                                    }
                                    name="user-password"
                                    placeholder="Mật Khẩu"
                                    style={{ paddingRight: "2.5em" }}
                                  />
                                  <FontAwesomeIcon
                                    icon={showNewPassword ? faEyeSlash : faEye}
                                    onClick={handleNewPasswordToggle}
                                    style={{
                                      position: "absolute",
                                      right: "0.3em",
                                      top: "30%",
                                      transform: "translateY(-30%)",
                                      cursor: "pointer",
                                    }}
                                  />
                                </div>
                                <span className="text-center text-danger font-weight-900 mt--8 m-lg-2">
                                  {newPasswordError}
                                </span>
                              </div>
                            </div>
                            <div className="col-lg-12 col-md-12">
                              <div className="billing-info">
                                <label>Xác thực mật khẩu mới</label>
                                <div style={{ position: "relative" }}>
                                  <input
                                    type={
                                      showConfirmPassword ? "text" : "password"
                                    }
                                    value={confirmPassword}
                                    onChange={(e) =>
                                      setConfirmPassword(e.target.value)
                                    }
                                    name="user-password"
                                    placeholder="Mật Khẩu"
                                    style={{ paddingRight: "2.5em" }}
                                  />
                                  <FontAwesomeIcon
                                    icon={
                                      showConfirmPassword ? faEyeSlash : faEye
                                    }
                                    onClick={handleConfirmPasswordToggle}
                                    style={{
                                      position: "absolute",
                                      right: "0.3em",
                                      top: "30%",
                                      transform: "translateY(-30%)",
                                      cursor: "pointer",
                                    }}
                                  />
                                </div>
                                <span className="text-center text-danger font-weight-900 mt--8 m-lg-2">
                                  {confirmPasswordError}
                                </span>
                              </div>
                            </div>
                          </div>
                          <div className="billing-back-btn">
                            <div className="billing-btn">
                              <button
                                type="submit"
                                onClick={handleChangePassword}
                              >
                                Đổi mật khẩu
                              </button>
                            </div>
                          </div>
                        </div>
                      </Accordion.Body>
                    </Accordion.Item>
                    <Accordion.Item
                      eventKey="2"
                      className="single-my-account mb-20"
                    >
                      <Accordion.Header className="panel-heading">
                        <span>3 .</span> Xác thực thông tin
                      </Accordion.Header>
                      <Accordion.Body>
                        {!identityData ? (
                          <div className="myaccount-info-wrapper">
                            <div className="account-info-wrapper row">
                              <div className="col-lg-6 col-md-6">
                                <span>Căn cước mặt trước</span>
                                <input
                                  type="file"
                                  onChange={handleFileIdChange}
                                />
                                {/* </div>
                              <div className="col-lg-6 col-md-6"> */}
                                {previewImage && (
                                  <div>
                                    <img
                                      src={previewImage}
                                      alt="Ảnh đã chọn"
                                      style={{
                                        maxWidth: "300px",
                                        maxHeight: "300px",
                                      }}
                                    />
                                  </div>
                                )}
                              </div>
                              <div className="col-lg-6 col-md-6">
                                <span>Căn cước mặt sau</span>
                                <input
                                  type="file"
                                  onChange={handleFileIdChange1}
                                />
                                {previewImage1 && (
                                  <div>
                                    <img
                                      src={previewImage1}
                                      alt="Ảnh đã chọn"
                                      style={{
                                        maxWidth: "300px",
                                        maxHeight: "300px",
                                      }}
                                    />
                                  </div>
                                )}
                              </div>
                              <div
                                className="col-lg-6 col-md-6"
                                style={{
                                  display: "flex",
                                  justifyContent: "center",
                                  alignItems: "center", 
                                }}
                              >
                                <button
                                  onClick={handleRecognition}
                                  style={{
                                    backgroundColor: "#007bff",
                                    color: "#ffffff",
                                    border: "none",
                                    borderRadius: "5px",
                                    padding: "10px 20px",
                                    cursor: "pointer",
                                    boxShadow: "0px 2px 4px rgba(0, 0, 0, 0.1)",
                                    fontSize: "16px",
                                    fontWeight: "bold",
                                  }}
                                >
                                  {loading ? (
                                    <ClipLoader
                                      color={"#ffffff"}
                                      loading={true}
                                      size={24}
                                    />
                                  ) : (
                                    "Tải lên"
                                  )}
                                </button>
                              </div>
                            </div>
                          </div>
                        ) : (
                          <div className="myaccount-info-wrapper">
                            {isValid ? (
                              <div className="account-info-wrapper row">
                                <div className="col-lg-6 col-md-6">
                                  {identityCardImage && (
                                    <div>
                                      <img
                                        src={identityCardImage}
                                        alt="Căn cước mặt trước"
                                        style={{
                                          maxWidth: "350px",
                                          maxHeight: "350px",
                                        }}
                                      />
                                    </div>
                                  )}
                                </div>
                                <div className="col-lg-6 col-md-6">
                                  {backIdentityCard && (
                                    <div>
                                      <img
                                        src={backIdentityCard}
                                        alt="Căn cước mặt sau"
                                        style={{
                                          maxWidth: "350px",
                                          maxHeight: "350px",
                                        }}
                                      />
                                    </div>
                                  )}
                                </div>
                                <div
                                  style={{
                                    backgroundColor: "#e6e6fa",
                                    padding: "20px",
                                    border: "1px solid #ccc",
                                    borderRadius: "8px",
                                    display: "flex",
                                    justifyContent: "center",
                                    alignItems: "center",
                                    marginTop: "10px",
                                  }}
                                >
                                  <div
                                    style={{
                                      fontSize: "16px",
                                      color: "#333",
                                      textAlign: "center",
                                      display: "flex",
                                      flexDirection: "column",
                                      alignItems: "center", // Căn giữa nội dung theo chiều dọc
                                    }}
                                  >
                                    <h4
                                      style={{
                                        fontSize: "1.5rem",
                                        fontWeight: "bold",
                                        marginBottom: "0.5rem",
                                      }}
                                    >
                                      Họ và tên: {identityData.fullname}
                                    </h4>
                                    <h4>Số CCCD: {identityData.cardNumber}</h4>
                                    <h4>
                                      Ngày sinh: {identityData.dateOfBirth}
                                    </h4>
                                    <h4>
                                      Giới tính:{" "}
                                      <span
                                        style={{ textTransform: "capitalize" }}
                                      >
                                        {identityData.gender === "Male"
                                          ? "Nữ"
                                          : "Nam"}
                                      </span>
                                    </h4>
                                    <h4>
                                      Quốc tịch: {identityData.nationality}
                                    </h4>
                                    <h4>
                                      Quê quán: {identityData.placeOfOrigin}
                                    </h4>
                                  </div>
                                  <h1
                                    className="pl-7"
                                    style={{
                                      fontFamily: "Tahoma, Arial, sans-serif",
                                      fontWeight: "bold",
                                      fontSize: "24px",
                                      color: "green",
                                    }}
                                  >
                                    Xác nhận thông tin thành công!
                                  </h1>
                                </div>
                              </div>
                            ) : (
                              <div className="account-info-wrapper row">
                                <div className="col-lg-6 col-md-6">
                                  {identityCardImage && (
                                    <div>
                                      <img
                                        src={identityCardImage}
                                        alt="Căn cước mặt trước"
                                        style={{
                                          maxWidth: "350px",
                                          maxHeight: "350px",
                                        }}
                                      />
                                    </div>
                                  )}
                                </div>
                                <div className="col-lg-6 col-md-6">
                                  {backIdentityCard && (
                                    <div>
                                      <img
                                        src={backIdentityCard}
                                        alt="Căn cước mặt sau"
                                        style={{
                                          maxWidth: "350px",
                                          maxHeight: "350px",
                                        }}
                                      />
                                    </div>
                                  )}
                                </div>
                                <div
                                  style={{
                                    backgroundColor: "#e6e6fa",
                                    padding: "20px",
                                    border: "1px solid #ccc",
                                    borderRadius: "8px",
                                    display: "flex",
                                    justifyContent: "center",
                                    alignItems: "center",
                                    marginTop: "10px",
                                  }}
                                >
                                  <div
                                    style={{
                                      fontSize: "16px",
                                      color: "#333",
                                      textAlign: "center",
                                      display: "flex",
                                      flexDirection: "column",
                                      alignItems: "center", // Căn giữa nội dung theo chiều dọc
                                    }}
                                  >
                                    <h4
                                      style={{
                                        fontSize: "1.5rem",
                                        fontWeight: "bold",
                                        marginBottom: "0.5rem",
                                      }}
                                    >
                                      Họ và tên: {identityData.fullname}
                                    </h4>
                                    <h4>Số CCCD: {identityData.cardNumber}</h4>
                                    <h4>
                                      Ngày sinh: {identityData.dateOfBirth}
                                    </h4>
                                    <h4>
                                      Giới tính:{" "}
                                      <span
                                        style={{ textTransform: "capitalize" }}
                                      >
                                        {identityData.gender === "Male"
                                          ? "Nữ"
                                          : "Nam"}
                                      </span>
                                    </h4>
                                    <h4>
                                      Quốc tịch: {identityData.nationality}
                                    </h4>
                                    <h4>
                                      Quê quán: {identityData.placeOfOrigin}
                                    </h4>
                                  </div>
                                  <h1
                                    className="pl-7"
                                    style={{
                                      fontFamily: "Tahoma, Arial, sans-serif",
                                      fontWeight: "bold",
                                      fontSize: "24px",
                                      color: "#f5e042",
                                    }}
                                  >
                                    Chờ xác thực thông tin!
                                  </h1>
                                </div>
                              </div>
                            )}
                          </div>
                        )}
                      </Accordion.Body>
                    </Accordion.Item>
                  </Accordion>
                </div>
              </div>
            </div>
          </div>
        </div>
      </LayoutOne>
    </Fragment>
  );
};

export default MyAccount;

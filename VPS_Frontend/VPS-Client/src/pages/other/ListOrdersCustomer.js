import React, { useEffect, useRef, useState } from "react";
// javascript plugin that creates a sortable object from a dom object
import List from "list.js";
// reactstrap components
import {
  Badge,
  CardHeader,
  CardFooter,
  Pagination,
  PaginationItem,
  PaginationLink,
  Table,
  Button,
} from "reactstrap";
// core components
import axios from "axios";
import SEO from "../../components/seo.jsx";
import LayoutOne from "../../layouts/LayoutOne.js";
import {
  Link,
  useLocation,
} from "react-router-dom/cjs/react-router-dom.min.js";
import Breadcrumb from "../../wrappers/breadcrumb/Breadcrumb";

const ListOrdersCustomer = () => {
  const [currentPage, setCurrentPage] = useState(1);
  const [sortedColumn, setSortedColumn] = useState("");
  const [sortedDirection, setSortedDirection] = useState("");
  const itemsPerPage = 5;

  const thirdListRef = useRef(null);
  const [data, setData] = useState([]);

  const totalPages = Math.ceil(data.length / itemsPerPage);

  const startIndex = (currentPage - 1) * itemsPerPage;
  const endIndex = startIndex + itemsPerPage;
  const displayedData = data.slice(startIndex, endIndex);
  let { pathname } = useLocation();

  const fetchData = async () => {
    try {
      const token = sessionStorage.getItem("token");
      const config = {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      };
      const response = await axios.get(
        "https://localhost:7050/api/Order/listbycusidd",
        config
      );
      const order = response.data.sort((a, b) => b.orderId - a.orderId);
      setData(order);
    } catch (error) {
      console.error("Error fetching data:", error);
    }
  };
  useEffect(() => {
    if (data.length > 0) {
      initializeList();
    }
    const totalPages = Math.ceil(data.length / itemsPerPage);
    if (currentPage > totalPages) {
      setCurrentPage(1);
    }
  }, [data, currentPage]);

  const initializeList = () => {
    new List(thirdListRef.current, {
      valueNames: ["date", "status", "customer", "address", "price"],
      listClass: "list",
    });
  };
  useEffect(() => {
    if (data.length > 0) {
      initializeList();
    }
  }, [data]);
  useEffect(() => {
    fetchData();
  }, []);

  const handleSort = (column) => {
    let direction = "asc";
    if (column === sortedColumn && sortedDirection === "asc") {
      direction = "desc";
    }
    setSortedColumn(column);
    setSortedDirection(direction);
    sortData(column, direction);
    setCurrentPage(1); // Reset current page when sorting
  };

  const sortData = (column, direction) => {
    const sortedData = [...data];
    sortedData.sort((a, b) => {
      const valueA = getValueByColumn(a, column);
      const valueB = getValueByColumn(b, column);
      if (valueA < valueB) {
        return direction === "asc" ? -1 : 1;
      }
      if (valueA > valueB) {
        return direction === "asc" ? 1 : -1;
      }
      return 0;
    });
    setData(sortedData);
  };

  const getValueByColumn = (item, column) => {
    switch (column) {
      case "date":
        return item.orderDate;
      case "status":
        return item.status;
      case "customer":
        return item.customer.fullName;
      //   case "address":
      //     return item.customer.address;
      case "price":
        return item.price;
      default:
        return "";
    }
  };
  const calculateTimeDifferenceInHours = (startDate, endDate) => {
    const diffInMilliseconds = new Date(endDate) - new Date(startDate);
    const diffInHours = diffInMilliseconds / (1000 * 60 * 60);
    return diffInHours;
  };

  const isNewlyCreatedOrder = (orderDate) => {
    const hoursThreshold = 1; // You can adjust this threshold as needed (1 hour in this case)
    const timeDifferenceInHours = calculateTimeDifferenceInHours(
      orderDate,
      new Date()
    );

    return timeDifferenceInHours <= hoursThreshold;
  };

  const VND = new Intl.NumberFormat("vi-VN", {
    style: "currency",
    currency: "VND",
  });

  return (
    <>
      <SEO
        titleTemplate="Order"
        description="Order page of flone react minimalist eCommerce template."
      />

      <LayoutOne headerTop="visible">
        <Breadcrumb
          pages={[
            { label: "Home", path: process.env.PUBLIC_URL + "/" },
            { label: "Order", path: process.env.PUBLIC_URL + pathname },
          ]}
        />
        <div className="cart-main-area pt--140 pb-50 pl-50 pr-50" fluid>
          <CardHeader className="bg-transparent border-0">
            <h3 className="text-dark mb-0">Danh sách đặt hàng</h3>
          </CardHeader>
          <div className="table-responsive" ref={thirdListRef}>
            <Table id="myTable" className="align-items-center table-flush">
              <thead className="thead-light">
                <tr>
                  <th>Id</th>
                  <th
                    className="sort"
                    data-sort="date"
                    scope="col"
                    onClick={() => handleSort("date")}
                  >
                    Ngày đặt hàng
                  </th>
                  <th
                    className="sort"
                    data-sort="status"
                    scope="col"
                    onClick={() => handleSort("status")}
                  >
                    Tình trạng
                  </th>
                  {/* <th
                    className="sort"
                    data-sort="address"
                    scope="col"
                    onClick={() => handleSort("address")}
                  >
                    Địa chỉ
                  </th> */}
                  <th
                    className="sort"
                    data-sort="price"
                    scope="col"
                    onClick={() => handleSort("price")}
                  >
                    Giá
                  </th>

                  <th scope="col" />
                </tr>
              </thead>
              <tbody className="list">
                {displayedData.map((order) => (
                  <tr key={order.orderId}>
                    <td className="id">#{order.orderId}</td>
                    <td className="date">
                      {new Date(order.orderDate).toLocaleDateString("en-US", {
                        month: "2-digit",
                        day: "2-digit",
                        year: "numeric",
                      })}
                    </td>
                    <td>
                      {isNewlyCreatedOrder(order.orderDate) &&
                      order.managerViewed === null ? (
                        <Badge color="" className="badge-dot mr-4">
                          <i className="bg-gray" />
                          <span className="status">Vừa được tạo</span>
                        </Badge>
                      ) : (
                        <>
                          {order.managerViewed !== null &&
                          order.managerCancelled === null &&
                          order.managerConfirm === null &&
                          order.SaleConfirm === null &&
                          order.shipConfirm === null ? (
                            <Badge color="" className="badge-dot mr-4">
                              <i className="bg-gray" />
                              <span className="status">Đã xem</span>
                            </Badge>
                          ) : (
                            <Badge color="" className="badge-dot mr-4">
                              <i
                                className={
                                  order.statusId === 1
                                    ? "bg-warning"
                                    : order.statusId === 2
                                    ? "bg-yellow "
                                    : order.statusId === 3
                                    ? "bg-yellow"
                                    : order.statusId === 4
                                    ? "bg-blue"
                                    : order.statusId === 5
                                    ? "bg-success"
                                    : order.statusId === 7
                                    ? "bg-danger"
                                    : "bg-success"
                                }
                              />
                              <span className="status">
                                {order.statusId === 1
                                  ? "Chờ manager xác nhận"
                                  : order.statusId === 2
                                  ? "Chờ saler xác nhận"
                                  : order.statusId === 3
                                  ? "Chờ shipper xác nhận"
                                  : order.statusId === 4
                                  ? "Chờ shipper giao hàng"
                                  : order.statusId === 5
                                  ? "Shipper giao hàng thành công"
                                  : order.statusId === 7
                                  ? "Đã hủy"
                                  : "Khách hàng xác nhận đã nhận được hàng"}
                              </span>
                            </Badge>
                          )}
                        </>
                      )}
                    </td>
                    {/* <td className="product">{order.customer.address}</td> */}
                    <td className="price">{VND.format(order.price)}</td>

                    <td className="text-right">
                      <Link
                        to={
                          process.env.PUBLIC_URL +
                          "/view-order-detail/" +
                          order.orderId
                        }
                      >
                        <Button color="default" size="sm">
                          Xem chi tiết
                        </Button>
                      </Link>
                    </td>
                  </tr>
                ))}
              </tbody>
            </Table>
          </div>
          <CardFooter className="py-4">
            <nav aria-label="...">
              <Pagination className="pagination justify-content-end mb-0">
                <PaginationItem disabled={currentPage === 1}>
                  <PaginationLink
                    previous
                    onClick={() => setCurrentPage(currentPage - 1)}
                  />
                </PaginationItem>
                {Array.from({ length: totalPages }, (_, i) => i + 1).map(
                  (page) => (
                    <PaginationItem key={page} active={currentPage === page}>
                      <PaginationLink onClick={() => setCurrentPage(page)}>
                        {page}
                      </PaginationLink>
                    </PaginationItem>
                  )
                )}
                <PaginationItem disabled={currentPage === totalPages}>
                  <PaginationLink
                    next
                    onClick={() => setCurrentPage(currentPage + 1)}
                  />
                </PaginationItem>
              </Pagination>
            </nav>
          </CardFooter>
        </div>
      </LayoutOne>
    </>
  );
};

export default ListOrdersCustomer;

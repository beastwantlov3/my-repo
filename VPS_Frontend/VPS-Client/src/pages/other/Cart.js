import { Fragment, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Link, useLocation } from "react-router-dom";
import SEO from "../../components/seo";
import { getDiscountPrice } from "../../helpers/product";
import LayoutOne from "../../layouts/LayoutOne";
import Breadcrumb from "../../wrappers/breadcrumb/Breadcrumb";
import {
  addToCart,
  decreaseQuantity,
  deleteFromCart,
  deleteAllFromCart,
  setTotalPrice,
  updateSelectedDurations,
} from "../../store/slices/cart-slice";
import { cartItemStock } from "../../helpers/product";
import { useEffect } from "react";
import { useHistory } from "react-router-dom/cjs/react-router-dom.min";
import AmountFormatted from "../../../src/components/price/numeral";

const Cart = () => {
  let cartTotalPrice = 0;
  const [selectedDurations, setSelectedDurations] = useState({});
  const [quantityCount] = useState(1);
  const dispatch = useDispatch();
  let { pathname } = useLocation();

  const currency = useSelector((state) => state.currency);
  const { cartItems } = useSelector((state) => state.cart);

  const durationMultipliers = {
    1: 1,
    3: 3,
    7: 7,
  };

  const history = useHistory();

  const handleDurationChange = (cartItemId, newDuration) => {
    setSelectedDurations((prevDurations) => ({
      ...prevDurations,
      [cartItemId]: newDuration,
    }));

    // Lưu thông tin vào session storage
    sessionStorage.setItem(cartItemId, newDuration);
    dispatch(updateSelectedDurations({ cartItemId, duration: newDuration }));
  };

  useEffect(() => {
    if (cartItems && cartItems.length > 0) {
      const initialSelectedDurations = {};
      cartItems.forEach((cartItem) => {
        const sessionDuration = sessionStorage.getItem(cartItem.cartItemId);
        initialSelectedDurations[cartItem.cartItemId] = sessionDuration || "1";
      });
      setSelectedDurations(initialSelectedDurations);
    }
  }, [cartItems]);

  const grandTotal = cartItems.reduce((total, cartItem) => {
    const selectedDurationForItem = selectedDurations[cartItem.cartItemId] || 1;

    const discountedPrice = getDiscountPrice(
      cartItem.unitPrice,
      cartItem.discount
    );

    const finalPrice =
      discountedPrice !== null
        ? discountedPrice * currency.currencyRate
        : cartItem.unitPrice * currency.currencyRate;

    // Apply discount based on selected duration
    const durationMultiplier = durationMultipliers[selectedDurationForItem];
    let productSubtotal = finalPrice * cartItem.quantity * durationMultiplier * cartItem.selectedQuality/100;

    // Apply additional discount for specific durations
    if (durationMultipliers[selectedDurationForItem] === 3) {
      productSubtotal *= 0.95;
    } else if (durationMultipliers[selectedDurationForItem] === 7) {
      productSubtotal *= 0.9;
    }

    return total + parseFloat(productSubtotal);
  }, 0);

  const handleCheckout = () => {
    const calculatedTotalPrice = grandTotal;
    dispatch(setTotalPrice(calculatedTotalPrice));
    const selectedDurationsArray = Object.values(selectedDurations);
    history.push({
      pathname: "/checkout",
      state: { selectedDurations: selectedDurationsArray },
    });
  };
  return (
    <Fragment>
      <SEO
        titleTemplate="Cart"
        description="Cart page of flone react minimalist eCommerce template."
      />

      <LayoutOne headerTop="visible">
        {/* breadcrumb */}
        <Breadcrumb
          pages={[
            { label: "Home", path: process.env.PUBLIC_URL + "/" },
            { label: "Cart", path: process.env.PUBLIC_URL + pathname },
          ]}
        />
        <div className="cart-main-area pt-90 pb-100">
          <div className="container">
            {cartItems && cartItems.length >= 1 ? (
              <Fragment>
                <h3 className="cart-page-title">Giỏ hàng của bạn</h3>
                <div className="row">
                  <div className="col-14">
                    <div className="table-content table-responsive cart-table-content">
                      <table>
                        <thead>
                          <tr>
                            <th>HÌNH ẢNH</th>
                            <th>TÊN SẢN PHẨM</th>
                            <th>ĐƠN GIÁ</th>
                            <th>CHẤT LƯỢNG</th>
                            <th>SỐ LƯỢNG</th>
                            <th>NGÀY THUÊ</th>
                            <th>TỔNG GIÁ</th>
                            <th>XÓA ĐƠN HÀNG</th>
                          </tr>
                        </thead>
                        <tbody>
                          {cartItems.map((cartItem, key) => {
                            const selectedDurationForItem =
                              selectedDurations[cartItem.cartItemId] || 1;
                            const discountedPrice = getDiscountPrice(
                              cartItem.unitPrice,
                              cartItem.discount
                            );
                            const finalProductPrice =
                              cartItem.unitPrice * currency.currencyRate;
                            const finalDiscountedPrice =
                              discountedPrice * currency.currencyRate;

                            discountedPrice != null
                              ? (cartTotalPrice +=
                                  finalDiscountedPrice * cartItem.quantity)
                              : (cartTotalPrice +=
                                  finalProductPrice * cartItem.quantity);

                            let productSubtotal =
                              discountedPrice !== null
                                ? finalDiscountedPrice *
                                  cartItem.quantity *
                                  durationMultipliers[selectedDurationForItem] * (cartItem.selectedQuality/100)
                                : finalProductPrice *
                                  cartItem.quantity *
                                  durationMultipliers[selectedDurationForItem] * (cartItem.selectedQuality/100)

                            // Apply additional discount for specific durations
                            if (
                              durationMultipliers[selectedDurationForItem] === 3
                            ) {
                              productSubtotal *= 0.95;
                            } else if (
                              durationMultipliers[selectedDurationForItem] === 7
                            ) {
                              productSubtotal *= 0.9;
                            }

                            return (
                              <tr key={key}>
                                <td className="product-thumbnail">
                                  <Link
                                    to={
                                      process.env.PUBLIC_URL +
                                      "/product/" +
                                      cartItem.productId
                                    }
                                  >
                                    <img
                                      className="img-fluid"
                                      src={
                                        process.env.PUBLIC_URL + cartItem.image
                                      }
                                      alt=""
                                    />
                                  </Link>
                                </td>

                                <td className="product-name">
                                  <Link
                                    to={
                                      process.env.PUBLIC_URL +
                                      "/product/" +
                                      cartItem.productId
                                    }
                                  >
                                    {cartItem.productName}
                                  </Link>
                                </td>

                                <td className="product-price-cart">
                                  {discountedPrice !== null ? (
                                    <Fragment>
                                      <span className="amount old">
                                        <AmountFormatted
                                          amount={
                                            finalProductPrice +
                                            " " +
                                            currency.currencySymbol
                                          }
                                        />
                                      </span>
                                      <span className="amount">
                                        <AmountFormatted
                                          amount={
                                            finalDiscountedPrice +
                                            " " +
                                            currency.currencySymbol
                                          }
                                        />
                                      </span>
                                    </Fragment>
                                  ) : (
                                    <span className="amount">
                                      <AmountFormatted
                                        amount={
                                          finalProductPrice +
                                          " " +
                                          currency.currencySymbol
                                        }
                                      />
                                    </span>
                                  )}
                                </td>
                                <td className="product-quality">
                                  {cartItem.selectedQuality}%{" "}
                                </td>

                                <td className="product-quantity">
                                  <div className="cart-plus-minus">
                                    <button
                                      className="dec qtybutton"
                                      onClick={() =>
                                        dispatch(decreaseQuantity(cartItem))
                                      }
                                    >
                                      -
                                    </button>
                                    <input
                                      className="cart-plus-minus-box"
                                      type="text"
                                      value={cartItem.quantity}
                                      readOnly
                                    />
                                    <button
                                      className="inc qtybutton"
                                      onClick={() =>
                                        dispatch(
                                          addToCart({
                                            ...cartItem,
                                            quantity: quantityCount,
                                          })
                                        )
                                      }
                                      disabled={
                                        cartItem !== undefined &&
                                        cartItem.quantity &&
                                        cartItem.quantity >=
                                          cartItemStock(cartItem)
                                      }
                                    >
                                      +
                                    </button>
                                  </div>
                                </td>
                                <td className="product-days">
                                  <div className="duration-selection">
                                    {Object.keys(durationMultipliers).map(
                                      (duration) => (
                                        <div
                                          className="custom-control custom-radio mb-3"
                                          key={duration}
                                        >
                                          <input
                                            className="custom-control-input"
                                            id={`customRadio${cartItem.cartItemId}-${duration}`}
                                            name={`custom-radio-${cartItem.cartItemId}`}
                                            type="radio"
                                            value={duration}
                                            checked={
                                              selectedDurationForItem ===
                                              duration
                                            }
                                            onChange={() =>
                                              handleDurationChange(
                                                cartItem.cartItemId,
                                                duration
                                              )
                                            }
                                          />

                                          <label
                                            className="custom-control-label"
                                            htmlFor={`customRadio${cartItem.cartItemId}-${duration}`}
                                          >
                                            {duration === "1"
                                              ? "1 ngày"
                                              : duration === "3"
                                              ? "3 ngày"
                                              : "1 tuần"}
                                          </label>
                                        </div>
                                      )
                                    )}
                                  </div>
                                </td>
                                <td className="product-subtotal">
                                  <AmountFormatted
                                    amount={
                                      productSubtotal +
                                      " " +
                                      currency.currencySymbol
                                    }
                                  />
                                </td>

                                <td className="product-remove">
                                  <button
                                    onClick={() =>
                                      dispatch(
                                        deleteFromCart(cartItem.cartItemId)
                                      )
                                    }
                                  >
                                    <i className="fa fa-times"></i>
                                  </button>
                                </td>
                              </tr>
                            );
                          })}
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
                <div className="row">
                  <div className="col-lg-12">
                    <div className="cart-shiping-update-wrapper">
                      <div className="cart-shiping-update">
                        <Link
                          to={process.env.PUBLIC_URL + "/shop-grid-standard/0"}
                        >
                          TIẾP TỤC XEM
                        </Link>
                      </div>
                      <div className="cart-clear">
                        <button onClick={() => dispatch(deleteAllFromCart())}>
                          XÓA GIỎ HÀNG
                        </button>
                      </div>
                    </div>
                  </div>
                </div>

                <div className="row">
                  <div className="col-lg-12 col-md-12">
                    <div className="grand-totall">
                      <div className="title-wrap">
                        <h4 className="cart-bottom-title section-bg-gary-cart">
                          Tổng giỏ hàng
                        </h4>
                      </div>
                      <h5>
                        Tổng sản phẩm{" "}
                        <span>
                          <AmountFormatted
                            amount={grandTotal + " " + currency.currencySymbol}
                          />
                        </span>
                      </h5>

                      <h4 className="grand-totall-title">
                        Tổng cộng{" "}
                        <span>
                          <AmountFormatted
                            amount={grandTotal + " " + currency.currencySymbol}
                          />
                        </span>
                      </h4>

                      <Link onClick={handleCheckout}>Tiến hành thuê</Link>
                    </div>
                  </div>
                </div>
              </Fragment>
            ) : (
              <div className="row">
                <div className="col-lg-12">
                  <div className="item-empty-area text-center">
                    <div className="item-empty-area__icon mb-30">
                      <i className="pe-7s-cart"></i>
                    </div>
                    <div className="item-empty-area__text">
                      Không tìm thấy sản phẩm nào <br />{" "}
                      <Link
                        to={process.env.PUBLIC_URL + "/shop-grid-standard/0"}
                      >
                        Mua ngay
                      </Link>
                    </div>
                  </div>
                </div>
              </div>
            )}
          </div>
        </div>
      </LayoutOne>
    </Fragment>
  );
};

export default Cart;

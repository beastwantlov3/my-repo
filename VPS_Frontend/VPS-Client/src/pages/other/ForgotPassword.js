import React, { Fragment, useState } from "react";
import {  useLocation } from "react-router-dom";
import Tab from "react-bootstrap/Tab";
import Nav from "react-bootstrap/Nav";
import SEO from "../../components/seo";
import LayoutOne from "../../layouts/LayoutOne";
import Breadcrumb from "../../wrappers/breadcrumb/Breadcrumb";
import axios from "axios";
import { useHistory } from "react-router-dom";
import cogoToast from 'cogo-toast';

const ForgotPassword = () => {
  let { pathname } = useLocation();
  const [email, setEmail] = useState("");
  const history = useHistory();
  const [emailError, setEmailError] = useState("");

  const handleForgot = async (e) => {
    e.preventDefault();
    try {
      setEmailError("");

      if (!email) {
        setEmailError("Vui lòng nhập email.");
      } else if (
        !/^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/.test(email)
      ) {
        setEmailError(
          "Email không hợp lệ. Vui lòng nhập địa chỉ email hợp lệ, ví dụ: example@gmail.com"
        );
      }
      // Gửi yêu cầu xác thực mã nhập vào đến API
      const params = new URLSearchParams();
      params.append("email", email);
      const url = `https://localhost:7050/api/Auth/forgot-password?${params.toString()}`;
      const response = await axios.post(url);
      // console.log(response.data);
      // Xử lý phản hồi từ API và hiển thị thông báo tương ứng
      // Xác thực thành công
      // Hiển thị thông báo thành công
      cogoToast.success("Đã gửi mật khẩu tới email của bạn", {position: "bottom-left"});
      history.push("/login-register");
    } catch (error) {
      if (error.response && error.response.data) {
        const errorMessage = error.response.data.error;
        if (errorMessage === "Email không tồn tại. Vui lòng nhập lại email!") {
          setEmailError(errorMessage);
        } else {
          console.error("Unknown error:", errorMessage);
        }
      } else {
        console.error("Registration failed:", error);
      }
    }
      // cogoToast.error("Gửi mật khẩu tới email thất bại", {position: "bottom-left"});
  };
  return (
    <Fragment>
      <SEO titleTemplate="Quên Mật Khẩu" description="Quên Mật Khẩu" />
      <LayoutOne headerTop="visible">
        {/* breadcrumb */}
        <Breadcrumb
          pages={[
            { label: "Home", path: process.env.PUBLIC_URL + "/" },
            {
              label: "Login Register",
              path: process.env.PUBLIC_URL + pathname,
            },
          ]}
        />
        <div className="login-register-area pt-100 pb-100">
          <div className="container">
          <div className="row d-flex justify-content-center" >
              <div className="col-lg-9 col-md-12 ms-auto me-auto">
                <div className="login-register-wrapper">
                  <Tab.Container defaultActiveKey="login">
                    <Nav variant="pills" className="login-register-tab-list">
                      <Nav.Item>
                        <Nav.Link eventKey="forgot">
                          <h4>Quên Mật Khẩu</h4>
                        </Nav.Link>
                      </Nav.Item>
                    </Nav>
                    <Tab.Content>
                      <Tab.Pane eventKey="forgot">
                        <div className="login-form-container">
                          <div className="login-register-form">
                            <form>
                              <input
                                name="user-email"
                                value={email}
                                onChange={(e) => setEmail(e.target.value)}
                                placeholder="Email"
                                type="email"
                              />
                              <span className="text-center text-danger font-weight-900 mt--8 m-lg-2">
                                  {emailError}
                                </span>
                              <div className="button-box">
                                <button type="submit" onClick={handleForgot}>
                                  <span>Quên mật khẩu</span>
                                </button>
                              </div>
                            </form>
                          </div>
                        </div>
                      </Tab.Pane>
                    </Tab.Content>
                  </Tab.Container>
                </div>
              </div>
            </div>
          </div>
        </div>
      </LayoutOne>
    </Fragment>
  );
};

export default ForgotPassword;

import { useEffect, Fragment, useState, useRef } from "react";
import { Link, useLocation, useHistory } from "react-router-dom";

import { useSelector } from "react-redux";
import { getDiscountPrice } from "../../helpers/product";
import SEO from "../../components/seo";
import LayoutOne from "../../layouts/LayoutOne";
import Breadcrumb from "../../wrappers/breadcrumb/Breadcrumb";
import axios from "axios";
import NotificationAlert from "react-notification-alert";
import { useDispatch } from "react-redux";
import { deleteAllFromCart } from "../../store/slices/cart-slice";
import cogoToast from "cogo-toast";
import {
  Form,
  Row,
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  FormGroup,
} from "reactstrap";
import AmountFormatted from "../../../src/components/price/numeral";

const Checkout = () => {
  const dispatch = useDispatch();
  let { pathname } = useLocation();
  const totalPrice = useSelector((state) => state.cart.totalPrice);

  const currency = useSelector((state) => state.currency);
  const { cartItems } = useSelector((state) => state.cart);
  const [fullName, setFullName] = useState("");
  const [phone, setPhone] = useState("");
  const [customerId, setCustomerId] = useState("");
  const [address, setAddress] = useState("");
  const shippingPercentage = 0.03;
  const [orderPlaced, setOrderPlaced] = useState(false);
  const notificationAlertRef = useRef(null);
  const [showModal, setShowModal] = useState(false);
  const [showConfirmationModal, setShowConfirmationModal] = useState(false);

  const history = useHistory();
  const toggleModal = () => {
    setShowModal(!showModal);
  };
  let cartTotalPrice = 0;

  const [remainingTime, setRemainingTime] = useState(15 * 60); // 15 minutes in seconds
  const videoRef = useRef(null);
  const [imageData, setImageData] = useState("");
  //state button camera, chụp
  const [showCaptureButton, setShowCaptureButton] = useState(true);
  const [showSaveButton, setShowSaveButton] = useState(false);
  //mở camera
  const handleCapture = async () => {
    const constraints = { video: true };

    try {
      const stream = await navigator.mediaDevices.getUserMedia(constraints);
      videoRef.current.srcObject = stream;
      setShowCaptureButton(false);
      setShowSaveButton(true);
      const countdownInterval = setInterval(() => {
        setRemainingTime((prevTime) => prevTime - 1);
        if (remainingTime <= 1) {
          clearInterval(countdownInterval);
          setShowModal(false); // Close the modal when the timer reaches 0
        }
      }, 1000);
    } catch (err) {
      console.error("Error accessing camera:", err);
    }
  };

  const resetCountdownTimer = () => {
    setRemainingTime(15 * 60); // Reset countdown timer to 15 minutes
  };

  //Chụp ảnh
  const handleTakePhoto = () => {
    const canvas = document.createElement("canvas");
    const video = videoRef.current;
    canvas.width = video.videoWidth;
    canvas.height = video.videoHeight;
    const ctx = canvas.getContext("2d");
    ctx.drawImage(video, 0, 0, canvas.width, canvas.height);

    // Convert the canvas image to base64 data URL in JPEG format
    const dataURL = canvas.toDataURL("image/jpeg"); // Thay đổi thành 'image/jpeg'
    setImageData(dataURL);
    handleCancelCapture();

    setTimeout(() => {
      toggleModal();
    }, 15 * 60 * 1000); // 15 minutes in milliseconds

    // Start countdown timer
    const countdownInterval = setInterval(() => {
      setRemainingTime((prevTime) => prevTime - 1);
    }, 1000); // Update every second

    // Clear countdown interval when the component unmounts
    return () => clearInterval(countdownInterval);
  };

  //tắt camera
  const handleCancelCapture = () => {
    // Thực hiện các xử lý khi người dùng bấm nút tắt camera
    // Cập nhật giá trị của các biến trạng thái để ẩn button "Lưu ảnh" và hiển thị lại button "Chụp ảnh"
    setShowCaptureButton(true);
    setShowSaveButton(false);
    // Tắt video bằng cách dừng stream và hủy kết nối video
    const videoElement = videoRef.current;
    if (videoElement && videoElement.srcObject) {
      const stream = videoElement.srcObject;
      const tracks = stream.getTracks();
      tracks.forEach((track) => track.stop());
      videoElement.srcObject = null;
    }
  };

  const dataURItoBlob = (dataURI) => {
    const byteString = atob(dataURI.split(",")[1]);
    const mimeString = dataURI.split(",")[0].split(":")[1].split(";")[0];
    const ab = new ArrayBuffer(byteString.length);
    const ia = new Uint8Array(ab);
    for (let i = 0; i < byteString.length; i++) {
      ia[i] = byteString.charCodeAt(i);
    }
    return new Blob([ab], { type: mimeString });
  };

  const getSelectedDurationFromSessionStorage = (cartItemId) => {
    const sessionDuration = sessionStorage.getItem(cartItemId);
    return sessionDuration || "1"; // Mặc định là 1 ngày
  };

  //Lấy thông tin data UserProfile

  const notifySuccess = async () => {
    let options = {
      place: "tc",
      message: (
        <div className="alert-text">
          <span className="alert-title" data-notify="title">
            {" "}
            Đặt hàng thành công
          </span>
        </div>
      ),
      type: "success",
      icon: "ni ni-bell-55",
      autoDismiss: 7,
    };
    // await addAlert(); // Wait for the addAlert function to complete
    notificationAlertRef.current.notificationAlert(options);
  };

  const notifyDanger = () => {
    let options = {
      place: "tc",
      message: (
        <div className="alert-text">
          <span className="alert-title" data-notify="title">
            {" "}
            Đặt hàng thất bại
          </span>
          <span data-notify="message">
            Vui lòng kiểm tra các thông tin đã điền
          </span>
        </div>
      ),
      type: "danger",
      icon: "ni ni-bell-55",
      autoDismiss: 7,
    };
    notificationAlertRef.current.notificationAlert(options);
  };

  useEffect(() => {
    const fetchUserData = async () => {
      try {
        const token = sessionStorage.getItem("token");
        const config = {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        };
        const response = await axios.get(
          "https://localhost:7050/api/Account/get-user",
          config
        );
        // Populate the form fields if user data is available
        if (response.data) {
          const { fullName, phone, address, customerId } = response.data;
          setFullName(fullName);
          setPhone(phone);
          setAddress(address);
          setCustomerId(customerId);
        }
      } catch (error) {
        console.error(error);
      }
    };

    fetchUserData();
  }, []);

  const handlePlaceOrder = async () => {
    try {
      const currentDate = new Date();
      const year = currentDate.getFullYear();
      const month = String(currentDate.getMonth() + 1).padStart(2, "0");
      const day = String(currentDate.getDate()).padStart(2, "0");
      const hours = String(currentDate.getHours()).padStart(2, "0");
      const minutes = String(currentDate.getMinutes()).padStart(2, "0");
      const seconds = String(currentDate.getSeconds()).padStart(2, "0");
      const milliseconds = String(currentDate.getMilliseconds()).padStart(
        3,
        "0"
      );
      const formattedDate = `${year}-${month}-${day}T${hours}:${minutes}:${seconds}.${milliseconds}Z`;

      const orderData = {
        customerId: customerId, // Replace with the actual customerId
        orderDate: formattedDate,
        price: totalPrice + totalPrice * shippingPercentage,
        shipCost: totalPrice * shippingPercentage,
        statusId: 1,
        currentImage: imageData,
        cartItems: [], // Initialize an empty array to hold OrderDetail data
      };
      for (const cartItem of cartItems) {
        const duration = parseFloat(
          getSelectedDurationFromSessionStorage(cartItem.cartItemId)
        );

        let threeDays = false;
        let oneWeeks = false;

        if (duration === 3) {
          threeDays = true;
        }
        if (duration === 7) {
          oneWeeks = true;
        }

        const orderDetailData = {
          productId: cartItem.productId,
          quantity: cartItem.quantity,
          unitPrice: cartItem.unitPrice,
          discount: cartItem.discount,
          threeDays: threeDays,
          oneWeeks: oneWeeks,
        };
        orderData.cartItems.push(orderDetailData);
      }

      // Tạo đối tượng FormData để chứa dữ liệu ảnh
      const formData = new FormData();
      // formData.append("customerId", customerId);
      formData.append("currentImage", dataURItoBlob(imageData));
      formData.append("orderDate", orderData.orderDate);
      formData.append("price", orderData.price);
      formData.append("shipCost", orderData.shipCost);
      formData.append("statusId", orderData.statusId);
      for (const [index, cartItem] of orderData.cartItems.entries()) {
        formData.append(`cartItems[${index}].productId`, cartItem.productId);
        formData.append(`cartItems[${index}].quantity`, cartItem.quantity);
        formData.append(`cartItems[${index}].unitPrice`, cartItem.unitPrice);
        formData.append(`cartItems[${index}].discount`, cartItem.discount);
        formData.append(`cartItems[${index}].threeDays`, cartItem.threeDays);
        formData.append(`cartItems[${index}].oneWeeks`, cartItem.oneWeeks);
      }
      const token = sessionStorage.getItem("token");
      const config = {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      };
      console.log(formData);
      // Make API call to create the order with orderDetails
      const response = await axios.post(
        "https://localhost:7050/api/Order/Checkout",
        formData,
        config
      );

      resetCountdownTimer();
      setOrderPlaced(true);
      const orderId = response.data.orderId;

      const responseOrderData = await axios.get(
        `https://localhost:7050/api/Order/${orderId}`
      );
      const alertData = {
        customerId: responseOrderData.data.customerId,
        message: `Khách hàng #${responseOrderData.data.customerId} đã đặt đơn hàng #${responseOrderData.data.orderId} thành công. Vui lòng kiểm tra thông tin đơn hàng`,
        currentDate: formattedDate,
      };

      // Make API call to post alert data
      await axios.post(
        "https://localhost:7050/api/Alert",
        alertData,
      );
      notifySuccess();
      dispatch(deleteAllFromCart());
      toggleModal();
      history.push(`/view-order-detail/${orderId}`);
    } catch (error) {
      notifyDanger();
      console.error("Error placing order:", error);
    }
  };

  //tính giá theo ngày thuê
  const calculateProductSubtotalWithDuration = (cartItem) => {
    const duration = parseFloat(
      getSelectedDurationFromSessionStorage(cartItem.cartItemId)
    );
    const discountedPrice = getDiscountPrice(
      cartItem.unitPrice,
      cartItem.discount
    );
    const finalProductPrice = cartItem.unitPrice * currency.currencyRate * (cartItem.selectedQuality/100);
    const finalDiscountedPrice = discountedPrice * currency.currencyRate * (cartItem.selectedQuality/100);

    const productPrice =
      discountedPrice !== null ? finalDiscountedPrice : finalProductPrice;

    if (duration === 3) {
      return productPrice * cartItem.quantity * duration * 0.95;
    }
    if (duration === 7) {
      return productPrice * cartItem.quantity * duration * 0.9;
    }
    return productPrice * cartItem.quantity * duration;
  };

  const openConfirmationModal = () => {
    setShowConfirmationModal(true);
  };

  const VND = new Intl.NumberFormat("vi-VN", {
    style: "currency",
    currency: "VND",
  });

  return (
    <Fragment>
      <div className="rna-wrapper">
        <NotificationAlert ref={notificationAlertRef} />
      </div>
      <SEO
        titleTemplate="Checkout"
        description="Checkout page of flone react minimalist eCommerce template."
      />
      <Modal isOpen={showModal} toggle={toggleModal}>
        <ModalHeader toggle={toggleModal}>Xác thực khuôn mặt</ModalHeader>
        <ModalBody>
          <Form>
            <FormGroup>
              <Row className="align-items-center">
                <div className="row justify-content-center">
                  <div className="col-sm-auto col-4">
                    <div>
                      {imageData && ( // Kiểm tra imageData có dữ liệu không
                        <img
                          src={imageData}
                          alt="Captured"
                          style={{
                            display: "block",
                            width: "50%",
                            marginLeft: "105px",
                          }}
                        />
                      )}
                    </div>
                    <div className="col-sm-auto col-8">
                      <div className="input-group justify-content-center">
                        <div className="d-flex justify-content-center">
                          <video
                            ref={videoRef}
                            style={{
                              display: "block",
                              width: "50%",
                            }}
                            className="justify-content-center"
                            autoPlay
                            muted
                          />
                        </div>
                        <div className="input-group-append justify-content-center m-lg-2 mt-3">
                          {showCaptureButton && (
                            <button
                              className="btn btn-sm float-end mt-2 mb-0 btn-primary"
                              type="button"
                              onClick={handleCapture}
                            >
                              Chụp ảnh
                            </button>
                          )}
                          {showSaveButton && (
                            <button
                              className="btn btn-sm float-end mt-2 mb-0 btn-primary"
                              type="button"
                              onClick={handleTakePhoto}
                            >
                              <i className="fa fa-camera"></i>
                            </button>
                          )}
                          {showSaveButton && (
                            <button
                              className="btn btn-sm float-end mt-2 mb-0 btn-danger"
                              type="button"
                              onClick={handleCancelCapture}
                            >
                              X
                            </button>
                          )}
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </Row>
            </FormGroup>
          </Form>
          <div className="text-center">
            {imageData && (
              <div>
                <h3>Vui lòng quét mã</h3>

                <img
                  src="/assets/img/QRcode.jpg"
                  alt="Captured"
                  className="img-fluid"
                />
                <div className="countdown-timer">
                  {Math.floor(remainingTime / 60)}:
                  {(remainingTime % 60).toString().padStart(2, "0")}
                </div>
                <div>
                  <h4>
                    <b>
                      Số tiền cọc trước:{" "}
                      {VND.format(
                        (totalPrice + totalPrice * shippingPercentage) / 2
                      )}
                    </b>
                  </h4>
                  <h4>
                    <b>Nội dung chuyển khoản: {phone}</b>
                  </h4>
                </div>
              </div>
            )}
          </div>
        </ModalBody>
        <ModalFooter>
          <Button color="primary" onClick={openConfirmationModal}>
            Xác nhận
          </Button>{" "}
          <Button color="secondary" onClick={toggleModal}>
            Hủy
          </Button>
        </ModalFooter>
      </Modal>
      {/* Confirmation Modal */}
      <Modal
        isOpen={showConfirmationModal}
        toggle={() => setShowConfirmationModal(false)}
      >
        <ModalHeader toggle={() => setShowConfirmationModal(false)}>
          Xác nhận chuyển khoản
        </ModalHeader>
        <ModalBody>Bạn đã chắc chắn là chuyển khoản chưa?</ModalBody>
        <ModalFooter>
          <Button
            color="primary"
            onClick={() => {
              setShowConfirmationModal(false);
              handlePlaceOrder();
            }}
          >
            Đồng ý
          </Button>{" "}
          <Button
            color="secondary"
            onClick={() => setShowConfirmationModal(false)}
          >
            Hủy
          </Button>
        </ModalFooter>
      </Modal>
      <LayoutOne headerTop="visible">
        {/* breadcrumb */}
        <Breadcrumb
          pages={[
            { label: "Home", path: process.env.PUBLIC_URL + "/" },
            { label: "Checkout", path: process.env.PUBLIC_URL + pathname },
          ]}
        />
        <div className="checkout-area pt-20 pb-100">
          <div className="container">
            <div className="row">
              <div className="col-14">
                <div className="table-content table-responsive cart-table-content">
                  <table>
                    {cartItems && cartItems.length >= 1 ? (
                      <thead>
                        <tr>
                          <th>Sản phẩm</th>
                          <th>Tên sản phẩm</th>
                          <th>Đơn giá</th>
                          <th>CHẤT LƯỢNG</th>
                          <th>Số lượng</th>
                          <th>Ngày thuê</th>
                          <th>Thành tiền</th>
                        </tr>
                      </thead>
                    ) : (
                      ""
                    )}
                    <tbody>
                      {cartItems.map((cartItem, key) => {
                        const discountedPrice = getDiscountPrice(
                          cartItem.unitPrice,
                          cartItem.discount
                        );
                        const finalProductPrice =
                          cartItem.unitPrice * currency.currencyRate;
                        const finalDiscountedPrice =
                          discountedPrice * currency.currencyRate;

                        discountedPrice != null
                          ? (cartTotalPrice +=
                              finalDiscountedPrice * cartItem.quantity)
                          : (cartTotalPrice +=
                              finalProductPrice * cartItem.quantity);

                        return (
                          <tr key={key}>
                            <td className="product-thumbnail">
                              <Link
                                to={
                                  process.env.PUBLIC_URL +
                                  "/product/" +
                                  cartItem.productId
                                }
                              >
                                <img
                                  className="img-fluid"
                                  src={process.env.PUBLIC_URL + cartItem.image}
                                  alt=""
                                />
                              </Link>
                            </td>
                            <td className="product-name">
                              <Link
                                to={
                                  process.env.PUBLIC_URL +
                                  "/product/" +
                                  cartItem.productId
                                }
                              >
                                {cartItem.productName}
                              </Link>
                            </td>
                            <td className="product-price-cart">
                              {discountedPrice !== null ? (
                                <Fragment>
                                  <span className="amount old">
                                    <AmountFormatted
                                      amount={
                                        finalProductPrice +
                                        " " +
                                        currency.currencySymbol
                                      }
                                    />
                                  </span>
                                  <span className="amount">
                                    <AmountFormatted
                                      amount={
                                        finalDiscountedPrice +
                                        " " +
                                        currency.currencySymbol
                                      }
                                    />
                                  </span>
                                </Fragment>
                              ) : (
                                <span className="amount">
                                  {finalProductPrice +
                                    " " +
                                    currency.currencySymbol}
                                </span>
                              )}
                            </td>
                            <td className="product-quality">
                                  {cartItem.selectedQuality}%{" "}
                                </td>
                            <td className="product-quantity">
                              <div className="cart-plus-minus">
                                <input
                                  className="cart-plus-minus-box"
                                  type="text"
                                  value={cartItem.quantity}
                                  readOnly
                                />
                              </div>
                            </td>
                            <td className="product-duration">
                              <td className="product-duration">
                                {getSelectedDurationFromSessionStorage(
                                  cartItem.cartItemId
                                )}{" "}
                                ngày
                              </td>
                            </td>

                            <td className="product-subtotal">
                              <AmountFormatted
                                amount={
                                  calculateProductSubtotalWithDuration(
                                    cartItem
                                  ) +
                                  " " +
                                  currency.currencySymbol
                                }
                              />
                            </td>
                          </tr>
                        );
                      })}
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
            {cartItems && cartItems.length >= 1 ? (
              <div className="row pt-3">
                <div className="col-lg-7">
                  <div className="billing-info-wrap">
                    <h3>Thông tin địa chỉ nhận hàng</h3>
                    <div className="row">
                      <div className="col-lg-6 col-md-6">
                        <div className="billing-info">
                          <label>Họ và tên</label>
                          <input
                            type="text"
                            value={fullName}
                            onChange={(e) => {
                              setFullName(e.target.value);
                            }}
                          />
                        </div>
                      </div>
                      <div className="col-lg-6 col-md-6">
                        <div className="billing-info">
                          <label>Số điện thoại</label>
                          <input
                            type="text"
                            value={phone}
                            onChange={(e) => {
                              setPhone(e.target.value);
                            }}
                          />
                        </div>
                      </div>
                      <div className="col-lg-12 col-md-12">
                        <div className="billing-info">
                          <label>Địa chỉ</label>
                          <textarea
                            type="text"
                            value={address}
                            onChange={(e) => {
                              setAddress(e.target.value);
                            }}
                          />
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

                <div className="col-lg-5">
                  <div className="your-order-area">
                    <h3>Tổng thanh toán</h3>
                    <div className="your-order-wrap gray-bg-4">
                      <div className="your-order-product-info">
                        <div className="your-order-top">
                          <ul>
                            <li>Sản phẩm</li>
                            <li>Thành tiền</li>
                          </ul>
                        </div>
                        <div className="your-order-middle">
                          <ul>
                            {cartItems.map((cartItem, key) => {
                              const discountedPrice = getDiscountPrice(
                                cartItem.unitPrice,
                                cartItem.discount
                              );
                              const finalProductPrice =
                                cartItem.unitPrice * currency.currencyRate;
                              const finalDiscountedPrice =
                                discountedPrice * currency.currencyRate;

                              discountedPrice != null
                                ? (cartTotalPrice +=
                                    finalDiscountedPrice * cartItem.quantity)
                                : (cartTotalPrice +=
                                    finalProductPrice * cartItem.quantity);
                              return (
                                <li key={key}>
                                  <span className="order-middle-left">
                                    {cartItem.productName} X {cartItem.quantity}
                                  </span>{" "}
                                  <span className="order-price">
                                    <AmountFormatted
                                      amount={calculateProductSubtotalWithDuration(
                                        cartItem
                                      )}
                                    />{" "}
                                  </span>
                                </li>
                              );
                            })}
                          </ul>
                        </div>
                        <div className="your-order-bottom">
                          <ul>
                            <li className="your-order-shipping">
                              Phí vận chuyển
                            </li>
                            <li>
                              <AmountFormatted
                                amount={
                                  totalPrice * shippingPercentage +
                                  " " +
                                  currency.currencySymbol
                                }
                              />
                            </li>
                          </ul>
                        </div>
                        <div className="your-order-total">
                          <ul>
                            <li className="order-total">Tổng thanh toán</li>
                            <li>
                              <AmountFormatted
                                amount={
                                  totalPrice +
                                  totalPrice * shippingPercentage +
                                  " " +
                                  currency.currencySymbol
                                }
                              />
                            </li>
                          </ul>
                        </div>
                      </div>
                      <div className="payment-method"></div>
                    </div>
                    <div className="place-order mt-25">
                      {orderPlaced ? (
                        <div>
                          <h3>Bạn đã đặt hàng thành công!</h3>

                          <Link to={process.env.PUBLIC_URL + "/"}>
                            Trờ về trang chủ
                          </Link>
                        </div>
                      ) : (
                        <button className="btn-hover" onClick={toggleModal}>
                          Đặt hàng
                        </button>
                      )}
                    </div>
                  </div>
                </div>
              </div>
            ) : (
              <div className="row">
                <div className="col-lg-12">
                  <div className="item-empty-area text-center">
                    <div className="item-empty-area__icon mb-30">
                      <i className="pe-7s-cash"></i>
                    </div>
                    <div className="item-empty-area__text">
                      Không tìm thấy sản phẩm nào <br />{" "}
                      <Link
                        to={process.env.PUBLIC_URL + "/shop-grid-standard/1"}
                      >
                        Mua hàng ngay
                      </Link>
                    </div>
                  </div>
                </div>
              </div>
            )}
          </div>
        </div>
      </LayoutOne>
    </Fragment>
  );
};

export default Checkout;

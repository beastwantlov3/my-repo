import React, { Fragment, useState, useRef } from "react";
import { useHistory, Link, useLocation } from "react-router-dom";
import Tab from "react-bootstrap/Tab";
import Nav from "react-bootstrap/Nav";
import SEO from "../../components/seo";
import LayoutOne from "../../layouts/LayoutOne";
import Breadcrumb from "../../wrappers/breadcrumb/Breadcrumb";
import axios from "axios";
import moment from "moment";
import cogoToast from "cogo-toast";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faEye, faEyeSlash } from "@fortawesome/free-solid-svg-icons";
import {
  addUserNameToCart,
  setCartItemsForUsername,
} from "../../store/slices/cart-slice";
import { useDispatch } from "react-redux";

const LoginRegister = () => {
  let { pathname } = useLocation();

  const [username, setUsername] = React.useState("");
  const [password, setPassword] = React.useState("");
  const [showPassword, setShowPassword] = useState(false);
  const [showConfirmPassword, setShowConfirmPassword] = useState(false);
  const [strength, setStrength] = useState("");
  const [confirmPassword, setConfirmPassword] = useState("");
  const [email, setEmail] = useState("");
  const [fullName, setFullName] = useState("");
  const [phone, setPhone] = useState("");
  const [dob, setDOB] = useState(moment().format("DD/MM/YYYY"));
  const [isRegistered, setIsRegistered] = useState(false);
  const [isChecked, setIsChecked] = useState(false);
  const [rememberMe, setRememberMe] = useState(false);
  const [verificationCode, setVerificationCode] = useState("");
  const history = useHistory();
  const inputsRef = useRef([]);

  //Hiển thị thông báo lỗi
  const [usernameError, setUsernameError] = useState("");
  const [passwordError, setPasswordError] = useState("");
  const [confirmPasswordError, setConfirmPasswordError] = useState("");
  const [fullNameError, setFullNameError] = useState("");
  const [emailError, setEmailError] = useState("");
  const [phoneNumberError, setPhoneNumberError] = useState("");
  const dispatch = useDispatch();

  const handlePasswordToggle = () => {
    setShowPassword(!showPassword);
  };

  const handleConfirmPasswordToggle = () => {
    setShowConfirmPassword(!showConfirmPassword);
  };

  const handleInputChange = (index, value) => {
    const newCode = verificationCode.split("");
    newCode[index] = value;
    setVerificationCode(newCode.join(""));

    if (value && index < inputsRef.current.length - 1) {
      inputsRef.current[index + 1].focus();
    }
  };

  const handleKeyPress = (e, index) => {
    const keyCode = e.which || e.keyCode;
    const keyValue = String.fromCharCode(keyCode);
    if (!/^\d$/.test(keyValue)) {
      e.preventDefault();
    } else if (
      index < inputsRef.current.length - 1 &&
      e.target.value.length >= e.target.maxLength
    ) {
      inputsRef.current[index + 1].focus();
    }
  };

  const handleEnterPress = (e) => {
    if (e.key === "Enter") {
      // Gửi mã xác minh hoặc thực hiện hành động liên quan tại đây
      console.log("Mã xác minh:", verificationCode);
    }
  };

  const handleLogin = async (e) => {
    e.preventDefault();
    try {
      setUsernameError("");
      setPasswordError("");

      if (!username) {
        setUsernameError("Vui lòng nhập tài khoản.");
      }

      if (!password) {
        setPasswordError("Vui lòng nhập mật khẩu.");
      }

      const response = await axios.post(
        "https://localhost:7050/api/Auth/login",
        {
          username: username,
          password: password,
          rememberMe: rememberMe,
        }
      );
      // Kiểm tra phản hồi của API để xem xét thành công hay không
      if (response.data.loggedIn) {
        const token = response.data.token;
        const username = response.data.username;

        // Lưu token vào sessionStorage
        sessionStorage.setItem("token", token);

        // Lưu thông tin đăng nhập vào sessionStorage hoặc Redux
        sessionStorage.setItem("loggedIn", response.data.loggedIn);
        sessionStorage.setItem("username", response.data.username);
        sessionStorage.setItem("roleName", response.data.roleName);

        if (response.data.roleName === "Admin") {
          window.location.href = "http://localhost:3000/admin/dashboard-admin";
          // history.push('/admin/dashboard-admin')
        } else if (response.data.roleName === "Blogger") {
          window.location.href = "http://localhost:3000/admin/list-blogs";
        } else if (response.data.roleName === "Consultant") {
          window.location.href = "http://localhost:3000/admin";
        } else if (response.data.roleName === "Customers") {
          window.location.href = "http://localhost:3000/";
          // history.push('/');
        } else if (response.data.roleName === "Manager") {
          window.location.href = "http://localhost:3000/admin/manager";
          // history.push('/admin/manager')
        } else if (response.data.roleName === "Sale") {
          window.location.href = "http://localhost:3000/admin/list-sales";
          // history.push('/admin/list-sales')
        } else if (response.data.roleName === "Shipper") {
          window.location.href = "http://localhost:3000/admin/list-shippers";
          // history.push('/admin/list-shippers')
        } else if (response.data.roleName === "Supplier") {
          window.location.href = "http://localhost:3000/admin/list-suppliers";
        } else {
          console.error("Vai trò không hợp lệ!");
        }

        cogoToast.success("Đăng nhập thành công", { position: "bottom-left" });

        dispatch(addUserNameToCart(username));

        const cartItems = JSON.parse(sessionStorage.getItem(username)) || [];

        dispatch(setCartItemsForUsername({ userName: username, cartItems }));

      } else {
        // Xử lý lỗi đăng nhập không thành công
        const errorMessage = response.data.error;
        if (
          errorMessage ===
          "Tài khoản không tồn tại. Vui lòng nhập lại tài khoản!"
        ) {
          setUsernameError(errorMessage);
        } else if (
          errorMessage === "Mật Khẩu sai. Vui lòng nhập lại mật khẩu!"
        ) {
          setPasswordError(errorMessage);
        } else {
          // Xử lý các lỗi khác
          console.error("Lỗi đăng nhập:", errorMessage);
        }
      }
    } catch (error) {
      if (error.response && error.response.data) {
        const errorMessage = error.response.data.error;
        if (
          errorMessage ===
          "Tài khoản không tồn tại. Vui lòng nhập lại tài khoản!"
        ) {
          setUsernameError(errorMessage);
        } else if (
          errorMessage === "Mật Khẩu sai. Vui lòng nhập lại mật khẩu!"
        ) {
          setPasswordError(errorMessage);
        } else {
          console.error("Unknown error:", errorMessage);
        }
      } else {
        cogoToast.error("Đăng nhập thất bại", { position: "bottom-left" });
      }
      // console.error("Lỗi đăng nhập:", error);
    }
  };

  const handleRegister = async (e) => {
    e.preventDefault();
    try {
      if (isChecked) {
        // Kiểm tra dữ liệu đầu vào
        setUsernameError("");
        setPasswordError("");
        setConfirmPasswordError("");
        setFullNameError("");
        setEmailError("");
        setPhoneNumberError("");

        if (!username) {
          setUsernameError("Vui lòng nhập tài khoản.");
        } else if (!/^[A-Za-z]*[A-Za-z0-9]{1,20}$/.test(username)) {
          setUsernameError(
            "Tài khoản không được chứa ký tự đặc biệt và không quá 20 ký tự."
          );
        } else if (/^[0-9]+$/.test(username)) {
          setUsernameError("Tên người dùng không được chỉ chứa toàn số.");
        }

        if (!password) {
          setPasswordError("Vui lòng nhập mật khẩu.");
        } else if (
          !/^(?=.*[A-Z])(?=.*\d)(?=.*[!@#$%^&*])[A-Za-z\d!@#$%^&*]{1,14}$/.test(
            password
          )
        ) {
          setPasswordError(
            "Mật khẩu không hợp lệ. Yêu cầu ít nhất 1 kí tự viết hoa, 1 kí tự đặc biệt, 1 kí tự số, và không quá 14 kí tự."
          );
        }

        if (!confirmPassword) {
          setConfirmPasswordError("Vui lòng nhập xác nhận mật khẩu.");
        } else if (password !== confirmPassword) {
          setConfirmPasswordError("Mật khẩu và Xác nhận mật khẩu không khớp.");
        }

        if (!email) {
          setEmailError("Vui lòng nhập email.");
        } else if (
          !/^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/.test(email)
        ) {
          setEmailError(
            "Email không hợp lệ. Vui lòng nhập địa chỉ email hợp lệ, ví dụ: example@gmail.com"
          );
        }

        if (!fullName) {
          setFullNameError("Vui lòng nhập họ và tên.");
        } else if (fullName.length > 20) {
          setFullNameError("Họ và tên không được vượt quá 20 ký tự.");
        } else if (fullName.indexOf(" ") === -1) {
          setFullNameError(
            "Họ và tên không hợp lệ. Phải có ít nhất 1 khoảng trắng."
          );
        }

        if (!phone) {
          setPhoneNumberError("Vui lòng nhập số điện thoại.");
        } else if (!/^\d{10,}$/.test(phone)) {
          setPhoneNumberError(
            "Số điện thoại không hợp lệ. Vui lòng nhập ít nhất 10 số."
          );
        }
        if (
          !usernameError &&
          !passwordError &&
          !confirmPasswordError &&
          !fullNameError &&
          !emailError &&
          !phoneNumberError
        ) {
          await axios.post("https://localhost:7050/api/Auth/register", {
            username: username,
            password: password,
            confirmPassword: confirmPassword,
            email: email,
            fullName: fullName,
            phone: phone,
            dob: dob,
          });
          cogoToast.success("Đăng ký thành công", { position: "bottom-left" });
          setIsRegistered(true);
        }
      } else {
        // Nếu checkbox không được chọn, hiển thị thông báo lỗi
        cogoToast.error("Bạn phải đồng ý với Điều khoản và Điều kiện", {
          position: "bottom-left",
        });
      }
    } catch (error) {
      if (error.response && error.response.data) {
        const errorMessage = error.response.data.error;
        if (errorMessage === "Tên người dùng đã tồn tại") {
          setUsernameError(errorMessage);
        } else if (errorMessage === "Email đã tồn tại") {
          setEmailError(errorMessage);
        } else {
          console.error("Unknown error:", errorMessage);
        }
      } else {
        console.error("Registration failed:", error);
      }
    }
  };

  const handleVerify = async (e) => {
    e.preventDefault();
    try {
      // Gửi yêu cầu xác thực mã nhập vào đến API
      const params = new URLSearchParams();
      params.append("email", email);
      params.append("verificationCode", verificationCode);
      const url = `https://localhost:7050/api/Auth/verify?${params.toString()}`;
      const response = await axios.post(url);
      console.log(response.data);
      cogoToast.success("Xác thực thành công", { position: "bottom-left" });
      // Xử lý phản hồi từ API và hiển thị thông báo tương ứng
      // Xác thực thành công
      // Hiển thị thông báo thành công
      history.push("/");
    } catch (error) {
      console.error(error); // Xử lý lỗi từ API
    }
  };

  const handleResendVerification = async (e) => {
    e.preventDefault();
    try {
      // Gửi yêu cầu xác thực mã nhập vào đến API
      const params = new URLSearchParams();
      params.append("email", email);
      params.append("username", username);
      const url = `https://localhost:7050/api/Auth/resendverification?${params.toString()}`;
      const response = await axios.post(url);
      cogoToast.success("Gửi mã xác thực lại thành công", {
        position: "bottom-left",
      });
      console.log(response.data);
      // Xử lý phản hồi từ API và hiển thị thông báo tương ứng
      // Xác thực thành công
      // Hiển thị thông báo thành công
    } catch (error) {
      console.error(error); // Xử lý lỗi từ API
    }
  };

  const handleCheckboxChange = () => {
    setIsChecked(!isChecked);
  };

  const handleChange = (event) => {
    const { value } = event.target;
    setPassword(value);
    checkPasswordStrength(value);
  };

  const checkPasswordStrength = (value) => {
    // Kiểm tra độ mạnh của mật khẩu ở đây
    let score = 0;

    if (value.length >= 8) {
      // Độ dài ít nhất 8 ký tự
      score += 1;
    }

    if (/[A-Z]/.test(value)) {
      // Chứa ít nhất một chữ hoa
      score += 1;
    }

    if (/[a-z]/.test(value)) {
      // Chứa ít nhất một chữ thường
      score += 1;
    }

    if (/[0-9]/.test(value)) {
      // Chứa ít nhất một chữ số
      score += 1;
    }

    if (/[!@#$%^&*]/.test(value)) {
      // Chứa ít nhất một ký tự đặc biệt
      score += 1;
    }

    // Đánh giá độ mạnh của mật khẩu dựa trên số điểm
    if (score === 5) {
      setStrength("Mật khẩu rất mạnh");
    } else if (score >= 3) {
      setStrength("Mật khẩu mạnh");
    } else if (score >= 2) {
      setStrength("Mật khẩu trung bình");
    } else {
      setStrength("Mật khẩu yếu");
    }
  };

  return (
    <Fragment>
      <SEO
        titleTemplate="Login"
        description="Login page of flone react minimalist eCommerce template."
      />
      <LayoutOne headerTop="visible">
        {/* breadcrumb */}
        <Breadcrumb
          pages={[
            { label: "Home", path: process.env.PUBLIC_URL + "/" },
            {
              label: "Login Register",
              path: process.env.PUBLIC_URL + pathname,
            },
          ]}
        />
        <div className="login-register-area pt-100 pb-100">
          <div className="container">
            <div className="row d-flex justify-content-center">
              <div className="col-lg-9 col-md-12 ms-auto me-auto">
                <div className="login-register-wrapper">
                  <Tab.Container defaultActiveKey="login">
                    <Nav variant="pills" className="login-register-tab-list">
                      <Nav.Item>
                        <Nav.Link eventKey="login">
                          <h4>Login</h4>
                        </Nav.Link>
                      </Nav.Item>
                      <Nav.Item>
                        <Nav.Link eventKey="register">
                          <h4>Register</h4>
                        </Nav.Link>
                      </Nav.Item>
                    </Nav>
                    <Tab.Content>
                      <Tab.Pane eventKey="login">
                        <div className="login-form-container">
                          <div className="login-register-form">
                            <form>
                              <input
                                type="text"
                                name="user-name"
                                value={username}
                                onChange={(e) => setUsername(e.target.value)}
                                placeholder="Username"
                              />
                              <span className="text-center text-danger font-weight-900 mt--8 m-lg-2">
                                {usernameError}
                              </span>
                              <div style={{ position: "relative" }}>
                                <input
                                  type={showPassword ? "text" : "password"}
                                  value={password}
                                  onChange={(e) => setPassword(e.target.value)}
                                  name="user-password"
                                  placeholder="Mật Khẩu"
                                  style={{ paddingRight: "2.5em" }}
                                />
                                <FontAwesomeIcon
                                  icon={showPassword ? faEyeSlash : faEye}
                                  onClick={handlePasswordToggle}
                                  style={{
                                    position: "absolute",
                                    right: "0.3em",
                                    top: "30%",
                                    transform: "translateY(-30%)",
                                    cursor: "pointer",
                                  }}
                                />
                              </div>
                              <span className="text-center text-danger font-weight-900 mt--8 m-lg-2">
                                {passwordError}
                              </span>
                              <div className="button-box">
                                <div className="login-toggle-btn">
                                  <input
                                    type="checkbox"
                                    checked={rememberMe}
                                    onChange={(e) =>
                                      setRememberMe(e.target.checked)
                                    }
                                  />
                                  <label className="ml-10">Remember me</label>
                                  <Link
                                    to={
                                      process.env.PUBLIC_URL +
                                      "/forgot-password"
                                    }
                                  >
                                    Forgot Password?
                                  </Link>
                                </div>
                                <button type="submit" onClick={handleLogin}>
                                  <span>Login</span>
                                </button>
                              </div>
                            </form>
                          </div>
                        </div>
                      </Tab.Pane>
                      <Tab.Pane eventKey="register">
                        {!isRegistered && (
                          <div className="login-form-container">
                            <div className="login-register-form">
                              <form>
                                <input
                                  type="text"
                                  value={username}
                                  onChange={(e) => setUsername(e.target.value)}
                                  name="user-name"
                                  placeholder="Tên Tài Khoản"
                                />
                                <span className="text-center text-danger font-weight-900 mt--8 m-lg-2">
                                  {usernameError}
                                </span>
                                <div style={{ position: "relative" }}>
                                  <input
                                    type={showPassword ? "text" : "password"}
                                    value={password}
                                    onChange={handleChange}
                                    name="user-password"
                                    placeholder="Mật Khẩu"
                                    style={{ paddingRight: "2.5em" }}
                                    autoComplete="new-password"
                                  />
                                  <FontAwesomeIcon
                                    icon={showPassword ? faEyeSlash : faEye}
                                    onClick={handlePasswordToggle}
                                    style={{
                                      position: "absolute",
                                      right: "0.3em",
                                      top: "30%",
                                      transform: "translateY(-30%)",
                                      cursor: "pointer",
                                    }}
                                  />
                                  <span className="text-center text-danger font-weight-900 mt--8 m-lg-2">
                                    {passwordError}
                                  </span>
                                </div>
                                <div style={{ position: "relative" }}>
                                  <input
                                    type={
                                      showConfirmPassword ? "text" : "password"
                                    }
                                    value={confirmPassword}
                                    onChange={(e) =>
                                      setConfirmPassword(e.target.value)
                                    }
                                    name="user-password"
                                    placeholder="Xác nhận mật khẩu"
                                    style={{ paddingRight: "2.5em" }}
                                    autoComplete="new-password"
                                  />
                                  <FontAwesomeIcon
                                    icon={
                                      showConfirmPassword ? faEyeSlash : faEye
                                    }
                                    onClick={handleConfirmPasswordToggle}
                                    style={{
                                      position: "absolute",
                                      right: "0.3em",
                                      top: "30%",
                                      transform: "translateY(-30%)",
                                      cursor: "pointer",
                                    }}
                                  />
                                  <span className="text-center text-danger font-weight-900 mt--8 m-lg-2">
                                    {confirmPasswordError}
                                  </span>
                                </div>
                                <input
                                  name="user-email"
                                  value={email}
                                  onChange={(e) => setEmail(e.target.value)}
                                  placeholder="Email"
                                  type="email"
                                />
                                <span className="text-center text-danger font-weight-900 mt--8 m-lg-2">
                                  {emailError}
                                </span>
                                <input
                                  name="full-name"
                                  value={fullName}
                                  onChange={(e) => setFullName(e.target.value)}
                                  placeholder="Họ và Tên"
                                  type="text"
                                />
                                <span className="text-center text-danger font-weight-900 mt--8 m-lg-2">
                                  {fullNameError}
                                </span>
                                <input
                                  name="phone"
                                  value={phone}
                                  onChange={(e) => setPhone(e.target.value)}
                                  placeholder="Số Điện Thoại"
                                  type="phone"
                                  onKeyPress={(e) => {
                                    const keyCode = e.which || e.keyCode;
                                    const keyValue =
                                      String.fromCharCode(keyCode);
                                    if (!/^\d$/.test(keyValue)) {
                                      e.preventDefault();
                                    }
                                  }}
                                />
                                <span className="text-center text-danger font-weight-900 mt--8 m-lg-2">
                                  {phoneNumberError}
                                </span>
                                <input
                                  type="date"
                                  className="form-control"
                                  placeholder="Ngày Sinh"
                                  value={moment(dob, "DD/MM/YYYY").format(
                                    "YYYY-MM-DD"
                                  )}
                                  onChange={(e) =>
                                    setDOB(
                                      moment(
                                        e.target.value,
                                        "YYYY-MM-DD"
                                      ).format("DD/MM/YYYY")
                                    )
                                  }
                                  timeFormat={false}
                                />
                                <div className="text-muted font-italic">
                                  <small>
                                    password strength:{" "}
                                    <span className="text-success font-weight-700">
                                      {strength}
                                    </span>
                                  </small>
                                </div>
                                <div className="custom-control custom-control-alternative custom-checkbox">
                                  <input
                                    className="custom-control-input"
                                    id="customCheckRegister"
                                    type="checkbox"
                                    checked={isChecked}
                                    onChange={handleCheckboxChange}
                                  />
                                  <label
                                    className="custom-control-label"
                                    htmlFor="customCheckRegister"
                                  >
                                    <span className="text-muted">
                                      Tôi đồng ý với{" "}
                                      <a
                                        href="#pablo"
                                        onClick={(e) => e.preventDefault()}
                                      >
                                        Điều khoản và Điều kiện
                                      </a>
                                    </span>
                                  </label>
                                </div>
                                <div className="button-box">
                                  <button
                                    type="submit"
                                    onClick={handleRegister}
                                  >
                                    <span>Register</span>
                                  </button>
                                </div>
                              </form>
                            </div>
                          </div>
                        )}
                      </Tab.Pane>
                    </Tab.Content>
                    {isRegistered && (
                      <div className="card">
                        <div className="card-body px-lg-5 py-lg-5 text-center">
                          <form>
                            <div className="info mb-4">
                              <div className="icon icon-shape icon-xl rounded-circle bg-gradient-info shadow text-center mx-auto">
                                <i className="ni ni-spaceship opacity-10"></i>
                              </div>
                            </div>
                            <div className="text-center text-muted mb-4">
                              <h2>Xác nhận mã OTP!</h2>
                              <h3 className="bold-text">
                                Email đã đăng ký: <strong>{email}</strong>
                              </h3>
                              <p className="mb-0">
                                Xin vui lòng nhập mã OTP được gửi đến Email của
                                bạn.
                              </p>
                            </div>
                            <div className="d-flex justify-content-between m-4">
                              {[0, 1, 2, 3, 4, 5].map((index) => (
                                <div className="col-md-2" key={index}>
                                  <div className="form-group">
                                    <input
                                      type="text"
                                      className="text-center form-control-lg"
                                      maxLength="1"
                                      autoComplete="off"
                                      autoCapitalize="off"
                                      value={verificationCode.charAt(index)}
                                      ref={(el) =>
                                        (inputsRef.current[index] = el)
                                      }
                                      onKeyPress={(e) =>
                                        handleKeyPress(e, index)
                                      }
                                      onChange={(e) =>
                                        handleInputChange(index, e.target.value)
                                      }
                                      onKeyDown={handleEnterPress}
                                    />
                                  </div>
                                </div>
                              ))}
                            </div>
                            <div className="text-center">
                              <button
                                className="btn btn-info w-100"
                                onClick={handleVerify}
                              >
                                Send code
                              </button>
                              <span className="text-muted text-sm">
                                Bạn chưa nhận được mã xác nhận?
                                <button
                                  className="btn btn-link"
                                  onClick={handleResendVerification}
                                >
                                  Gửi lại mã xác thực
                                </button>
                              </span>
                            </div>
                          </form>
                        </div>
                      </div>
                    )}
                  </Tab.Container>
                </div>
              </div>
            </div>
          </div>
        </div>
      </LayoutOne>
    </Fragment>
  );
};

export default LoginRegister;

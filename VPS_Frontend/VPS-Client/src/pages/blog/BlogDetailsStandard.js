import { Fragment } from "react";
import { useLocation, useParams } from "react-router-dom"; 
import SEO from "../../components/seo";
import LayoutOne from "../../layouts/LayoutOne";
import Breadcrumb from "../../wrappers/breadcrumb/Breadcrumb";
import BlogSidebar from "../../wrappers/blog/BlogSidebar";
import BlogPost from "../../wrappers/blog/BlogPost";
import {useEffect, useState } from "react";

import axios from "axios";

const BlogDetailsStandard = () => {
  let { pathname } = useLocation();

  const { blogId } = useParams();
  const [blogDetailData, setBlogDetailData] = useState([]);

  useEffect(() => {
    const fetchBlogDetailData = async () => {
      try {
        const response = await axios.get(`https://localhost:7050/api/Blog/${blogId}`);
        setBlogDetailData(response.data);
        console.log(response.data);
      } catch (error) {
        console.error("Error fetching blog detail data:", error);
      }
    };

    fetchBlogDetailData();
  }, [blogId]);

  return (
    <Fragment>
      <SEO
        titleTemplate="Blog Post"
        description="Blog Post of VPS react minimalist eCommerce template."
      />
      <LayoutOne headerTop="visible">
        {/* breadcrumb */}
        <Breadcrumb 
          pages={[
            {label: "Home", path: process.env.PUBLIC_URL + "/" },
            {label: "Blog Post", path: process.env.PUBLIC_URL + pathname }
          ]} 
        />
        <div className="blog-area pt-100 pb-100">
          <div className="container">
            <div className="row flex-row-reverse">
              <div className="col-lg-9">
                <div className="blog-details-wrapper ml-20">
                  {/* blog post */}
                  <BlogPost blogDetailData={blogDetailData}/>

                  {/* blog post comment */}
                </div>
              </div>
              <div className="col-lg-3">
                {/* blog sidebar */}
                <BlogSidebar />
              </div>
            </div>
          </div>
        </div>
      </LayoutOne>
    </Fragment>
  );
};

export default BlogDetailsStandard;

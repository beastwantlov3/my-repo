import React, { Fragment, useState, useEffect } from "react";
import { useLocation } from "react-router-dom";
import SEO from "../../components/seo";
import LayoutOne from "../../layouts/LayoutOne";
import Breadcrumb from "../../wrappers/breadcrumb/Breadcrumb";
import BlogSidebar from "../../wrappers/blog/BlogSidebar";
import BlogPagination from "../../wrappers/blog/BlogPagination";
import BlogPosts from "../../wrappers/blog/BlogPosts";
import axios from "axios";

const BlogStandard = () => {
  let { pathname } = useLocation();
  const [searchKey, setSearchKey] = useState("");
  const [data, setData] = useState([]);
  const [originalData, setOriginalData] = useState([]);
  const [currentPage, setCurrentPage] = useState(1);
  const itemsPerPage = 6; // Set the number of items to display per page

  useEffect(() => {
    fetchData();
  }, []);

  useEffect(() => {
    // When the data or searchKey changes, reset the current page to 1
    setCurrentPage(1);
  }, [data, searchKey]);

  const handleSearchBar = (e) => {
    e.preventDefault();
    handleSearchResults();
  };

  const handleSearchResults = () => {
    const filteredBlogs = originalData.filter((blog) =>
      blog.title.toLowerCase().includes(searchKey.toLowerCase().trim())
    );
    setData(filteredBlogs);
  };

  const handleClearSearch = () => {
    setData(originalData);
    setSearchKey("");
  };

  const fetchData = async () => {
    try {
      const response = await axios.get("https://localhost:7050/api/Blog/list");
      setData(response.data);
      setOriginalData(response.data);
    } catch (error) {
      console.error("Error fetching data:", error);
    }
  };

  const handlePageChange = (pageNumber) => {
    setCurrentPage(pageNumber);
  };

  return (
    <Fragment>
      <SEO
        titleTemplate="Blog"
        description="Blog of VPS react minimalist eCommerce template."
      />
      <LayoutOne headerTop="visible">
        <Breadcrumb
          pages={[
            { label: "Home", path: process.env.PUBLIC_URL + "/" },
            { label: "Blog", path: process.env.PUBLIC_URL + pathname },
          ]}
        />
        <div className="blog-area pt-100 pb-100">
          <div className="container">
            <div className="row flex-row-reverse">
              <div className="col-lg-9">
                <div className="ml-20">
                  <div className="row">
                    <BlogPosts data={data} />
                  </div>
                  <BlogPagination
                    currentPage={currentPage}
                    totalPages={Math.ceil(data.length / itemsPerPage)}
                    handlePageChange={handlePageChange}
                  />
                </div>
              </div>
              <div className="col-lg-3">
                <BlogSidebar
                  value={searchKey}
                  clearSearch={handleClearSearch}
                  formSubmit={handleSearchBar}
                  handleSearchKey={(e) => setSearchKey(e.target.value)}
                />
              </div>
            </div>
          </div>
        </div>
      </LayoutOne>
    </Fragment>
  );
};

export default BlogStandard;

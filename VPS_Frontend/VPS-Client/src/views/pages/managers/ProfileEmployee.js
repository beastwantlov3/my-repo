import React, { useEffect, useState } from "react";

// reactstrap components
import {
  Card,
  CardHeader,
  CardBody,
  CardImg,
  FormGroup,
  Form,
  Input,
  Container,
  Row,
  Col,
  Badge,
} from "reactstrap";
// core components
import { useParams } from "react-router-dom/cjs/react-router-dom.min";
import axios from "axios";

const ProfileEmployee = () => {
  const { accountId } = useParams();
  const [accountData, setAccountData] = useState(null);

  useEffect(() => {
    const fetchAccountData = async () => {
      try {
        const response = await axios.get(
          `https://localhost:7050/api/Account/${accountId}`
        );
        setAccountData(response.data);
      } catch (error) {
        console.error("Error fetching account data:", error);
      }
    };

    fetchAccountData();
  }, [accountId, accountData]);
  return (
    <>
      <div
        className="header pb-6 d-flex align-items-center"
        style={{
          minHeight: "400px",
          backgroundImage:
            'url("' + require("../../../assets/img/theme/profile-cover.jpg") + '")',
          backgroundSize: "cover",
          backgroundPosition: "center top",
        }}
      >
        <span className="mask bg-gradient-info opacity-8" />

        <Container className="d-flex align-items-center" fluid>
          <Row>
            <Col lg="7" md="10">
              <h2 className="display-2 text-white">
                Thông tin của {accountData?.userName}
              </h2>
              <p className="text-white mt-0 mb-5">
                Đây là trang hồ sơ của tài khoản{" "}
                {accountData?.sale?.fullName ?? accountData?.shipper?.fullName}.
                Bạn có thể thấy được những thông tin và trạng thái hoạt động của
                nhân viên.
              </p>
            </Col>
          </Row>
        </Container>
      </div>
      <Container className="mt--6" fluid>
        <Row>
          <Col className="order-xl-2" xl="4">
            <Card className="card-profile">
              <CardImg
                alt="..."
                src={require("../../../assets/img/theme/img-1-1000x600.jpg")}
                top
              />
              <Row className="justify-content-center">
                <Col className="order-lg-2" lg="3">
                  <div className="card-profile-image">
                    <a href="#pablo" onClick={(e) => e.preventDefault()}>
                      <img
                        alt="Avatar"
                        className="rounded-circle"
                        src={accountData?.avatar}
                      />
                    </a>
                  </div>
                </Col>
              </Row>
              <CardBody className="pt-0">
                <div className="text-center mt-6">
                  <h5 className="h3">
                    {accountData?.sale?.fullName ??
                      accountData?.shipper?.fullName}
                  </h5>
                  <div className="h5 font-weight-300">
                    <i className="ni location_pin mr-2" />
                    {accountData?.sale?.address ??
                      accountData?.shipper?.address}
                  </div>
                  <div className="h5 mt-4">
                    <i className="ni business_briefcase-24 mr-2" />
                    {accountData?.saleId !== null
                      ? "Nhân viên bán hàng"
                      : "Nhân viên giao hàng"}
                  </div>
                  <div>
                    <i className="ni education_hat mr-3" />
                    <Badge color="" className="badge-dot mr-4">
                      <i
                        className={
                          accountData?.status === 1
                            ? "bg-success"
                            : "bg-warning"
                        }
                      />
                      <span className="status">
                        {accountData?.status === 1
                          ? "Hoạt động"
                          : "Ngừng hoạt động"}
                      </span>
                    </Badge>
                  </div>
                </div>
              </CardBody>
            </Card>
          </Col>
          <Col className="order-xl-1" xl="8">
            <Card>
              <CardHeader>
                <Row className="align-items-center">
                  <Col xs="8">
                    <h3 className="mb-0">Tài khoản</h3>
                  </Col>
                  {/* <Col className="text-right" xs="4">
                    <Button
                      color="primary"
                      href="#pablo"
                      onClick={(e) => e.preventDefault()}
                      size="sm"
                    >
                      Delete
                    </Button>
                  </Col> */}
                </Row>
              </CardHeader>
              <CardBody>
                <Form>
                  <h6 className="heading-small text-muted mb-4">
                    Thông tin tài khoản
                  </h6>
                  <div className="pl-lg-4">
                    <Row>
                      <Col lg="6">
                        <FormGroup>
                          <label
                            className="form-control-label"
                            htmlFor="input-username"
                          >
                            Tài khoản
                          </label>
                          <Input
                            defaultValue={accountData?.userName}
                            id="input-username"
                            type="text"
                            disabled
                          />
                        </FormGroup>
                      </Col>
                      <Col lg="6">
                        <FormGroup>
                          <label
                            className="form-control-label"
                            htmlFor="input-email"
                          >
                            Địa chỉ Email
                          </label>
                          <Input
                            defaultValue={accountData?.email}
                            id="input-email"
                            placeholder="jesse@example.com"
                            type="email"
                            disabled
                          />
                        </FormGroup>
                      </Col>
                    </Row>
                    <Row>
                      <Col lg="6">
                        <FormGroup>
                          <label
                            className="form-control-label"
                            htmlFor="input-first-name"
                          >
                            Họ và tên
                          </label>
                          <Input
                            defaultValue={
                              accountData?.sale?.fullName ??
                              accountData?.shipper?.fullName
                            }
                            id="input-first-name"
                            placeholder="First name"
                            type="text"
                            disabled
                          />
                        </FormGroup>
                      </Col>
                      <Col lg="6">
                        <FormGroup>
                          <label
                            className="form-control-label"
                            htmlFor="input-last-name"
                          >
                            Số điện thoại
                          </label>
                          <Input
                            defaultValue={
                              accountData?.sale?.phone ??
                              accountData?.shipper?.phone
                            }
                            id="input-last-name"
                            type="text"
                            disabled
                          />
                        </FormGroup>
                      </Col>
                    </Row>
                  </div>
                  <div className="pl-lg-4">
                    <Row>
                      <Col md="12">
                        <FormGroup>
                          <label
                            className="form-control-label"
                            htmlFor="input-address"
                          >
                            Địa chỉ
                          </label>
                          <Input
                            defaultValue={
                              accountData?.sale?.address ??
                              accountData?.shipper?.address
                            }
                            id="input-address"
                            placeholder="Home Address"
                            type="text"
                            disabled
                          />
                        </FormGroup>
                      </Col>
                    </Row>
                  </div>
                  <hr className="my-4" />
                </Form>
              </CardBody>
            </Card>
          </Col>
        </Row>
      </Container>
    </>
  );
};

export default ProfileEmployee;

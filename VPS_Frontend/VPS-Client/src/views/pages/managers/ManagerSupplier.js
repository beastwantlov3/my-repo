import React, { useEffect, useRef, useState } from "react";
import List from "list.js";
import {
  Badge,
  Card,
  CardHeader,
  CardFooter,
  Media,
  Pagination,
  PaginationItem,
  PaginationLink,
  Progress,
  Table,
  Container,
  Row,
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Form,
  FormGroup,
  Input,
} from "reactstrap";
import SimpleHeader from "../../../components/Headers/SimpleHeader.js";
import axios from "axios";
import { useParams } from "react-router-dom/cjs/react-router-dom.min";
import NotificationAlert from "react-notification-alert";

const ManagerSupplier = () => {
  const { supplierId } = useParams();
  const [currentPage, setCurrentPage] = useState(1);
  const [sortedColumn, setSortedColumn] = useState("");
  const [sortedDirection, setSortedDirection] = useState("");
  const itemsPerPage = 5;

  const firstListRef = useRef(null);
  const [productSupplierData, setProductSupplierData] = useState([]);
  const [supplierData, setSupplierData] = useState([]);

  const totalPages = Math.ceil(productSupplierData.length / itemsPerPage);

  const startIndex = (currentPage - 1) * itemsPerPage;
  const endIndex = Math.min(
    startIndex + itemsPerPage,
    productSupplierData.length
  );
  const displayedData = productSupplierData.slice(startIndex, endIndex);
  const [selectedProductId, setSelectedProductId] = useState(null);
  const notificationAlertRef = useRef(null);
  const [showModal, setShowModal] = useState(false);
  const [quantity, setQuantity] = useState("");

  useEffect(() => {
    fetchSupplierData();
    fetchProductSupplierData();
  }, []);

  useEffect(() => {
    if (productSupplierData.length > 0) {
      initializeList();
    }
  }, [productSupplierData]);

  const fetchSupplierData = async () => {
    try {
      const response = await axios.get(
        `https://localhost:7050/api/Supplier/${supplierId}`
      );
      setSupplierData(response.data);
      console.log("supplier:", response.data);
    } catch (error) {
      console.error("Error fetching data:", error);
    }
  };
  const fetchProductSupplierData = async () => {
    try {
      const response = await axios.get(
        `https://localhost:7050/api/SupplierStock/GetCurrentProductBySupplierId/${supplierId}`
      );
      setProductSupplierData(response.data);
      setCurrentPage(1); // Reset current page when fetching new data
    } catch (error) {
      console.error("Error fetching data:", error);
    }
  };

  const notifySuccess = () => {
    let options = {
      place: "tc",
      message: (
        <div className="alert-text">
          <span className="alert-title" data-notify="title">
            {" "}
            Đã gửi thông báo đến nhà cung cấp sản phẩm 
          </span>
        </div>
      ),
      type: "success",
      icon: "ni ni-bell-55",
      autoDismiss: 7,
    };
    notificationAlertRef.current.notificationAlert(options);
  };
  const notifyDanger = () => {
    let options = {
      place: "tc",
      message: (
        <div className="alert-text">
          <span className="alert-title" data-notify="title">
            {" "}
            Thêm sản phẩm thất bại!
          </span>
          <span data-notify="message">Số lượng vượt quá số lượng tồn kho.</span>
        </div>
      ),
      type: "danger",
      icon: "ni ni-bell-55",
      autoDismiss: 7,
    };
    notificationAlertRef.current.notificationAlert(options);
  };

  const toggleModal = () => {
    setShowModal(!showModal);
  };

  const handleQuantityChange = (e) => {
    setQuantity(e.target.value);
  };

  const initializeList = () => {
    new List(firstListRef.current, {
      valueNames: [
        { name: "name", attr: "data-name" },
        { name: "brand", attr: "data-brand" },
        { name: "budget", attr: "data-budget" },
        { name: "status", attr: "data-status" },
        { name: "unitInStock", attr: "data-unitInStock" },
        { name: "category", attr: "data-category" },
        { name: "completion", attr: "data-completion" },
      ],
      listClass: "list",
    });
  };

  const handleSort = (column) => {
    let direction = "asc";
    if (column === sortedColumn && sortedDirection === "asc") {
      direction = "desc";
    }
    setSortedColumn(column);
    setSortedDirection(direction);
    sortData(column, direction);
    setCurrentPage(1); // Reset current page when sorting
  };

  const sortData = (column, direction) => {
    const sortedData = [...productSupplierData];
    sortedData.sort((a, b) => {
      const valueA = getValueByColumn(a, column);
      const valueB = getValueByColumn(b, column);
      if (valueA < valueB) {
        return direction === "asc" ? -1 : 1;
      }
      if (valueA > valueB) {
        return direction === "asc" ? 1 : -1;
      }
      return 0;
    });
    setProductSupplierData(sortedData);
  };

  const getValueByColumn = (item, column) => {
    switch (column) {
      case "name":
        return item.product.productName;
      case "brand":
        return item.product.brand;
      case "budget":
        return item.product.unitPrice;
      case "status":
        return item.product.status.statusId;
      case "unitInStock":
        return item.product.unitInStock;
      case "category":
        return item.product.category.categoryName;
      case "completion":
        return item.product.quality;
      default:
        return "";
    }
  };

  const handleFormSubmit = async (e) => {
    e.preventDefault();
    const form = e.target;
    if (form.checkValidity()) {
      try {
        const parsedQuantity = parseInt(quantity, 10);
        // Check if the entered quantity exceeds the available unitInStock

        const selectedProduct = productSupplierData.find(
          (product) => product.product.productId === selectedProductId
        );

        const responseProductSupplier = await axios.get(
          `https://localhost:7050/api/SupplierProduct/${selectedProductId}`
        );

        if (parsedQuantity > selectedProduct.product.unitInStock) {
          notifyDanger();
          console.log(parsedQuantity);
          return;
        }

        const currentDate = new Date();
        const year = currentDate.getFullYear();
        const month = String(currentDate.getMonth() + 1).padStart(2, "0");
        const day = String(currentDate.getDate()).padStart(2, "0");
        const hours = String(currentDate.getHours()).padStart(2, "0");
        const minutes = String(currentDate.getMinutes()).padStart(2, "0");
        const seconds = String(currentDate.getSeconds()).padStart(2, "0");
        const milliseconds = String(currentDate.getMilliseconds()).padStart(
          3,
          "0"
        );

        const formattedDate = `${year}-${month}-${day}T${hours}:${minutes}:${seconds}.${milliseconds}Z`;
        const addArert = {
          sproductId: selectedProductId,
          quantity: quantity,
          supplierId: supplierId,
          message: `Chúng tôi muốn nhập với số lượng ${quantity} sản phẩm #${selectedProductId} ${responseProductSupplier.data.productName} từ nhà cung cấp ${supplierData.supplierName}. Vui lòng xác nhận hoặc liên hệ với shop để biết thêm thông tin chi tiết. Xin cảm ơn! `,
          statusId: false,
          currentDate: formattedDate,
        };
        await axios.post(`https://localhost:7050/api/SupplierAlert`, addArert);

        setShowModal(false);
        // Show success alert
        notifySuccess();
        fetchProductSupplierData();
        // Reset the form fields or redirect to another page
      } catch (error) {
        notifyDanger();
        console.error("Error adding product:", error);
      }
    }
  };

  const VND = new Intl.NumberFormat('vi-VN', {
    style: 'currency',
    currency: 'VND',
  });

  return (
    <>
      <NotificationAlert ref={notificationAlertRef} />
      <SimpleHeader name="Supplier management" parentName="Manager" />

      <Modal isOpen={showModal} toggle={toggleModal}>
        <ModalHeader toggle={toggleModal}>Nhập số lượng</ModalHeader>
        <ModalBody>
          <Form>
            <FormGroup>
              <Input
                type="number"
                placeholder="Nhập số lượng"
                value={quantity}
                onInput={(e) => {
                  const minValue = 1;
                  const maxValue = 100;
                  let enteredValue = parseInt(e.target.value, 10);

                  // Check if the entered value is a valid number and within the valid range
                  if (isNaN(enteredValue) || enteredValue < minValue) {
                    enteredValue = minValue;
                  } else if (enteredValue > maxValue) {
                    enteredValue = maxValue;
                  }

                  e.target.value = enteredValue;
                  handleQuantityChange(e); // Update the state with the new value
                }}
              />
            </FormGroup>
          </Form>
        </ModalBody>
        <ModalFooter>
          <Button color="primary" onClick={handleFormSubmit}>
            Xác nhận
          </Button>{" "}
          <Button color="secondary" onClick={toggleModal}>
            Hủy
          </Button>
        </ModalFooter>
      </Modal>

      <Container className="mt--6" fluid>
        <Row>
          <div className="col">
            <Card>
              <CardHeader className="border-0">
                <h3 className="mb-0">Danh sách sản phẩm</h3>
              </CardHeader>
              {productSupplierData.length > 0 ? (
                <div className="table-responsive" ref={firstListRef}>
                  <Table className="align-items-center table-flush">
                    <thead className="thead-light">
                      <tr>
                        <th
                          className="sort"
                          data-sort="name"
                          scope="col"
                          onClick={() => handleSort("name")}
                        >
                          Sản phẩm
                        </th>
                        <th
                          className="sort"
                          data-sort="brand"
                          scope="col"
                          onClick={() => handleSort("brand")}
                        >
                          Nhãn hiệu
                        </th>
                        <th
                          className="sort"
                          data-sort="budget"
                          scope="col"
                          onClick={() => handleSort("budget")}
                        >
                          Giá
                        </th>
                        <th
                          className="sort"
                          data-sort="status"
                          scope="col"
                          onClick={() => handleSort("status")}
                        >
                          Tình trạng
                        </th>
                        <th
                          className="sort"
                          data-sort="unitInStock"
                          scope="col"
                          onClick={() => handleSort("unitInStock")}
                        >
                          Số lượng
                        </th>
                        <th
                          className="sort"
                          data-sort="category"
                          scope="col"
                          onClick={() => handleSort("category")}
                        >
                          Phân loại
                        </th>
                        <th
                          className="sort"
                          data-sort="completion"
                          scope="col"
                          onClick={() => handleSort("completion")}
                        >
                          Đánh giá
                        </th>
                        <th scope="col" />
                      </tr>
                    </thead>
                    <tbody className="list">
                      {displayedData.map((product, index) => (
                        <tr key={index}>
                          <th scope="row">
                            <Media className="align-items-center">
                              <a
                                className="avatar rounded-circle mr-3"
                                href="#pablo"
                                onClick={(e) => e.preventDefault()}
                              >
                                <img
                                  alt="Image..."
                                  src={product.product.image}
                                />
                              </a>
                              <Media>
                                <span className="name mb-0 text-sm">
                                  {product.product.productName}
                                </span>
                              </Media>
                            </Media>
                          </th>
                          <td className="brand">{product.product.brand}</td>
                          <td className="budget">
                            {VND.format(product.product.unitPrice)}
                          </td>
                          <td>
                            <Badge color="" className="badge-dot mr-4">
                              <i
                                className={
                                  product.product.statusId === 1
                                    ? "bg-warning"
                                    : product.product.statusId === 2
                                    ? "bg-success"
                                    : product.product.statusId === 3
                                    ? "bg-yellow"
                                    : "bg-gray"
                                }
                              />
                              <span className="status">
                                {product.product.statusId === 1
                                  ? "Sản phẩm hết hàng"
                                  : product.product.statusId === 2
                                  ? "Sản phẩm còn trong kho"
                                  : product.product.statusId === 3
                                  ? "Sản phẩm đang được khuyến mãi"
                                  : "Hỏng"}
                              </span>
                            </Badge>
                          </td>
                          <td className="unitInStock">
                            {product.product.unitInStock}
                          </td>
                          <td className="category">
                            {product.product.category.categoryName}
                          </td>
                          <td>
                            <div className="d-flex align-items-center">
                              <span className="completion mr-2">
                                {product.product.quality}/10
                              </span>
                              <div>
                                <Progress
                                  max="10"
                                  value={product.product.quality}
                                  color="warning"
                                />
                              </div>
                            </div>
                          </td>
                          <td className="text-right">
                            {product.product.unitInStock > 0 && product.product.statusId === 2 && (
                              <Button
                                className="btn-neutral"
                                color="default"
                                size="sm"
                                onClick={() => {
                                  setSelectedProductId(
                                    product.product.productId
                                  );
                                  toggleModal();
                                }}
                              >
                                Nhập
                              </Button>
                            )}
                          </td>
                        </tr>
                      ))}
                    </tbody>
                  </Table>
                </div>
              ) : (
                <div>Loading...</div>
              )}
              <CardFooter className="py-4 bg-transparent">
                <nav aria-label="...">
                  <Pagination className="pagination justify-content-end mb-0">
                    <PaginationItem disabled={currentPage === 1}>
                      <PaginationLink
                        previous
                        onClick={() => setCurrentPage(currentPage - 1)}
                      />
                    </PaginationItem>
                    {Array.from({ length: totalPages }, (_, i) => i + 1).map(
                      (page) => (
                        <PaginationItem
                          key={page}
                          active={currentPage === page}
                        >
                          <PaginationLink onClick={() => setCurrentPage(page)}>
                            {page}
                          </PaginationLink>
                        </PaginationItem>
                      )
                    )}
                    <PaginationItem disabled={currentPage === totalPages}>
                      <PaginationLink
                        next
                        onClick={() => setCurrentPage(currentPage + 1)}
                      />
                    </PaginationItem>
                  </Pagination>
                </nav>
              </CardFooter>
            </Card>
          </div>
        </Row>
      </Container>
    </>
  );
};

export default ManagerSupplier;

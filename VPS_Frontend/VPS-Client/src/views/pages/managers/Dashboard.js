import React, { useEffect, useRef, useState } from "react";
// node.js library that concatenates classes (strings)
import classnames from "classnames";
// javascipt plugin for creating charts
import Chart from "chart.js";
// react plugin used to create charts
import { Line } from "react-chartjs-2";
// reactstrap components
import {
  // Badge,
  Button,
  Card,
  CardHeader,
  CardBody,
  ListGroupItem,
  ListGroup,
  Media,
  Progress,
  Table,
  Container,
  Row,
  Col,
  PaginationItem,
  PaginationLink,
  Badge,
  Pagination,
  CardFooter,
  FormGroup,
  ModalFooter,
  Input,
  Form,
  ModalBody,
  ModalHeader,
  Modal,
} from "reactstrap";

// core components
import CardsHeader from "../../../components/Headers/CardsHeader.js";
import List from "list.js";

import { chartOptions, parseOptions } from "../../../variables/charts.js";
import axios from "axios";
import { Link } from "react-router-dom/cjs/react-router-dom.min";
import ReactDatetimeClass from "react-datetime";

function Dashboard() {
  const [startDate, setStartDate] = React.useState(null);
  const [endDate, setEndDate] = React.useState(null);

  const [currentPageSaler, setCurrentPageSaler] = useState(1);
  const [currentPageSupplier, setCurrentPageSupplier] = useState(1);
  const itemsPerPage = 5;

  const firstListRef = useRef(null);
  const [dataSaler, setDataSaler] = useState([]);
  const [dataSupplier, setDataSupplier] = useState([]);
  const [productData, setProductData] = useState([]);
  const [buyerData, setBuyerData] = useState([]);
  const [employeeData, setEmployeeData] = useState([]);
  const [displayedAccount, setDisplayedAccount] = useState(4);
  const [displayedBuyer, setDisplayedBuyer] = useState(4);
  const [displayedProduct, setDisplayedProduct] = useState(4);
  const [showAllAccount, setShowAllAccount] = useState(false);
  const [showAllBuyer, setShowAllBuyer] = useState(false);
  const [showAllProduct, setShowAllProduct] = useState(false);

  const totalPagesSaler = Math.ceil(dataSaler.length / itemsPerPage);
  const totalPagesSupplier = Math.ceil(dataSupplier.length / itemsPerPage);

  const startIndexSaler = (currentPageSaler - 1) * itemsPerPage;
  const startIndexSupplier = (currentPageSupplier - 1) * itemsPerPage;
  const endIndexSaler = startIndexSaler + itemsPerPage;
  const endIndexSupplier = startIndexSupplier + itemsPerPage;
  const displayedDataSaler = dataSaler.slice(startIndexSaler, endIndexSaler);
  const displayedDataSupplier = dataSupplier.slice(startIndexSupplier, endIndexSupplier);

  const [showModal, setShowModal] = useState(false);
  const [voucher, setVoucher] = useState("");
  const [selectedCustomerId, setSelectedCustomerId] = useState(null);

  const [chartData, setChartData] = useState({
    labels: [],
    datasets: [
      {
        label: "Revenue",
        data: [],
        borderColor: "#5e72e4",
        pointRadius: 0,
        pointHoverRadius: 0,
        borderWidth: 3,
      },
    ],
  });

  useEffect(() => {
    fetchDataSale();
    fetchDataSupplier();
    fetchProductSale();
    fetchEmployee();
    fetchBuyerData();
  }, []);

  useEffect(() => {
    if (dataSaler.length > 0) {
      initializeListSaler();
    }
  }, [dataSaler]);

  const fetchDataSale = async () => {
    try {
      const response = await axios.get("https://localhost:7050/api/Sale/list");
      setDataSaler(response.data);
    } catch (error) {
      console.error("Error fetching data:", error);
    }
  };
  const fetchDataSupplier = async () => {
    try {
      const response = await axios.get(
        "https://localhost:7050/api/Supplier/list"
      );
      setDataSupplier(response.data);
    } catch (error) {
      console.error("Error fetching data:", error);
    }
  };

  const fetchProductSale = async () => {
    try {
      const response = await axios.get(
        "https://localhost:7050/api/Product/top8bestsale"
      );
      setProductData(response.data);
    } catch (error) {
      console.error("Error fetching data:", error);
    }
  };

  const fetchEmployee = async () => {
    try {
      const response = await axios.get(
        "https://localhost:7050/api/Account/GetAllEmployee"
      );
      setEmployeeData(response.data);
    } catch (error) {
      console.error("Error fetching data:", error);
    }
  };
  const handleShowMoreAccount = () => {
    // Display all items when "Show More" is clicked
    setDisplayedAccount(employeeData.length);
    setShowAllAccount(true);
  };
  const handleShowLessAccount = () => {
    setDisplayedAccount(4);
    setShowAllAccount(false);
  };
  const handleShowMoreBuyer = () => {
    // Display all items when "Show More" is clicked
    setDisplayedBuyer(buyerData.length);
    setShowAllBuyer(true);
  };
  const handleShowLessBuyer = () => {
    setDisplayedBuyer(4);
    setShowAllBuyer(false);
  };
  const handleShowMoreProduct = () => {
    // Display all items when "Show More" is clicked
    setDisplayedProduct(productData.length);
    setShowAllProduct(true);
  };
  const handleShowLessProduct = () => {
    setDisplayedProduct(4);
    setShowAllProduct(false);
  };

  const fetchBuyerData = async () => {
    try {
      const buyerResponse = await axios.get(
        "https://localhost:7050/api/Order/top8bestbuyer"
      );
      const buyers = buyerResponse.data;

      // Fetch customer data for each buyer
      const customerDataPromises = buyers.map(async (buyer) => {
        const customerResponse = await axios.get(
          `https://localhost:7050/api/Customer/${buyer.customerId}`
        );
        return {
          ...buyer,
          fullName: customerResponse.data.fullName,
        };
      });

      // Wait for all customer data to be fetched
      const buyerDataWithCustomer = await Promise.all(customerDataPromises);
      setBuyerData(buyerDataWithCustomer);
    } catch (error) {
      console.error("Error fetching data:", error);
    }
  };

  const toggleModal = () => {
    setShowModal(!showModal);
  };

  const handleVoucherChange = (e) => {
    setVoucher(e.target.value);
  };

  const handleSubmitVoucher = async () => {
    try {
      // Send voucher data to API for the selected customer
      if (selectedCustomerId) {
        const response = await axios.get(
          `https://localhost:7050/api/Customer/${selectedCustomerId}`
        );
        const existingData = response.data;
        await axios.put(
          `https://localhost:7050/api/Customer/${selectedCustomerId}`,
          {
            ...existingData,
            voucher: voucher,
          }
        );
        // Close the modal after successful submission
        toggleModal();
      }
    } catch (error) {
      console.error("Error submitting voucher:", error);
    }
  };

  const initializeListSaler = () => {
    new List(firstListRef.current, {
      valueNames: ["id", "name", "phone", "status", "address", "dob"],
      listClass: "list",
    });
  };

  const monthNames = [
    "Jan",
    "Feb",
    "Mar",
    "Apr",
    "May",
    "Jun",
    "Jul",
    "Aug",
    "Sep",
    "Oct",
    "Nov",
    "Dec",
  ];
  useEffect(() => {
    // Fetch data from the API and update the chart data state
    const fetchData = async () => {
      try {
        const monthData = [];
        for (let i = 1; i <= 12; i++) {
          const response = await axios.get(
            `https://localhost:7050/api/Order/doanhthuthang${i}`
          );
          const data = response.data; // Replace this with the actual data received from the API
          monthData.push(data);
        }
        // Update the chart data state with the received data
        setChartData({
          labels: monthNames,
          datasets: [
            {
              label: "Revenue",
              data: monthData, // Put the single data value into the first element of the data array
              borderColor: "#5e72e4",
              pointRadius: 0,
              pointHoverRadius: 0,
              borderWidth: 3,
            },
          ],
        });
      } catch (error) {
        console.error("Error fetching data:", error);
      }
    };

    fetchData();
  }, []);

  // Make sure to parse the chart options
  if (window.Chart) {
    parseOptions(Chart, chartOptions());
  }

  const dateFormatter = (cell, row) => {
    const dateObj = new Date(row.dob);
    const year = dateObj.getFullYear();
    const month = String(dateObj.getMonth() + 1).padStart(2, "0");
    const day = String(dateObj.getDate()).padStart(2, "0");
    const formattedDob = `${year}/${month}/${day}`;
    return formattedDob;
  };


  return (
    <>
      <CardsHeader name="Manager" parentName="Dashboards" />

      <Container className="mt--6" fluid>
        <Row>
          <Col xl="12">
            <Card className="bg-default">
              <CardHeader className="bg-transparent">
                <Row className="align-items-center">
                  <div className="col">
                    <h6 className="text-light text-uppercase ls-1 mb-1">
                      Tổng quan
                    </h6>
                    <h5 className="h3 text-white mb-0">Doanh thu</h5>
                  </div>
                </Row>
              </CardHeader>
              <CardBody>
                <div className="chart">
                  <Line
                    data={chartData}
                    options={chartOptions().defaults}
                    id="chart-sales"
                    className="chart-canvas"
                  />
                </div>
              </CardBody>
            </Card>
          </Col>
        </Row>
        <Row>
          <Col xl="4">
            <Card>
              <CardHeader>
                <h5 className="h3 mb-0">Danh sách tài khoản nhân viên</h5>
              </CardHeader>

              <CardBody>
                <ListGroup className="list my--3" flush>
                  {employeeData.slice(0, displayedAccount).map((employee) => (
                    <ListGroupItem className="px-0" key={employee.accountId}>
                      <Row className="align-items-center">
                        <Col className="col-auto">
                          <a
                            className="avatar rounded-circle"
                            href="#pablo"
                            onClick={(e) => e.preventDefault()}
                          >
                            <img alt="..." src={employee.avatar} />
                          </a>
                        </Col>
                        <div className="col ml--2">
                          <h4 className="mb-0">
                            <a
                              href="#pablo"
                              onClick={(e) => e.preventDefault()}
                            >
                              {employee.userName}
                            </a>
                          </h4>
                          <small>{employee.role.roleName}</small>
                        </div>
                        <Col className="col-auto">
                          <Link
                            to={`/admin/profile-employee/${
                              employee.accountId}`}
                          >
                            <Button color="primary" size="sm" type="button">
                              Xem
                            </Button>
                          </Link>
                        </Col>
                      </Row>
                    </ListGroupItem>
                  ))}
                </ListGroup>
                <div className="d-flex justify-content-center mt-3">
                  {!showAllAccount ? (
                    <Button color="info" onClick={handleShowMoreAccount}>
                      Xem thêm
                    </Button>
                  ) : (
                    <Button color="info" onClick={handleShowLessAccount}>
                      Trở lại
                    </Button>
                  )}
                </div>
              </CardBody>
            </Card>
          </Col>
          <Col xl="5">
            <Card>
              <CardHeader>
                <h5 className="h3 mb-0">Top người dùng đặt hàng</h5>
              </CardHeader>

              <CardBody>
                <ListGroup className="list my--3" flush>
                  {buyerData.slice(0, displayedBuyer).map((buyer, index) => (
                    <ListGroupItem className="px-0" key={index}>
                      <Row className="align-items-center">
                        <Col className="col-auto">
                          <a
                            className="avatar rounded-circle"
                            href="#pablo"
                            onClick={(e) => e.preventDefault()}
                          >
                            <img
                              alt="..."
                              src={require("../../../assets/img/theme/team-1.jpg")}
                            />
                          </a>
                        </Col>
                        <Col className="col ml--2">
                          <h4 className="mb-0">
                            <a
                              href="#pablo"
                              onClick={(e) => e.preventDefault()}
                            >
                              {buyer.fullName}
                            </a>
                          </h4>
                          <small>
                            <b>Tổng chi: </b>
                            {buyer.totalPrice} 
                          </small>
                        </Col>
                        <Col className="col-auto">
                          <Button
                            color="primary"
                            size="sm"
                            type="button"
                            onClick={() => {
                              setSelectedCustomerId(buyer.customerId);
                              toggleModal();
                            }}
                          >
                            Tặng Voucher
                          </Button>
                        </Col>
                      </Row>
                    </ListGroupItem>
                  ))}
                </ListGroup>
                <div className="d-flex justify-content-center mt-3">
                  {!showAllBuyer ? (
                    <Button color="info" onClick={handleShowMoreBuyer}>
                      Xem thêm
                    </Button>
                  ) : (
                    <Button color="info" onClick={handleShowLessBuyer}>
                      Trở lại
                    </Button>
                  )}
                </div>
                <Modal isOpen={showModal} toggle={toggleModal}>
                  <ModalHeader toggle={toggleModal}>Nhập Voucher</ModalHeader>
                  <ModalBody>
                    <Form>
                      <FormGroup>
                        <Input
                          type="number"
                          placeholder="Nhập Voucher"
                          value={voucher}
                          onInput={(e) => {
                            const minValue = 1;
                            const maxValue = 100;
                            let enteredValue = parseInt(e.target.value, 10);

                            // Check if the entered value is a valid number and within the valid range
                            if (
                              isNaN(enteredValue) ||
                              enteredValue < minValue
                            ) {
                              enteredValue = minValue;
                            } else if (enteredValue > maxValue) {
                              enteredValue = maxValue;
                            }

                            e.target.value = enteredValue;
                            handleVoucherChange(e); // Update the state with the new value
                          }}
                        />
                      </FormGroup>
                    </Form>
                  </ModalBody>
                  <ModalFooter>
                    <Button color="primary" onClick={handleSubmitVoucher}>
                      Xác nhận
                    </Button>{" "}
                    <Button color="secondary" onClick={toggleModal}>
                      Hủy
                    </Button>
                  </ModalFooter>
                </Modal>
              </CardBody>
            </Card>
          </Col>
          <Col xl="3">
            <Card>
              <CardHeader>
                <h5 className="h3 mb-0">Top sản phẩm đặt hàng</h5>
              </CardHeader>

              <CardBody>
                <ListGroup className="list my--3" flush>
                  {productData.slice(0, displayedProduct).map((product) => (
                    <ListGroupItem className="px-0" key={product.productId}>
                      <Row className="align-items-center">
                        <Col className="col-auto">
                          <a
                            className="avatar rounded-circle"
                            href="#pablo"
                            onClick={(e) => e.preventDefault()}
                          >
                            <img
                              alt="..."
                              src={require("../../../assets/img/theme/bootstrap.jpg")}
                            />
                          </a>
                        </Col>
                        <Col className="col">
                          <h5>{product.productName}</h5>
                          <Progress
                            className="progress-xs mb-0"
                            color="orange"
                            max="100"
                            value={product.totalOrder}
                          />
                        </Col>
                      </Row>
                    </ListGroupItem>
                  ))}
                </ListGroup>
                <div className="d-flex justify-content-center mt-3">
                  {!showAllProduct ? (
                    <Button color="info" onClick={handleShowMoreProduct}>
                      Xem thêm
                    </Button>
                  ) : (
                    <Button color="info" onClick={handleShowLessProduct}>
                      Trở lại
                    </Button>
                  )}
                </div>
              </CardBody>
            </Card>
          </Col>
        </Row>
      </Container>
      <Container className="mt-2" fluid>
        <Row>
          <div className="col">
            <Card>
              <CardHeader className="border-0">
                <h3 className="mb-0">Danh sách nhân viên bán hàng</h3>
              </CardHeader>
              {dataSaler.length > 0 ? (
                <div className="table-responsive" ref={firstListRef}>
                  <Table className="align-items-center table-flush">
                    <thead className="thead-light">
                      <tr>
                        <th className="sort" data-sort="id" scope="col">
                          ID
                        </th>
                        <th className="sort" data-sort="name" scope="col">
                          Họ tên
                        </th>
                        <th className="sort" data-sort="phone" scope="col">
                          Số điện thoại
                        </th>
                        <th className="sort" data-sort="status" scope="col">
                          Tình trạng
                        </th>
                        <th className="sort" data-sort="address" scope="col">
                          Địa chỉ
                        </th>
                        <th className="sort" data-sort="dob" scope="col">
                          Ngày sinh
                        </th>
                        <th scope="col" />
                      </tr>
                    </thead>
                    <tbody className="list">
                      {displayedDataSaler.map((sale) => (
                        <tr key={sale.saleId}>
                          <td className="id">#{sale.saleId}</td>
                          <th scope="row">
                            <Media className="align-items-center">
                              <a
                                className="avatar rounded-circle mr-3"
                                href="#pablo"
                                onClick={(e) => e.preventDefault()}
                              >
                                <img
                                  alt="..."
                                  src={require("../../../assets/img/theme/bootstrap.jpg")}
                                  />
                              </a>
                              <Media>
                                <span className="name mb-0 text-sm">
                                  {sale.fullName}
                                </span>
                              </Media>
                            </Media>
                          </th>
                          <td className="phone">{sale.phone}</td>
                          <td>
                            <Badge color="" className="badge-dot mr-4">
                              <i
                                className={
                                  sale.statusId === 1
                                    ? "bg-success"
                                    : "bg-warning"
                                }
                              />
                              <span className="status">
                                {sale.statusId === 1
                                  ? "Hoạt động"
                                  : "Ngừng hoạt động"}
                              </span>
                            </Badge>
                          </td>
                          <td className="address">{sale.address}</td>
                          <td className="dob">{dateFormatter(null, sale)}</td>

                          <td className="text-right">
                            <Link to={`/admin/manager-saler/${sale.saleId}`}>
                              <Button
                                className="btn-neutral"
                                color="default"
                                size="sm"
                              >
                                Xem
                              </Button>
                            </Link>
                          </td>
                        </tr>
                      ))}
                    </tbody>
                  </Table>
                </div>
              ) : (
                <div>Loading...</div>
              )}
              <CardFooter className="py-4 bg-transparent">
                <nav aria-label="...">
                  <Pagination className="pagination justify-content-end mb-0">
                    <PaginationItem disabled={currentPageSaler === 1}>
                      <PaginationLink
                        previous
                        onClick={() => setCurrentPageSaler(currentPageSaler - 1)}
                      />
                    </PaginationItem>
                    {Array.from(
                      { length: totalPagesSaler },
                      (_, i) => i + 1
                    ).map((page) => (
                      <PaginationItem key={page} active={currentPageSaler === page}>
                        <PaginationLink onClick={() => setCurrentPageSaler(page)}>
                          {page}
                        </PaginationLink>
                      </PaginationItem>
                    ))}
                    <PaginationItem disabled={currentPageSaler === totalPagesSaler}>
                      <PaginationLink
                        next
                        onClick={() => setCurrentPageSaler(currentPageSaler + 1)}
                      />
                    </PaginationItem>
                  </Pagination>
                </nav>
              </CardFooter>
            </Card>
          </div>
        </Row>
      </Container>
      <Container className="mt-2" fluid>
        <Row>
          <div className="col">
            <Card>
              <CardHeader className="border-0">
                <h3 className="mb-0">Danh sách nhà cung cấp</h3>
              </CardHeader>
              {dataSupplier.length > 0 ? (
                <div className="table-responsive" ref={firstListRef}>
                  <Table className="align-items-center table-flush">
                    <thead className="thead-light">
                      <tr>
                        <th className="sort" data-sort="id" scope="col">
                          ID
                        </th>
                        <th className="sort" data-sort="name" scope="col">
                          Họ tên
                        </th>
                        <th className="sort" data-sort="phone" scope="col">
                          Số điện thoại
                        </th>
                        <th className="sort" data-sort="status" scope="col">
                          Tình trạng
                        </th>
                        <th className="sort" data-sort="address" scope="col">
                          Địa chỉ
                        </th>
                        <th className="sort" data-sort="dob" scope="col">
                          Ngày sinh
                        </th>
                        <th scope="col" />
                      </tr>
                    </thead>
                    <tbody className="list">
                      {displayedDataSupplier.map((supplier) => (
                        <tr key={supplier.supplierId}>
                          <td className="id">#{supplier.supplierId}</td>
                          <th scope="row">
                            <Media className="align-items-center">
                              <a
                                className="avatar rounded-circle mr-3"
                                href="#pablo"
                                onClick={(e) => e.preventDefault()}
                              >
                                <img
                                  alt="..."
                                  src={require("../../../assets/img/theme/bootstrap.jpg")}
                                  />
                              </a>
                              <Media>
                                <span className="name mb-0 text-sm">
                                  {supplier.supplierName}
                                </span>
                              </Media>
                            </Media>
                          </th>
                          <td className="phone">{supplier.phone}</td>
                          <td>
                            <Badge color="" className="badge-dot mr-4">
                              <i
                                className={
                                  supplier.statusId === 1
                                    ? "bg-success"
                                    : "bg-warning"
                                }
                              />
                              <span className="status">
                                {supplier.statusId === 1
                                  ? "Hoạt động"
                                  : "Ngừng hoạt động"}
                              </span>
                            </Badge>
                          </td>
                          <td className="address">{supplier.address}</td>
                          <td className="dob">
                            {dateFormatter(null, supplier)}
                          </td>

                          <td className="text-right">
                            <Link
                              to={`/admin/manager-supplier/${supplier.supplierId}`}
                            >
                              <Button
                                className="btn-neutral"
                                color="default"
                                size="sm"
                              >
                                Xem
                              </Button>
                            </Link>
                          </td>
                        </tr>
                      ))}
                    </tbody>
                  </Table>
                </div>
              ) : (
                <div>Loading...</div>
              )}
              <CardFooter className="py-4 bg-transparent">
                <nav aria-label="...">
                  <Pagination className="pagination justify-content-end mb-0">
                    <PaginationItem disabled={currentPageSupplier === 1}>
                      <PaginationLink
                        previous
                        onClick={() => setCurrentPageSupplier(currentPageSupplier - 1)}
                      />
                    </PaginationItem>
                    {Array.from(
                      { length: totalPagesSupplier },
                      (_, i) => i + 1
                    ).map((page) => (
                      <PaginationItem key={page} active={currentPageSupplier === page}>
                        <PaginationLink onClick={() => setCurrentPageSupplier(page)}>
                          {page}
                        </PaginationLink>
                      </PaginationItem>
                    ))}
                    <PaginationItem disabled={currentPageSupplier === totalPagesSupplier}>
                      <PaginationLink
                        next
                        onClick={() => setCurrentPageSupplier(currentPageSupplier + 1)}
                      />
                    </PaginationItem>
                  </Pagination>
                </nav>
              </CardFooter>
            </Card>
          </div>
        </Row>
      </Container>
    </>
  );
}

export default Dashboard;

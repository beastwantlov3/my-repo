import classnames from "classnames";
import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom/cjs/react-router-dom.min";
// reactstrap components
import {
  ButtonGroup,
  Button,
  Card,
  CardHeader,
  CardBody,
  FormGroup,
  Form,
  Input,
  Container,
  Row,
  Col,
  ListGroup,
  ListGroupItem,
} from "reactstrap";
// core components
// import ProfileHeader from "components/Headers/ProfileHeader.js";
import NotificationAlert from "react-notification-alert";
import Select2 from "react-select2-wrapper";
// import TagsInput from "components/TagsInput/TagsInput.js";
import Dropzone from "dropzone";
import Slider from "nouislider";
import axios from "axios";
import { useDropzone } from "react-dropzone";

Dropzone.autoDiscover = false;

const AddProduct = () => {
  const [categoryData, setCategoryData] = useState([]);
  const [statusData, setStatusData] = useState([]);
  const [sliderValue, setSliderValue] = React.useState("0");
  const [selectedStatus, setSelectedStatus] = useState("");
  const [isFormSubmitted, setIsFormSubmitted] = useState(false);
  const [selectedCategory, setSelectedCategory] = useState("");
  const notificationAlertRef = React.useRef(null);
  const [selectedFile, setSelectedFile] = useState(null);
  const [selectedImages, setSelectedImages] = useState([]);
  const [ProductName, setProductName] = useState("");
  const [Brand, setBrand] = useState("");
  const [Discount, setDiscount] = useState("");
  const [UnitInStock, setUnitInStock] = useState("");
  const [UnitPrice, setUnitPrice] = useState("");
  const [Description, setDescription] = useState("");
  const [qualityPercentages, setQualityPercentages] = useState([]);

  const notifySuccess = async () => {
    let options = {
      place: "tc",
      message: (
        <div className="alert-text">
          <span className="alert-title" data-notify="title">
            {" "}
            Thêm sản phẩm thành công
          </span>
        </div>
      ),
      type: "success",
      icon: "ni ni-bell-55",
      autoDismiss: 7,
    };
    notificationAlertRef.current.notificationAlert(options);
    await handleImageUpdate();
  };

  const notifyDanger = () => {
    let options = {
      place: "tc",
      message: (
        <div className="alert-text">
          <span className="alert-title" data-notify="title">
            {" "}
            Thêm sản phẩm thất bại
          </span>
          <span data-notify="message">
            Vui lòng kiểm tra các trường đã điền
          </span>
        </div>
      ),
      type: "danger",
      icon: "ni ni-bell-55",
      autoDismiss: 7,
    };
    notificationAlertRef.current.notificationAlert(options);
  };

  const sliderRef = React.useRef(null);
  useEffect(() => {
    Slider.create(sliderRef.current, {
      start: [0],
      connect: [true, false],
      step: 1,
      range: { min: 0, max: 10 },
    }).on("update", function (values, handle) {
      setSliderValue(values[0]);
    });
  }, []);

  useEffect(() => {
    const fetchCategoryData = async () => {
      try {
        const response = await axios.get(
          "https://localhost:7050/api/Category/list"
        );
        setCategoryData(response.data);
        console.log(response.data);
      } catch (error) {
        console.error("Error fetching category data:", error);
      }
    };

    fetchCategoryData();
  }, []);

  useEffect(() => {
    const fetchStatusData = async () => {
      try {
        const response = await axios.get(
          "https://localhost:7050/api/PStatus/list"
        );
        const slicedData = response.data.slice(0, 4); // Get the first four values
        setStatusData(slicedData);
      } catch (error) {
        console.error("Error fetching category data:", error);
      }
    };

    fetchStatusData();
  }, []);

  const handleStatusSelection = (statusId) => {
    setSelectedStatus(statusId);
  };

  const handleFileChange = (event) => {
    setSelectedFile(event.target.files[0]);
  };

  const onDrop = (acceptedFiles) => {
    setSelectedImages([...selectedImages, ...acceptedFiles]);
  };
  const { getRootProps, getInputProps } = useDropzone({
    onDrop,
    accept: "image/*",
  });

  const handleFormSubmit = async (e) => {
    e.preventDefault();
    setIsFormSubmitted(true);
    const form = e.target;
    if (form.checkValidity()) {
      try {
        const formData = new FormData();
        var quality = parseInt(sliderValue);
        formData.append("productName", ProductName);
        formData.append("categoryId", selectedCategory);
        formData.append("unitInStock", UnitInStock);
        formData.append("unitPrice", UnitPrice);
        formData.append("image", selectedFile);
        formData.append("statusId", selectedStatus);
        formData.append("brand", Brand);
        formData.append("discount", Discount);
        formData.append("description", Description);
        formData.append("quality", quality);
        for (const image of selectedImages) {
          formData.append("coreImage", image);
        }
        // qualityPercentages.forEach((percentage, index) => {
        //   formData.append(`qualityPercentage[${index}]`, percentage);
        // });
        for (let index = 0; index < qualityPercentages.length; index++) {
          const percentage = qualityPercentages[index];
          formData.append("qualityPercentage", percentage);
        }
        // Make a POST request to the API to add the product
        await axios.post(
          "https://localhost:7050/api/Product/update-product",
          formData
        );
        form.reset();
        setIsFormSubmitted(false);
        // Show success alert
        notifySuccess();
        // history.push("/admin/products");

        // Reset the form fields or redirect to another page
      } catch (error) {
        notifyDanger();
        console.error("Error adding product:", error);
      }
    }
  };

  const handleQualityChange = (index, value) => {
    const updatedQualityPercentages = [...qualityPercentages];
    updatedQualityPercentages[index] = value;
    setQualityPercentages(updatedQualityPercentages);
  };

  const handleImageUpdate = async () => {
    const responseList = await axios.get("https://localhost:7050/api/Product");

    const lastProduct = responseList.data[responseList.data.length - 1];
    const newProductId = lastProduct.productId;

    // Increment the newProductId by 1
    const nextProductId = newProductId;

    // Fetch the existing product data using nextProductId (assuming you have a way to get the product data based on productId)
    const existingProductResponse = await axios.get(
      `https://localhost:7050/api/Product/${nextProductId}`
    );
    const existingProductData = existingProductResponse.data;

    // Create a new product object with the updated image and other existing data
    const updatedProduct = {
      ...existingProductData,
    };

    // Make the POST request to update the product with the new image
    const updateProduct = await axios.put(
      `https://localhost:7050/api/Product/update-product/${nextProductId}`,
      updatedProduct // Send the updated product object as the request data
    );

    // Handle the response of the second POST request
    console.log("updateProduct added:", updateProduct.data);
    console.log("id ", nextProductId);
  };

  return (
    <>
      <div className="rna-wrapper">
        <NotificationAlert ref={notificationAlertRef} />
      </div>
      <Container className="mt--12 " fluid>
        <Row className="mt-4 ml-8">
          <Col className="order-xl-1 mt-4" xl="10">
            <Card>
              <CardHeader>
                <Row className="align-items-center">
                  <Col xs="8">
                    <h3 className="mb-0">Thêm sản phẩm</h3>
                  </Col>
                </Row>
              </CardHeader>
              <CardBody>
                <Form onSubmit={handleFormSubmit}>
                  <h6 className="heading-small text-muted mb-4">
                    Thông tin sản phẩm
                  </h6>
                  <div className="pl-lg-4 ml-7">
                    <Row>
                      <Col lg="5">
                        <FormGroup>
                          <label
                            className="form-control-label"
                            htmlFor="input-username"
                          >
                            Tên sản phẩm
                          </label>
                          <Input
                            name="name"
                            id="input-username"
                            placeholder="Name of product"
                            type="text"
                            value={ProductName}
                            onChange={(e) => setProductName(e.target.value)}
                          />
                        </FormGroup>
                      </Col>
                      <Col lg="5">
                        <FormGroup>
                          <label
                            className="form-control-label"
                            htmlFor="input-first-name"
                          >
                            Nhãn hiệu
                          </label>
                          <Input
                            name="brand"
                            defaultValue=""
                            id="input-first-name"
                            placeholder="Brand"
                            type="text"
                            value={Brand}
                            onChange={(e) => setBrand(e.target.value)}
                          />
                        </FormGroup>
                      </Col>
                    </Row>
                    <Row>
                      <Col lg="5">
                        <FormGroup>
                          <label
                            className="form-control-label"
                            htmlFor="input-email"
                          >
                            Phân loại sản phẩm
                          </label>
                          <Card>
                            <Select2
                              className="form-control"
                              name="category"
                              value={selectedCategory}
                              options={{
                                placeholder: "Select",
                              }}
                              data={categoryData.map((category) => ({
                                id: category.categoryId,
                                text: category.categoryName,
                              }))}
                              onChange={(event) =>
                                setSelectedCategory(event.target.value)
                              } // Update the selectedCategory state on change
                            />
                          </Card>
                        </FormGroup>
                      </Col>
                      <Col lg="5">
                        <FormGroup>
                          <label
                            className="form-control-label"
                            htmlFor="input-first-name"
                          >
                            Số lượng
                          </label>
                          <Input
                            name="unitInStock"
                            defaultValue=""
                            id="input-first-name"
                            placeholder="Quantity"
                            type="number"
                            value={UnitInStock}
                            onChange={(e) => setUnitInStock(e.target.value)}
                          />
                        </FormGroup>
                      </Col>
                    </Row>
                    <Row>
                      <Col lg="5">
                        <FormGroup>
                          <label
                            className="form-control-label"
                            htmlFor="input-last-name"
                          >
                            Giá sản phẩm
                          </label>
                          <Input
                            name="price"
                            defaultValue=""
                            id="input-last-name"
                            placeholder="Price"
                            type="number"
                            value={UnitPrice}
                            onChange={(e) => setUnitPrice(e.target.value)}
                          />
                        </FormGroup>
                      </Col>
                      <Col lg="5">
                        <FormGroup>
                          <label
                            className="form-control-label"
                            htmlFor="input-first-name"
                          >
                            Giảm giá
                          </label>
                          <Input
                            name="discount"
                            defaultValue=""
                            id="input-first-name"
                            placeholder="Discount"
                            type="number"
                            value={Discount}
                            onChange={(e) => setDiscount(e.target.value)}
                          />
                        </FormGroup>
                      </Col>
                    </Row>
                  </div>

                  <div className="pl-lg-4">
                    <label
                      className="form-control-label mb-4 ml-7"
                      htmlFor="input-address"
                    >
                      Tình trạng sản phẩm
                    </label>
                    <div>
                      {statusData.map((status) => (
                        <ButtonGroup
                          key={status.statusId}
                          className="btn-group-toggle btn-group-colors event-tag mb-4 ml-7"
                          data-toggle="buttons"
                        >
                          <i className="ni business_briefcase-24 mr-2">
                            {status.statusValue}
                          </i>
                          <Button
                            className={classnames("bg-info", {
                              active: status.statusId === selectedStatus,
                            })}
                            color=""
                            type="button"
                            onClick={() =>
                              handleStatusSelection(status.statusId)
                            }
                          />
                        </ButtonGroup>
                      ))}
                    </div>
                  </div>

                  <div className="pl-lg-4 mt-4">
                    <Row>
                      <Col md="12">
                        <FormGroup>
                          <label
                            className="form-control-label"
                            htmlFor="input-address"
                          >
                            Chất lượng sản phẩm
                          </label>
                          <div>
                            {Array.from({ length: UnitInStock }, (_, index) => (
                              <div key={index}>
                                <input
                                  type="number"
                                  value={qualityPercentages[index] || ""}
                                  onChange={(e) =>
                                    handleQualityChange(index, e.target.value)
                                  }
                                  placeholder={`Phần trăm chất lượng sản phẩm ${
                                    index + 1
                                  }`}
                                  required
                                />
                                {qualityPercentages[index] !== undefined && (
                                  <span>{qualityPercentages[index]}%</span>
                                )}
                              </div>
                            ))}
                          </div>
                        </FormGroup>
                      </Col>
                    </Row>
                  </div>

                  <div className="pl-lg-4 mt-4">
                    <Row>
                      <Col md="12">
                        <FormGroup>
                          <label
                            className="form-control-label"
                            htmlFor="input-address"
                          >
                            Đánh giá sản phẩm
                          </label>
                          <div className="input-slider-container">
                            <div className="input-slider" ref={sliderRef} />
                            <Row className="mt-3">
                              <Col xs="6">
                                <span
                                  className="range-slider-value"
                                  name="quality"
                                >
                                  {sliderValue}
                                </span>
                              </Col>
                            </Row>
                          </div>
                        </FormGroup>
                      </Col>
                    </Row>
                  </div>
                  <hr className="my-4" />

                  <h6 className="heading-small text-muted mb-4">
                    Giới thiệu sản phẩm
                  </h6>
                  <div className="pl-lg-4">
                    <FormGroup>
                      <label className="form-control-label">Giới thiệu</label>
                      <Input
                        defaultValue=""
                        name="description"
                        id="exampleFormControlTextarea2"
                        rows="3"
                        type="textarea"
                        value={Description}
                        onChange={(e) => setDescription(e.target.value)}
                      />
                    </FormGroup>
                  </div>
                  <div className="pl-lg-4">
                    <label
                      className="form-control-label mb-4"
                      htmlFor="input-address"
                    >
                      Chọn ảnh đại diện
                    </label>
                    <br />
                    <input
                      type="file"
                      className="custom-file-input"
                      id="upload-avatar"
                      onChange={handleFileChange}
                    />
                  </div>
                  <div className="pl-lg-4">
                    <label
                      className="form-control-label mb-4"
                      htmlFor="input-address"
                    >
                      Chọn các ảnh khác
                    </label>
                    <div
                      className="dropzone dropzone-multiple pl-lg-4"
                      id="dropzone-multiple"
                      {...getRootProps()}
                    >
                      <div className="fallback">
                        <div className="custom-file">
                          <input
                            className="image custom-file-input"
                            id="customFileUploadMultiple"
                            multiple="multiple"
                            type="file"
                            {...getInputProps()}
                          />
                          <label
                            className="custom-file-label"
                            htmlFor="customFileUploadMultiple"
                          >
                            Choose file
                          </label>
                        </div>
                      </div>
                      <ListGroup
                        className="dz-preview dz-preview-multiple list-group-lg"
                        flush
                      >
                        {selectedImages.map((file, index) => (
                          <ListGroupItem className="px-0" key={index}>
                            <Row className="align-items-center">
                              <Col className="col-auto">
                                <div className="avatar">
                                  <img
                                    alt="..."
                                    className="avatar-img rounded"
                                    src={URL.createObjectURL(file)}
                                  />
                                </div>
                              </Col>
                              <div className="col ml--3">
                                <h4 className="mb-1">{file.name}</h4>
                                <p className="small text-muted mb-0">
                                  {file.size} bytes
                                </p>
                              </div>
                              <Col className="col-auto">
                                <Button
                                  size="sm"
                                  color="danger"
                                  onClick={() =>
                                    setSelectedImages(
                                      selectedImages.filter(
                                        (_, i) => i !== index
                                      )
                                    )
                                  }
                                >
                                  <i className="fas fa-trash" />
                                </Button>
                              </Col>
                            </Row>
                          </ListGroupItem>
                        ))}
                      </ListGroup>
                    </div>
                  </div>
                  <Button className="ml-4 mt-4" color="primary" type="submit">
                    LƯU SẢN PHẨM
                  </Button>
                </Form>
              </CardBody>
            </Card>
          </Col>
        </Row>
      </Container>
    </>
  );
};

export default AddProduct;

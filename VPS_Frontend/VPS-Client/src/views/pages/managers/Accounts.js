import React, { useEffect, useRef, useState } from "react";
// javascript plugin that creates a sortable object from a dom object
import List from "list.js";
// reactstrap components
import {
  Card,
  CardHeader,
  CardFooter,
  Media,
  Pagination,
  PaginationItem,
  PaginationLink,
  Table,
  Container,
  Row,
  Col,
  Button,
  Badge,
} from "reactstrap";
// core components
import SimpleHeader from "../../../components/Headers/SimpleHeader.js";
import axios from "axios";
import { Link } from "react-router-dom/cjs/react-router-dom.min";

const Accounts = () => {
  const [currentPage, setCurrentPage] = useState(1);
  const [sortedColumn, setSortedColumn] = useState("");
  const [sortedDirection, setSortedDirection] = useState("");
  const itemsPerPage = 5;

  const thirdListRef = useRef(null);
  const [data, setData] = useState([]);

  const totalPages = Math.ceil(data.length / itemsPerPage);

  const startIndex = (currentPage - 1) * itemsPerPage;
  const endIndex = startIndex + itemsPerPage;
  const displayedData = data.slice(startIndex, endIndex);

  const fetchData = async () => {
    try {
      const response = await axios.get(
        "https://localhost:7050/api/Customer/list"
      );
      const sortedData = response.data.sort(
        (a, b) => b.customerId - a.customerId
      );

      setData(sortedData);
    } catch (error) {
      console.error("Error fetching data:", error);
    }
  };
  useEffect(() => {
    if (data.length > 0) {
      initializeList();
    }
    const totalPages = Math.ceil(data.length / itemsPerPage);
    if (currentPage > totalPages) {
      setCurrentPage(1);
    }
  }, [data, currentPage]);

  const initializeList = () => {
    new List(thirdListRef.current, {
      valueNames: ["fullName", "status", "phone", "address"],
      listClass: "list",
    });
  };
  useEffect(() => {
    if (data.length > 0) {
      initializeList();
    }
  }, [data]);
  useEffect(() => {
    fetchData();
  }, []);

  const handleSort = (column) => {
    let direction = "asc";
    if (column === sortedColumn && sortedDirection === "asc") {
      direction = "desc";
    }
    setSortedColumn(column);
    setSortedDirection(direction);
    sortData(column, direction);
    setCurrentPage(1); // Reset current page when sorting
  };

  const sortData = (column, direction) => {
    const sortedData = [...data];
    sortedData.sort((a, b) => {
      const valueA = getValueByColumn(a, column);
      const valueB = getValueByColumn(b, column);
      if (valueA < valueB) {
        return direction === "asc" ? -1 : 1;
      }
      if (valueA > valueB) {
        return direction === "asc" ? 1 : -1;
      }
      return 0;
    });
    setData(sortedData);
  };

  const getValueByColumn = (item, column) => {
    switch (column) {
      case "fullName":
        return item.fullName;
      case "status":
        return item.status;
      case "phone":
        return item.phone;
      case "address":
        return item.address;
      default:
        return "";
    }
  };

  return (
    <>
      <SimpleHeader name="Orders" parentName="Manager" />
      <Container className="mt--6" fluid>
        <Row>
          <div className="col">
            <Card>
              <Col className="mt-3 mb--4 text-md-right" lg="6" xs="5"></Col>
              <CardHeader className="bg-transparent border-0">
                <h3 className="text-white mb-0">Danh sách khách hàng</h3>
              </CardHeader>
              <div className="table-responsive" ref={thirdListRef}>
                <Table id="myTable" className="align-items-center table-flush">
                  <thead className="thead-light">
                    <tr>
                      <th>Id</th>
                      <th
                        className="sort"
                        data-sort="fullName"
                        scope="col"
                        onClick={() => handleSort("fullName")}
                      >
                        Tên người dùng
                      </th>
                      <th
                        className="sort"
                        data-sort="status"
                        scope="col"
                        onClick={() => handleSort("status")}
                      >
                        Tình trạng
                      </th>
                      <th
                        className="sort"
                        data-sort="phone"
                        scope="col"
                        onClick={() => handleSort("phone")}
                      >
                        Số điện thoại
                      </th>
                      <th
                        className="sort"
                        data-sort="address"
                        scope="col"
                        onClick={() => handleSort("address")}
                      >
                        Địa chỉ
                      </th>

                      <th scope="col" />
                    </tr>
                  </thead>
                  <tbody className="list">
                    {displayedData.map((customer) => (
                      <tr key={customer.customerId}>
                        <td className="id">#{customer.customerId}</td>
                        <th scope="row">
                          <Media className="align-items-center">
                            <a
                              className="avatar rounded-circle mr-3"
                              href="#pablo"
                              onClick={(e) => e.preventDefault()}
                            >
                              <img
                                alt="..."
                                // src={require("../../../../assets/img/theme/bootstrap.jpg")}
                              />
                            </a>
                            <Media>
                              <span className="customer mb-0 text-sm">
                                {customer.fullName}
                              </span>
                            </Media>
                          </Media>
                        </th>
                        <td>
                          <Badge color="" className="badge-dot mr-4">
                            <i
                              className={
                                customer.status.statusId === 1
                                  ? "bg-yellow"
                                  : customer.status.statusId === 2
                                  ? "bg-warning"
                                  : "bg-success"
                              }
                            />
                            <span className="status">
                              {customer.status.statusId === 1
                                ? "Chờ xác nhận"
                                : customer.status.statusId === 2
                                ? "Ngừng hoạt động"
                                : "Đã xác nhận"}
                            </span>
                          </Badge>
                        </td>
                        <td className="phone">{customer.phone}</td>
                        <td className="address">{customer.address}</td>
                        <td className="text-right">
                          {customer.status.statusId !== 3 ? (
                              <Link to={`/admin/account-confirm/${customer.customerId}`}>
                              <Button color="primary" size="sm">
                              Xác nhận
                            </Button>
                            </Link>
                          ) : (
                            ""
                          )}
                        </td>
                      </tr>
                    ))}
                  </tbody>
                </Table>
              </div>
              <CardFooter className="py-4">
                <nav aria-label="...">
                  <Pagination className="pagination justify-content-end mb-0">
                    <PaginationItem disabled={currentPage === 1}>
                      <PaginationLink
                        previous
                        onClick={() => setCurrentPage(currentPage - 1)}
                      />
                    </PaginationItem>
                    {Array.from({ length: totalPages }, (_, i) => i + 1).map(
                      (page) => (
                        <PaginationItem
                          key={page}
                          active={currentPage === page}
                        >
                          <PaginationLink onClick={() => setCurrentPage(page)}>
                            {page}
                          </PaginationLink>
                        </PaginationItem>
                      )
                    )}
                    <PaginationItem disabled={currentPage === totalPages}>
                      <PaginationLink
                        next
                        onClick={() => setCurrentPage(currentPage + 1)}
                      />
                    </PaginationItem>
                  </Pagination>
                </nav>
              </CardFooter>
            </Card>
          </div>
        </Row>
      </Container>
    </>
  );
};

export default Accounts;

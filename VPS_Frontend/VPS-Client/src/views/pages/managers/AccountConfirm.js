import React, { useEffect, useState } from "react";

// reactstrap components
import {
  Card,
  CardHeader,
  CardBody,
  CardImg,
  FormGroup,
  Form,
  Input,
  Container,
  Row,
  Col,
  Button,
} from "reactstrap";
import { Modal } from "react-bootstrap"; // Import the Modal component

// core components
import { useParams } from "react-router-dom/cjs/react-router-dom.min";
import axios from "axios";
// import "../../../../src/assets/css/account-confirm.css";
import NotificationAlert from "react-notification-alert";

const AccountConfirm = () => {
  const { customerId } = useParams();
  const [accountData, setAccountData] = useState(null);
  const [identityCard, setIdentityCard] = useState(null);
  const [showModal, setShowModal] = useState(false);
  const [selectedImage, setSelectedImage] = useState(null);
  const notificationAlertRef = React.useRef(null);

  const notifySuccess = () => {
    let options = {
      place: "tc",
      message: (
        <div className="alert-text">
          <span className="alert-title" data-notify="title">
            {" "}
            Thay đổi trạng thái tài khoản thành công
          </span>
        </div>
      ),
      type: "success",
      icon: "ni ni-bell-55",
      autoDismiss: 7,
    };
    notificationAlertRef.current.notificationAlert(options);
  };
  const notifyDanger = () => {
    let options = {
      place: "tc",
      message: (
        <div className="alert-text">
          <span className="alert-title" data-notify="title">
            {" "}
            Thay đổi trạng thái tài khoản thất bại!
          </span>
          <span data-notify="message">Vui lòng kiểm tra lại đường truyền</span>
        </div>
      ),
      type: "danger",
      icon: "ni ni-bell-55",
      autoDismiss: 7,
    };
    notificationAlertRef.current.notificationAlert(options);
  };

  const handleImageClick = (imageURL) => {
    setSelectedImage(imageURL);
    setShowModal(true);
  };

  useEffect(() => {
    const fetchAccountData = async () => {
      try {
        const response = await axios.get(
          `https://localhost:7050/api/Customer/${customerId}`
        );
        setAccountData(response.data);
      } catch (error) {
        console.error("Error fetching account data:", error);
      }
    };

    fetchAccountData();
  }, [customerId, accountData]);

  useEffect(() => {
    const fetchIdentityCard = async () => {
      try {
        const response = await axios.get(
          `https://localhost:7050/api/Customer/GetIdentityCardByCustomerId/${customerId}`
        );
        setIdentityCard(response.data);
      } catch (error) {
        console.error("Error fetching account data:", error);
      }
    };

    fetchIdentityCard();
  }, [customerId]);

  const handleIdentityConfirm = async () => {
    try {
      const response = await axios.get(
        `https://localhost:7050/api/Customer/${customerId}`
      );
      const existingData = response.data;
      const updatedData = { ...existingData, statusId: 3, isValid: true };

      await axios.put(
        `https://localhost:7050/api/Customer/${customerId}`,
        updatedData
      );
      console.log(existingData);
      notifySuccess();
    } catch (error) {
      notifyDanger();
      console.error("Error updating status:", error);
    }
  };

  return (
    <>
      <NotificationAlert ref={notificationAlertRef} />
      <Modal show={showModal} onHide={() => setShowModal(false)}>
        {selectedImage && (
          <img
            src={selectedImage}
            alt="Zoomed Image"
            style={{ width: "140%", height: "auto" }}
          />
        )}
      </Modal>
      <div
        className="header pb-6 d-flex align-items-center"
        style={{
          minHeight: "300px",
          backgroundImage:
            'url("' +
            require("../../../assets/img/theme/profile-cover.jpg") +
            '")',
          backgroundSize: "cover",
          backgroundPosition: "center top",
        }}
      >
        <span className="mask bg-gradient-info opacity-8" />

        <Container className="d-flex align-items-center" fluid>
          <Row>
            <Col lg="12" md="12">
              <h2 className="display-2 text-white">
                Thông tin của {accountData?.fullName}
              </h2>
              <p className="text-white mt-0 mb-5">
                Đây là trang hồ sơ của tài khoản {accountData?.fullName}. Bạn có
                thể thấy được những thông tin và trạng thái hoạt động của nhân
                viên.
              </p>
            </Col>
          </Row>
        </Container>
      </div>
      <Container className="mt--6" fluid>
        <Row>
          <Col className="order-xl-1" xl="12">
            <Card>
              <CardHeader>
                <Row className="align-items-center">
                  <Col xs="8">
                    <h3 className="mb-0">Xác nhận khách hàng</h3>
                  </Col>
                  <Col className="text-right" xs="4">
                    <Button
                      color="primary"
                      href="#pablo"
                      onClick= {handleIdentityConfirm}
                      size="xs"
                    >
                      Xác nhận
                    </Button>
                  </Col>
                </Row>
              </CardHeader>
              <CardBody>
                <Form>
                  <h6 className="heading-small text-muted mb-4">
                    Thông tin tài khoản
                  </h6>
                  <div className="pl-lg-4">
                    <Row>
                      <Col lg="6">
                        <FormGroup>
                          <label
                            className="form-control-label"
                            htmlFor="input-first-name"
                          >
                            Họ và tên
                          </label>
                          <Input
                            defaultValue={accountData?.fullName}
                            id="input-first-name"
                            placeholder="First name"
                            type="text"
                            disabled
                          />
                        </FormGroup>
                      </Col>
                      <Col lg="6">
                        <FormGroup>
                          <label
                            className="form-control-label"
                            htmlFor="input-last-name"
                          >
                            Số điện thoại
                          </label>
                          <Input
                            defaultValue={accountData?.phone}
                            id="input-last-name"
                            type="text"
                            disabled
                          />
                        </FormGroup>
                      </Col>
                    </Row>
                  </div>

                  <div className="pl-lg-4">
                    <Row>
                      <Col md="12">
                        <FormGroup>
                          <label
                            className="form-control-label"
                            htmlFor="input-address"
                          >
                            Địa chỉ
                          </label>
                          <Input
                            defaultValue={accountData?.address}
                            id="input-address"
                            placeholder="Home Address"
                            type="text"
                            disabled
                          />
                        </FormGroup>
                      </Col>
                    </Row>
                  </div>
                  <hr className="my-4" />

                  <div className="pl-lg-4">
                    <Row>
                      <Col lg="6">
                        <label
                          className="form-control-label"
                          htmlFor="input-address"
                        >
                          Mặt trước CMND
                        </label>{" "}
                        <br />
                        <img
                          onClick={() =>
                            handleImageClick(identityCard?.identityCardImage)
                          }
                          style={{
                            width: "350px", // Đặt chiều rộng ảnh là 100%
                            height: "350px", // Giữ tỷ lệ khung hình
                          }}
                          src={identityCard?.identityCardImage}
                          alt="Mặt trước CMND"
                        />
                      </Col>
                      <Col lg="6">
                        <label
                          className="form-control-label"
                          htmlFor="input-address"
                        >
                          Mặt sau CMND
                        </label>{" "}
                        <br />
                        <img
                          onClick={() =>
                            handleImageClick(identityCard?.backIdentityCard)
                          }
                          style={{
                            width: "350px", // Đặt chiều rộng ảnh là 100%
                            height: "350px", // Giữ tỷ lệ khung hình
                          }}
                          src={identityCard?.backIdentityCard}
                          alt="Mặt sau CMND"
                        />
                      </Col>
                    </Row>
                  </div>
                </Form>
              </CardBody>
            </Card>
          </Col>
        </Row>
      </Container>
    </>
  );
};

export default AccountConfirm;

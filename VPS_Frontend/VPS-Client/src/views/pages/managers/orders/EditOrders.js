import axios from "axios";
import classnames from "classnames";
import React, { useEffect, useState } from "react";
import ReactDatetime from "react-datetime";
// import { useParams } from "react-router-dom/cjs/react-router-dom.min";
// reactstrap components
import {
  ButtonGroup,
  Button,
  Card,
  CardHeader,
  CardBody,
  //   ListGroupItem,
  //   ListGroup,
  //   CardImg,
  FormGroup,
  Form,
  Input,
  Container,
  Row,
  Col,
} from "reactstrap";
import Select2 from "react-select2-wrapper";
import { useParams } from "react-router-dom/cjs/react-router-dom.min";
import NotificationAlert from "react-notification-alert";
import { useHistory } from "react-router-dom";

const EditOrders = () => {
  const history = useHistory();
  const { orderId, customerId } = useParams();
  const [orderData, setOrderData] = useState([]);
  const [customerData, setCustomerData] = useState([]);
  const [saleData, setSaleData] = useState([]);
  const [isFormSubmitted, setIsFormSubmitted] = useState(false);
  const [selectedCustomer, setSelectedCustomer] = useState("");
  const [selectedSale, setSelectedSale] = useState("");
  const notificationAlertRef = React.useRef(null);
  const [identityCard, setIdentityCard] = useState(null);
  const [startDate, setStartDate] = React.useState(null);
  const [endDate, setEndDate] = React.useState(null);

  const getClassNameReactDatetimeDays = (date) => {
    if (startDate && endDate) {
    }
    if (startDate && endDate && startDate._d + "" !== endDate._d + "") {
      if (
        new Date(endDate._d + "") > new Date(date._d + "") &&
        new Date(startDate._d + "") < new Date(date._d + "")
      ) {
        return " middle-date";
      }
      if (endDate._d + "" === date._d + "") {
        return " end-date";
      }
      if (startDate._d + "" === date._d + "") {
        return " start-date";
      }
    }
    return "";
  };

  const formatDate = (dateString) => {
    const dateOptions = {
      weekday: "long",
      day: "numeric",
      month: "long",
      year: "numeric",
    };

    const timeOptions = {
      hour: "numeric",
      minute: "numeric",
      hour12: true, // Use 12-hour clock format (AM/PM)
    };

    // Convert the date to the desired format
    const date = new Date(dateString);
    const formattedDate = date.toLocaleDateString("vi-VN", dateOptions);
    const formattedTime = date.toLocaleTimeString("vi-VN", timeOptions);

    // Return the formatted date string
    return `${formattedDate} vào ${formattedTime}`;
  };

  useEffect(() => {
    const fetchIdentityCard = async () => {
      try {
        const response = await axios.get(
          `https://localhost:7050/api/Customer/GetIdentityCardByCustomerId/${customerId}`
        );
        setIdentityCard(response.data);
      } catch (error) {
        console.error("Error fetching account data:", error);
      }
    };

    fetchIdentityCard();
  }, [customerId]);

  useEffect(() => {
    const fetchData = async () => {
      try {
        const orderResponse = await axios.get(
          `https://localhost:7050/api/Order/${orderId}`
        );
        setOrderData(orderResponse.data);
        console.log(orderResponse.data);
      } catch (error) {
        console.error("Error fetching data:", error);
      }
    };

    fetchData();
  }, [orderId]);

  const notifySuccess = () => {
    let options = {
      place: "tc",
      message: (
        <div className="alert-text">
          <span className="alert-title" data-notify="title">
            {" "}
            Thêm sản phẩm thành công
          </span>
        </div>
      ),
      type: "success",
      icon: "ni ni-bell-55",
      autoDismiss: 7,
    };
    notificationAlertRef.current.notificationAlert(options);
  };

  const notifyDanger = () => {
    let options = {
      place: "tc",
      message: (
        <div className="alert-text">
          <span className="alert-title" data-notify="title">
            {" "}
            Thêm sản phẩm thất bại
          </span>
          <span data-notify="message">
            Vui lòng kiểm tra các trường đã điền
          </span>
        </div>
      ),
      type: "danger",
      icon: "ni ni-bell-55",
      autoDismiss: 7,
    };
    notificationAlertRef.current.notificationAlert(options);
  };

  useEffect(() => {
    const fetchCustomerData = async () => {
      try {
        const response = await axios.get(
          "https://localhost:7050/api/Customer/list"
        );
        setCustomerData(response.data); // <-- Update this line
      } catch (error) {
        console.error("Error fetching customer data:", error);
      }
    };

    const fetchSaleData = async () => {
      try {
        const response = await axios.get(
          "https://localhost:7050/api/Sale/list"
        );
        setSaleData(response.data);
      } catch (error) {
        console.error("Error fetching sale data:", error);
      }
    };

    fetchCustomerData();
    fetchSaleData();
  }, []);

  const handleFormSubmit = async (e) => {
    e.preventDefault();
    setIsFormSubmitted(true);
    const form = e.target;

    const currentDate = new Date();
    const year = currentDate.getFullYear();
    const month = String(currentDate.getMonth() + 1).padStart(2, "0");
    const day = String(currentDate.getDate()).padStart(2, "0");
    const hours = String(currentDate.getHours()).padStart(2, "0");
    const minutes = String(currentDate.getMinutes()).padStart(2, "0");
    const seconds = String(currentDate.getSeconds()).padStart(2, "0");
    const milliseconds = String(currentDate.getMilliseconds()).padStart(3, "0");
    const formattedDate = `${year}-${month}-${day}T${hours}:${minutes}:${seconds}.${milliseconds}Z`;

    if (form.checkValidity()) {
      try {
        const response1 =  await axios.get(`https://localhost:7050/api/Order/${orderId}`)
        const updateOrder = {
          ...response1.data,
          customerId: e.target.elements.customer.value,
          saleId: e.target.elements.sale.value,
          // price: e.target.elements.price.value,
          managerConfirm: formattedDate, // Convert the value to ISO 8601 format
          statusId: 2,
        };

        const response = await axios.put(
          `https://localhost:7050/api/Order/${orderId}`,
          updateOrder
        );

        console.log(response.data);
        form.reset();
        setIsFormSubmitted(false);
        notifySuccess();
        history.push("/admin/orders");
      } catch (error) {
        notifyDanger();
        console.error("Error adding order:", error);
      }
    }
  };

  return (
    <>
      <div className="rna-wrapper">
        <NotificationAlert ref={notificationAlertRef} />
      </div>
      <Container className="mt--12" fluid>
        <Row className="mt-4 ml-8">
          <Col className="order-xl-1 mt-4" xl="10">
            <Card>
              <CardHeader>
                <Row className="align-items-center">
                  <Col xs="8">
                    <h3 className="mb-0">Chỉnh sửa đơn hàng</h3>
                  </Col>
                </Row>
              </CardHeader>
              <CardBody>
                <Form onSubmit={handleFormSubmit}>
                  <h6 className="heading-small text-muted mb-4">
                    Thông tin sản phẩm
                  </h6>
                  <div className="pl-lg-4 ml-7 mt-6 mb-6">
                    <Row>
                      <Col lg="11">
                        <FormGroup>
                          <label
                            className="form-control-label"
                            htmlFor="input-email"
                          >
                            Chọn khách hàng
                          </label>
                          <Card>
                            <Select2
                              className="form-control"
                              name="customer"
                              value={
                                selectedCustomer ||
                                (orderData?.customerId
                                  ? orderData.customerId.toString()
                                  : "")
                              }
                              options={{
                                placeholder: "Select",
                              }}
                              data={customerData.map((customer) => ({
                                id: customer.customerId.toString(),
                                text: customer.fullName,
                              }))}
                              onChange={(event) =>
                                setSelectedCustomer(event.target.value)
                              } // Update the selectedCustomer state on change
                            />
                          </Card>
                        </FormGroup>
                      </Col>
                      {/* <Col lg="5">
                        <FormGroup>
                          <label
                            className="form-control-label"
                            htmlFor="input-first-name"
                          >
                            Hóa đơn
                          </label>
                          <Input
                            name="price"
                            defaultValue={orderData.price}
                            id="input-first-name"
                            placeholder="Price"
                            type="text"
                          />
                        </FormGroup>
                      </Col> */}
                    </Row>
                    <Row>
                      <Col lg="5">
                        <FormGroup>
                          <label
                            className="form-control-label"
                            htmlFor="input-email"
                          >
                            Chọn nhân viên
                          </label>
                          <Card>
                            <Select2
                              className="form-control"
                              name="sale"
                              value={
                                selectedSale ||
                                (orderData?.saleId
                                  ? orderData.saleId.toString()
                                  : "")
                              }
                              options={{
                                placeholder: "Select",
                              }}
                              data={saleData.map((sale) => ({
                                id: sale.saleId.toString(),
                                text: sale.fullName,
                              }))}
                              onChange={(event) =>
                                setSelectedSale(event.target.value)
                              } // Update the selectedSale state on change
                            />
                          </Card>
                        </FormGroup>
                      </Col>
                      <Col xs={12} sm={6}>
                        <label className=" form-control-label">
                          Ngày đặt hàng
                        </label>
                        <FormGroup>
                          <ReactDatetime
                            inputProps={{
                              name: "orderDate",
                              placeholder: "Date Picker Here",
                              readOnly: true,
                              disabled: true, // Add the disabled prop to disable the input field
                            }}
                            value={
                              orderData.orderDate
                                ? formatDate(
                                    new Date(orderData.orderDate).toISOString()
                                  )
                                : formatDate(new Date().toISOString())
                            }
                            timeFormat={false}
                            renderDay={(props, currentDate, selectedDate) => {
                              let classes = props.className;
                              classes +=
                                getClassNameReactDatetimeDays(currentDate);
                              return (
                                <td {...props} className={classes}>
                                  {currentDate.date()}
                                </td>
                              );
                            }}
                          />
                        </FormGroup>
                      </Col>
                    </Row>
                    <Row>
                      <Col lg="6">
                        <label
                          className="form-control-label"
                          htmlFor="input-address"
                        >
                          Mặt trước CMND
                        </label>{" "}
                        <br />
                        <img
                          // onClick={() =>
                          //   handleImageClick(identityCard?.identityCardImage)
                          // }
                          style={{
                            width: "350px", // Đặt chiều rộng ảnh là 100%
                            height: "350px", // Giữ tỷ lệ khung hình
                          }}
                          src={identityCard?.identityCardImage}
                          alt="Mặt trước CMND"
                        />
                      </Col>
                      <Col lg="6">
                        <label
                          className="form-control-label"
                          htmlFor="input-address"
                        >
                          Ảnh chụp xác thực từ đơn hàng
                        </label>{" "}
                        <br />
                        <img
                          style={{
                            width: "350px", // Đặt chiều rộng ảnh là 100%
                            height: "350px", // Giữ tỷ lệ khung hình
                          }}
                          src={orderData?.currentImage}
                          alt="Ảnh chụp xác thực từ đơn hàng"
                        />
                      </Col>
                    </Row>
                  </div>
                  <hr className="my-4" />
                  <Button className="ml-4 mt-4" color="primary" type="submit">
                    LƯU ĐƠN HÀNG
                  </Button>
                </Form>
              </CardBody>
            </Card>
          </Col>
        </Row>
      </Container>
    </>
  );
};

export default EditOrders;

import axios from "axios";
import classnames from "classnames";
import React, { useEffect, useState } from "react";
// import { useParams } from "react-router-dom/cjs/react-router-dom.min";
// reactstrap components
import {
  Button,
  Card,
  CardHeader,
  CardBody,
  FormGroup,
  Form,
  Input,
  Container,
  Row,
  Col,
} from "reactstrap";
import Select2 from "react-select2-wrapper";
import NotificationAlert from "react-notification-alert";
import { useHistory } from "react-router-dom";

const AddOrder = () => {
  const history = useHistory();

  const [customerData, setCustomerData] = useState([]);
  const [saleData, setSaleData] = useState([]);
  const [productData, setProductData] = useState([]);
  // const [selectedStatus, setSelectedStatus] = useState("");
  const [isFormSubmitted, setIsFormSubmitted] = useState(false);
  const [selectedCustomer, setSelectedCustomer] = useState("");
  const [selectedSale, setSelectedSale] = useState("");
  const notificationAlertRef = React.useRef(null);
  useEffect(() => {
    // Set the current date as the default value for "Ngày đặt hàng" field
    const currentDate = new Date();
    const year = currentDate.getFullYear();
    const month = String(currentDate.getMonth() + 1).padStart(2, "0");
    const day = String(currentDate.getDate()).padStart(2, "0");

    const formattedDate = `${year}-${month}-${day}`;
    const orderDateInput = document.getElementById("input-current-date");
    orderDateInput.value = formattedDate;
  }, []);

  const notifySuccess = () => {
    let options = {
      place: "tc",
      message: (
        <div className="alert-text">
          <span className="alert-title" data-notify="title">
            {" "}
            Thêm đơn hàng thành công
          </span>
        </div>
      ),
      type: "success",
      icon: "ni ni-bell-55",
      autoDismiss: 7,
    };
    notificationAlertRef.current.notificationAlert(options);
  };

  const notifyDanger = () => {
    let options = {
      place: "tc",
      message: (
        <div className="alert-text">
          <span className="alert-title" data-notify="title">
            {" "}
            Thêm đơn hàng thất bại
          </span>
          <span data-notify="message">
            Vui lòng kiểm tra các trường đã điền
          </span>
        </div>
      ),
      type: "danger",
      icon: "ni ni-bell-55",
      autoDismiss: 7,
    };
    notificationAlertRef.current.notificationAlert(options);
  };
  useEffect(() => {
    const fetchCustomerData = async () => {
      try {
        const response = await axios.get(
          "https://localhost:7050/api/Customer/list"
        );
        setCustomerData(response.data);
      } catch (error) {
        console.error("Error fetching customer data:", error);
      }
    };

    const fetchSaleData = async () => {
      try {
        const response = await axios.get(
          "https://localhost:7050/api/Sale/list"
        );
        setSaleData(response.data);
        console.log(response.data);
      } catch (error) {
        console.error("Error fetching sale data:", error);
      }
    };

    const fetchProductData = async () => {
      try {
        const response = await axios.get("https://localhost:7050/api/Product");
        setProductData(response.data);
        console.log(response.data);
      } catch (error) {
        console.error("Error fetching sale data:", error);
      }
    };

    fetchProductData();
    fetchCustomerData();
    fetchSaleData();
  }, []);

  const handleFormSubmit = async (e) => {
    e.preventDefault();
    setIsFormSubmitted(true);
    const form = e.target;
    const currentDate = new Date();
    const year = currentDate.getFullYear();
    const month = String(currentDate.getMonth() + 1).padStart(2, "0");
    const day = String(currentDate.getDate()).padStart(2, "0");
    const hours = String(currentDate.getHours()).padStart(2, "0");
    const minutes = String(currentDate.getMinutes()).padStart(2, "0");
    const seconds = String(currentDate.getSeconds()).padStart(2, "0");
    const milliseconds = String(currentDate.getMilliseconds()).padStart(3, "0");

    const formattedDate = `${year}-${month}-${day}T${hours}:${minutes}:${seconds}.${milliseconds}Z`;
    if (form.checkValidity()) {
      try {
        const order = {
          customerId: e.target.elements.customer.value,
          saleId: e.target.elements.sale.value,
          orderDate: formattedDate, // Convert the value to ISO 8601 format
          statusId: 2,
          managerConfirm: formattedDate,
        };

        const response = await axios.post(
          "https://localhost:7050/api/Order",
          order
        );

        console.log(response.data);
        form.reset();
        setIsFormSubmitted(false);
        history.push("/admin/add-orderDetails");
        notifySuccess();
      } catch (error) {
        notifyDanger();
        console.error("Error adding order:", error);
      }
    }
  };

  return (
    <>
      <div className="rna-wrapper">
        <NotificationAlert ref={notificationAlertRef} />
      </div>
      <Container className="mt--12" fluid>
        <Row className="mt-4 ml-8">
          <Col className="order-xl-1 mt-4" xl="10">
            <Card>
              <CardHeader>
                <Row className="align-items-center">
                  <Col xs="8">
                    <h3 className="mb-0">Thêm đơn hàng</h3>
                  </Col>
                </Row>
              </CardHeader>
              <CardBody>
                <Form onSubmit={handleFormSubmit}>
                  <h6 className="heading-small text-muted mb-4">
                    Thông tin đơn hàng
                  </h6>
                  <div className="pl-lg-4 ml-7">
                    <Row>
                      <Col lg="10">
                        <FormGroup>
                          <label
                            className="form-control-label"
                            htmlFor="input-email"
                          >
                            Chọn khách hàng
                          </label>
                          <Card>
                            <Select2
                              className="form-control"
                              name="customer"
                              value={selectedCustomer}
                              options={{
                                placeholder: "Select",
                              }}
                              data={customerData.map((customer) => ({
                                id: customer.customerId,
                                text: customer.fullName,
                              }))}
                              onChange={(event) =>
                                setSelectedCustomer(event.target.value)
                              } // Update the selectedCategory state on change
                            />
                          </Card>
                        </FormGroup>
                      </Col>
                    </Row>
                    <Row>
                      <Col lg="5">
                        <FormGroup>
                          <label
                            className="form-control-label"
                            htmlFor="input-email"
                          >
                            Chọn nhân viên
                          </label>
                          <Card>
                            <Select2
                              className="form-control"
                              name="sale"
                              value={selectedSale}
                              options={{
                                placeholder: "Select",
                              }}
                              data={saleData.map((sale) => ({
                                id: sale.saleId,
                                text: sale.fullName,
                              }))}
                              onChange={(event) =>
                                setSelectedSale(event.target.value)
                              }
                            />
                          </Card>
                        </FormGroup>
                      </Col>
                      <Col lg="5">
                        <FormGroup>
                          <label
                            className="form-control-label"
                            htmlFor="input-current-date"
                          >
                            Ngày đặt hàng
                          </label>
                          <Input
                            name="orderDate"
                            defaultValue=""
                            id="input-current-date"
                            type="date"
                            readOnly
                          />
                        </FormGroup>
                      </Col>
                    </Row>
                  </div>
                  <hr className="my-4" />
                  <Button className="ml-4 mt-4" color="primary" type="submit">
                    LƯU ĐƠN HÀNG
                  </Button>
                </Form>
              </CardBody>
            </Card>
          </Col>
        </Row>
      </Container>
    </>
  );
};

export default AddOrder;

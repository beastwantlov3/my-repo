import axios from "axios";
import React, { useEffect, useRef, useState } from "react";
import {
  Button,
  Card,
  CardHeader,
  CardBody,
  FormGroup,
  Form,
  Input,
  Container,
  Row,
  Col,
  Modal,
} from "reactstrap";
import Select2 from "react-select2-wrapper";
import NotificationAlert from "react-notification-alert";
import { useHistory } from "react-router-dom";

const AddOrderDetail = () => {
  const history = useHistory();

  const [productData, setProductData] = useState([]);
  const [selectedProduct, setSelectedProduct] = useState("");
  const [selectedProductPrice, setSelectedProductPrice] = useState(0);
  const notificationAlertRef = React.useRef(null);
  // const formRef = useRef(null);

  useEffect(() => {
    const fetchProductData = async () => {
      try {
        const response = await axios.get("https://localhost:7050/api/Product");
        setProductData(response.data);
        console.log(response.data);
      } catch (error) {
        console.error("Error fetching product data:", error);
      }
    };

    fetchProductData();
  }, []);

  const [showModal, setShowModal] = useState(false);

  // Function to handle the "yes" button in the modal
  const handleModalYes = () => {
    // Hide the modal
    setShowModal(false);
    setSelectedProduct("");
    setSelectedProductPrice(0);
  };

  const handleProductSelection = async (productId) => {
    setSelectedProduct(productId);

    // Fetch the selected product's details, including the unitPrice
    try {
      const response = await axios.get(
        `https://localhost:7050/api/Product/GetProductById/${productId}`
      );
      const product = response.data;
      setSelectedProductPrice(product.unitPrice);
    } catch (error) {
      console.error("Error fetching product details:", error);
      setSelectedProductPrice(0);
    }
  };

  const handleFormSubmit = async (e) => {
    e.preventDefault();

    const form = e.target;
    if (form.checkValidity()) {
      try {
        const ordersResponse = await axios.get(
          "https://localhost:7050/api/Order/list"
        );
        const orders = ordersResponse.data;

        // Find the last order and extract the orderId
        const lastOrder = orders[orders.length - 1];
        const orderId = lastOrder.orderId;

        const unitPrice = selectedProductPrice;
        const quantity = parseInt(form.elements.quantity.value);
        const discount = parseFloat(form.elements.discount.value);
        const price = unitPrice * quantity * (1 - discount);

        const ordersById = await axios.get(
          `https://localhost:7050/api/Order/${orderId}`
        );

        const order = {
          ...ordersById.data,
          price: price,
          shipCost: price * 0.03
        };

        // Update the order with the calculated price
        await axios.put(`https://localhost:7050/api/Order/${orderId}`, order);

        const orderDetail = {
          orderId: orderId,
          productId: selectedProduct,
          quantity: form.elements.quantity.value,
          unitPrice: form.elements.price.value,
          discount: form.elements.discount.value,
        };

        const response = await axios.post(
          "https://localhost:7050/api/OrderDetail",
          orderDetail
        );

        console.log(response.data);
        form.reset()
        setShowModal(true);
        notifySuccess()
      } catch (error) {
        notifyDanger();
        console.error("Error adding order detail:", error);
      }
    }
  };

  const notifySuccess = () => {
    let options = {
      place: "tc",
      message: (
        <div className="alert-text">
          <span className="alert-title" data-notify="title">
            {" "}
            Thêm đơn hàng thành công
          </span>
        </div>
      ),
      type: "success",
      icon: "ni ni-bell-55",
      autoDismiss: 7,
    };
    notificationAlertRef.current.notificationAlert(options);
  };

  const notifyDanger = () => {
    let options = {
      place: "tc",
      message: (
        <div className="alert-text">
          <span className="alert-title" data-notify="title">
            {" "}
            Thêm đơn hàng thất bại
          </span>
          <span data-notify="message">
            Vui lòng kiểm tra các trường đã điền
          </span>
        </div>
      ),
      type: "danger",
      icon: "ni ni-bell-55",
      autoDismiss: 7,
    };
    notificationAlertRef.current.notificationAlert(options);
  };

  const handleModalButtonClick = (shouldContinue) => {
    setShowModal(false);
    if (!shouldContinue) {
      // Redirect to "admin/orders" when the user clicks "No"
      history.push("/admin/orders");
    } else {
      // Clear form fields to add more OrderDetail entries when the user clicks "Yes"
      setSelectedProduct("");
      setSelectedProductPrice(0);
    }
  };

  return (
    <>
      <div className="rna-wrapper">
        <NotificationAlert ref={notificationAlertRef} />
      </div>

      <Container className="mt--12" fluid>
        <Row className="mt-4 ml-8">
          <Col className="order-xl-1 mt-4" xl="10">
            <Card>
              <CardHeader>
                <Row className="align-items-center">
                  <Col xs="8">
                    <h3 className="mb-0">Thêm đơn hàng</h3>
                  </Col>
                </Row>
              </CardHeader>
              <CardBody>
                <Form onSubmit={handleFormSubmit}>
                  <h6 className="heading-small text-muted mb-4">
                    Thông tin sản phẩm
                  </h6>
                  <div className="pl-lg-4 ml-7">
                    <Row>
                      <Col lg="5">
                        <FormGroup>
                          <label
                            className="form-control-label"
                            htmlFor="input-email"
                          >
                            Chọn sản phẩm
                          </label>
                          <Card>
                            <Select2
                              className="form-control"
                              name="product"
                              value={selectedProduct}
                              options={{
                                placeholder: "Select",
                              }}
                              data={productData.map((product) => ({
                                id: product.productId,
                                text: product.productName,
                              }))}
                              onChange={(event) =>
                                handleProductSelection(event.target.value)
                              }
                            />
                          </Card>
                        </FormGroup>
                      </Col>
                      <Col lg="5">
                        <FormGroup>
                          <label
                            className="form-control-label"
                            htmlFor="input-first-name"
                          >
                            Giá sản phẩm
                          </label>
                          <Input
                            name="price"
                            defaultValue=""
                            id="input-first-name"
                            placeholder="Price"
                            type="number"
                            value={selectedProductPrice}
                            disabled
                          />
                        </FormGroup>
                      </Col>
                    </Row>
                    <Row>
                      <Col lg="5">
                        <FormGroup>
                          <label
                            className="form-control-label"
                            htmlFor="input-first-name"
                          >
                            Số lượng
                          </label>
                          <Input
                            name="quantity"
                            defaultValue=""
                            id="input-first-name"
                            placeholder="Quantity"
                            type="number"
                          />
                        </FormGroup>
                      </Col>
                      <Col lg="5">
                        <FormGroup>
                          <label
                            className="form-control-label"
                            htmlFor="input-first-name"
                          >
                            Giảm giá
                          </label>
                          <Input
                            name="discount"
                            defaultValue=""
                            id="input-first-name"
                            placeholder="Price"
                            type="number"
                            step="0.05"
                            max="1"
                          />
                        </FormGroup>
                      </Col>
                    </Row>
                  </div>
                  <hr className="my-4" />
                  <Button className="ml-4 mt-4" color="primary" type="submit">
                    LƯU ĐƠN HÀNG
                  </Button>
                </Form>
                <Modal
                  isOpen={showModal}
                  toggle={() => setShowModal(false)}
                  onExited={() => handleModalButtonClick(true)} // Call handleModalButtonClick with true for "Yes"
                >
                  <div className="modal-header">
                    <h5 className="modal-title" id="modal-title-default">
                      Bạn có muốn tiếp tục?
                    </h5>
                    <button
                      type="button"
                      className="close"
                      data-dismiss="modal"
                      aria-label="Close"
                      onClick={() => setShowModal(false)}
                    >
                      <span aria-hidden="true">×</span>
                    </button>
                  </div>
                  <div className="modal-body">
                    Nếu bạn muốn tiếp tục thêm đơn hàng mới, nhấn "Yes".
                  </div>
                  <div className="modal-footer">
                    <Button
                      color="secondary"
                      type="button"
                      onClick={() => handleModalButtonClick(true)} // Pass false for "No"
                    >
                      Yes
                    </Button>
                    <Button
                      color="primary"
                      type="button"
                      onClick={() => handleModalButtonClick(false)} // Pass false for "No"
                    >
                      No
                    </Button>
                  </div>
                </Modal>
              </CardBody>
            </Card>
          </Col>
        </Row>
      </Container>
    </>
  );
};

export default AddOrderDetail;

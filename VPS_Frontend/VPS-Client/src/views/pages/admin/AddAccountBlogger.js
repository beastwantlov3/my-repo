import classnames from "classnames";
import React, { useEffect, useState } from "react";
// reactstrap components
import {
  Button,
  Card,
  CardHeader,
  CardBody,
  FormGroup,
  Form,
  Input,
  Container,
  Row,
  Col,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
} from "reactstrap";
// core components
import NotificationAlert from "react-notification-alert";
import Dropzone from "dropzone";
import axios from "axios";
import { useHistory } from "react-router-dom";

Dropzone.autoDiscover = false;

const AddAccountBlogger = () => {
  const history = useHistory();
  const [isFormSubmitted, setIsFormSubmitted] = useState(false);
  const notificationAlertRef = React.useRef(null);
  const [yourName, setyourName] = React.useState(false);
  const [password, setPassword] = React.useState(false);

  const notifySuccess = () => {
    let options = {
      place: "tc",
      message: (
        <div className="alert-text">
          <span className="alert-title" data-notify="title">
            {" "}
            Thêm tài khoản thành công
          </span>
        </div>
      ),
      type: "success",
      icon: "ni ni-bell-55",
      autoDismiss: 7,
    };
    notificationAlertRef.current.notificationAlert(options);
  };

  const notifyDanger = () => {
    let options = {
      place: "tc",
      message: (
        <div className="alert-text">
          <span className="alert-title" data-notify="title">
            {" "}
            Thêm tài khoản thất bại
          </span>
          <span data-notify="message">
            Vui lòng kiểm tra các trường đã điền
          </span>
        </div>
      ),
      type: "danger",
      icon: "ni ni-bell-55",
      autoDismiss: 7,
    };
    notificationAlertRef.current.notificationAlert(options);
  };

  const handleFormSubmit = async (e) => {
    e.preventDefault();
    setIsFormSubmitted(true);
    const form = e.target;
    if (form.checkValidity()) {
      try {
        const currentDate = new Date();
        const year = currentDate.getFullYear();
        const month = String(currentDate.getMonth() + 1).padStart(2, "0");
        const day = String(currentDate.getDate()).padStart(2, "0");
        const hours = String(currentDate.getHours()).padStart(2, "0");
        const minutes = String(currentDate.getMinutes()).padStart(2, "0");
        const seconds = String(currentDate.getSeconds()).padStart(2, "0");
        const milliseconds = String(currentDate.getMilliseconds()).padStart(
          3,
          "0"
        );

        const formattedDate = `${year}-${month}-${day}T${hours}:${minutes}:${seconds}.${milliseconds}Z`;
        const acccountResponse = await axios.get(
          "https://localhost:7050/api/Blogger/list"
        );
        const accounts = acccountResponse.data;
        const lastOrder = accounts[accounts.length - 1];
        const accountId = lastOrder.bloggerId;

        // Build the product object with the form data
        const account = {
          userName: e.target.elements.username.value,
          password: e.target.elements.password.value, // Get the category value from the defaultValue prop of Select2
          roleId: 2,
          email: e.target.elements.username.value + accountId + '@gmail.com',
          status: 1,
          bloggerId: accountId,
          createdDate: formattedDate,
          // Add other fields as needed
        };

        // Make a POST request to the API to add the product
        const response = await axios.post(
          "https://localhost:7050/api/Account",
          account
        );

        // Handle the response
        console.log(response.data);
        form.reset();
        setIsFormSubmitted(false);
        // Show success alert
        notifySuccess();
        history.push("/admin/bloggers");

        // Reset the form fields or redirect to another page
      } catch (error) {
        notifyDanger();
        console.error("Error adding product:", error);
      }
    }
  };
  return (
    <>
      <div className="rna-wrapper">
        <NotificationAlert ref={notificationAlertRef} />
      </div>
      <Container className="mt--12 " fluid>
        <Row className="mt-4 ml-8">
          <Col className="order-xl-1 mt-4" xl="10">
            <Card>
              <CardHeader>
                <Row className="align-items-center">
                  <Col xs="8">
                    <h3 className="mb-0">Thêm tài khoản người viết bài </h3>
                  </Col>
                </Row>
              </CardHeader>
              <CardBody>
                <div className="d-flex justify-content-center">
                  <Form onSubmit={handleFormSubmit}>
                    <div className="pl-lg-4">
                      <Col lg="30">
                        <FormGroup>
                          <label
                            className="form-control-label"
                            htmlFor="input-username"
                          >
                            Tên tài khoản
                          </label>
                          <InputGroup
                            className={classnames("input-group-merge", {
                              focused: yourName,
                            })}
                            style={{ width: "400px" }}
                          >
                            <InputGroupAddon addonType="prepend">
                              <InputGroupText>
                                <i className="fas fa-user" />
                              </InputGroupText>
                            </InputGroupAddon>
                            <Input
                              name="username"
                              placeholder="Username"
                              type="text"
                              onFocus={(e) => setyourName(true)}
                              onBlur={(e) => setyourName(false)}
                              required
                            />
                          </InputGroup>
                        </FormGroup>
                      </Col>
                      <Col lg="30">
                        <FormGroup>
                          <label
                            className="form-control-label"
                            htmlFor="input-first-name"
                          >
                            Mật khẩu
                          </label>
                          <InputGroup
                            className={classnames("input-group-merge", {
                              focused: password,
                            })}
                          >
                            <InputGroupAddon addonType="prepend">
                              <InputGroupText>
                                <i className="ni ni-lock-circle-open" />
                              </InputGroupText>
                            </InputGroupAddon>
                            <Input
                              name="password"
                              placeholder="Password"
                              type="text"
                              onFocus={(e) => setPassword(true)}
                              onBlur={(e) => setPassword(false)}
                              required
                            />
                          </InputGroup>
                        </FormGroup>
                      </Col>
                    </div>
                    <Button className="ml-8 mt-4" color="primary" type="submit">
                      THÊM TÀI KHOẢN
                    </Button>
                  </Form>
                </div>
              </CardBody>
            </Card>
          </Col>
        </Row>
      </Container>
    </>
  );
};

export default AddAccountBlogger;

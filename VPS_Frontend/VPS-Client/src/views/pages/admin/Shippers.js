import React, { useEffect, useRef, useState } from "react";
// react plugin that prints a given react component
import ReactToPrint from "react-to-print";
// react component for creating dynamic tables
import BootstrapTable from "react-bootstrap-table-next";
import paginationFactory from "react-bootstrap-table2-paginator";
import ToolkitProvider, { Search } from "react-bootstrap-table2-toolkit";
// react component used to create sweet alerts
import ReactBSAlert from "react-bootstrap-sweetalert";
// reactstrap components
import {
  Button,
  ButtonGroup,
  Card,
  CardHeader,
  Container,
  Row,
  Col,
  UncontrolledTooltip,
  Badge,
  //   Modal,
  //   ModalHeader,
  //   ModalBody,
  //   ModalFooter,
} from "reactstrap";
// core components
import SimpleHeader from "../../../components/Headers/SimpleHeader.js";
import axios from "axios";
import NotificationAlert from "react-notification-alert";
import { Link } from "react-router-dom/cjs/react-router-dom.min";

const pagination = paginationFactory({
  page: 1,
  alwaysShowAllBtns: true,
  showTotal: true,
  withFirstAndLast: false,
  sizePerPageRenderer: ({ options, currSizePerPage, onSizePerPageChange }) => (
    <div className="dataTables_length" id="datatable-basic_length">
      <label>
        Show{" "}
        {
          <select
            name="datatable-basic_length"
            aria-controls="datatable-basic"
            className="form-control form-control-sm"
            onChange={(e) => onSizePerPageChange(e.target.value)}
          >
            <option value="10">10</option>
            <option value="25">25</option>
            <option value="50">50</option>
            <option value="100">100</option>
          </select>
        }{" "}
        entries.
      </label>
    </div>
  ),
});

const { SearchBar } = Search;

const Shippers = () => {
  const [alert, setAlert] = useState(null);
  const componentRef = useRef(null);
  const [data, setData] = useState([]);
  const notificationAlertRef = React.useRef(null);

  const notifySuccess = () => {
    let options = {
      place: "tc",
      message: (
        <div className="alert-text">
          <span className="alert-title" data-notify="title">
            {" "}
            Thay đổi trạng thái tài khoản thành công
          </span>
        </div>
      ),
      type: "success",
      icon: "ni ni-bell-55",
      autoDismiss: 7,
    };
    notificationAlertRef.current.notificationAlert(options);
  };
  const notifyDanger = () => {
    let options = {
      place: "tc",
      message: (
        <div className="alert-text">
          <span className="alert-title" data-notify="title">
            {" "}
            Thay đổi trạng thái tài khoản thất bại!
          </span>
          <span data-notify="message">Vui lòng kiểm tra lại đường truyền</span>
        </div>
      ),
      type: "danger",
      icon: "ni ni-bell-55",
      autoDismiss: 7,
    };
    notificationAlertRef.current.notificationAlert(options);
  };

  // this function will copy to clipboard an entire table,
  // so you can paste it inside an excel or csv file
  const copyToClipboardAsTable = (el) => {
    var body = document.body,
      range,
      sel;
    if (document.createRange && window.getSelection) {
      range = document.createRange();
      sel = window.getSelection();
      sel.removeAllRanges();
      try {
        range.selectNodeContents(el);
        sel.addRange(range);
      } catch (e) {
        range.selectNode(el);
        sel.addRange(range);
      }
      document.execCommand("copy");
    } else if (body.createTextRange) {
      range = body.createTextRange();
      range.moveToElementText(el);
      range.select();
      range.execCommand("Copy");
    }
    setAlert(
      <ReactBSAlert
        success
        style={{ display: "block", marginTop: "-100px" }}
        title="Good job!"
        onConfirm={() => setAlert(null)}
        onCancel={() => setAlert(null)}
        confirmBtnBsStyle="info"
        btnSize=""
      >
        Copied to clipboard!
      </ReactBSAlert>
    );
  };
  useEffect(() => {
    fetchData();
  }, []);
  const fetchData = async () => {
    try {
      const response = await axios.get(
        "https://localhost:7050/api/Shipper/list"
      );
      setData(response.data);
    } catch (error) {
      console.error("Error fetching data:", error);
    }
  };

  const handleToggleStatus = async (shipperId, statusId) => {
    try {
      const response = await axios.get(
        `https://localhost:7050/api/Shipper/${shipperId}`
      );
      const existingData = response.data;
      const updatedData = { ...existingData, statusId: statusId === 1 ? 2 : 1 };

      await axios.put(
        `https://localhost:7050/api/Shipper/${shipperId}`,
        updatedData
      );
      notifySuccess();
      fetchData();
    } catch (error) {
      notifyDanger();
      console.error("Error updating status:", error);
    }
  };

  const dataLabelOnOff = (statusId) => {
    return statusId === 1 ? ["Bật", "Tắt"] : ["Bật", "Tắt"];
  };

  const actionsFormatter = (cell, row) => {
    const [dataLabelOn, dataLabelOff] = dataLabelOnOff(row.statusId);

    return (
      <label className="custom-toggle custom-toggle-danger mr-1">
        <input
          defaultChecked={row.statusId === 1}
          type="checkbox"
          onChange={() => handleToggleStatus(row.shipperId, row.statusId)}
        />
        <span
          className="custom-toggle-slider rounded-circle"
          data-label-off={dataLabelOff}
          data-label-on={dataLabelOn}
        />
      </label>
    );
  };

  const dateFormatter = (cell, row) => {
    const dateObj = new Date(row.dob);
    const formattedDob = dateObj.toLocaleDateString("en-CA");
    return formattedDob;
  };

  return (
    <>
      {alert}
      <NotificationAlert ref={notificationAlertRef} />
      <SimpleHeader name="Shippers" parentName="Admin" />
      <Container className="mt--6" fluid>
        <Row>
          <div className="col">
            <Card>
            <Col className="mt-3 mb--4 text-md-right" lg="6" xs="5">
                <Link className="text-shadow" to="/admin/add-shipper">
                  <Button color="default" size="sm">
                    Thêm tài khoản
                  </Button>
                </Link>

                {/* <Button className="btn-neutral" color="default" size="sm">
                  Filters
                </Button> */}
              </Col>
              <CardHeader>
                <h3 className="mb-0">Danh sách người giao hàng</h3>
              </CardHeader>
              <ToolkitProvider
                data={data}
                keyField="shipperId"
                columns={[
                  {
                    dataField: "shipperId",
                    text: "Id",
                    sort: true,
                    formatter: (cell) => {
                      return `# ${cell}`;
                    },
                  },
                  {
                    dataField: "fullName",
                    text: "Họ tên",
                    sort: true,
                  },
                  // {
                  //   dataField: "email",
                  //   text: "Email",
                  //   sort: true,
                  // },
                  {
                    dataField: "phone",
                    text: "Số điện thoại",
                    sort: true,
                  },
                  {
                    dataField: "address",
                    text: "Địa chỉ",
                    sort: true,
                  },
                  {
                    dataField: "dob",
                    text: "Ngày sinh",
                    sort: true,
                    formatter: dateFormatter,
                  },
                  {
                    dataField: "status",
                    text: "Tình trạng",
                    sort: true,
                    formatter: (cell, row) => {
                      const className =
                        row.statusId === 1 ? "bg-success" : "bg-warning";
                      const statusText =
                        row.statusId === 1 ? "Hoạt động" : "Ngừng hoạt động";

                      return (
                        <Badge color="" className="badge-dot mr-4">
                          <i className={className} />
                          <span className="status">{statusText}</span>
                        </Badge>
                      );
                    },
                  },
                  {
                    dataField: "actions",
                    text: "Bật/tắt hoạt động",
                    formatter: actionsFormatter,
                  },
                ]}
                search
              >
                {(props) => (
                  <div className="py-4 table-responsive">
                    <Container fluid>
                      <Row>
                        <Col xs={12} sm={6}>
                          <ButtonGroup>
                            <Button
                              className="buttons-copy buttons-html5"
                              color="default"
                              size="sm"
                              id="copy-tooltip"
                              onClick={() =>
                                copyToClipboardAsTable(
                                  document.getElementById("react-bs-table")
                                )
                              }
                            >
                              <span>Copy</span>
                            </Button>
                            <ReactToPrint
                              trigger={() => (
                                <Button
                                  color="default"
                                  size="sm"
                                  className="buttons-copy buttons-html5"
                                  id="print-tooltip"
                                >
                                  Print
                                </Button>
                              )}
                              content={() => componentRef.current}
                            />
                          </ButtonGroup>
                          <UncontrolledTooltip
                            placement="top"
                            target="print-tooltip"
                          >
                            This will open a print page with the visible rows of
                            the table.
                          </UncontrolledTooltip>
                          <UncontrolledTooltip
                            placement="top"
                            target="copy-tooltip"
                          >
                            This will copy to your clipboard the visible rows of
                            the table.
                          </UncontrolledTooltip>
                        </Col>
                        <Col xs={12} sm={6}>
                          <div
                            id="datatable-basic_filter"
                            className="dataTables_filter px-4 pb-1 float-right"
                          >
                            <label>
                              Tìm kiếm:
                              <SearchBar
                                className="form-control-sm"
                                placeholder=""
                                {...props.searchProps}
                              />
                            </label>
                          </div>
                        </Col>
                      </Row>
                    </Container>
                    <BootstrapTable
                      ref={componentRef}
                      {...props.baseProps}
                      bootstrap4={true}
                      pagination={pagination}
                      bordered={false}
                      id="react-bs-table"
                    />
                  </div>
                )}
              </ToolkitProvider>
            </Card>
          </div>
        </Row>
      </Container>
    </>
  );
};

export default Shippers;

import classnames from "classnames";
import React, { useEffect, useState } from "react";
// reactstrap components
import {
  Button,
  Card,
  CardHeader,
  CardBody,
  ListGroupItem,
  ListGroup,
  FormGroup,
  Form,
  Input,
  Container,
  Row,
  Col,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
} from "reactstrap";
// core components
import NotificationAlert from "react-notification-alert";
import Dropzone from "dropzone";
import axios from "axios";
import { useHistory } from "react-router-dom";
import ReactDatetime from "react-datetime";
import moment from "moment";

Dropzone.autoDiscover = false;

const AddBlogger = () => {
  const history = useHistory();
  const [isFormSubmitted, setIsFormSubmitted] = useState(false);
  const notificationAlertRef = React.useRef(null);
  const [location, setlocation] = React.useState(false);
  const [yourName, setyourName] = React.useState(false);
  const [phoneNumber, setphoneNumber] = React.useState(false);
  const [dob, setDob] = useState("");

  
  useEffect(() => {
    let currentSingleFile = undefined;
    new Dropzone(document.getElementById("dropzone-single"), {
      url: "https://example.com/upload",
      thumbnailWidth: null,
      thumbnailHeight: null,
      previewsContainer:
        document.getElementsByClassName("dz-preview-single")[0],
      previewTemplate:
        document.getElementsByClassName("dz-preview-single")[0].innerHTML,
      maxFiles: 1, // Set maxFiles to 1 to allow only a single file to be uploaded
      acceptedFiles: "image/*", // Set acceptedFiles to limit file type to images
      init: function () {
        this.on("addedfile", function (file) {
          if (currentSingleFile) {
            this.removeFile(currentSingleFile);
          }
          currentSingleFile = file;
        });
      },
    });
    document.getElementsByClassName("dz-preview-single")[0].innerHTML = "";
  }, []);

  const notifySuccess = () => {
    let options = {
      place: "tc",
      message: (
        <div className="alert-text">
          <span className="alert-title" data-notify="title">
            {" "}
            Thêm tài khoản thành công
          </span>
        </div>
      ),
      type: "success",
      icon: "ni ni-bell-55",
      autoDismiss: 7,
    };
    notificationAlertRef.current.notificationAlert(options);
  };

  const notifyDanger = () => {
    let options = {
      place: "tc",
      message: (
        <div className="alert-text">
          <span className="alert-title" data-notify="title">
            {" "}
            Thêm tài khoản thất bại
          </span>
          <span data-notify="message">
            Vui lòng kiểm tra các trường đã điền
          </span>
        </div>
      ),
      type: "danger",
      icon: "ni ni-bell-55",
      autoDismiss: 7,
    };
    notificationAlertRef.current.notificationAlert(options);
  };

  const handleFormSubmit = async (e) => {
    e.preventDefault();
    setIsFormSubmitted(true);
    const form = e.target;
    if (form.checkValidity()) {
      try {
        // Build the product object with the form data
        const blogger = {
          fullName: e.target.elements.name.value,
          phone: e.target.elements.phone.value, // Get the category value from the defaultValue prop of Select2
          address: e.target.elements.address.value,
          dob: dob,
          statusId: 1,
          // avatar: e.target.elements.image.value,
          // Add other fields as needed
        };

        // Make a POST request to the API to add the product
        const response = await axios.post(
          "https://localhost:7050/api/Blogger",
          blogger
        );

        // Handle the response
        console.log(response.data);
        form.reset();
        setIsFormSubmitted(false);
        // Show success alert
        notifySuccess();
        history.push("/admin/add-account-blogger");

        // Reset the form fields or redirect to another page
      } catch (error) {
        notifyDanger();
        console.error("Error adding product:", error);
      }
    }
  };
  const handleDateChange = (selectedDate) => {
    if (moment.isMoment(selectedDate)) {
      // Check if selectedDate is an instance of moment
      // Format the selectedDate to YYYY-MM-DD and store it in the state
      setDob(selectedDate.format("YYYY-MM-DD"));
    } else {
      // If selectedDate is not an instance of moment (null or undefined), clear the dob state
      setDob("");
    }
  };
  return (
    <>
      <div className="rna-wrapper">
        <NotificationAlert ref={notificationAlertRef} />
      </div>
      <Container className="mt--12 " fluid>
        <Row className="mt-4 ml-8">
          <Col className="order-xl-1 mt-4" xl="10">
            <Card>
              <CardHeader>
                <Row className="align-items-center">
                  <Col xs="8">
                    <h3 className="mb-0">Thêm nhân viên đăng bài </h3>
                  </Col>
                </Row>
              </CardHeader>
              <CardBody>
                <Form onSubmit={handleFormSubmit}>
                  <h6 className="heading-small text-muted mb-4">
                    Thông tin tài khoản
                  </h6>
                  <div className="pl-lg-4 ml-7">
                    <Row>
                      <Col lg="5">
                        <FormGroup>
                          <label
                            className="form-control-label"
                            htmlFor="input-username"
                          >
                            Tên người viết bài
                          </label>
                          <InputGroup
                            className={classnames("input-group-merge", {
                              focused: yourName,
                            })}
                          >
                            <InputGroupAddon addonType="prepend">
                              <InputGroupText>
                                <i className="fas fa-user" />
                              </InputGroupText>
                            </InputGroupAddon>
                            <Input
                              name="name"
                              placeholder="Your name"
                              type="text"
                              onFocus={(e) => setyourName(true)}
                              onBlur={(e) => setyourName(false)}
                              required
                            />
                          </InputGroup>
                        </FormGroup>
                      </Col>
                      <Col lg="5">
                        <FormGroup>
                          <label
                            className="form-control-label"
                            htmlFor="input-first-name"
                          >
                            Số điện thoại
                          </label>
                          <InputGroup
                            className={classnames("input-group-merge", {
                              focused: phoneNumber,
                            })}
                          >
                            <InputGroupAddon addonType="prepend">
                              <InputGroupText>
                                <i className="fas fa-globe-americas" />
                              </InputGroupText>
                            </InputGroupAddon>
                            <Input
                              name="phone"
                              placeholder="Phone number"
                              type="text"
                              onFocus={(e) => setphoneNumber(true)}
                              onBlur={(e) => setphoneNumber(false)}
                              required
                            />
                            <InputGroupAddon addonType="append">
                              <InputGroupText>
                                <i className="fas fa-phone" />
                              </InputGroupText>
                            </InputGroupAddon>
                          </InputGroup>
                        </FormGroup>
                      </Col>
                    </Row>
                    <Row>
                      <Col md="5">
                        <FormGroup>
                          <label
                            className="form-control-label"
                            htmlFor="input-first-name"
                          >
                            Địa chỉ
                          </label>
                          <InputGroup
                            className={classnames("input-group-merge", {
                              focused: location,
                            })}
                          >
                            <Input
                              name="address"
                              placeholder="Location"
                              type="text"
                              onFocus={(e) => setlocation(true)}
                              onBlur={(e) => setlocation(false)}
                              required
                            />
                            <InputGroupAddon addonType="append">
                              <InputGroupText>
                                <i className="fas fa-map-marker" />
                              </InputGroupText>
                            </InputGroupAddon>
                          </InputGroup>
                        </FormGroup>
                      </Col>
                      <Col lg="5">
                        <FormGroup>
                          <label
                            className="form-control-label"
                            htmlFor="input-last-name"
                          >
                            Ngày sinh
                          </label>
                          <ReactDatetime
                            inputProps={{
                              placeholder: "Date Picker Here",
                            }}
                            timeFormat={false}
                            onChange={handleDateChange} // Use the updated event handler
                          />
                        </FormGroup>
                      </Col>
                    </Row>
                  </div>

                  <div className="pl-lg-4">
                    <label
                      className="form-control-label mb-4"
                      htmlFor="input-address"
                    >
                      Chọn ảnh đại diện
                    </label>
                    <div
                      className="dropzone dropzone-single pl-lg-4"
                      id="dropzone-single"
                    >
                      <div className="fallback">
                        <div className="custom-file">
                          <input
                            className="image custom-file-input"
                            id="customFileUploadSingle"
                            type="file"
                          />
                          <label
                            className="custom-file-label"
                            htmlFor="customFileUploadSingle"
                          >
                            Choose file
                          </label>
                        </div>
                      </div>
                      <ListGroup
                        className="dz-preview dz-preview-single list-group-lg"
                        flush
                      >
                        <ListGroupItem className="px-0">
                          <Row className="align-items-center">
                            <Col className="col-auto">
                              <div className="avatar">
                                <img
                                  alt="..."
                                  className="avatar-img rounded"
                                  data-dz-thumbnail
                                />
                              </div>
                            </Col>
                            <div className="col ml--3">
                              <h4 className="mb-1" data-dz-name>
                                ...
                              </h4>
                              <p className="small text-muted mb-0" data-dz-size>
                                ...
                              </p>
                            </div>
                            <Col className="col-auto">
                              <Button size="sm" color="danger" data-dz-remove>
                                <i className="fas fa-trash" />
                              </Button>
                            </Col>
                          </Row>
                        </ListGroupItem>
                      </ListGroup>
                    </div>
                  </div>
                  <Button className="ml-4 mt-4" color="primary" type="submit">
                    LƯU TÀI KHOẢN
                  </Button>
                </Form>
              </CardBody>
            </Card>
          </Col>
        </Row>
      </Container>
    </>
  );
};

export default AddBlogger;

import React, { useEffect, useState } from "react";

// reactstrap components
import {
  Button,
  Card,
  CardHeader,
  CardBody,
  CardImg,
  // CardTitle,
  FormGroup,
  Form,
  Input,
  // ListGroupItem,
  // ListGroup,
  // Progress,
  Container,
  Row,
  Col,
} from "reactstrap";
// core components
import { useParams } from "react-router-dom/cjs/react-router-dom.min";
import axios from "axios";

const Profile = () => {
  const { accountId } = useParams();
  const [accountData, setAccountData] = useState([]);
  const [saleData, setSaleData] = useState(null);
  const [shipperData, setShipperData] = useState(null);

  useEffect(() => {
    const fetchAccountData = async () => {
      try {
        const response = await axios.get(
          `https://localhost:7050/api/Account/${accountId}`
        );
        setAccountData(response.data);
      } catch (error) {
        console.error("Error fetching account data:", error);
      }
    };

    const fetchAdditionalData = async () => {
        const { roleId, saleId, shipperId } = accountData;

        switch (roleId) {
          case 1:
            // Fetch sale data if saleId exists
            if (saleId) {
              try {
                const response = await axios.get(
                  `https://localhost:7050/api/Sale/${saleId}`
                );
                setSaleData(response.data);
                console.log(response.data);
              } catch (error) {
                console.error("Error fetching sale data:", error);
              }
            }
            break;
          case 2:
            // Fetch shipper data if shipperId exists
            if (shipperId) {
              try {
                const response = await axios.get(
                  `https://localhost:7050/api/Shipper/${shipperId}`
                );
                setShipperData(response.data);
                console.log(response.data);
              } catch (error) {
                console.error("Error fetching shipper data:", error);
              }
            }
            break;
          default:
            break;
        }
    };

    fetchAccountData();
    fetchAdditionalData();
  }, [accountId, accountData]);
  const fullName = shipperData ? shipperData.fullName : "Loading...";

  return (
    <>
      <div
        className="header pb-6 d-flex align-items-center"
        style={{
          minHeight: "400px",
          backgroundImage:
            'url("' + require("../../../assets/img/theme/profile-cover.jpg") + '")',
          backgroundSize: "cover",
          backgroundPosition: "center top",
        }}
      >
        <span className="mask bg-gradient-info opacity-8" />

        <Container className="d-flex align-items-center" fluid>
          <Row>
            <Col lg="7" md="10">
              <h2 className="display-2 text-white">Thông tin của {accountData.shipperId}</h2>
              <p className="text-white mt-0 mb-5">
              Đây là trang hồ sơ của tài khoản {accountData.fullName}. Bạn có thể thấy được những thông tin
               và trạng thái hoạt động của nhân viên.
              </p>
            </Col>
          </Row>
        </Container>
      </div>
      <Container className="mt--6" fluid>
        <Row>
          <Col className="order-xl-2" xl="4">
            <Card className="card-profile">
              <CardImg
                alt="..."
                src={require("../../../assets/img/theme/img-1-1000x600.jpg")}
                top
              />
              <Row className="justify-content-center">
                <Col className="order-lg-2" lg="3">
                  <div className="card-profile-image">
                    <a href="#pablo" onClick={(e) => e.preventDefault()}>
                      <img
                        alt="..."
                        className="rounded-circle"
                        src={require("../../../assets/img/theme/team-4.jpg")}
                      />
                    </a>
                  </div>
                </Col>
              </Row>
              <CardBody className="pt-0">
                <div className="text-center mt-6">
                  <h5 className="h3">
                    Jessica Jones
                  </h5>
                  <div className="h5 font-weight-300">
                    <i className="ni location_pin mr-2" />
                    Bucharest, Romania
                  </div>
                  <div className="h5 mt-4">
                    <i className="ni business_briefcase-24 mr-2" />
                    Solution Manager - Creative Tim Officer
                  </div>
                  <div>
                    <i className="ni education_hat mr-2" />
                    University of Computer Science
                  </div>
                </div>
              </CardBody>
            </Card>
          </Col>
          <Col className="order-xl-1" xl="8">
            <Card>
              <CardHeader>
                <Row className="align-items-center">
                  <Col xs="8">
                    <h3 className="mb-0">Tài khoản</h3>
                  </Col>
                  {/* <Col className="text-right" xs="4">
                    <Button
                      color="primary"
                      href="#pablo"
                      onClick={(e) => e.preventDefault()}
                      size="sm"
                    >
                      Delete
                    </Button>
                  </Col> */}
                </Row>
              </CardHeader>
              <CardBody>
                <Form>
                  <h6 className="heading-small text-muted mb-4">
                    Thông tin tài khoản
                  </h6>
                  <div className="pl-lg-4">
                    <Row>
                      <Col lg="6">
                        <FormGroup>
                          <label
                            className="form-control-label"
                            htmlFor="input-username"
                          >
                            Tài khoản
                          </label>
                          <Input
                            defaultValue="lucky.jesse"
                            id="input-username"
                            placeholder="Username"
                            type="text"
                          />
                        </FormGroup>
                      </Col>
                      <Col lg="6">
                        <FormGroup>
                          <label
                            className="form-control-label"
                            htmlFor="input-email"
                          >
                            Địa chỉ Email
                          </label>
                          <Input
                            id="input-email"
                            placeholder="jesse@example.com"
                            type="email"
                          />
                        </FormGroup>
                      </Col>
                    </Row>
                    <Row>
                      <Col lg="6">
                        <FormGroup>
                          <label
                            className="form-control-label"
                            htmlFor="input-first-name"
                          >
                            Họ và tên
                          </label>
                          <Input
                            defaultValue={fullName}
                            id="input-first-name"
                            placeholder="First name"
                            type="text"
                          />
                        </FormGroup>
                      </Col>
                      <Col lg="6">
                        <FormGroup>
                          <label
                            className="form-control-label"
                            htmlFor="input-last-name"
                          >
                            Số điện thoại
                          </label>
                          <Input
                            defaultValue="Jesse"
                            id="input-last-name"
                            placeholder="Last name"
                            type="text"
                          />
                        </FormGroup>
                      </Col>
                    </Row>
                  </div>
                  <div className="pl-lg-4">
                    <Row>
                      <Col md="12">
                        <FormGroup>
                          <label
                            className="form-control-label"
                            htmlFor="input-address"
                          >
                            Địa chỉ
                          </label>
                          <Input
                            defaultValue="Bld Mihail Kogalniceanu, nr. 8 Bl 1, Sc 1, Ap 09"
                            id="input-address"
                            placeholder="Home Address"
                            type="text"
                          />
                        </FormGroup>
                      </Col>
                    </Row>
                  </div>
                  <hr className="my-4" />

                  <h6 className="heading-small text-muted mb-4">About me</h6>
                  <div className="pl-lg-4">
                    <FormGroup>
                      <label className="form-control-label">About Me</label>
                      <Input
                        placeholder="A few words about you ..."
                        rows="4"
                        type="textarea"
                        defaultValue="A beautiful premium dashboard for Bootstrap 4."
                      />
                    </FormGroup>
                  </div>
                </Form>
              </CardBody>
            </Card>
          </Col>
        </Row>
      </Container>
    </>
  );
};

export default Profile;

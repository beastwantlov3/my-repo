import React, { useEffect, useRef, useState } from "react";
// node.js library that concatenates classes (strings)
// javascipt plugin for creating charts
import Chart from "chart.js";
// react plugin used to create charts
import { Line } from "react-chartjs-2";
// reactstrap components
import {
  Button,
  Card,
  CardHeader,
  CardBody,
  Container,
  Row,
  Col,
} from "reactstrap";

// core components
import CardsHeader from "../../../components/Headers/CardsHeader.js";
import List from "list.js";

import { chartOptions, parseOptions } from "../../../variables/charts.js";
import axios from "axios";
import { Link } from "react-router-dom/cjs/react-router-dom";

function DashboardAdmin() {
  const firstListRef = useRef(null);
  const [chartData, setChartData] = useState({
    labels: [],
    datasets: [
      {
        label: "Quantity",
        data: [],
        borderColor: "#5e72e4",
        pointRadius: 0,
        pointHoverRadius: 0,
        borderWidth: 3,
      },
    ],
  });

  useEffect(() => {
    initializeListSaler();
  }, []);

  const initializeListSaler = () => {
    new List(firstListRef.current, {
      valueNames: ["id", "name", "phone", "status", "address", "dob"],
      listClass: "list",
    });
  };

  const monthNames = [
    "Jan",
    "Feb",
    "Mar",
    "Apr",
    "May",
    "Jun",
    "Jul",
    "Aug",
    "Sep",
    "Oct",
    "Nov",
    "Dec",
  ];
  useEffect(() => {
    // Fetch data from the API and update the chart data state
    const fetchData = async () => {
      try {
        const accountData = [];
        for (let i = 1; i <= 12; i++) {
          const response = await axios.get(
            `https://localhost:7050/api/Account/taikhoangthang${i}`
          );
          const data = response.data; // Replace this with the actual data received from the API
          accountData.push(data);
        }
        // Update the chart data state with the received data
        setChartData({
          labels: monthNames,
          datasets: [
            {
              label: "Quantity",
              data: accountData, // Put the single data value into the first element of the data array
              borderColor: "#5e72e4",
              pointRadius: 0,
              pointHoverRadius: 0,
              borderWidth: 3,
            },
          ],
        });
      } catch (error) {
        console.error("Error fetching data:", error);
      }
    };

    fetchData();
  }, []);

  // Make sure to parse the chart options
  if (window.Chart) {
    parseOptions(Chart, chartOptions());
  }

  return (
    <>
      <CardsHeader name="Dashboard" parentName="Admin" />
      <Container className="mt--6" fluid>
        <Row>
          <Col xl="12">
            <Card className="bg-default">
              <CardHeader className="bg-transparent">
                <Row className="align-items-center">
                  <div className="col">
                    <h6 className="text-light text-uppercase ls-1 mb-1">
                      Số lượng
                    </h6>
                    <h5 className="h3 text-white mb-0">Tài khoản đăng ký</h5>
                  </div>
                </Row>
              </CardHeader>
              <CardBody>
                <div className="chart">
                  <Line
                    data={chartData}
                    options={chartOptions().defaults}
                    id="chart-sales"
                    className="chart-canvas"
                  />
                </div>
              </CardBody>
            </Card>
          </Col>
        </Row>
        <Row className="ml-8 mt-4 mb-4">
          <Col xl="2">
            <Link to={`/admin/salers`}>
              <Button
                color="primary"
                size="lg"
                type="button"
              >
                Quản lý nhân viên bán hàng
              </Button>
            </Link>
          </Col>
          <Col xl="2">
            <Link to={`/admin/shippers`}>
              <Button
                color="primary"
                size="lg"
                type="button"
              >
                Quản lý nhân viên giao hàng
              </Button>
            </Link>
          </Col>
          <Col xl="2">
            <Link to={`/admin/suppliers`}>
              <Button
                color="primary"
                size="lg"
                type="button"
              >
                Quản lý nhà cung cấp
              </Button>
            </Link>
          </Col>
          <Col xl="2">
            <Link to={`/admin/bloggers`}>
              <Button
                color="primary"
                size="lg"
                type="button"
              >
                Quản lý nhân viên đăng bài
              </Button>
            </Link>
          </Col>
          <Col xl="2">
            <Link to={`/admin/customers`}>
              <Button
                color="primary"
                size="lg"
                type="button"
              >
                Quản lý khách hàng
              </Button>
            </Link>
          </Col>
        </Row>
      </Container>
    </>
  );
}

export default DashboardAdmin;

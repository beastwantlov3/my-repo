import React, { useState, useEffect, useRef, useCallback } from "react";
import axios from "axios";
import SimpleHeader from "../../../components/Headers/SimpleHeader.js";
import cogoToast from "cogo-toast";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faEye, faEyeSlash } from "@fortawesome/free-solid-svg-icons";
import {
  Button,
  Card,
  CardHeader,
  CardBody,
  CardImg,
  // CardTitle,
  FormGroup,
  Form,
  Input,
  // ListGroupItem,
  // ListGroup,
  // Progress,
  Container,
  Row,
  Col,
} from "reactstrap";

function Profile() {
  const [fullName, setFullName] = useState("");
  const [phone, setPhone] = useState("");
  const [address, setAddress] = useState("");
  const [dob, setDob] = useState("");
  const [profile, setProfile] = useState("");
  const [userData, setUserData] = useState(null);
  const [avatar, setAvatar] = useState("");
  const [selectedFile, setSelectedFile] = useState(null);
  const videoRef = useRef(null);
  const [imageData, setImageData] = useState("");
  const handleFileChange = (event) => {
    setSelectedFile(event.target.files[0]);
  };

  //Khai báo đổi mật khẩu
  const [currentPassword, setCurrentPassword] = useState("");
  const [newPassword, setNewPassword] = useState("");
  const [confirmPassword, setConfirmPassword] = useState("");
  const [showCurrentPassword, setShowCurrentPassword] = useState(false);
  const [showNewPassword, setShowNewPassword] = useState(false);
  const [showConfirmPassword, setShowConfirmPassword] = useState(false);

  //Hiển thị lỗi đổi mật khẩu
  const [currentPasswordError, setCurrentPasswordError] = useState("");
  const [newPasswordError, setNewPasswordError] = useState("");
  const [confirmPasswordError, setConfirmPasswordError] = useState("");

  //Hiển thị mật khẩu
  const handleCurrentPasswordToggle = () => {
    setShowCurrentPassword(!showCurrentPassword);
  };
  const handleNewPasswordToggle = () => {
    setShowNewPassword(!showNewPassword);
  };
  const handleConfirmPasswordToggle = () => {
    setShowConfirmPassword(!showConfirmPassword);
  };

  //state button camera, chụp
  const [showCaptureButton, setShowCaptureButton] = useState(true);
  const [showSaveButton, setShowSaveButton] = useState(false);
  //mở camera
  const handleCapture = async () => {
    const constraints = { video: true };

    try {
      const stream = await navigator.mediaDevices.getUserMedia(constraints);
      videoRef.current.srcObject = stream;
      setShowCaptureButton(false);
      setShowSaveButton(true);
    } catch (err) {
      console.error("Error accessing camera:", err);
    }
  };

  //Chụp ảnh
  const handleTakePhoto = () => {
    const canvas = document.createElement("canvas");
    const video = videoRef.current;
    canvas.width = video.videoWidth;
    canvas.height = video.videoHeight;
    const ctx = canvas.getContext("2d");
    ctx.drawImage(video, 0, 0, canvas.width, canvas.height);

    // Convert the canvas image to base64 data URL in JPEG format
    const dataURL = canvas.toDataURL("image/jpeg"); // Thay đổi thành 'image/jpeg'
    setImageData(dataURL);
  };

  //tắt camera
  const handleCancelCapture = () => {
    // Thực hiện các xử lý khi người dùng bấm nút tắt camera
    // Cập nhật giá trị của các biến trạng thái để ẩn button "Lưu ảnh" và hiển thị lại button "Chụp ảnh"
    setShowCaptureButton(true);
    setShowSaveButton(false);
    // Tắt video bằng cách dừng stream và hủy kết nối video
    const videoElement = videoRef.current;
    if (videoElement && videoElement.srcObject) {
      const stream = videoElement.srcObject;
      const tracks = stream.getTracks();
      tracks.forEach((track) => track.stop());
      videoElement.srcObject = null;
    }
  };

  //upload ảnh
  //upload ảnh
  const handleUpload = useCallback(async () => {
    if (imageData) {
      // Nếu có ảnh từ camera được chụp, tải lên ảnh này
      // const formData = new FormData();
      // const base64Image = imageData.split(",")[1];
      // const imageBlob = new Blob([atob(base64Image)], { type: "image/jpeg" }); // Thay đổi thành 'image/jpeg'
      // formData.append("file", imageBlob);
      const formData = new FormData();
      formData.append("file", dataURItoBlob(imageData));
      try {
        const token = sessionStorage.getItem("token");
        const config = {
          headers: {
            Authorization: `Bearer ${token}`,
            "Content-Type": "image/jpeg", // Đặt Content-Type là image/jpeg
          },
        };
        await axios.post(
          "https://localhost:7050/api/Account/update-avatar",
          formData,
          config
        );
        cogoToast.success("Tải ảnh avatar thành công", {
          position: "bottom-left",
        });
      } catch (error) {
        cogoToast.error("Tải ảnh avatar thất bại", { position: "bottom-left" });
        console.error(error);
      }
    } else if (selectedFile) {
      // Nếu không có ảnh từ camera, nhưng có ảnh từ file input, tải lên ảnh này
      const formData = new FormData();
      formData.append("file", selectedFile);
      const token = sessionStorage.getItem("token");
      const config = {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      };
      try {
        await axios.post(
          "https://localhost:7050/api/Account/update-avatar",
          formData,
          config
        );

        cogoToast.success("Tải ảnh avatar thành công", {
          position: "bottom-left",
        });
      } catch (error) {
        cogoToast.error("Tải ảnh avatar thất bại", { position: "bottom-left" });
        console.error(error);
      }
    }
  }, [imageData, selectedFile]);

  const dataURItoBlob = (dataURI) => {
    const byteString = atob(dataURI.split(",")[1]);
    const mimeString = dataURI.split(",")[0].split(":")[1].split(";")[0];
    const ab = new ArrayBuffer(byteString.length);
    const ia = new Uint8Array(ab);
    for (let i = 0; i < byteString.length; i++) {
      ia[i] = byteString.charCodeAt(i);
    }
    return new Blob([ab], { type: mimeString });
  };

  //Lấy thông tin data UserProfile
  useEffect(() => {
    const fetchUserData = async () => {
      try {
        const token = sessionStorage.getItem("token");
        const config = {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        };
        const response = await axios.get(
          "https://localhost:7050/api/Account/get-user",
          config
        );
        setUserData(response.data);
        // Populate the form fields if user data is available
        if (response.data) {
          const { fullName, phone, address, dob, profile, avatar } =
            response.data;
          setFullName(fullName);
          setPhone(phone);
          setAddress(address);
          setDob(dob);
          setProfile(profile);
          setAvatar(avatar);
        }
      } catch (error) {
        console.error(error);
      }
    };

    fetchUserData();
  }, []);

  //Đổi mật khẩu user
  const handleChangePassword = async (e) => {
    e.preventDefault();
    try {
      setCurrentPasswordError("");
      setNewPasswordError("");
      setConfirmPasswordError("");
      if (!currentPassword) {
        setCurrentPasswordError("Vui lòng nhập mật khẩu hiện tại.");
      }
      if (!newPassword) {
        setNewPasswordError("Vui lòng nhập mật khẩu mới.");
      }
      if (!confirmPassword) {
        setConfirmPasswordError("Vui lòng nhập xác nhận mật khẩu.");
      }
      const token = sessionStorage.getItem("token");
      const config = {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      };

      // Gửi yêu cầu xác thực mã nhập vào đến API
      const params = new URLSearchParams();
      params.append("currentPassword", currentPassword);
      params.append("newPassword", newPassword);
      params.append("confirmPassword", confirmPassword);
      await axios.put(
        `https://localhost:7050/api/Auth/change-password`,
        params,
        config
      );
      // Xử lý phản hồi từ API và hiển thị thông báo tương ứng
      // Xác thực thành công
      // Hiển thị thông báo thành công
      // if (!currentPasswordError && !newPasswordError && !confirmPasswordError) {
      cogoToast.success("Đổi mật khẩu thành công", {
        position: "bottom-left",
      });
      // navigate.push("/my-account");
      window.location.href = "http://localhost:3000/my-account";
      // } else {
      //   cogoToast.error("Đổi mật khẩu thất bại", { position: "bottom-left" });
      // }
    } catch (error) {
      if (error.response && error.response.data) {
        const errorMessage = error.response.data.error;
        if (
          errorMessage ===
          "Mật khẩu hiện tại không đúng. Vui lòng nhập lại mật khẩu!"
        ) {
          setCurrentPasswordError(errorMessage);
        } else if (
          errorMessage ===
          "Mật khẩu không hợp lệ. Mật khẩu yêu cầu ít nhất 1 kí tự viết hoa và 1 kí tự đặc biệt và 1 kí tự số và không được quá 14 kí tự"
        ) {
          setNewPasswordError(errorMessage);
        } else if (
          errorMessage === "Xác nhận mật khẩu không khớp với mật khẩu."
        ) {
          setConfirmPasswordError(errorMessage);
        } else {
          console.error("Unknown error:", errorMessage);
          cogoToast.error("Đổi mật khẩu thất bại", { position: "bottom-left" });
        }
      } else {
        console.error("Registration failed:", error);
        cogoToast.error("Đổi mật khẩu thất bại", { position: "bottom-left" });
      }
    }
  };
  return (
    <>
      <SimpleHeader name="Profile" parentName="Dashboard" />
      <Container className="mt--6" fluid>
        <div className="container-fluid my-5 py-2">
          <div className="row mb-5">
            <div className="col-lg-3">
              <div className="card position-sticky top-1">
                <ul className="nav flex-column bg-white border-radius-lg p-3">
                  <li className="nav-item">
                    <a
                      className="nav-link text-body d-flex align-items-center"
                      data-scroll=""
                      href="#profile"
                    >
                      <i className="ni ni-spaceship me-2 text-dark opacity-6"></i>
                      <span className="text-sm">Profile</span>
                    </a>
                  </li>
                  <li className="nav-item">
                    <a
                      className="nav-link text-body d-flex align-items-center"
                      data-scroll=""
                      href="#password"
                    >
                      <i className="ni ni-spaceship me-2 text-dark opacity-6"></i>
                      <span className="text-sm">Đổi mật khẩu</span>
                    </a>
                  </li>
                </ul>
              </div>
            </div>
            <div className="col-lg-9 mt-lg-0 mt-4">
              {/* Card Basic Info */}
              {userData ? (
                <Row>
                  <Col className="order-xl-2" xl="4" id="profile">
                    <Card className="card-profile">
                      <CardImg
                        alt="..."
                        src={require("../../../assets/img/theme/img-1-1000x600.jpg")}
                        top
                      />
                      <Row className="justify-content-center">
                        <Col className="order-lg-2" lg="3">
                          <div className="card-profile-image">
                            <a
                              href="#pablo"
                              onClick={(e) => e.preventDefault()}
                            >
                              {imageData ? ( // Kiểm tra imageData có dữ liệu không
                                <img
                                  src={imageData}
                                  alt="Captured"
                                  className="rounded-circle"
                                />
                              ) : selectedFile ? ( // Kiểm tra selectedFile có dữ liệu không
                                <img
                                  src={URL.createObjectURL(selectedFile)}
                                  alt="Avatar"
                                  className="rounded-circle"
                                />
                              ) : (
                                avatar && ( // Kiểm tra avatar có dữ liệu không
                                  <img
                                    src={avatar}
                                    alt="Avatar"
                                    className="rounded-circle"
                                  />
                                )
                              )}
                            </a>
                          </div>
                        </Col>
                      </Row>
                      <CardBody className="pt-0">
                        <div className="text-center mt-6">
                          <h5 className="h3">{userData.fullName}</h5>
                          <div className="h5 mt-4">
                            <i className="ni business_briefcase-24 mr-2" />
                            {userData.role} - ViewPoint System
                          </div>
                          <div>
                            <div className="col-sm-auto col-8 my-auto">
                              <div className="input-group justify-content-center">
                                <div className="custom-file custom-file-sm float-end mt-2 mb-0">
                                  <input
                                    type="file"
                                    className="custom-file-input form-control-sm"
                                    id="upload-avatar"
                                    onChange={handleFileChange}
                                  />
                                </div>
                                <div className="input-group-append justify-content-center m-lg-2 mt-3">
                                  <button
                                    className="btn btn-sm float-end mt-2 mb-0 btn-outline-success"
                                    type="button"
                                    onClick={handleUpload}
                                  >
                                    Tải ảnh
                                  </button>
                                </div>
                                <div className="input-group-append justify-content-center m-lg-2 mt-3">
                                  {showCaptureButton && (
                                    <button
                                      className="btn btn-sm float-end mt-2 mb-0 btn-primary"
                                      type="button"
                                      onClick={handleCapture}
                                    >
                                      Chụp ảnh
                                    </button>
                                  )}
                                  {showSaveButton && (
                                    <button
                                      className="btn btn-sm float-end mt-2 mb-0 btn-primary"
                                      type="button"
                                      onClick={handleTakePhoto}
                                    >
                                      <i className="fa fa-camera"></i>
                                    </button>
                                  )}
                                  {showSaveButton && (
                                    <button
                                      className="btn btn-sm float-end mt-2 mb-0 btn-danger"
                                      type="button"
                                      onClick={handleCancelCapture}
                                    >
                                      X
                                    </button>
                                  )}
                                </div>
                                <div className="d-flex justify-content-center">
                                  <video
                                    ref={videoRef}
                                    style={{
                                      display: "block",
                                      width: "50%",
                                    }}
                                    className="justify-content-center"
                                    autoPlay
                                    muted
                                  />
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </CardBody>
                    </Card>
                  </Col>
                  <Col className="order-xl-1" xl="8">
                    <Card>
                      <CardHeader>
                        <Row className="align-items-center">
                          <Col xs="8">
                            <h3 className="mb-0">Tài khoản</h3>
                          </Col>
                        </Row>
                      </CardHeader>
                      <CardBody>
                        <Form>
                          <h6 className="heading-small text-muted mb-4">
                            Thông tin tài khoản
                          </h6>
                          <div className="pl-lg-4">
                            <Row>
                              <Col lg="6">
                                <FormGroup>
                                  <label
                                    className="form-control-label"
                                    htmlFor="input-username"
                                  >
                                    Tài khoản
                                  </label>
                                  <Input
                                    defaultValue={userData.userName}
                                    id="input-username"
                                    type="text"
                                    disabled
                                  />
                                </FormGroup>
                              </Col>
                              <Col lg="6">
                                <FormGroup>
                                  <label
                                    className="form-control-label"
                                    htmlFor="input-email"
                                  >
                                    Địa chỉ Email
                                  </label>
                                  <Input
                                    defaultValue={userData.email}
                                    id="input-email"
                                    disabled
                                    type="email"
                                  />
                                </FormGroup>
                              </Col>
                            </Row>
                            <Row>
                              <Col lg="6">
                                <FormGroup>
                                  <label
                                    className="form-control-label"
                                    htmlFor="input-first-name"
                                  >
                                    Họ và tên
                                  </label>
                                  <Input
                                    defaultValue={fullName}
                                    id="input-first-name"
                                    placeholder="First name"
                                    type="text"
                                    disabled
                                  />
                                </FormGroup>
                              </Col>
                              <Col lg="6">
                                <FormGroup>
                                  <label
                                    className="form-control-label"
                                    htmlFor="input-last-name"
                                  >
                                    Số điện thoại
                                  </label>
                                  <Input
                                    defaultValue={userData.phone}
                                    id="input-last-name"
                                    disabled
                                    type="text"
                                  />
                                </FormGroup>
                              </Col>
                            </Row>
                          </div>
                          <div className="pl-lg-4">
                            <Row>
                              <Col md="12">
                                <FormGroup>
                                  <label
                                    className="form-control-label"
                                    htmlFor="input-address"
                                  >
                                    Địa chỉ
                                  </label>
                                  <Input
                                    defaultValue={userData.address}
                                    id="input-address"
                                    type="text"
                                    disabled
                                  />
                                </FormGroup>
                              </Col>
                            </Row>
                          </div>
                          <hr className="my-4" />

                          <h6 className="heading-small text-muted mb-4">
                            About me
                          </h6>
                          <div className="pl-lg-4">
                            <FormGroup>
                              <label className="form-control-label">
                                About Me
                              </label>
                              <Input
                                placeholder={userData.profile}
                                rows="4"
                                type="textarea"
                              />
                            </FormGroup>
                          </div>
                        </Form>
                      </CardBody>
                    </Card>
                  </Col>
                </Row>
              ) : (
                <p>Loading...</p>
              )}
              {/* Card Change Password */}
              <div className="card mt-4" id="password">
                <div className="card-header">
                  <h5>Đổi mật khẩu</h5>
                </div>
                <div className="card-body pt-0">
                  <label className="form-label">Mật khẩu hiện tại</label>
                  <div style={{ position: "relative" }} className="form-group">
                    <input
                      className="form-control"
                      type={showCurrentPassword ? "text" : "password"}
                      value={currentPassword}
                      onChange={(e) => setCurrentPassword(e.target.value)}
                      name="user-password"
                      placeholder="Mật Khẩu hiện tại"
                      style={{ paddingRight: "2.5em" }}
                    />
                    <FontAwesomeIcon
                      icon={showCurrentPassword ? faEyeSlash : faEye}
                      onClick={handleCurrentPasswordToggle}
                      style={{
                        position: "absolute",
                        right: "0.3em",
                        top: "30%",
                        transform: "translateY(-30%)",
                        cursor: "pointer",
                      }}
                    />
                  </div>
                  <span className="text-center text-danger font-weight-900 mt--8 m-lg-2">
                    {currentPasswordError}
                  </span>
                  <label className="form-label">Mật khẩu mới</label>
                  <div className="form-group" style={{ position: "relative" }}>
                    <input
                      className="form-control"
                      type={showNewPassword ? "text" : "password"}
                      value={newPassword}
                      onChange={(e) => setNewPassword(e.target.value)}
                      name="user-password"
                      placeholder="Mật Khẩu mới"
                      style={{ paddingRight: "2.5em" }}
                    />
                    <FontAwesomeIcon
                      icon={showNewPassword ? faEyeSlash : faEye}
                      onClick={handleNewPasswordToggle}
                      style={{
                        position: "absolute",
                        right: "0.3em",
                        top: "30%",
                        transform: "translateY(-30%)",
                        cursor: "pointer",
                      }}
                    />
                  </div>
                  <span className="text-center text-danger font-weight-900 mt--8 m-lg-2">
                    {newPasswordError}
                  </span>
                  <label className="form-label">Xác thực mật khẩu mới</label>
                  <div style={{ position: "relative" }} className="form-group">
                    <input
                      className="form-control"
                      type={showConfirmPassword ? "text" : "password"}
                      value={confirmPassword}
                      onChange={(e) => setConfirmPassword(e.target.value)}
                      name="user-password"
                      placeholder="Mật Khẩu"
                      style={{ paddingRight: "2.5em" }}
                    />
                    <FontAwesomeIcon
                      icon={showConfirmPassword ? faEyeSlash : faEye}
                      onClick={handleConfirmPasswordToggle}
                      style={{
                        position: "absolute",
                        right: "0.3em",
                        top: "30%",
                        transform: "translateY(-30%)",
                        cursor: "pointer",
                      }}
                    />
                  </div>
                  <span className="text-center text-danger font-weight-900 mt--8 m-lg-2">
                    {confirmPasswordError}
                  </span>
                  <button
                    className="btn bg-gradient-dark btn-sm float-end mt-6 mb-0"
                    type="submit"
                    onClick={handleChangePassword}
                  >
                    Update password
                  </button>
                </div>
              </div>
              {/* Card 2FA */}
              <div className="card mt-4" id="2fa">
                {/* ... Content of the Card 2FA ... */}
              </div>
              {/* Card Accounts */}
              <div className="card mt-4" id="accounts">
                {/* ... Content of the Card Accounts ... */}
              </div>
              {/* Card Notifications */}
              <div className="card mt-4" id="notifications">
                {/* ... Content of the Card Notifications ... */}
              </div>
              {/* Card Sessions */}
              <div className="card mt-4" id="sessions">
                {/* ... Content of the Card Sessions ... */}
              </div>
              {/* Card Delete Account */}
              <div className="card mt-4" id="delete">
                {/* ... Content of the Card Delete Account ... */}
              </div>
            </div>
          </div>
        </div>
      </Container>
    </>
  );
}

export default Profile;

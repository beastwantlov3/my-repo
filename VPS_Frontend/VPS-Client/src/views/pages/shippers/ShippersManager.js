import React, { useEffect, useRef, useState } from "react";
import List from "list.js";
import classnames from "classnames";
import {
  Badge,
  Card,
  CardHeader,
  CardFooter,
  Media,
  Pagination,
  PaginationItem,
  PaginationLink,
  Table,
  Container,
  Row,
  Button,
  Modal,
  ModalHeader,
  ModalFooter,
  ModalBody,
} from "reactstrap";
import SimpleHeader from "../../../components/Headers/SimpleHeader.js";
import axios from "axios";
import { Link, useParams } from "react-router-dom/cjs/react-router-dom.min";
import NotificationAlert from "react-notification-alert";

const ShippersManager = () => {
  const [currentPage, setCurrentPage] = useState(1);
  const itemsPerPage = 10;
  const [sortedColumn, setSortedColumn] = useState("");
  const [sortedDirection, setSortedDirection] = useState("");
  const firstListRef = useRef(null);
  const [orderData, setOrderData] = useState([]);
  const filteredData = orderData.filter((order) =>
    [3, 4, 5, 8, 9].includes(order.statusId)
  );
  const [cancellationReason, setCancellationReason] = useState("");

  const totalPages = Math.ceil(filteredData.length / itemsPerPage);

  const startIndex = (currentPage - 1) * itemsPerPage;
  const endIndex = Math.min(startIndex + itemsPerPage, filteredData.length);

  // Get the displayedData based on the startIndex and endIndex
  const displayedData = filteredData.slice(startIndex, endIndex);
  const notificationAlertRef = useRef(null);
  const [shipData, setShipData] = useState(null);

  const [cancelModal, setCancelModal] = useState(false);
  const [selectedOrderId, setSelectedOrderId] = useState(null);

  useEffect(() => {
    if (orderData.length > 0) {
      initializeList();
    }
    const totalPages = Math.ceil(orderData.length / itemsPerPage);
    if (currentPage > totalPages) {
      setCurrentPage(1);
    }
  }, [orderData, currentPage]);

  const fetchData = async () => {
    const token = sessionStorage.getItem("token");
    try {
      const config = {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      };
      const response = await axios.get(
        `https://localhost:7050/api/Order/listbyshipidd`,
        config
      );
      setOrderData(response.data);
    } catch (error) {
      console.error("Error fetching data:", error);
    }
  };

  const fetchShipperData = async () => {
    try {
      const token = sessionStorage.getItem("token");
      const config = {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      };
      const response = await axios.get(
        `https://localhost:7050/api/Shipper/get-shipper-id`,
        config
      );
      setShipData(response.data);
      console.log(response.data);
    } catch (error) {
      console.error("Error fetching Saler data:", error);
    }
  };

  useEffect(() => {
    fetchData();
    fetchShipperData();
  }, []);

  const notifySuccess = () => {
    let options = {
      place: "tc",
      message: (
        <div className="alert-text">
          <span className="alert-title" data-notify="title">
            {" "}
            Hủy đơn hàng thành công
          </span>
        </div>
      ),
      type: "success",
      icon: "ni ni-bell-55",
      autoDismiss: 7,
    };
    notificationAlertRef.current.notificationAlert(options);
  };
  const notifyDanger = () => {
    let options = {
      place: "tc",
      message: (
        <div className="alert-text">
          <span className="alert-title" data-notify="title">
            {" "}
            Hủy đơn hàng thất bại!
          </span>
          <span data-notify="message"> Vui lòng kiểm tra lại đường truyền</span>
        </div>
      ),
      type: "danger",
      icon: "ni ni-bell-55",
      autoDismiss: 7,
    };
    notificationAlertRef.current.notificationAlert(options);
  };

  const handleSort = (column) => {
    let direction = "asc";
    if (column === sortedColumn && sortedDirection === "asc") {
      direction = "desc";
    }
    setSortedColumn(column);
    setSortedDirection(direction);
    sortData(column, direction);
    setCurrentPage(1); // Reset current page when sorting
  };

  const getValueByColumn = (item, column) => {
    switch (column) {
      case "id":
        return item.orderId;
      case "shipId":
        return item.saleId;
      case "orderDate":
        return item.orderDate;
      case "status":
        return item.status;
      case "price":
        return item.price;
      default:
        return "";
    }
  };

  const sortData = (column, direction) => {
    const sortedData = [...orderData];
    sortedData.sort((a, b) => {
      const valueA = getValueByColumn(a, column);
      const valueB = getValueByColumn(b, column);
      if (valueA < valueB) {
        return direction === "asc" ? -1 : 1;
      }
      if (valueA > valueB) {
        return direction === "asc" ? 1 : -1;
      }
      return 0;
    });
    setOrderData(sortedData);
  };

  const initializeList = () => {
    if (displayedData.length > 0) {
      new List(firstListRef.current, {
        valueNames: ["id", "saleId", "orderDate", "status", "price", "action"],
        listClass: "list",
      });
    }
  };
  const renderTableBody = () => {
    if (orderData.length === 0) {
      return (
        <div colSpan="6" className="text-center text-red">
          Hiện bạn không có đơn hàng nào cần xử lý
        </div>
      );
    }
  };
  const handleCancelOrderClick = (orderId) => {
    setSelectedOrderId(orderId);
    setCancelModal(true);
  };

  const handleCancelOrder = async () => {
    try {
      // Fetch the existing order data
      const response = await axios.get(
        `https://localhost:7050/api/Order/${selectedOrderId}`
      );
      const currentDate = new Date();
      const year = currentDate.getFullYear();
      const month = String(currentDate.getMonth() + 1).padStart(2, "0");
      const day = String(currentDate.getDate()).padStart(2, "0");
      const hours = String(currentDate.getHours()).padStart(2, "0");
      const minutes = String(currentDate.getMinutes()).padStart(2, "0");
      const seconds = String(currentDate.getSeconds()).padStart(2, "0");
      const milliseconds = String(currentDate.getMilliseconds()).padStart(
        3,
        "0"
      );
      const formattedDate = `${year}-${month}-${day}T${hours}:${minutes}:${seconds}.${milliseconds}Z`;

      const existingOrder = response.data;
      // Prepare the data to be sent in the request body

      const updateAlertData = {
        shipperId: shipData.shipperId,
        message: cancellationReason,
        currentDate: formattedDate,
      };
      await axios.post(`https://localhost:7050/api/Alert`, updateAlertData);

      const updatedOrderData = {
        ...existingOrder,
        shipperCancelled: formattedDate,
        statusId: 2,
        shipId: null,
      };

      // Send the PATCH request to update the order
      await axios.put(
        `https://localhost:7050/api/Order/${selectedOrderId}`,
        updatedOrderData
      );
      // After successfully updating the order, close the modal
      setCancelModal(false);
      notifySuccess();
      fetchData();
    } catch (error) {
      notifyDanger();
      console.error("Error updating the order:", error);
    }
  };

  const toggleCancelModal = () => {
    setCancelModal(false);
  };

  const VND = new Intl.NumberFormat("vi-VN", {
    style: "currency",
    currency: "VND",
  });

  return (
    <>
      <NotificationAlert ref={notificationAlertRef} />
      <SimpleHeader name="Dashboard" parentName="Shipper" />
      <Modal isOpen={cancelModal} toggle={toggleCancelModal}>
        <ModalHeader toggle={toggleCancelModal}>Hủy đơn hàng</ModalHeader>
        <ModalBody>
          <div className="form-group">
            <label htmlFor="cancellationReason">Lý do hủy đơn hàng:</label>
            <textarea
              className="form-control"
              id="cancellationReason"
              rows="4"
              value={cancellationReason}
              onChange={(e) => setCancellationReason(e.target.value)}
            />
          </div>
        </ModalBody>
        <ModalFooter>
          <Button color="danger" onClick={handleCancelOrder}>
            Xác nhận hủy đơn
          </Button>{" "}
          <Button color="secondary" onClick={toggleCancelModal}>
            Hủy
          </Button>
        </ModalFooter>
      </Modal>
      <Container className="mt--6" fluid>
        <Row>
          <div className="col">
            <Card>
              <CardHeader className="border-0">
                <h3 className="mb-0">Danh sách đơn hàng</h3>
              </CardHeader>
              {orderData.length > 0 ? (
                <div className="table-responsive" ref={firstListRef}>
                  <Table className="align-items-center table-flush">
                    <thead className="thead-light">
                      <tr>
                        <th
                          className="sort"
                          data-sort="id"
                          scope="col"
                          onClick={() => handleSort("name")}
                        >
                          ID
                        </th>
                        <th
                          className="sort"
                          data-sort="saleId"
                          scope="col"
                          onClick={() => handleSort("saleId")}
                        >
                          Tên
                        </th>
                        <th
                          className="sort"
                          data-sort="orderDate"
                          scope="col"
                          onClick={() => handleSort("orderDate")}
                        >
                          Ngày đặt hàng
                        </th>
                        <th
                          className="sort"
                          data-sort="status"
                          scope="col"
                          onClick={() => handleSort("status")}
                        >
                          Tình trạng
                        </th>
                        <th
                          className="sort"
                          data-sort="price"
                          scope="col"
                          onClick={() => handleSort("price")}
                        >
                          Giá đơn hàng
                        </th>
                        <th scope="col" />
                      </tr>
                    </thead>
                    <tbody className="list">
                      {displayedData.map((order) => (
                        <tr key={order.orderId}>
                          <td className="id">#{order.orderId}</td>
                          <th scope="row">
                            <Media className="align-items-center">
                              <a
                                className="avatar rounded-circle mr-3"
                                href="#pablo"
                                onClick={(e) => e.preventDefault()}
                              >
                                <img
                                  alt="..."
                                  src={require("../../../assets/img/theme/bootstrap.jpg")}
                                />
                              </a>
                              <Media>
                                <span className="saleId mb-0 text-sm">
                                  {shipData && shipData.fullName}
                                </span>
                              </Media>
                            </Media>
                          </th>
                          <td className="date">
                            {new Date(order.orderDate).toLocaleDateString(
                              "en-US",
                              {
                                month: "2-digit",
                                day: "2-digit",
                                year: "numeric",
                              }
                            )}
                          </td>
                          <td>
                            <Badge color="" className="badge-dot mr-4">
                              <i
                                className={
                                  order.statusId === 1
                                    ? "bg-warning"
                                    : order.statusId === 2
                                    ? "bg-yellow "
                                    : order.statusId === 3
                                    ? "bg-gray"
                                    : order.statusId === 4
                                    ? "bg-blue"
                                    : order.statusId === 5
                                    ? "bg-success"
                                    : order.statusId === 6
                                    ? "bg-success"
                                    : order.statusId === 7
                                    ? "bg-danger"
                                    : order.statusId === 8
                                    ? "bg-danger"
                                    : order.statusId === 9
                                    ? "bg-yellow"
                                    : "bg-grey"
                                }
                              />
                              <span className="status">
                                {order.statusId === 1
                                  ? "Chờ manager xác nhận"
                                  : order.statusId === 2
                                  ? "Chờ saler xác nhận"
                                  : order.statusId === 3
                                  ? "Chờ shipper xác nhận"
                                  : order.statusId === 4
                                  ? "Chờ shipper giao hàng"
                                  : order.statusId === 5
                                  ? "Shipper giao hàng thành công"
                                  : order.statusId === 6
                                  ? "Khách hàng xác nhận đơn hàng"
                                  : order.statusId === 7
                                  ? "Đơn hàng đã bị hủy"
                                  : order.statusId === 8
                                  ? "Đơn hàng cần hoàn trả"
                                  : order.statusId === 9
                                  ? "Đơn hàng đã dùng xong"
                                  : "Đơn hàng đã về kho"}
                              </span>
                            </Badge>
                          </td>
                          <td className="price">{VND.format(order.price)}</td>
                          <td className="text-right">
                            <Link to={`/admin/order-details/${order.orderId}`}>
                              <Button
                                className="btn-neutral"
                                color="default"
                                size="sm"
                              >
                                Xem
                              </Button>
                            </Link>
                            {order.statusId === 4 ||
                            order.statusId === 5 ||
                            order.statusId === 6 ? null : (
                              <Button
                                className="btn-neutral"
                                color="default"
                                size="sm"
                                onClick={() =>
                                  handleCancelOrderClick(order.orderId)
                                }
                              >
                                Hủy đơn
                              </Button>
                            )}
                          </td>
                        </tr>
                      ))}
                    </tbody>
                  </Table>
                </div>
              ) : (
                renderTableBody()
              )}
              <CardFooter className="py-4 bg-transparent">
                <nav aria-label="...">
                  <Pagination className="pagination justify-content-end mb-0">
                    <PaginationItem disabled={currentPage === 1}>
                      <PaginationLink
                        previous
                        onClick={() => setCurrentPage(currentPage - 1)}
                      />
                    </PaginationItem>
                    {Array.from({ length: totalPages }, (_, i) => i + 1).map(
                      (page) => (
                        <PaginationItem
                          key={page}
                          active={currentPage === page}
                        >
                          <PaginationLink onClick={() => setCurrentPage(page)}>
                            {page}
                          </PaginationLink>
                        </PaginationItem>
                      )
                    )}
                    <PaginationItem disabled={currentPage === totalPages}>
                      <PaginationLink
                        next
                        onClick={() => setCurrentPage(currentPage + 1)}
                      />
                    </PaginationItem>
                  </Pagination>
                </nav>
              </CardFooter>
            </Card>
          </div>
        </Row>
      </Container>
    </>
  );
};

export default ShippersManager;

import axios from "axios";
import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom/cjs/react-router-dom.min";
// reactstrap components
import {
  Button,
  Card,
  CardHeader,
  CardBody,
  FormGroup,
  Form,
  Input,
  Container,
  Row,
  Col,
} from "reactstrap";
// core components
// import ProfileHeader from "components/Headers/ProfileHeader.js";
import { useHistory } from "react-router-dom";
import ReactDatetime from "react-datetime";
import Select2 from "react-select2-wrapper";
import moment from "moment";
// import TagsInput from "components/TagsInput/TagsInput.js";
import NotificationAlert from "react-notification-alert";

const SaleDetails = () => {
  const { saleId } = useParams();
  const [orderData, setOrderData] = useState([]);
  const [shipperData, setShipperData] = useState([]);
  const [saleData, setSaleData] = useState([]);

  const [startDate, setStartDate] = useState(null);
  const [endDate, setEndDate] = useState(new Date());
  const history = useHistory();
  const notificationAlertRef = React.useRef(null);
  const [selectedShipper, setSelectedShipper] = useState("");

  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await axios.get(
          `https://localhost:7050/api/Order/${saleId}`
        );
        setOrderData(response.data);
        const orderDate = new Date(response.data.orderDate); // Convert orderDate to a Date object
        setStartDate(orderDate); // Update the startDate value
      } catch (error) {
        console.error("Error fetching data:", error);
      }
    };

    fetchData();
  }, [saleId]);

  useEffect(() => {
    const fetchSaleData = async () => {
      try {
        const response = await axios.get(
          `https://localhost:7050/api/Sale/${orderData.saleId}`
        );
        setSaleData(response.data);
      } catch (error) {
        console.error("Error fetching category data:", error);
      }
    };
    fetchSaleData();
  }, [orderData.saleId]);

  useEffect(() => {
    const fetchShipperData = async () => {
      try {
        const response = await axios.get(
          `https://localhost:7050/api/Shipper/list`
        );
        setShipperData(response.data);
      } catch (error) {
        console.error("Error fetching category data:", error);
      }
    };

    fetchShipperData();
  }, []);

  const notifySuccess = () => {
    let options = {
      place: "tc",
      message: (
        <div className="alert-text">
          <span className="alert-title" data-notify="title">
            {" "}
            Xác nhận đơn hàng thành công
          </span>
        </div>
      ),
      type: "success",
      icon: "ni ni-bell-55",
      autoDismiss: 7,
    };
    notificationAlertRef.current.notificationAlert(options);
  };

  const notifyDanger = () => {
    let options = {
      place: "tc",
      message: (
        <div className="alert-text">
          <span className="alert-title" data-notify="title">
            {" "}
            Xác nhận đơn hàng thất bại
          </span>
          <span data-notify="message">
            Vui lòng kiểm tra các trường đã điền
          </span>
        </div>
      ),
      type: "danger",
      icon: "ni ni-bell-55",
      autoDismiss: 7,
    };
    notificationAlertRef.current.notificationAlert(options);
  };

  const handleFormSubmit = async (e) => {
    e.preventDefault();

    const form = e.target;
    if (form.checkValidity()) {
      if (!selectedShipper) {
        notifyDanger();
        return;
      }
      try {
        const ordersById = await axios.get(
          `https://localhost:7050/api/Order/${saleId}`
        );

      const currentDate = new Date();
      const year = currentDate.getFullYear();
      const month = String(currentDate.getMonth() + 1).padStart(2, "0");
      const day = String(currentDate.getDate()).padStart(2, "0");
      const hours = String(currentDate.getHours()).padStart(2, "0");
      const minutes = String(currentDate.getMinutes()).padStart(2, "0");
      const seconds = String(currentDate.getSeconds()).padStart(2, "0");
      const milliseconds = String(currentDate.getMilliseconds()).padStart(
        3,
        "0"
      );

      const formattedDate = `${year}-${month}-${day}T${hours}:${minutes}:${seconds}.${milliseconds}Z`;

        const updatedOrderData = {
          ...ordersById.data,
          statusId: 3,
          saleConfirm: formattedDate,
          shipId: parseInt(e.target.elements.ship.value),
          shippedDate: moment(endDate).format("YYYY-MM-DD"),
        };

        // Update the order with the calculated price
        await axios.put(`https://localhost:7050/api/Order/${saleId}`, updatedOrderData);

        form.reset();
        history.push(`/admin/list-sales/${ordersById.data.saleId}`);
        notifySuccess();
      } catch (error) {
        notifyDanger();
        form.reportValidity();
        console.error("Error adding order detail:", error);
      }
    }
  };

  const handleReactDatetimeChange = (who, date) => {
    if (
      startDate &&
      who === "endDate" &&
      new Date(startDate._d + "") > new Date(date._d + "")
    ) {
      setStartDate(date);
      setEndDate(date);
    } else if (
      endDate &&
      who === "startDate" &&
      new Date(endDate._d + "") < new Date(date._d + "")
    ) {
      setStartDate(date);
      setEndDate(date);
    } else {
      if (who === "startDate") {
        setStartDate(date);
      } else {
        setEndDate(date);
      }
    }
  };

  const isValidDate = (current) => {
    return current.isSameOrAfter(moment().startOf("day"));
  };

  const getClassNameReactDatetimeDays = (date) => {
    if (startDate && endDate) {
    }
    if (startDate && endDate && startDate._d + "" !== endDate._d + "") {
      if (
        new Date(endDate._d + "") > new Date(date._d + "") &&
        new Date(startDate._d + "") < new Date(date._d + "")
      ) {
        return " middle-date";
      }
      if (endDate._d + "" === date._d + "") {
        return " end-date";
      }
      if (startDate._d + "" === date._d + "") {
        return " start-date";
      }
    }
    return "";
  };

  return (
    <>
      <div className="rna-wrapper">
        <NotificationAlert ref={notificationAlertRef} />
      </div>
      <Container className="mt--12" fluid>
        <Row className="mt-4">
          <Col className="order-xl-1 mt-4" xl="12">
            <Card>
              <CardHeader>
                <Row className="align-items-center">
                  <Col xs="8">
                    <h3 className="mb-0">Đơn hàng</h3>
                  </Col>
                </Row>
              </CardHeader>
              <CardBody>
                <Form onSubmit={handleFormSubmit}>
                  <h6 className="heading-small text-muted mb-4">
                    Thông tin đơn hàng
                  </h6>
                  <div className="pl-lg-4">
                    <Row>
                      <Col lg="6">
                        <FormGroup>
                          <label
                            className="form-control-label"
                            htmlFor="input-username"
                          >
                            Mã đơn hàng
                          </label>
                          <Input
                            defaultValue={orderData.orderId}
                            id="input-username"
                            placeholder="Order ID"
                            type="text"
                            disabled
                          />
                        </FormGroup>
                      </Col>
                      <Col lg="6">
                        <FormGroup>
                          <label
                            className="form-control-label"
                            htmlFor="input-username"
                          >
                            Người bán hàng
                          </label>
                          <Input
                            defaultValue={saleData.fullName}
                            id="input-username"
                            placeholder="Sale Name"
                            type="text"
                            disabled
                          />
                        </FormGroup>
                      </Col>
                    </Row>
                    <Row>
                      <Col lg="6">
                        <FormGroup>
                          <label
                            className="form-control-label"
                            htmlFor="input-first-name"
                          >
                            Giá trị đơn hàng
                          </label>
                          <Input
                            defaultValue={orderData.price}
                            id="input-first-name"
                            placeholder="Price"
                            type="text"
                            disabled
                          />
                        </FormGroup>
                      </Col>
                      <Col xs={12} sm={6}>
                        <label className=" form-control-label">
                          Ngày đặt hàng
                        </label>
                        <FormGroup>
                          <ReactDatetime
                            inputProps={{
                              placeholder: "Date Picker Here",
                              disabled: true,
                            }}
                            value={startDate}
                            timeFormat={false}
                            onChange={(e) =>
                              handleReactDatetimeChange("startDate", e)
                            }
                            renderDay={(props, currentDate, selectedDate) => {
                              let classes = props.className;
                              classes +=
                                getClassNameReactDatetimeDays(currentDate);
                              return (
                                <td {...props} className={classes}>
                                  {currentDate.date()}
                                </td>
                              );
                            }}
                          />
                        </FormGroup>
                      </Col>
                    </Row>
                    <Row>
                      <Col lg="6">
                        <FormGroup>
                          <label
                            className="form-control-label"
                            htmlFor="input-email"
                          >
                            Chọn người giao hàng
                          </label>
                          <Card>
                            <Select2
                              className="form-control"
                              name="ship"
                              value={selectedShipper}
                              options={{
                                placeholder: "Select",
                              }}
                              data={shipperData.map((ship) => ({
                                id: ship.shipperId,
                                text: ship.fullName,
                              }))}
                              onChange={(event) =>
                                setSelectedShipper(event.target.value)
                              }
                            />
                          </Card>
                        </FormGroup>
                      </Col>
                      <Col xs={12} sm={6}>
                        <FormGroup>
                          <label className="form-control-label">
                            Ngày giao hàng
                          </label>
                          <ReactDatetime
                            inputProps={{
                              placeholder: "Date Picker Here",
                            }}
                            isValidDate={isValidDate}
                            value={endDate}
                            timeFormat={false}
                            name="shippedDate"
                            onChange={(e) =>
                              handleReactDatetimeChange("endDate", e)
                            }
                            renderDay={(props, currentDate, selectedDate) => {
                              let classes = props.className;
                              classes +=
                                getClassNameReactDatetimeDays(currentDate);
                              return (
                                <td {...props} className={classes}>
                                  {currentDate.date()}
                                </td>
                              );
                            }}
                          />
                        </FormGroup>
                      </Col>
                    </Row>
                  </div>

                  <hr className="my-4" />
                  <Button className="ml-4 mt-4" color="primary" type="submit">
                    XÁC NHẬN ĐƠN HÀNG
                  </Button>
                </Form>
              </CardBody>
            </Card>
          </Col>
        </Row>
      </Container>
    </>
  );
};

export default SaleDetails;

import React, { useEffect, useState } from "react";
import BlogList from "./blogList/BlogList";
import SearchBar from "./blogList/SearchBar";
import "../../../assets/css/blog.css";
import axios from "axios";
import { Link } from "react-router-dom/cjs/react-router-dom.min";
import { Button } from "reactstrap";

const Blogs = () => {
  const [searchKey, setSearchKey] = useState("");
  const [data, setData] = useState([]);
  const [originalData, setOriginalData] = useState([]);

  useEffect(() => {
    fetchData();
  }, []);

  const fetchData = async () => {
    try {
      const response = await axios.get("https://localhost:7050/api/Blog/list");
      setData(response.data);
      setOriginalData(response.data); // Store the original data
    } catch (error) {
      console.error("Error fetching data:", error);
    }
  };

  // Search submit
  const handleSearchBar = (e) => {
    e.preventDefault();
    handleSearchResults();
  };

  // Search for blog by title
  const handleSearchResults = () => {
    const filteredBlogs = originalData.filter((blog) =>
      blog.title.toLowerCase().includes(searchKey.toLowerCase().trim())
    );
    setData(filteredBlogs);
  };

  // Clear search and show all blogs
  const handleClearSearch = () => {
    setData(originalData); // Reset the data back to the original data
    setSearchKey("");
  };

  return (
    <div className="ml-8 mr-8">
      <div className="d-flex justify-content-between align-items-center">
        <div className="btn-wrapper">
          <Link to="/admin/add-blog">
            <Button className="btn-primary btn-neutral">Thêm bài viết</Button>
          </Link>
        </div>
        <SearchBar
          value={searchKey}
          clearSearch={handleClearSearch}
          formSubmit={handleSearchBar}
          handleSearchKey={(e) => setSearchKey(e.target.value)}
        />
      </div>
      <BlogList blogs={data} />
    </div>
  );
};

export default Blogs;

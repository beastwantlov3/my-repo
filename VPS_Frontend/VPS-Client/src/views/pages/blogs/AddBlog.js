import React, { useEffect, useState } from "react";

// reactstrap components
import {
  Card,
  CardHeader,
  CardBody,
  FormGroup,
  Form,
  Input,
  Container,
  Row,
  Col,
  Button,
  ListGroup,
  ListGroupItem,
} from "reactstrap";
// core components
// import ProfileHeader from "components/Headers/ProfileHeader.js";
import "quill/dist/quill.snow.css"; // Import Quill styles
import "dropzone/dist/dropzone.css";
import ReactQuill from "react-quill";
import Dropzone from "dropzone";
import axios from "axios";
import { useHistory } from "react-router-dom/cjs/react-router-dom.min";
import Select2 from "react-select2-wrapper";
import NotificationAlert from "react-notification-alert";

const AddBlog = () => {
  const [reactQuillText, setReactQuillText] = React.useState("");
  const [categoryData, setCategoryData] = useState([]);
  const [bloggerData, setBloggerData] = useState([]);
  const [selectedCategory, setSelectedCategory] = useState("");
  const [selectedBlogger, setSelectedBlogger] = useState("");
  const [isFormSubmitted, setIsFormSubmitted] = useState(false);
  const [selectedFile, setSelectedFile] = useState(null);
  const history = useHistory();

  const handleFileChange = (event) => {
    setSelectedFile(event.target.files[0]);
  };
  const notificationAlertRef = React.useRef(null);

  const notifySuccess = () => {
    let options = {
      place: "tc",
      message: (
        <div className="alert-text">
          <span className="alert-title" data-notify="title">
            {" "}
            Thêm sản phẩm thành công
          </span>
        </div>
      ),
      type: "success",
      icon: "ni ni-bell-55",
      autoDismiss: 7,
    };
    notificationAlertRef.current.notificationAlert(options);
  };

  const notifyDanger = () => {
    let options = {
      place: "tc",
      message: (
        <div className="alert-text">
          <span className="alert-title" data-notify="title">
            {" "}
            Thêm sản phẩm thất bại
          </span>
          <span data-notify="message">
            Vui lòng kiểm tra các trường đã điền
          </span>
        </div>
      ),
      type: "danger",
      icon: "ni ni-bell-55",
      autoDismiss: 7,
    };
    notificationAlertRef.current.notificationAlert(options);
  };

  useEffect(() => {
    const fetchCategoryData = async () => {
      try {
        const response = await axios.get(
          `https://localhost:7050/api/Category/list`
        );
        setCategoryData(response.data);
        console.log(response.data);
      } catch (error) {
        console.error("Error fetching category data:", error);
      }
    };

    fetchCategoryData();
  }, []);

  useEffect(() => {
    const fetchBloggerData = async () => {
      try {
        const response = await axios.get(
          `https://localhost:7050/api/Blogger/list`
        );
        setBloggerData(response.data);
        console.log(response.data);
      } catch (error) {
        console.error("Error fetching category data:", error);
      }
    };

    fetchBloggerData();
  }, []);

  const handleFormSubmit = async (e) => {
    e.preventDefault();
    setIsFormSubmitted(true);
    const form = e.target;
    if (form.checkValidity()) {
      try {
        const formData = new FormData();
        formData.append("title", e.target.elements.title.value);
        formData.append("cateId", selectedCategory);
        formData.append("bloggerId", e.target.elements.blogger.value);
        formData.append("description", reactQuillText);
        formData.append("image", selectedFile);
        // Make a POST request to the API to add the product
        const response = await axios.post(
          "https://localhost:7050/api/Blog/post-product",
          formData
        );

        // Handle the response
        console.log(response.data);
        form.reset();
        setIsFormSubmitted(false);
        // Show success alert
        notifySuccess();
        history.push("/admin/list-blogs");

        // Reset the form fields or redirect to another page
      } catch (error) {
        notifyDanger();
        console.error("Error adding product:", error);
      }
    }
  };
  return (
    <>
      <div className="rna-wrapper">
        <NotificationAlert ref={notificationAlertRef} />
      </div>
      <Container className="mt--12" fluid>
        <Col className="order-xl-1 mt-4" xl="12">
          <Card>
            <CardHeader>
              <Row className="align-items-center">
                <Col xs="8">
                  <h3 className="mb-0">Thêm bài viết</h3>
                </Col>
              </Row>
            </CardHeader>
            <CardBody>
              <Form onSubmit={handleFormSubmit}>
                <div className="pl-lg-4">
                  <Row>
                    <Col lg="12">
                      <FormGroup>
                        <label
                          className="form-control-label"
                          htmlFor="input-username"
                        >
                          Tiêu đề
                        </label>
                        <Input
                          name="title"
                          id="input-username"
                          placeholder="Name of product"
                          type="text"
                        />
                      </FormGroup>
                    </Col>
                  </Row>
                </div>
                <div className="pl-lg-4">
                  <Row>
                    <Col lg="10">
                      <FormGroup>
                        <label
                          className="form-control-label"
                          htmlFor="input-email"
                        >
                          Chọn tên tác giả
                        </label>
                        <Card>
                          <Select2
                            className="form-control"
                            name="blogger"
                            value={selectedBlogger}
                            options={{
                              placeholder: "Select",
                            }}
                            data={bloggerData.map((blog) => ({
                              id: blog.bloggerId,
                              text: blog.fullName,
                            }))}
                            onChange={(event) =>
                              setSelectedBlogger(event.target.value)
                            } // Update the selectedCategory state on change
                          />
                        </Card>
                      </FormGroup>
                    </Col>
                  </Row>
                  <Row>
                    <Col lg="10">
                      <FormGroup>
                        <label
                          className="form-control-label"
                          htmlFor="input-email"
                        >
                          Phân loại sản phẩm
                        </label>
                        <Card>
                          <Select2
                            className="form-control"
                            name="category"
                            value={selectedCategory}
                            options={{
                              placeholder: "Select",
                            }}
                            data={categoryData.map((category) => ({
                              id: category.categoryId,
                              text: category.categoryName,
                            }))}
                            onChange={(event) =>
                              setSelectedCategory(event.target.value)
                            } // Update the selectedCategory state on change
                          />
                        </Card>
                      </FormGroup>
                    </Col>
                  </Row>
                </div>
                <div className="pl-lg-4">
                  <FormGroup>
                    <label className="form-control-label">Nội dung</label>
                    <div
                      data-quill-placeholder="Quill WYSIWYG"
                      data-toggle="quill"
                    />
                    <ReactQuill
                      name="description"
                      value={reactQuillText}
                      onChange={(value) => setReactQuillText(value)}
                      theme="snow"
                      modules={{
                        toolbar: [
                          ["bold", "italic"],
                          ["link", "blockquote", "code", "image"],
                          [
                            {
                              list: "ordered",
                            },
                            {
                              list: "bullet",
                            },
                          ],
                        ],
                      }}
                    />
                  </FormGroup>
                </div>
                <div className="custom-file  mt-2">
                  <input
                    type="file"
                    className="custom-file-input"
                    id="upload-avatar"
                    onChange={handleFileChange}
                  />
                </div>
                <Button className="ml-4 mt-4" color="primary" type="submit">
                  LƯU BÀI VIẾT
                </Button>
              </Form>
            </CardBody>
          </Card>
        </Col>
      </Container>
    </>
  );
};

export default AddBlog;

import React from 'react';
import { Link } from 'react-router-dom';
import '../../../../../assets/css/blog.css'
import ReactHtmlParser from "react-html-parser";

const BlogItem = ({
  blog: {
    description,
    title,
    publishDate,
    image,
    id,
  },
  category,
  authorAvatar,
  authorName,
}) => {
 const formatDate = (dateString) => {
    const dateOptions = {
      day: "numeric",
      month: "long",
      year: "numeric",
    };


    // Convert the date to the desired format
    const date = new Date(dateString);
    const formattedDate = date.toLocaleDateString("vi-VN", dateOptions);

    // Return the formatted date string
    return `${formattedDate}`;
  };
  return (
    <div className='blogItem-wrap'>
      <img className='blogItem-cover' src={image} alt='cover' />
      <p className='chip'>{category}</p>
      <h3>{title}</h3>
      <p className="blogItem-desc">{ReactHtmlParser(description)}</p>
      <footer>
        <div className='blogItem-author'>
          <img src={authorAvatar} alt='avatar' />
          <div>
            <h5>Tác giả: {authorName}</h5>
            <p>{formatDate(publishDate)}</p>
          </div>
        </div>
        <Link className='blogItem-link' to={`/admin/blog-details/${id}`}>
          <i className='ni ni-curved-next text-purple'></i>
        </Link>
      </footer>
    </div>
  );
};

export default BlogItem;

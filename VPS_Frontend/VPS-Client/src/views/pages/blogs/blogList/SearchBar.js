import React from "react";
import "../../../../assets/css/blog.css";

const SearchBar = ({ formSubmit, value, handleSearchKey, clearSearch }) => (
  <div className="searchBar-wrap">
    <form onSubmit={formSubmit}>
      <input
        type="text"
        placeholder="Tìm kiếm blog..."
        value={value}
        onChange={handleSearchKey}
      />
      {value && (
        <span onClick={clearSearch}>
          <i className="ni ni-fat-remove"></i>
        </span>
      )}
      <button>Tìm</button>
    </form>
  </div>
);

export default SearchBar;

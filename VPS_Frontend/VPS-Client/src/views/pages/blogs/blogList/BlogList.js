import React from 'react';
import BlogItem from './blogItem/BlogItem';
import '../../../../assets/css/blog.css'

const BlogList = ({ blogs }) => {
  return (
    <div className='blogList-wrap'>
      {blogs.map((blog, item) => (
        <BlogItem key={item} blog={blog} category={blog.cate.bname} authorAvatar={blog.blogger.accounts.avatar} authorName={blog.blogger.fullName} />
      ))}
    </div>
  );
};

export default BlogList;

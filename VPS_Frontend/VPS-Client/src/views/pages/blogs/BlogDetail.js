import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router';
import { blogList } from '../../../variables/blog';
import '../../../assets/css/blog.css'
import { Link } from 'react-router-dom';
import axios from 'axios';
import ReactHtmlParser from "react-html-parser";

const BlogDetail = () => {
  const { blogId } = useParams();
  const [blog, setBlog] = useState(null);
  const [data, setData] = useState([]);

  useEffect(() => {
    let blog = blogList.find((blog) => blog.id === parseInt(blogId));
    if (blog) {
      setBlog(blog);
    }
  }, []);

  useEffect(()=>{
    fetchData()
  })
  const fetchData = async() =>{
    try {
      const response = await axios.get(`https://localhost:7050/api/Blog/${blogId}`);
      setData(response.data);
    } catch (error) {
      console.error("Error fetching data:", error);
    }
  };
  const formatDate = (dateString) => {
    const dateOptions = {
      day: "numeric",
      month: "long",
      year: "numeric",
    };


    // Convert the date to the desired format
    const date = new Date(dateString);
    const formattedDate = date.toLocaleDateString("vi-VN", dateOptions);

    // Return the formatted date string
    return `${formattedDate}`;
  };

  return (
    <>
      <Link className='blog-goBack' to='/admin/list-blogs'>
        <span> &#8592;</span> <span>Go Back</span>
      </Link>
        <div className='blog-wrap'>
          <header>
            <p className='blog-date'>Ngày đăng bài: {formatDate(data.publishDate)}</p>
            <h1>{data.title}</h1>
           
          </header>
          <img src={data.image} alt='cover' />
          <p className='blog-desc'>{ReactHtmlParser(data.description)}</p>
        </div>
    </>
  );
};

export default BlogDetail;

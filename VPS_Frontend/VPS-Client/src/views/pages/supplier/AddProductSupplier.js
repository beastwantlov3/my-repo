import classnames from "classnames";
import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom/cjs/react-router-dom.min";
// reactstrap components
import {
  ButtonGroup,
  Button,
  Card,
  CardHeader,
  CardBody,
  ListGroupItem,
  ListGroup,
  // CardImg,
  // CardTitle,
  FormGroup,
  Form,
  Input,
  // ListGroupItem,
  // ListGroup,
  // Progress,
  Container,
  Row,
  Col,
} from "reactstrap";
// core components
// import ProfileHeader from "components/Headers/ProfileHeader.js";
import NotificationAlert from "react-notification-alert";
import Select2 from "react-select2-wrapper";
// import TagsInput from "components/TagsInput/TagsInput.js";
import Dropzone from "dropzone";
import Slider from "nouislider";
import axios from "axios";
import { useHistory } from "react-router-dom";

Dropzone.autoDiscover = false;

const AddProductSupplier = () => {
  const history = useHistory();

  const [categoryData, setCategoryData] = useState([]);
  const [statusData, setStatusData] = useState([]);
  const [sliderValue, setSliderValue] = React.useState("0");
  const [selectedStatus, setSelectedStatus] = useState("");
  const [isFormSubmitted, setIsFormSubmitted] = useState(false);
  const [selectedCategory, setSelectedCategory] = useState("");
  const notificationAlertRef = React.useRef(null);
  const [selectedFile, setSelectedFile] = useState(null);
  const [ProductName, setProductName] = useState("");
  const [Brand, setBrand] = useState("");
  const [UnitInStock, setUnitInStock] = useState("");
  const [UnitPrice, setUnitPrice] = useState("");
  const [Description, setDescription] = useState("");

  const notifySuccess = async() => {
    let options = {
      place: "tc",
      message: (
        <div className="alert-text">
          <span className="alert-title" data-notify="title">
            {" "}
            Thêm sản phẩm thành công
          </span>
        </div>
      ),
      type: "success",
      icon: "ni ni-bell-55",
      autoDismiss: 7,
    };
    notificationAlertRef.current.notificationAlert(options);
    await handlePostSupplierStock();
  };

  const notifyDanger = () => {
    let options = {
      place: "tc",
      message: (
        <div className="alert-text">
          <span className="alert-title" data-notify="title">
            {" "}
            Thêm sản phẩm thất bại
          </span>
          <span data-notify="message">
            Vui lòng kiểm tra các trường đã điền
          </span>
        </div>
      ),
      type: "danger",
      icon: "ni ni-bell-55",
      autoDismiss: 7,
    };
    notificationAlertRef.current.notificationAlert(options);
  };

  const sliderRef = React.useRef(null);
  useEffect(() => {
    Slider.create(sliderRef.current, {
      start: [0],
      connect: [true, false],
      step: 1,
      range: { min: 0, max: 10 },
    }).on("update", function (values, handle) {
      setSliderValue(values[0]);
      console.log(values[0]);
    });
  }, []);

  useEffect(() => {
    const fetchCategoryData = async () => {
      try {
        const response = await axios.get(
          `https://localhost:7050/api/Category/list`
        );
        setCategoryData(response.data);
        console.log(response.data);
      } catch (error) {
        console.error("Error fetching category data:", error);
      }
    };

    fetchCategoryData();
  }, []);

  useEffect(() => {
    const fetchStatusData = async () => {
      try {
        const response = await axios.get(
          `https://localhost:7050/api/PStatus/list`
        );
        const slicedData = response.data.slice(0, 4); // Get the first four values
        setStatusData(slicedData);
        console.log(slicedData);
      } catch (error) {
        console.error("Error fetching category data:", error);
      }
    };

    fetchStatusData();
  }, []);

  const handleStatusSelection = (statusId) => {
    setSelectedStatus(statusId);
  };

  const handleFileChange = (event) => {
    setSelectedFile(event.target.files[0]);
  };


  const handleFormSubmit = async (e) => {
    e.preventDefault();
    setIsFormSubmitted(true);
    const form = e.target;
    if (form.checkValidity()) {
      try {
        // Build the product object with the form data
        // const product = {
        //   productName: e.target.elements.name.value,
        //   categoryId: e.target.elements.category.value, // Get the category value from the defaultValue prop of Select2
        //   brand: e.target.elements.brand.value,
        //   unitPrice: e.target.elements.price.value,
        //   unitInStock: e.target.elements.unitInStock.value,
        //   statusId: selectedStatus,
        //   // quantity: e.target.elements.quantity.value,
        //   description: e.target.elements.description.value,
        //   quality: parseInt(sliderValue),
        //   // image: e.target.elements.image.value,
        //   // Add other fields as needed
        // };
        const formData = new FormData();
        var quality = parseInt(sliderValue);
        formData.append("productName", ProductName);
        formData.append("categoryId", selectedCategory);
        formData.append("unitInStock", UnitInStock);
        formData.append("unitPrice", UnitPrice);
        formData.append("image", selectedFile);
        formData.append("statusId", selectedStatus);
        formData.append("brand", Brand);
        formData.append("description", Description);
        formData.append("quality", quality);

        // Make a POST request to the API to add the product
        await axios.post("https://localhost:7050/api/SupplierProduct", formData);
        // Handle the response
        form.reset();
        setIsFormSubmitted(false);
        // Show success alert
        notifySuccess();
        // history.push("/admin/list-suppliers");
      } catch (error) {
        notifyDanger();
        console.error("Error adding product:", error);
      }
    }
  };

  const handlePostSupplierStock = async () => {
      const responseList = await axios.get(
        "https://localhost:7050/api/SupplierProduct/list"
      );

      const lastProduct = responseList.data[responseList.data.length - 1];
      const newProductId = lastProduct.productId;
      // https://localhost:7050/api/Supplier/get-supplier-id
      // Increment the newProductId by 1
      const nextProductId = newProductId;
      const token = sessionStorage.getItem('token');
      const config = {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      };
      const responseSupplierID = await axios.get(
        `https://localhost:7050/api/Supplier/get-supplier-id`,
        config
      );
      const supplierId = responseSupplierID.data.supplierId;
      console.log("sssssssssssss"+supplierId);
      // Use the nextProductId as needed (e.g., to create the SupplierStock)
      // Create the SupplierStock object with the nextProductId and other required data
      const supplierStock = {
        productId: nextProductId,
        supplierId: supplierId, // Replace 5 with the actual supplierId you want to use
      };

      // Make the POST request to add the SupplierStock
      const responseSupplierStock = await axios.post(
        "https://localhost:7050/api/SupplierStock",
        supplierStock
      );

      // Handle the response of the second POST request
      console.log("SupplierStock added:", responseSupplierStock.data);
      console.log("id ", nextProductId);
    
  };


  return (
    <>
      <div className="rna-wrapper">
        <NotificationAlert ref={notificationAlertRef} />
      </div>
      <Container className="mt--12 " fluid>
        <Row className="mt-4 ml-8">
          <Col className="order-xl-1 mt-4" xl="10">
            <Card>
              <CardHeader>
                <Row className="align-items-center">
                  <Col xs="8">
                    <h3 className="mb-0">Thêm sản phẩm</h3>
                  </Col>
                </Row>
              </CardHeader>
              <CardBody>
                <Form onSubmit={handleFormSubmit}>
                  <h6 className="heading-small text-muted mb-4">
                    Thông tin sản phẩm
                  </h6>
                  <div className="pl-lg-4 ml-7">
                    <Row>
                      <Col lg="5">
                        <FormGroup>
                          <label
                            className="form-control-label"
                            htmlFor="input-username"
                          >
                            Tên sản phẩm
                          </label>
                          <Input
                            name="name"
                            id="input-username"
                            placeholder="Name of product"
                            type="text"
                            value={ProductName}
                            onChange={(e) => setProductName(e.target.value)}
                          />
                        </FormGroup>
                      </Col>
                      <Col lg="5">
                        <FormGroup>
                          <label
                            className="form-control-label"
                            htmlFor="input-first-name"
                          >
                            Nhãn hiệu
                          </label>
                          <Input
                            name="brand"
                            defaultValue=""
                            id="input-first-name"
                            placeholder="Brand"
                            type="text"
                            value={Brand}
                            onChange={(e) => setBrand(e.target.value)}
                          />
                        </FormGroup>
                      </Col>
                    </Row>
                    <Row>
                      <Col lg="10">
                        <FormGroup>
                          <label
                            className="form-control-label"
                            htmlFor="input-email"
                          >
                            Phân loại sản phẩm
                          </label>
                          <Card>
                            <Select2
                              className="form-control"
                              name="category"
                              value={selectedCategory}
                              options={{
                                placeholder: "Select",
                              }}
                              data={categoryData.map((category) => ({
                                id: category.categoryId,
                                text: category.categoryName,
                              }))}
                              onChange={(event) =>
                                setSelectedCategory(event.target.value)
                              } // Update the selectedCategory state on change
                            />
                          </Card>
                        </FormGroup>
                      </Col>
                    </Row>
                    <Row>
                      <Col lg="5">
                        <FormGroup>
                          <label
                            className="form-control-label"
                            htmlFor="input-last-name"
                          >
                            Giá sản phẩm
                          </label>
                          <Input
                            name="price"
                            defaultValue=""
                            id="input-last-name"
                            placeholder="VND"
                            type="number"
                            value={UnitPrice}
                            onChange={(e) => setUnitPrice(e.target.value)}
                          />
                        </FormGroup>
                      </Col>
                      <Col lg="5">
                        <FormGroup>
                          <label
                            className="form-control-label"
                            htmlFor="input-first-name"
                          >
                            Số lượng
                          </label>
                          <Input
                            name="unitInStock"
                            defaultValue=""
                            id="input-first-name"
                            placeholder="Quantity"
                            type="number"
                            value={UnitInStock}
                            onChange={(e) => setUnitInStock(e.target.value)}
                          />
                        </FormGroup>
                      </Col>
                    </Row>
                  </div>

                  <div className="pl-lg-4">
                    <label
                      className="form-control-label mb-4 ml-7"
                      htmlFor="input-address"
                    >
                      Tình trạng sản phẩm
                    </label>
                    <div>
                      {statusData.map((status) => (
                        <ButtonGroup
                          key={status.statusId}
                          className="btn-group-toggle btn-group-colors event-tag mb-4 ml-7"
                          data-toggle="buttons"
                        >
                          <i className="ni business_briefcase-24 mr-2">
                            {status.statusValue}
                          </i>
                          <Button
                            className={classnames("bg-info", {
                              active: status.statusId === selectedStatus,
                            })}
                            color=""
                            type="button"
                            onClick={() =>
                              handleStatusSelection(status.statusId)
                            }
                          />
                        </ButtonGroup>
                      ))}
                    </div>
                  </div>

                  <div className="pl-lg-4 mt-4">
                    <Row>
                      <Col md="12">
                        <FormGroup>
                          <label
                            className="form-control-label"
                            htmlFor="input-address"
                          >
                            Đánh giá sản phẩm
                          </label>
                          <div className="input-slider-container">
                            <div className="input-slider" ref={sliderRef} />
                            <Row className="mt-3">
                              <Col xs="6">
                                <span
                                  className="range-slider-value"
                                  name="quality"
                                >
                                  {sliderValue}
                                </span>
                              </Col>
                            </Row>
                          </div>
                        </FormGroup>
                      </Col>
                    </Row>
                  </div>
                  <hr className="my-4" />

                  <h6 className="heading-small text-muted mb-4">
                    Giới thiệu sản phẩm
                  </h6>
                  <div className="pl-lg-4">
                    <FormGroup>
                      <label className="form-control-label">Giới thiệu</label>
                      <Input
                        defaultValue=""
                        name="description"
                        id="exampleFormControlTextarea2"
                        rows="3"
                        type="textarea"
                        value={Description}
                        onChange={(e) => setDescription(e.target.value)}
                      />
                    </FormGroup>
                  </div>
                  <div className="pl-lg-4">
                    <label
                      className="form-control-label mb-4"
                      htmlFor="input-address"
                    >
                      Chọn ảnh đại diện
                    </label>
                    <br />
                    <input
                      type="file"
                      className="custom-file-input"
                      id="upload-avatar"
                      onChange={handleFileChange}
                    />
                  </div>
                  <Button className="ml-4 mt-4" color="primary" type="submit">
                    LƯU SẢN PHẨM
                  </Button>
                </Form>
              </CardBody>
            </Card>
          </Col>
        </Row>
      </Container>
    </>
  );
};

export default AddProductSupplier;

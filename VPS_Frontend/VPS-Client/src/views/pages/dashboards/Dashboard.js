import React, { useEffect, useRef, useState } from "react";
// node.js library that concatenates classes (strings)
import classnames from "classnames";
// javascipt plugin for creating charts
import Chart from "chart.js";
// react plugin used to create charts
import { Line } from "react-chartjs-2";
// reactstrap components
import {
  // Badge,
  Button,
  Card,
  CardHeader,
  CardBody,
  // DropdownMenu,
  // DropdownItem,
  // DropdownToggle,
  // UncontrolledDropdown,
  // Form,
  // Input,
  ListGroupItem,
  ListGroup,
  Media,
  NavItem,
  NavLink,
  Nav,
  Progress,
  Table,
  Container,
  Row,
  Col,
  // UncontrolledTooltip,
  PaginationItem,
  PaginationLink,
  DropdownItem,
  DropdownMenu,
  UncontrolledDropdown,
  DropdownToggle,
  Badge,
  Pagination,
  CardFooter,
  FormGroup,
  ModalFooter,
  Input,
  Form,
  ModalBody,
  ModalHeader,
  Modal,
} from "reactstrap";

// core components
import CardsHeader from "../../../components/Headers/CardsHeader";
import List from "list.js";
import ReactDatetime from "react-datetime";

import { chartOptions, parseOptions } from "../../../variables/charts";
import axios from "axios";
import { Link } from "react-router-dom/cjs/react-router-dom.min";
import ReactDatetimeClass from "react-datetime";

function Dashboard() {
  const [startDate, setStartDate] = React.useState(null);
  const [endDate, setEndDate] = React.useState(null);

  const [currentPage, setCurrentPage] = useState(1);
  const itemsPerPage = 5;

  const firstListRef = useRef(null);
  const [dataSaler, setDataSaler] = useState([]);
  const [productData, setProductData] = useState([]);
  const [buyerData, setBuyerData] = useState([]);
  const [employeeData, setEmployeeData] = useState([]);
  const [displayedAccount, setDisplayedAccount] = useState(4);
  const [displayedBuyer, setDisplayedBuyer] = useState(4);
  const [displayedProduct, setDisplayedProduct] = useState(4);
  const [showAllAccount, setShowAllAccount] = useState(false);
  const [showAllBuyer, setShowAllBuyer] = useState(false);
  const [showAllProduct, setShowAllProduct] = useState(false);

  const totalPagesSaler = Math.ceil(dataSaler.length / itemsPerPage);

  const startIndex = (currentPage - 1) * itemsPerPage;
  const endIndex = startIndex + itemsPerPage;
  const displayedDataSaler = dataSaler.slice(startIndex, endIndex);

  const [showModal, setShowModal] = useState(false);
  const [voucher, setVoucher] = useState("");
  const [selectedCustomerId, setSelectedCustomerId] = useState(null);

  const [chartData, setChartData] = useState({
    labels: [],
    datasets: [
      {
        label: "Revenue",
        data: [],
        borderColor: "#5e72e4",
        pointRadius: 0,
        pointHoverRadius: 0,
        borderWidth: 3,
      },
    ],
  });

  const handleReactDatetimeChange = (who, date) => {
    if (
      startDate &&
      who === "endDate" &&
      new Date(startDate._d + "") > new Date(date._d + "")
    ) {
      setStartDate(date);
      setEndDate(date);
    } else if (
      endDate &&
      who === "startDate" &&
      new Date(endDate._d + "") < new Date(date._d + "")
    ) {
      setStartDate(date);
      setEndDate(date);
    } else {
      if (who === "startDate") {
        setStartDate(date);
      } else {
        setEndDate(date);
      }
    }
  };

  const getClassNameReactDatetimeDays = (date) => {
    if (startDate && endDate) {
    }
    if (startDate && endDate && startDate._d + "" !== endDate._d + "") {
      if (
        new Date(endDate._d + "") > new Date(date._d + "") &&
        new Date(startDate._d + "") < new Date(date._d + "")
      ) {
        return " middle-date";
      }
      if (endDate._d + "" === date._d + "") {
        return " end-date";
      }
      if (startDate._d + "" === date._d + "") {
        return " start-date";
      }
    }
    return "";
  };

  useEffect(() => {
    fetchDataSale();
    fetchProductSale();
    fetchEmployee();
    fetchBuyerData();
  }, []);

  useEffect(() => {
    if (dataSaler.length > 0) {
      initializeListSaler();
    }
  }, [dataSaler]);

  const fetchDataSale = async () => {
    try {
      const response = await axios.get("https://localhost:7050/api/Sale/list");
      setDataSaler(response.data);
    } catch (error) {
      console.error("Error fetching data:", error);
    }
  };

  const fetchProductSale = async () => {
    try {
      const response = await axios.get(
        "https://localhost:7050/api/Product/top8bestsale"
      );
      setProductData(response.data);
    } catch (error) {
      console.error("Error fetching data:", error);
    }
  };

  const fetchEmployee = async () => {
    try {
      const response = await axios.get(
        "https://localhost:7050/api/Account/GetAllEmployee"
      );
      setEmployeeData(response.data);
    } catch (error) {
      console.error("Error fetching data:", error);
    }
  };
  const handleShowMoreAccount = () => {
    // Display all items when "Show More" is clicked
    setDisplayedAccount(employeeData.length);
    setShowAllAccount(true);
  };
  const handleShowLessAccount = () => {
    setDisplayedAccount(4);
    setShowAllAccount(false);
  };
  const handleShowMoreBuyer = () => {
    // Display all items when "Show More" is clicked
    setDisplayedBuyer(buyerData.length);
    setShowAllBuyer(true);
  };
  const handleShowLessBuyer = () => {
    setDisplayedBuyer(4);
    setShowAllBuyer(false);
  };
  const handleShowMoreProduct = () => {
    // Display all items when "Show More" is clicked
    setDisplayedProduct(productData.length);
    setShowAllProduct(true);
  };
  const handleShowLessProduct = () => {
    setDisplayedProduct(4);
    setShowAllProduct(false);
  };

  const fetchBuyerData = async () => {
    try {
      const buyerResponse = await axios.get(
        "https://localhost:7050/api/Order/top8bestbuyer"
      );
      const buyers = buyerResponse.data;

      // Fetch customer data for each buyer
      const customerDataPromises = buyers.map(async (buyer) => {
        const customerResponse = await axios.get(
          `https://localhost:7050/api/Customer/${buyer.customerId}`
        );
        return {
          ...buyer,
          fullName: customerResponse.data.fullName,
        };
      });

      // Wait for all customer data to be fetched
      const buyerDataWithCustomer = await Promise.all(customerDataPromises);
      setBuyerData(buyerDataWithCustomer);
    } catch (error) {
      console.error("Error fetching data:", error);
    }
  };

  const toggleModal = () => {
    setShowModal(!showModal);
  };

  const handleVoucherChange = (e) => {
    setVoucher(e.target.value);
  };

  const handleSubmitVoucher = async () => {
    try {
      // Send voucher data to API for the selected customer
      if (selectedCustomerId) {
        const response = await axios.get(
          `https://localhost:7050/api/Customer/${selectedCustomerId}`
        );
        const existingData = response.data;
        await axios.put(
          `https://localhost:7050/api/Customer/${selectedCustomerId}`,
          {
            ...existingData,
            voucher: voucher,
          }
        );
        // Close the modal after successful submission
        toggleModal();
      }
    } catch (error) {
      console.error("Error submitting voucher:", error);
    }
  };

  const initializeListSaler = () => {
    new List(firstListRef.current, {
      valueNames: ["id", "name", "phone", "status", "address", "dob"],
      listClass: "list",
    });
  };
  
  const monthNames = [
    "Jan",
    "Feb",
    "Mar",
    "Apr",
    "May",
    "Jun",
    "Jul",
    "Aug",
    "Sep",
    "Oct",
    "Nov",
    "Dec",
  ];
  useEffect(() => {
    // Fetch data from the API and update the chart data state
    const fetchData = async () => {
      try {
        const monthData = [];
        for (let i = 1; i <= 12; i++) {
          const response = await axios.get(
            `https://localhost:7050/api/Order/doanhthuthang${i}`
          );
          const data = response.data; // Replace this with the actual data received from the API
          monthData.push(data);
        }
        // Update the chart data state with the received data
        setChartData({
          labels: monthNames,
          datasets: [
            {
              label: "Revenue",
              data: monthData, // Put the single data value into the first element of the data array
              borderColor: "#5e72e4",
              pointRadius: 0,
              pointHoverRadius: 0,
              borderWidth: 3,
            },
          ],
        });
      } catch (error) {
        console.error("Error fetching data:", error);
      }
    };

    fetchData();
  }, []);

  // Make sure to parse the chart options
  if (window.Chart) {
    parseOptions(Chart, chartOptions());
  }

  const dateFormatter = (cell, row) => {
    const dateObj = new Date(row.dob);
    const year = dateObj.getFullYear();
    const month = String(dateObj.getMonth() + 1).padStart(2, "0");
    const day = String(dateObj.getDate()).padStart(2, "0");
    const formattedDob = `${year}/${month}/${day}`;
    return formattedDob;
  };

  const VND = new Intl.NumberFormat('vi-VN', {
    style: 'currency',
    currency: 'VND',
  });

  return (
    <>
      <CardsHeader name="Manager" parentName="Dashboards" />

      <Container className="mt--6" fluid>
        <Row className="input-daterange datepicker align-items-center justify-content-end mb-2">
          <Col xs={2} sm={2}>
            <label className=" form-control-label text-dark">
              Ngày bắt đầu
            </label>
            <FormGroup>
              <ReactDatetime
                inputProps={{
                  placeholder: "Chọn ngày bắt đầu",
                }}
                value={startDate}
                timeFormat={false}
                onChange={(e) => handleReactDatetimeChange("startDate", e)}
                renderDay={(props, currentDate, selectedDate) => {
                  let classes = props.className;
                  classes += getClassNameReactDatetimeDays(currentDate);
                  return (
                    <td {...props} className={classes}>
                      {currentDate.date()}
                    </td>
                  );
                }}
              />
            </FormGroup>
          </Col>
          <Col xs={2} sm={2}>
            <FormGroup>
              <label className=" form-control-label text-dark">
                Ngày kết thúc
              </label>
              <ReactDatetimeClass
                inputProps={{
                  placeholder: "Chọn ngày kết thúc",
                }}
                value={endDate}
                timeFormat={false}
                onChange={(e) => handleReactDatetimeChange("endDate", e)}
                renderDay={(props, currentDate, selectedDate) => {
                  let classes = props.className;
                  classes += getClassNameReactDatetimeDays(currentDate);
                  return (
                    <td {...props} className={classes}>
                      {currentDate.date()}
                    </td>
                  );
                }}
              />
            </FormGroup>
          </Col>
        </Row>
        <Row>
          <Col xl="12">
            <Card className="bg-default">
              <CardHeader className="bg-transparent">
                <Row className="align-items-center">
                  <div className="col">
                    <h6 className="text-light text-uppercase ls-1 mb-1">
                      Tổng quan
                    </h6>
                    <h5 className="h3 text-white mb-0">Doanh thu</h5>
                  </div>
                  {/* <div className="col">
                    <Nav className="justify-content-end" pills>
                      <NavItem className="mr-2 mr-md-0">
                        <NavLink
                          className={classnames("py-2 px-3", {
                            active: activeNav === 1,
                          })}
                          href="#pablo"
                          onClick={(e) => toggleNavs(e, 1)}
                        >
                          <span className="d-none d-md-block">Tháng</span>
                          <span className="d-md-none">M</span>
                        </NavLink>
                      </NavItem>
                      <NavItem>
                        <NavLink
                          className={classnames("py-2 px-3", {
                            active: activeNav === 2,
                          })}
                          data-toggle="tab"
                          href="#pablo"
                          onClick={(e) => toggleNavs(e, 2)}
                        >
                          <span className="d-none d-md-block">Tuần</span>
                          <span className="d-md-none">W</span>
                        </NavLink>
                      </NavItem>
                    </Nav>
                  </div> */}
                </Row>
              </CardHeader>
              <CardBody>
                <div className="chart">
                  <Line
                    data={chartData}
                    options={chartOptions().defaults}
                    id="chart-sales"
                    className="chart-canvas"
                  />
                </div>
              </CardBody>
            </Card>
          </Col>
        </Row>
        <Row>
          <Col xl="4">
            <Card>
              <CardHeader>
                <h5 className="h3 mb-0">Danh sách tài khoản nhân viên</h5>
              </CardHeader>

              <CardBody>
                <ListGroup className="list my--3" flush>
                  {employeeData.slice(0, displayedAccount).map((employee) => (
                    <ListGroupItem className="px-0" key={employee.accountId}>
                      <Row className="align-items-center">
                        <Col className="col-auto">
                          <a
                            className="avatar rounded-circle"
                            href="#pablo"
                            onClick={(e) => e.preventDefault()}
                          >
                            <img
                              alt="..."
                              src={require("../../../assets/img/theme/team-1.jpg")}
                            />
                          </a>
                        </Col>
                        <div className="col ml--2">
                          <h4 className="mb-0">
                            <a
                              href="#pablo"
                              onClick={(e) => e.preventDefault()}
                            >
                              {employee.userName}
                            </a>
                          </h4>
                          <small>{employee.role.roleName}</small>
                        </div>
                        <Col className="col-auto">
                          <Button color="primary" size="sm" type="button">
                            Xem
                          </Button>
                        </Col>
                      </Row>
                    </ListGroupItem>
                  ))}
                </ListGroup>
                <div className="d-flex justify-content-center mt-3">
                  {!showAllAccount ? (
                    <Button color="info" onClick={handleShowMoreAccount}>
                      Xem thêm
                    </Button>
                  ) : (
                    <Button color="info" onClick={handleShowLessAccount}>
                      Trở lại
                    </Button>
                  )}
                </div>
              </CardBody>
            </Card>
          </Col>
          <Col xl="5">
            <Card>
              <CardHeader>
                <h5 className="h3 mb-0">Top người dùng đặt hàng</h5>
              </CardHeader>

              <CardBody>
                <ListGroup className="list my--3" flush>
                  {buyerData.slice(0, displayedBuyer).map((buyer, index) => (
                    <ListGroupItem className="px-0" key={index}>
                      <Row className="align-items-center">
                        <Col className="col-auto">
                          <a
                            className="avatar rounded-circle"
                            href="#pablo"
                            onClick={(e) => e.preventDefault()}
                          >
                            <img
                              alt="..."
                              src={require("../../../assets/img/theme/team-1.jpg")}
                            />
                          </a>
                        </Col>
                        <Col className="col ml--2">
                          <h4 className="mb-0">
                            <a
                              href="#pablo"
                              onClick={(e) => e.preventDefault()}
                            >
                              {buyer.fullName}
                            </a>
                          </h4>
                          <small>
                            <b>Tổng chi: </b>
                            {VND.format(buyer.totalPrice)}
                          </small>
                        </Col>
                        <Col className="col-auto">
                          <Button
                            color="primary"
                            size="sm"
                            type="button"
                            onClick={() => {
                              setSelectedCustomerId(buyer.customerId);
                              toggleModal();
                            }}
                          >
                            Tặng Voucher
                          </Button>
                        </Col>
                      </Row>
                    </ListGroupItem>
                  ))}
                </ListGroup>
                <div className="d-flex justify-content-center mt-3">
                  {!showAllBuyer ? (
                    <Button color="info" onClick={handleShowMoreBuyer}>
                      Xem thêm
                    </Button>
                  ) : (
                    <Button color="info" onClick={handleShowLessBuyer}>
                      Trở lại
                    </Button>
                  )}
                </div>
                <Modal isOpen={showModal} toggle={toggleModal}>
                  <ModalHeader toggle={toggleModal}>Nhập Voucher</ModalHeader>
                  <ModalBody>
                    <Form>
                      <FormGroup>
                        <Input
                          type="number"
                          placeholder="Nhập Voucher"
                          value={voucher}
                          onInput={(e) => {
                            const minValue = 1;
                            const maxValue = 100;
                            let enteredValue = parseInt(e.target.value, 10);

                            // Check if the entered value is a valid number and within the valid range
                            if (
                              isNaN(enteredValue) ||
                              enteredValue < minValue
                            ) {
                              enteredValue = minValue;
                            } else if (enteredValue > maxValue) {
                              enteredValue = maxValue;
                            }

                            e.target.value = enteredValue;
                            handleVoucherChange(e); // Update the state with the new value
                          }}
                        />
                      </FormGroup>
                    </Form>
                  </ModalBody>
                  <ModalFooter>
                    <Button color="primary" onClick={handleSubmitVoucher}>
                      Xác nhận
                    </Button>{" "}
                    <Button color="secondary" onClick={toggleModal}>
                      Hủy
                    </Button>
                  </ModalFooter>
                </Modal>
              </CardBody>
            </Card>
          </Col>
          <Col xl="3">
            <Card>
              <CardHeader>
                <h5 className="h3 mb-0">Top sản phẩm đặt hàng</h5>
              </CardHeader>

              <CardBody>
                <ListGroup className="list my--3" flush>
                  {productData.slice(0, displayedProduct).map((product) => (
                    <ListGroupItem className="px-0" key={product.productId}>
                      <Row className="align-items-center">
                        <Col className="col-auto">
                          <a
                            className="avatar rounded-circle"
                            href="#pablo"
                            onClick={(e) => e.preventDefault()}
                          >
                            <img
                              alt="..."
                              src={require("../../../assets/img/theme/bootstrap.jpg")}
                            />
                          </a>
                        </Col>
                        <Col className="col">
                          <h5>{product.productName}</h5>
                          <Progress
                            className="progress-xs mb-0"
                            color="orange"
                            max="500"
                            value={product.totalOrder}
                          />
                        </Col>
                      </Row>
                    </ListGroupItem>
                  ))}
                </ListGroup>
                <div className="d-flex justify-content-center mt-3">
                  {!showAllProduct ? (
                    <Button color="info" onClick={handleShowMoreProduct}>
                      Xem thêm
                    </Button>
                  ) : (
                    <Button color="info" onClick={handleShowLessProduct}>
                      Trở lại
                    </Button>
                  )}
                </div>
              </CardBody>
            </Card>
          </Col>
        </Row>
      </Container>
      <Container className="mt-2" fluid>
        <Row>
          <div className="col">
            <Card>
              <CardHeader className="border-0">
                <h3 className="mb-0">Danh sách salers</h3>
              </CardHeader>
              {dataSaler.length > 0 ? (
                <div className="table-responsive" ref={firstListRef}>
                  <Table className="align-items-center table-flush">
                    <thead className="thead-light">
                      <tr>
                        <th className="sort" data-sort="id" scope="col">
                          ID
                        </th>
                        <th className="sort" data-sort="name" scope="col">
                          Họ tên
                        </th>
                        <th className="sort" data-sort="phone" scope="col">
                          Số điện thoại
                        </th>
                        <th className="sort" data-sort="status" scope="col">
                          Tình trạng
                        </th>
                        <th className="sort" data-sort="address" scope="col">
                          Địa chỉ
                        </th>
                        <th className="sort" data-sort="dob" scope="col">
                          Ngày sinh
                        </th>
                        <th scope="col" />
                      </tr>
                    </thead>
                    <tbody className="list">
                      {displayedDataSaler.map((sale) => (
                        <tr key={sale.saleId}>
                          <td className="id">#{sale.saleId}</td>
                          <th scope="row">
                            <Media className="align-items-center">
                              <a
                                className="avatar rounded-circle mr-3"
                                href="#pablo"
                                onClick={(e) => e.preventDefault()}
                              >
                                <img
                                  alt="..."
                                  src={require("assets/img/theme/bootstrap.jpg")}
                                />
                              </a>
                              <Media>
                                <span className="name mb-0 text-sm">
                                  {sale.fullName}
                                </span>
                              </Media>
                            </Media>
                          </th>
                          <td className="phone">{sale.phone}</td>
                          <td>
                            <Badge color="" className="badge-dot mr-4">
                              <i
                                className={
                                  sale.statusId === 1
                                    ? "bg-success"
                                    : "bg-warning"
                                }
                              />
                              <span className="status">
                                {sale.statusId === 1
                                  ? "Hoạt động"
                                  : "Ngừng hoạt động"}
                              </span>
                            </Badge>
                          </td>
                          <td className="address">{sale.address}</td>
                          <td className="dob">{dateFormatter(null, sale)}</td>

                          <td className="text-right">
                            <UncontrolledDropdown>
                              <DropdownToggle
                                className="btn-icon-only text-light"
                                color=""
                                role="button"
                                size="sm"
                              >
                                <i className="fas fa-ellipsis-v" />
                              </DropdownToggle>
                              <DropdownMenu
                                className="dropdown-menu-arrow"
                                right
                              >
                                <Link to={`/admin/list-sales/${sale.saleId}`}>
                                  <DropdownItem>View</DropdownItem>
                                </Link>
                                <DropdownItem
                                  href="#pablo"
                                  onClick={(e) => e.preventDefault()}
                                >
                                  Delete
                                </DropdownItem>
                              </DropdownMenu>
                            </UncontrolledDropdown>
                          </td>
                        </tr>
                      ))}
                    </tbody>
                  </Table>
                </div>
              ) : (
                <div>Loading...</div>
              )}
              <CardFooter className="py-4 bg-transparent">
                <nav aria-label="...">
                  <Pagination className="pagination justify-content-end mb-0">
                    <PaginationItem disabled={currentPage === 1}>
                      <PaginationLink
                        previous
                        onClick={() => setCurrentPage(currentPage - 1)}
                      />
                    </PaginationItem>
                    {Array.from(
                      { length: totalPagesSaler },
                      (_, i) => i + 1
                    ).map((page) => (
                      <PaginationItem key={page} active={currentPage === page}>
                        <PaginationLink onClick={() => setCurrentPage(page)}>
                          {page}
                        </PaginationLink>
                      </PaginationItem>
                    ))}
                    <PaginationItem disabled={currentPage === totalPagesSaler}>
                      <PaginationLink
                        next
                        onClick={() => setCurrentPage(currentPage + 1)}
                      />
                    </PaginationItem>
                  </Pagination>
                </nav>
              </CardFooter>
            </Card>
          </div>
        </Row>
      </Container>
    </>
  );
}

export default Dashboard;

﻿using BusinessObject.Models;
using MailKit.Security;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using MimeKit;
using Repositories;

namespace APIConnect.Helper
{
	public class EmailWorker : BackgroundService
	{
		private readonly IServiceScopeFactory _serviceScopeFactory;
		private ICustomerRepository repositoryCustomer = new CustomerRepository();
		private IOrderDetailRepository repositoryOrderDetail = new OrderDetailRepository();
		private IAccountRepository repositoryAccount = new AccountRepository();

		public EmailWorker(IServiceScopeFactory serviceScopeFactory)
		{
			_serviceScopeFactory = serviceScopeFactory;
		}

		protected override async Task ExecuteAsync(CancellationToken stoppingToken)
		{
			while (!stoppingToken.IsCancellationRequested)
			{
				using (var scope = _serviceScopeFactory.CreateScope())
				{
					var orderService = scope.ServiceProvider.GetRequiredService<IOrderRepository>();

					// Lấy danh sách các đơn hàng cần kiểm tra
					var orders = orderService.GetOrder();

					foreach (var order in orders)
					{
						if (order.StatusId == 5)
						{
							await CheckAndSendEmail(order);
						}
					}
				}

				await Task.Delay(TimeSpan.FromDays(1), stoppingToken); // Đợi 1 ngày trước khi kiểm tra lại
			}
		}

		private async Task CheckAndSendEmail(Order order)
		{
			var orderDetails = repositoryOrderDetail.GetOrderDetailByOrderId(order.OrderId);
			var customer = repositoryCustomer.GetCustomerById((int)order.CustomerId);
			var account = repositoryAccount.GetAccountByCus(customer.CustomerId);

			if (account != null)
			{
				foreach (var orderDetail in orderDetails)
				{
					string email = account.Email;
					var currentDate = DateTime.Now;
					var deliveryTime = Convert.ToDateTime(order.ShipSuccessConfirm);

					if (orderDetail.ThreeDays ?? false)
					{
						var expirationTime = deliveryTime.AddDays(3);
						if (currentDate > expirationTime)
						{
							await SendExpirationAlertEmail(customer.FullName, email, orderDetail.Product.ProductName);
						}
					}

					if (orderDetail.OneWeeks ?? false)
					{
						var expirationTime = deliveryTime.AddDays(7);
						if (currentDate > expirationTime)
						{
							await SendExpirationAlertEmail(customer.FullName, email, orderDetail.Product.ProductName);
						}
					}

					if (!(orderDetail.ThreeDays ?? false) && !(orderDetail.OneWeeks ?? false))
					{
						var expirationTime = deliveryTime.AddDays(1);
						if (currentDate > expirationTime)
						{
							await SendExpirationAlertEmail(customer.FullName, email, orderDetail.Product.ProductName);
						}
					}
				}
			}
		}

		private async Task<bool> SendExpirationAlertEmail(string fullname, string email, string productName)
		{
			try
			{
				var message = new MimeMessage();
				message.From.Add(new MailboxAddress("ViewPoint System", "systemviewpoint@gmail.com")); // Replace with your email address
				message.To.Add(new MailboxAddress(email, email));
				message.Subject = "Đơn hàng đã hết thời gian thuê.";
				message.Body = new TextPart("plain")
				{
					Text = $"Xin chào {fullname}! Sản phẩm {productName} thuộc đơn hàng của bạn đã hết thời gian thuê."
				};

				using (var client = new MailKit.Net.Smtp.SmtpClient())
				{
					await client.ConnectAsync("smtp.gmail.com", 587, SecureSocketOptions.StartTls);
					await client.AuthenticateAsync("systemviewpoint@gmail.com", "cwgmaisruuwanuld");

					await client.SendAsync(message);
					await client.DisconnectAsync(true);
				}

				return true;
			}
			catch (Exception ex)
			{
				// Handle email sending exception
				return false;
			}
		}
	}
}
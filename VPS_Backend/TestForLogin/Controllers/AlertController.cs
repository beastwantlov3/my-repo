﻿using BusinessObject.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Repositories;

namespace APIConnect.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AlertController : ControllerBase
    {
        private IAlertRepository repository = new AlertRepository();
        [HttpGet]
        [Route("list")]
        public async Task<ActionResult<IEnumerable<Alert>>> GetAlert()
        {
            return repository.GetAlert();
        }
        //[HttpGet]
        [HttpGet("{id}")]
        public async Task<ActionResult<Alert>> GetAlertById(int id)
        {
            return repository.GetAlertById(id);
        }
        [HttpPut("{id}")]
        public async Task<IActionResult> PutComment(int id, Alert alert)
        {
            repository.UpdateAlert(id, alert);
            return NoContent();
        }
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteAlert(int id)
        {
            var account = repository.GetAlertById(id);
            if (account == null)
            {
                return NotFound();
            }

            repository.DeleteAlert(id);
            return NoContent();
        }
        [HttpPost]
        public async Task<ActionResult<Alert>> PostAlert(Alert comment)
        {
            repository.AddAlert(comment);
            return NoContent();
        }
    }
}

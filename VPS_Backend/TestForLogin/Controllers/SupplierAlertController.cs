﻿using BusinessObject.Models;
using DataAccess;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Repositories;

namespace APIConnect.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	public class SupplierAlertController : ControllerBase
	{
		private IAccountRepository repositoryAccount = new AccountRepository();
		private ISupplierAlert repository = new SupplierAlertRepository();
		private ISupplierRepository repositorySupplier = new SupplierRepository();
		[HttpGet]
		[Route("list")]
		public async Task<ActionResult<IEnumerable<SupplierAlert>>> GetSupplierAlert()
		{
			return repository.GetSupplierAlert();
		}
		//[HttpGet]
		[HttpGet("listbysupplierid")]
		public async Task<ActionResult<SupplierAlert>> GetSupplierAlertById()
		{

			var username = HttpContext.User.Identity.Name;
			if (!string.IsNullOrEmpty(username))
			{
				//Trả về thông tin người dùng
				var account = repositoryAccount.GetAccountByUsername(username);
				if (account != null)
				{
					var supplier = repositorySupplier.GetSupplierbyId((int)account.SupplierId);
					if (supplier != null)
					{
						var supplierAlert = repository.GetListSupplierAlertById(supplier.SupplierId);
						return Ok(supplierAlert);
					}
					else
					{
						// Khách hàng không tồn tại trong cơ sở dữ liệu
						return NotFound();
					}
				}
				else
				{
					// Người dùng không tồn tại trong cơ sở dữ liệu
					return NotFound();
				}
			}
			else
			{
				// Người dùng chưa đăng nhập, trả về lỗi hoặc chuyển hướng đến trang đăng nhập
				return Unauthorized(new { error = "Người dùng chưa đăng nhập." });
			}
		}
		[HttpPut("{id}")]
		public async Task<IActionResult> PutSupplierAlert(int id, SupplierAlert blogger)
		{
			repository.UpdateSupplierAlert(id, blogger);
			return NoContent();
		}
		[HttpDelete("{id}")]
		public async Task<IActionResult> DeleteSupplierAlert(int id)
		{
			var account = SupplierAlertDAO.GetSupplierAlertById(id);
			if (account == null)
			{
				return NotFound();
			}

			SupplierAlertDAO.DeleteSupplierAlert(id);
			return NoContent();
		}
		[HttpPost]
		public async Task<ActionResult<SupplierAlert>> PostSupplierAlertr(SupplierAlert blogger)
		{
			repository.AddSupplierAlert(blogger);
			return NoContent();
		}
	}
}

﻿using Amazon.Auth.AccessControlPolicy;
using Amazon.S3.Model;
using Amazon.S3;
using APIConnect.DTO;
using BusinessObject.Models;
using Castle.Core.Resource;
using DataAccess;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Repositories;
using System.Security.Claims;
using SixLabors.ImageSharp.Formats.Png;
using Image = SixLabors.ImageSharp.Image;
using Amazon;
using MimeKit;
using MailKit.Security;

namespace APIConnect.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	public class OrderController : ControllerBase
	{
		private readonly string bucketName = "system-viewpoint";
		private readonly string accessKey = "AKIAX3J7OK3364RPYPEB";
		private readonly string secretKey = "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";
		private IOrderRepository repository = new OrderRepository();
		private IAccountRepository repositoryAccount = new AccountRepository();
		private ICustomerRepository repositoryCustomer = new CustomerRepository();
		private IOrderDetailRepository repositoryOrderDetail = new OrderDetailRepository();
		private readonly VPSContext _vPSContext;
		public OrderController(VPSContext vPSContext
			)
		{
			_vPSContext = vPSContext;
		}
		[HttpGet]
		[Route("list")]
		public async Task<ActionResult<IEnumerable<Order>>> GetOrder()
		{
			return repository.GetOrder();
		}

		[HttpGet("[action]/{id}")]
		public async Task<ActionResult<IEnumerable<Order>>> GetCurrentOrderBySaleId(int id)
		{
			return OrderDAO.GetOrderBySaleId(id);
		}

		[HttpGet]
		[Route("listbysaleidd")]
		public async Task<ActionResult<IEnumerable<Order>>> GetOrderBySaleId()
		{
			// Trích xuất thông tin người dùng từ mã thông báo
			var username = HttpContext.User.Identity.Name;
			if (!string.IsNullOrEmpty(username))
			{
				//Trả về thông tin người dùng
				var account = repositoryAccount.GetAccountByUsername(username);
				if (account != null)
				{
					var sale = OrderDAO.GetOrderBySaleId((int)account.SaleId);
					return Ok(sale);
				}
				else
				{
					// Người dùng không tồn tại trong cơ sở dữ liệu
					return NotFound();
				}
			}
			else
			{
				// Người dùng chưa đăng nhập, trả về lỗi hoặc chuyển hướng đến trang đăng nhập
				return Unauthorized(new { error = "Người dùng chưa đăng nhập." });
			}
		}
		[HttpGet]
		[Route("listbycusidd")]
		public async Task<ActionResult<IEnumerable<Order>>> GetOrderByCustomerId()
		{
			// Trích xuất thông tin người dùng từ mã thông báo
			var username = HttpContext.User.Identity.Name;
			if (!string.IsNullOrEmpty(username))
			{
				//Trả về thông tin người dùng
				var account = repositoryAccount.GetAccountByUsername(username);
				if (account != null)
				{
					var sale = OrderDAO.GetOrderByCusId((int)account.CustomerId);
					return Ok(sale);
				}
				else
				{
					// Người dùng không tồn tại trong cơ sở dữ liệu
					return NotFound();
				}
			}
			else
			{
				// Người dùng chưa đăng nhập, trả về lỗi hoặc chuyển hướng đến trang đăng nhập
				return Unauthorized(new { error = "Người dùng chưa đăng nhập." });
			}
		}
		[HttpGet]
		[Route("listbyshipidd")]
		public async Task<ActionResult<IEnumerable<Order>>> GetOrderByShipId()
		{
			var username = HttpContext.User.Identity.Name;
			if (!string.IsNullOrEmpty(username))
			{
				//Trả về thông tin người dùng
				var account = repositoryAccount.GetAccountByUsername(username);
				if (account != null)
				{
					var shipper = OrderDAO.GetOrderByShipId((int)account.ShipperId);
					return Ok(shipper);
				}
				else
				{
					// Người dùng không tồn tại trong cơ sở dữ liệu
					return NotFound();
				}
			}
			else
			{
				// Người dùng chưa đăng nhập, trả về lỗi hoặc chuyển hướng đến trang đăng nhập
				return Unauthorized(new { error = "Người dùng chưa đăng nhập." });
			}
		}
		[HttpGet]
		[Route("top8bestbuyer")]
		public IActionResult GetTop4()
		{
			VPSContext context = new VPSContext();
			var list = context.Orders.Include(x => x.Customer)
				.GroupBy(o => o.CustomerId)
				.Select(g => new { CustomerId = g.Key, TotalPrice = g.Sum(o => o.Price) })
				.OrderByDescending(x => x.TotalPrice)
							.Take(8)
				.ToList();
			return Ok(list);
		}
		[HttpGet]
		[Route("listbydate")]
		public async Task<ActionResult<IEnumerable<Order>>> GetOrderByDate(string from, string to)
		{
			return repository.GetOrderByDate(from, to);
		}
		[HttpGet]
		[Route("doanhthuthang1")]
		public IActionResult GetOrder1()
		{
			VPSContext context = new VPSContext();
			var totalRevenue = context.Orders
		.Where(o => o.OrderDate >= Convert.ToDateTime("2023-01-01") && o.OrderDate <= Convert.ToDateTime("2023-01-30"))
		.Sum(o => o.Price);
			return Ok(totalRevenue);
		}
		[HttpGet]
		[Route("doanhthuthang2")]
		public IActionResult GetOrder2()
		{
			VPSContext context = new VPSContext();
			var totalRevenue = context.Orders
		.Where(o => o.OrderDate >= Convert.ToDateTime("2023-02-01") && o.OrderDate <= Convert.ToDateTime("2023-02-28"))
		.Sum(o => o.Price);
			return Ok(totalRevenue);
		}
		[HttpGet]
		[Route("doanhthuthang3")]
		public IActionResult GetOrder3()
		{
			VPSContext context = new VPSContext();
			var totalRevenue = context.Orders
		.Where(o => o.OrderDate >= Convert.ToDateTime("2023-03-01") && o.OrderDate <= Convert.ToDateTime("2023-03-30"))
		.Sum(o => o.Price);
			return Ok(totalRevenue);
		}
		[HttpGet]
		[Route("doanhthuthang4")]
		public IActionResult GetOrder4()
		{
			VPSContext context = new VPSContext();
			var totalRevenue = context.Orders
		.Where(o => o.OrderDate >= Convert.ToDateTime("2023-04-01") && o.OrderDate <= Convert.ToDateTime("2023-04-30"))
		.Sum(o => o.Price);
			return Ok(totalRevenue);
		}
		[HttpGet]
		[Route("doanhthuthang5")]
		public IActionResult GetOrder5()
		{
			VPSContext context = new VPSContext();
			var totalRevenue = context.Orders
		.Where(o => o.OrderDate >= Convert.ToDateTime("2023-05-01") && o.OrderDate <= Convert.ToDateTime("2023-05-30"))
		.Sum(o => o.Price);
			return Ok(totalRevenue);
		}
		[HttpGet]
		[Route("doanhthuthang6")]
		public IActionResult GetOrder6()
		{
			VPSContext context = new VPSContext();
			var totalRevenue = context.Orders
		.Where(o => o.OrderDate >= Convert.ToDateTime("2023-06-01") && o.OrderDate <= Convert.ToDateTime("2023-06-30"))
		.Sum(o => o.Price);
			return Ok(totalRevenue);
		}
		[HttpGet]
		[Route("doanhthuthang7")]
		public IActionResult GetOrder7()
		{
			VPSContext context = new VPSContext();
			var totalRevenue = context.Orders
		.Where(o => o.OrderDate >= Convert.ToDateTime("2023-07-01") && o.OrderDate <= Convert.ToDateTime("2023-07-30"))
		.Sum(o => o.Price);
			return Ok(totalRevenue);
		}
		[HttpGet]
		[Route("doanhthuthang8")]
		public IActionResult GetOrder8()
		{
			VPSContext context = new VPSContext();
			var totalRevenue = context.Orders
		.Where(o => o.OrderDate >= Convert.ToDateTime("2023-08-01") && o.OrderDate <= Convert.ToDateTime("2023-08-30"))
		.Sum(o => o.Price);
			return Ok(totalRevenue);
		}
		[HttpGet]
		[Route("doanhthuthang9")]
		public IActionResult GetOrder9()
		{
			VPSContext context = new VPSContext();
			var totalRevenue = context.Orders
		.Where(o => o.OrderDate >= Convert.ToDateTime("2023-09-01") && o.OrderDate <= Convert.ToDateTime("2023-09-30"))
		.Sum(o => o.Price);
			return Ok(totalRevenue);
		}
		[HttpGet]
		[Route("doanhthuthang10")]
		public IActionResult GetOrder10()
		{
			VPSContext context = new VPSContext();
			var totalRevenue = context.Orders
		.Where(o => o.OrderDate >= Convert.ToDateTime("2023-10-01") && o.OrderDate <= Convert.ToDateTime("2023-10-30"))
		.Sum(o => o.Price);
			return Ok(totalRevenue);
		}
		[HttpGet]
		[Route("doanhthuthang11")]
		public IActionResult GetOrder11()
		{
			VPSContext context = new VPSContext();
			var totalRevenue = context.Orders
		.Where(o => o.OrderDate >= Convert.ToDateTime("2023-11-01") && o.OrderDate <= Convert.ToDateTime("2023-11-30"))
		.Sum(o => o.Price);
			return Ok(totalRevenue);
		}
		[HttpGet]
		[Route("doanhthuthang12")]
		public IActionResult GetOrder12()
		{
			VPSContext context = new VPSContext();
			var totalRevenue = context.Orders
		.Where(o => o.OrderDate >= Convert.ToDateTime("2023-12-01") && o.OrderDate <= Convert.ToDateTime("2023-12-30"))
		.Sum(o => o.Price);
			return Ok(totalRevenue);
		}
		[HttpGet]
		[Route("MoneyByDate")]
		public IActionResult GetMoneyByDate(string start, string end)
		{
			VPSContext context = new VPSContext();
			var totalRevenue = context.Orders
		.Where(o => o.OrderDate >= Convert.ToDateTime(start) && o.OrderDate <= Convert.ToDateTime(end))
		.Sum(o => o.Price);
			return Ok(totalRevenue);
		}
		[HttpGet]
		[Route("TotalByDate")]
		public IActionResult GetTotalByDate(string start, string end)
		{
			VPSContext context = new VPSContext();
			var totalRevenue = context.Orders
		.Where(o => o.OrderDate >= Convert.ToDateTime(start) && o.OrderDate <= Convert.ToDateTime(end))
		.Count();
			return Ok(totalRevenue);
		}
		[HttpGet]
		[Route("total")]
		public async Task<int> GetTotalOrder()
		{
			return repository.GetTotalOrder();
		}
		[HttpGet("{id}")]
		public async Task<ActionResult<Order>> GetOrderById(int id)
		{
			var order = repository.GetOrderById(id);
			order.CurrentImage = GetOrderImageUrlFromS3(order.CurrentImage);
			return Ok(order);
		}

		[HttpPut("{id}")]
		public async Task<IActionResult> PutOrder(int id, Order order)
		{
			repository.UpdateOrder(id, order);
			return NoContent();
		}
		[HttpDelete("{id}")]
		public async Task<IActionResult> DeleteOrder(int id)
		{
			var account = repository.GetOrderById(id);
			if (account == null)
			{
				return NotFound();
			}

			repository.DeleteOrder(id);
			return NoContent();
		}

        [HttpPost]
        public async Task<IActionResult> PostOrder(Order order)
        {
            repository.AddOrder(order);
            return NoContent();
        }

        [Authorize]
		[HttpPost("Checkout")]
		public async Task<IActionResult> CheckOut([FromForm] OrderRequestModel orderRequest)
		{
			var username = HttpContext.User.Identity.Name;
			if (!string.IsNullOrEmpty(username))
			{
				//Trả về thông tin người dùng
				var account = repositoryAccount.GetAccountByUsername(username);

				var customer = repositoryCustomer.GetCustomerById((int)account.CustomerId);
				try
				{
					// Tạo và lưu thumbnail vào vị trí tạm thời
					string tempThumbnailPath = Path.GetTempFileName();
					int thumbnailWidth = 200; // Điều chỉnh kích thước thumbnail tùy ý
					int thumbnailHeight = 200; // Điều chỉnh kích thước thumbnail tùy ý

					using (var stream = new FileStream(tempThumbnailPath, FileMode.Create))
					{
						using (var image = Image.Load(orderRequest.CurrentImage.OpenReadStream()))
						{
							image.Mutate(x => x.Resize(new ResizeOptions
							{
								Mode = ResizeMode.Max,
								Size = new SixLabors.ImageSharp.Size(thumbnailWidth, thumbnailHeight)
							}));

							image.Save(stream, new PngEncoder()); // Lưu thumbnail dưới dạng PNG
						}
					}
					// Tải thumbnail lên Amazon S3
					string s3ThumbnailUrl = await UploadImageToS3(orderRequest.CurrentImage);

					// Xóa tệp tạm thời
					System.IO.File.Delete(tempThumbnailPath);

					// Tạo đối tượng Order từ dữ liệu trong orderRequest
					var order = new Order
					{
						CustomerId = customer.CustomerId,
						OrderDate = DateTime.Now,
						CurrentImage = s3ThumbnailUrl,
						// Set other order properties as needed
						StatusId = 1
					};

					// Tính tổng số tiền của đơn hàng từ các OrderDetail
					order.Price = orderRequest.Price;
					order.ShipCost = orderRequest.ShipCost;
					_vPSContext.Orders.Add(order);
					await _vPSContext.SaveChangesAsync();

					// Lặp qua các sản phẩm trong đơn hàng và thêm chúng vào order
					foreach (var cartItem in orderRequest.CartItems)
					{
						var product = await _vPSContext.Products.FindAsync(cartItem.ProductId);
						if (product == null)
						{
							return BadRequest(new { message = "Product not found." });
						}

						var orderDetail = new OrderDetail
						{
							ProductId = cartItem.ProductId,
							Quantity = cartItem.Quantity,
							UnitPrice = cartItem.UnitPrice,
							Discount = cartItem.Discount,
							ThreeDays = cartItem.ThreeDays,
							OneWeeks = cartItem.OneWeeks,
						};
						// Kiểm tra giá trị của ThreeDays và OneWeeks trước khi gán
						if (!cartItem.ThreeDays.HasValue)
						{
							orderDetail.ThreeDays = false;
						}

						if (!cartItem.OneWeeks.HasValue)
						{
							orderDetail.OneWeeks = false;
						}

						// Xác định giá trị của OneDays dựa trên ThreeDays và OneWeeks
						if (!(orderDetail.ThreeDays ?? false) && !(orderDetail.OneWeeks ?? false))
						{
							orderDetail.OneWeeks = false;
							orderDetail.ThreeDays = false;
						}
						orderDetail.OrderId = order.OrderId;

						await UpdateProductQuantity(cartItem.ProductId, cartItem.Quantity);
						order.OrderDetails.Add(orderDetail);
						_vPSContext.OrderDetails.Add(orderDetail);
						_vPSContext.SaveChanges();
					}

					return Ok(new { orderId = order.OrderId });
				}
				catch (Exception ex)
				{
					return Unauthorized(new { error = "Không thể đặt hàng." });
				}
			}
			else
			{
				return Unauthorized(new { error = "Người dùng chưa đăng nhập." });
			}
		}

		private async Task<string> UploadImageToS3(IFormFile imageFile)
		{
			// Tạo tên tệp duy nhất
			string extension = Path.GetExtension(imageFile.FileName);
			string filename = "thumbnail-" + Guid.NewGuid().ToString() + extension;

			// Tải ảnh lên Amazon S3
			// (Thay thế "your-bucket-name" bằng tên thật của bucket S3 của bạn)
			var s3Client = new AmazonS3Client(accessKey, secretKey, RegionEndpoint.APSoutheast1);
			using (var imageStream = imageFile.OpenReadStream())
			{
				var putRequest = new PutObjectRequest
				{
					BucketName = bucketName,
					Key = "product/" + filename,
					InputStream = imageStream,
					ContentType = imageFile.ContentType
				};
				await s3Client.PutObjectAsync(putRequest);
			}

			// Trả về URL S3 của ảnh đã tải lên
			return "product/" + filename;
		}

		[HttpPut("update-product-quantity")]
		public async Task<IActionResult> UpdateProductQuantity(int productId, int quantity)
		{
			try
			{
				var product = await _vPSContext.Products.FindAsync(productId);
				if (product == null)
				{
					return BadRequest(new { message = "Product not found." });
				}

				if (product.UnitInStock < quantity)
				{
					return BadRequest(new { message = "Insufficient product quantity." });
				}

				product.UnitInStock -= quantity;
				await _vPSContext.SaveChangesAsync();

				return Ok(new { message = "Product quantity updated successfully." });
			}
			catch (Exception ex)
			{
				return BadRequest(new { message = "Error updating product quantity.", error = ex.Message });
			}
		}

		private string GetOrderImageUrlFromS3(string avatarKey)
		{
			try
			{
				// Tạo đối tượng Amazon S3
				using (var client = new AmazonS3Client(accessKey, secretKey, RegionEndpoint.APSoutheast1))
				{
					// Tạo request để lấy đường dẫn URL của tệp ảnh
					var request = new GetPreSignedUrlRequest
					{
						BucketName = bucketName,
						Key = avatarKey,
						Expires = DateTime.Now.AddMinutes(60) // Thời gian URL hết hạn (tùy chỉnh)
					};

					// Lấy đường dẫn URL đã ký hợp lệ của tệp ảnh
					string imageUrl = client.GetPreSignedURL(request);
					return imageUrl;
				}
			}
			catch (Exception ex)
			{
				// Xử lý ngoại lệ và ghi log
				// Thay vì in ra message, bạn có thể ghi log chi tiết về lỗi để dễ dàng gỡ lỗi sau này
				Console.WriteLine($"Error getting avatar URL from S3: {ex.Message}");
				return null;
			}
		}
	}
}

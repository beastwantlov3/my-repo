﻿using BusinessObject.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Repositories;

namespace APIConnect.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	public class SaleController : ControllerBase

	{
        private IAccountRepository repositoryAccount = new AccountRepository();
        private ISaleRepository repository = new SaleRepository();
		[HttpGet]
		[Route("list")]
		public async Task<ActionResult<IEnumerable<Sale>>> GetSale()
		{
			return repository.GetSale();
		}
		//[HttpGet]
		[HttpGet("{id}")]
		public async Task<ActionResult<Sale>> GetCurrentSaleById(int id)
		{
			return repository.GetSalebyId(id);
		}

        [HttpGet]
        [Route("get-sale-id")]
        public async Task<ActionResult<IEnumerable<Sale>>> GetSaleById()
        {
            // Trích xuất thông tin người dùng từ mã thông báo
            var username = HttpContext.User.Identity.Name;
            if (!string.IsNullOrEmpty(username))
            {
                //Trả về thông tin người dùng
                var account = repositoryAccount.GetAccountByUsername(username);
                if (account != null)
                {
                    var customer = repository.GetSalebyId((int)account.SaleId);
                    return Ok(customer);
                }
                else
                {
                    // Người dùng không tồn tại trong cơ sở dữ liệu
                    return NotFound();
                }
            }
            else
            {
                // Người dùng chưa đăng nhập, trả về lỗi hoặc chuyển hướng đến trang đăng nhập
                return Unauthorized(new { error = "Người dùng chưa đăng nhập." });
            }
        }
        [HttpGet]
        [Route("doanhthuthang1")]
        public IActionResult GetOrder1(int id)
        {
            VPSContext context = new VPSContext();
            var totalRevenue = context.Orders
        .Where(o => o.SaleId == id && o.OrderDate.Value.Month == 1)
        .Sum(o => o.Price);
            return Ok(totalRevenue);
        }
        [HttpGet]
        [Route("doanhthuthang2")]
        public IActionResult GetOrder2(int id)
        {
            VPSContext context = new VPSContext();
            var totalRevenue = context.Orders
        .Where(o => o.SaleId == id && o.OrderDate.Value.Month == 2)
        .Sum(o => o.Price);
            return Ok(totalRevenue);
        }
        [HttpGet]
        [Route("doanhthuthang3")]
        public IActionResult GetOrder3(int id)
        {
            VPSContext context = new VPSContext();
            var totalRevenue = context.Orders
        .Where(o => o.SaleId == id && o.OrderDate.Value.Month == 3)
        .Sum(o => o.Price);
            return Ok(totalRevenue);
        }
        [HttpGet]
        [Route("doanhthuthang4")]
        public IActionResult GetOrder4(int id)
        {
            VPSContext context = new VPSContext();
            var totalRevenue = context.Orders
        .Where(o => o.SaleId == id && o.OrderDate.Value.Month == 4)
        .Sum(o => o.Price);
            return Ok(totalRevenue);
        }
        [HttpGet]
        [Route("doanhthuthang5")]
        public IActionResult GetOrder5(int id)
        {
            VPSContext context = new VPSContext();
            var totalRevenue = context.Orders
        .Where(o => o.SaleId == id && o.OrderDate.Value.Month == 5)
        .Sum(o => o.Price);
            return Ok(totalRevenue);
        }
        [HttpGet]
        [Route("doanhthuthang6")]
        public IActionResult GetOrder6(int id)
        {
            VPSContext context = new VPSContext();
            var totalRevenue = context.Orders
        .Where(o => o.SaleId == id && o.OrderDate.Value.Month == 6)
        .Sum(o => o.Price);
            return Ok(totalRevenue);
        }
        [HttpGet]
        [Route("doanhthuthang7")]
        public IActionResult GetOrder7(int id)
        {
            VPSContext context = new VPSContext();
            var totalRevenue = context.Orders
        .Where(o => o.SaleId == id && o.OrderDate.Value.Month == 7)
        .Sum(o => o.Price);
            return Ok(totalRevenue);
        }
        [HttpGet]
        [Route("doanhthuthang8")]
        public IActionResult GetOrder8(int id)
        {
            VPSContext context = new VPSContext();
            var totalRevenue = context.Orders
        .Where(o => o.SaleId == id && o.OrderDate.Value.Month == 8)
        .Sum(o => o.Price);
            return Ok(totalRevenue);
        }
        [HttpGet]
        [Route("doanhthuthang9")]
        public IActionResult GetOrder9(int id)
        {
            VPSContext context = new VPSContext();
            var totalRevenue = context.Orders
        .Where(o => o.SaleId == id && o.OrderDate.Value.Month == 9)
        .Sum(o => o.Price);
            return Ok(totalRevenue);
        }
        [HttpGet]
        [Route("doanhthuthang10")]
        public IActionResult GetOrder10(int id)
        {
            VPSContext context = new VPSContext();
            var totalRevenue = context.Orders
        .Where(o => o.SaleId == id && o.OrderDate.Value.Month == 10)
        .Sum(o => o.Price);
            return Ok(totalRevenue);
        }
        [HttpGet]
        [Route("doanhthuthang11")]
        public IActionResult GetOrder11(int id)
        {
            VPSContext context = new VPSContext();
            var totalRevenue = context.Orders
        .Where(o => o.SaleId == id && o.OrderDate.Value.Month == 11)
        .Sum(o => o.Price);
            return Ok(totalRevenue);
        }
        [HttpGet]
        [Route("doanhthuthang12")]
        public IActionResult GetOrder12(int id)
        {
            VPSContext context = new VPSContext();
            var totalRevenue = context.Orders
        .Where(o => o.SaleId == id && o.OrderDate.Value.Month == 12)
        .Sum(o => o.Price);
            return Ok(totalRevenue);
        }
        [HttpPut("{id}")]
		public async Task<IActionResult> PutSale(int id, Sale sale)
		{
			repository.UpdateSale(id, sale);
			return NoContent();
		}
		[HttpDelete("{id}")]
		public async Task<IActionResult> DeleteSale(int id)
		{
			var account = repository.GetSalebyId(id);
			if (account == null)
			{
				return NotFound();
			}

			repository.DeleteSale(id);
			return NoContent();
		}
		[HttpPost]
		public async Task<ActionResult<Sale>> PostSale(Sale sale)
		{
			repository.AddSale(sale);
			return NoContent();
		}
	}
}

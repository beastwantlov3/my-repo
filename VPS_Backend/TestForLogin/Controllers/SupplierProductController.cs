﻿using Amazon.S3.Model;
using Amazon.S3;
using APIConnect.DTO;
using BusinessObject.Models;
using DataAccess;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Repositories;
using SixLabors.ImageSharp.Formats.Png;
using Image = SixLabors.ImageSharp.Image;
using Amazon;

namespace APIConnect.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	public class SupplierProductController : ControllerBase
	{
		private readonly string bucketName = "system-viewpoint";
		private readonly string accessKey = "AKIAX3J7OK3364RPYPEB";
		private readonly string secretKey = "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";
		private ISupplierProductRepository repository = new SupplierProductRepository();
		[HttpGet]
		[Route("list")]
		public async Task<ActionResult<IEnumerable<SupplierProduct>>> GetSupplierProduct()
		{
			return repository.GetSupplierProduct();
		}

		//[HttpGet]
		[HttpGet("{id}")]
		public async Task<ActionResult<SupplierProduct>> GetSupplierProductById(int id)
		{
			return repository.GetSupplierProductbyId(id);
		}

		[HttpPut("{id}")]
		public async Task<IActionResult> PutSupplier(int id, SupplierProduct supplierProduct)
		{
			repository.UpdateSupplierProduct(id, supplierProduct);
			return NoContent();
		}

		[HttpDelete("{id}")]
		public async Task<IActionResult> DeleteSupplierProduct(int id)
		{
			var account = repository.GetSupplierProductbyId(id);
			if (account == null)
			{
				return NotFound();
			}

			repository.DeleteSupplierProduct(id);
			return NoContent();
		}

		[HttpPost]
		public async Task<IActionResult> PostSupplierProduct([FromForm] SupplierProductDTO supplierProductDTO)
		{
			if (supplierProductDTO.Image == null || supplierProductDTO.Image.Length <= 0)
			{
				return BadRequest("Không tìm thấy tệp ảnh.");
			}

			// Tạo và lưu thumbnail vào vị trí tạm thời
			string tempThumbnailPath = Path.GetTempFileName();
			int thumbnailWidth = 200; // Điều chỉnh kích thước thumbnail tùy ý
			int thumbnailHeight = 200; // Điều chỉnh kích thước thumbnail tùy ý

			using (var stream = new FileStream(tempThumbnailPath, FileMode.Create))
			{
				using (var image = Image.Load(supplierProductDTO.Image.OpenReadStream()))
				{
					image.Mutate(x => x.Resize(new ResizeOptions
					{
						Mode = ResizeMode.Max,
						Size = new SixLabors.ImageSharp.Size(thumbnailWidth, thumbnailHeight)
					}));

					image.Save(stream, new PngEncoder()); // Lưu thumbnail dưới dạng PNG
				}
			}

			// Tải thumbnail lên Amazon S3
			string s3ThumbnailUrl = await UploadImageToS3(supplierProductDTO.Image);

			// Xóa tệp tạm thời
			System.IO.File.Delete(tempThumbnailPath);
			// Tạo một đối tượng sản phẩm mới
			var supplierProduct = new SupplierProduct
			{
				ProductName = supplierProductDTO.ProductName,
				CategoryId = supplierProductDTO.CategoryId,
				UnitInStock = supplierProductDTO.UnitInStock,
				UnitPrice = supplierProductDTO.UnitPrice,
				Image = s3ThumbnailUrl,
				StatusId = supplierProductDTO.StatusId,
				Brand = supplierProductDTO.Brand,
				Description = supplierProductDTO.Description,
				Quality = supplierProductDTO.Quality,
			};
			repository.AddSupplierProduct(supplierProduct);
			return NoContent();
		}

		private async Task<string> UploadImageToS3(IFormFile imageFile)
		{
			// Tạo tên tệp duy nhất
			string extension = Path.GetExtension(imageFile.FileName);
			string filename = "product-" + Guid.NewGuid().ToString() + extension;

			// Tải ảnh lên Amazon S3
			var s3Client = new AmazonS3Client(accessKey, secretKey, RegionEndpoint.APSoutheast1);
			using (var imageStream = imageFile.OpenReadStream())
			{
				var putRequest = new PutObjectRequest
				{
					BucketName = bucketName,
					Key = "product/" + filename,
					InputStream = imageStream,
					ContentType = imageFile.ContentType
				};
				await s3Client.PutObjectAsync(putRequest);
			}

			// Trả về URL S3 của ảnh đã tải lên
			return "product/" + filename;
		}
	}
}

﻿using Amazon.S3.Model;
using Amazon.S3.Transfer;
using Amazon.S3;
using BusinessObject.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Repositories;
using System.Drawing.Drawing2D;
using System.Drawing;
using System.Text.Json;
using System.Text.Json.Serialization;
using Amazon;
using System.Drawing.Imaging;
using System.Security.Principal;
using APIConnect.DTO;
using System.Globalization;
using SixLabors.ImageSharp.Formats.Png;
using Image = SixLabors.ImageSharp.Image;
using System.Text;
using System.Security.Cryptography;

namespace APIConnect.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	public class ProductController : ControllerBase
	{
		private readonly string bucketName = "system-viewpoint";
		private readonly string accessKey = "AKIAX3J7OK3364RPYPEB";
		private readonly string secretKey = "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";
		private IProductCodeRepository repositoryProductCodeRepository = new ProductCodeRepository();
		private IProductRepository repository = new ProductRepository();
		private IProductDetailRepository repositoryProductDetail = new ProductDetailRepository();
		private IProductCodeRepository repositoryProductCode = new ProductCodeRepository();
		private readonly VPSContext _vPSContext;
		public ProductController(VPSContext vPSContext
			)
		{
			_vPSContext = vPSContext;
		}

		[HttpGet]
		public async Task<ActionResult<IEnumerable<Product>>> GetProduct()
		{
			VPSContext context = new VPSContext();
			var products = context.Products.Include(x => x.Category).Include(x => x.Status).ToList();

			foreach (var product in products)
			{
				product.Image = GetImageUrlForProduct(product.Image);
			}

			return products;
		}

		private string GetImageUrlForProduct(string avatarKey)
		{
			try
			{
				// Tạo đối tượng Amazon S3
				using (var client = new AmazonS3Client(accessKey, secretKey, RegionEndpoint.APSoutheast1))
				{
					// Tạo request để lấy đường dẫn URL của tệp ảnh
					var request = new GetPreSignedUrlRequest
					{
						BucketName = bucketName,
						Key = avatarKey,
						Expires = DateTime.Now.AddMinutes(60) // Thời gian URL hết hạn (tùy chỉnh)
					};

					// Lấy đường dẫn URL đã ký hợp lệ của tệp ảnh
					string imageUrl = client.GetPreSignedURL(request);
					return imageUrl;
				}
			}
			catch (Exception ex)
			{
				// Xử lý ngoại lệ và ghi log
				// Thay vì in ra message, bạn có thể ghi log chi tiết về lỗi để dễ dàng gỡ lỗi sau này
				Console.WriteLine($"Error getting avatar URL from S3: {ex.Message}");
				return null;
			}
		}

		[HttpGet("[action]/{cateid}")]
		public async Task<ActionResult<IEnumerable<Product>>> GetProductByCateId(int cateid)
		{
			if (cateid == 0)
			{
				var products = repository.GetProduct();

				// Iterate through each product and set the AvatarUrl property using GetAvatarUrlFromS3 method
				foreach (var product in products)
				{
					product.Image = GetImageUrlForProduct(product.Image);
				}
				return products;
			}
			else
			{
				var pCid = repository.GetProductByCateId(cateid);
				foreach (var products in pCid)
				{
					products.Image = GetImageUrlForProduct(products.Image);
				}
				return pCid;
			}
		}
		[HttpGet("[action]/{id}")]
		public async Task<ActionResult<Product>> GetProductById(int id)
		{
			return repository.GetProductById(id);
		}
		[HttpGet("[action]/{text}")]
		public async Task<ActionResult<IEnumerable<Product>>> GetProductByText(string text)
		{
			return repository.GetProductByName(text);
		}
		[HttpGet]
		[Route("top4newproduct")]
		public async Task<ActionResult<IEnumerable<Product>>> GetTop4NewProduct()
		{
			var products = repository.Get4NewProduct();
			foreach (var product in products)
			{
				product.Image = GetImageUrlForProduct(product.Image);
			}
			return products;
		}
		[HttpGet]
		[Route("top4expensiveproduct")]
		public async Task<ActionResult<IEnumerable<Product>>> GetTop4ExpensiveProduct()
		{
			var products = repository.Get4ExpensiveProduct();
			foreach (var product in products)
			{
				product.Image = GetImageUrlForProduct(product.Image);
			}
			return products;
		}
		[HttpGet]
		[Route("top4bestsale")]
		public IActionResult GetTop4SellingProducts()
		{
			VPSContext context = new VPSContext();
			var topProducts = context.OrderDetails.Include(x => x.Product)
							.Join(context.Products, od => od.ProductId, p => p.ProductId, (od, p) => new { od, p })
							.GroupBy(x => new { x.p.ProductId, x.p.ProductName, x.p.Image, x.p.UnitPrice, x.p.UnitInStock, x.p.StatusId, x.p.Brand, x.p.Description, x.p.DiscountDate, x.p.Discount, x.p.Quality })
							.Select(g => new Top4SellingProductsDTO
							{
								ProductId = g.Key.ProductId,
								ProductName = g.Key.ProductName,
								UnitPrice = g.Key.UnitPrice,
								StatusId = g.Key.StatusId,
								Brand = g.Key.Brand,
								Description = g.Key.Description,
								Discount = g.Key.Discount,
								DiscountDate = g.Key.DiscountDate,
								Quality = g.Key.Quality,
								Image = g.Key.Image,
								UnitInStock = g.Key.UnitInStock,
								TotalOrder = g.Sum(x => x.od.Quantity)
							})
							.OrderByDescending(x => x.TotalOrder)
							.Take(4)
							.ToList();

			foreach (var product in topProducts)
			{
				product.Image = GetImageUrlForProduct(product.Image);
			}
			return Ok(topProducts);
		}

		[HttpGet]
		[Route("top8bestsale")]
		public IActionResult GetTop8SellingProducts()
		{
			VPSContext context = new VPSContext();
			var topProducts = context.OrderDetails.Include(x => x.Product)
							.Join(context.Products, od => od.ProductId, p => p.ProductId, (od, p) => new { od, p })
							.GroupBy(x => new { x.p.ProductId, x.p.ProductName, x.p.Image })
							.Select(g => new
							{
								g.Key.ProductId,
								g.Key.ProductName,
								g.Key.Image,
								TotalOrder = g.Sum(x => x.od.Quantity)
							})
							.OrderByDescending(x => x.TotalOrder)
							.Take(8)
							.ToList();
			return Ok(topProducts);
		}

		[HttpPut("{id}")]
		public async Task<IActionResult> PutProduct(int id, Product product)
		{
			repository.UpdateProduct(id, product);
			return NoContent();
		}
		[HttpDelete("{id}")]
		public async Task<IActionResult> DeleteOrderDetail(int id)
		{
			var account = repository.GetProductById(id);
			if (account == null)
			{
				return NotFound();
			}

			repository.DeleteProduct(id);
			return NoContent();
		}

		[HttpPost]
		public async Task<ActionResult<Product>> PostProductt(Product product)
		{

			repository.AddProduct(product);
			return NoContent();
		}

		[HttpPost("update-product")]
		public async Task<IActionResult> UploadProduct([FromForm] ProductDTO productDTO)
		{
			if (productDTO.Image == null || productDTO.Image.Length <= 0)
			{
				return BadRequest("Không tìm thấy tệp ảnh.");
			}

			// Tạo và lưu thumbnail vào vị trí tạm thời
			string tempThumbnailPath = Path.GetTempFileName();
			int thumbnailWidth = 200; // Điều chỉnh kích thước thumbnail tùy ý
			int thumbnailHeight = 200; // Điều chỉnh kích thước thumbnail tùy ý

			using (var stream = new FileStream(tempThumbnailPath, FileMode.Create))
			{
				using (var image = Image.Load(productDTO.Image.OpenReadStream()))
				{
					image.Mutate(x => x.Resize(new ResizeOptions
					{
						Mode = ResizeMode.Max,
						Size = new SixLabors.ImageSharp.Size(thumbnailWidth, thumbnailHeight)
					}));

					image.Save(stream, new PngEncoder()); // Lưu thumbnail dưới dạng PNG
				}
			}

			// Tải thumbnail lên Amazon S3
			string s3ThumbnailUrl = await UploadImageToS3(productDTO.Image);

			// Xóa tệp tạm thời
			System.IO.File.Delete(tempThumbnailPath);

			//List thumbnail coreImage
			List<string> thumbnailUrls = new List<string>();

			foreach (var imageFile in productDTO.CoreImage)
			{
				string tempListThumbnailPath = Path.GetTempFileName();
				int thumbnailWidthList = 200; // Điều chỉnh kích thước thumbnail tùy ý
				int thumbnailHeightList = 200; // Điều chỉnh kích thước thumbnail tùy ý

				using (var stream = new FileStream(tempListThumbnailPath, FileMode.Create))
				{
					using (var image = Image.Load(imageFile.OpenReadStream()))
					{
						image.Mutate(x => x.Resize(new ResizeOptions
						{
							Mode = ResizeMode.Max,
							Size = new SixLabors.ImageSharp.Size(thumbnailWidthList, thumbnailHeightList)
						}));

						image.Save(stream, new PngEncoder()); // Lưu thumbnail dưới dạng PNG
					}
				}

				string s3ThumbnailUrl1 = await UploadImageToS3(imageFile);
				thumbnailUrls.Add(s3ThumbnailUrl1);
			}

			// Tạo một đối tượng sản phẩm mới
			var product = new Product
			{
				ProductName = productDTO.ProductName,
				CategoryId = productDTO.CategoryId,
				UnitInStock = productDTO.UnitInStock,
				UnitPrice = productDTO.UnitPrice,
				Image = s3ThumbnailUrl,
				StatusId = productDTO.StatusId,
				Brand = productDTO.Brand,
				Description = productDTO.Description,
				Quality = productDTO.Quality,
				Discount = productDTO.Discount,
			};
			repository.AddProduct(product);

			foreach (var thumbnailUrl in thumbnailUrls)
			{
				var productDetail = new ProductDetail
				{
					CoreImage = thumbnailUrl,
					ProductId = product.ProductId,
				};
				repositoryProductDetail.AddProductDetail(productDetail);
			}
			foreach (var qualityPecentage in productDTO.QualityPercentage)
			{
				var productCode = new ProductCode
				{
					ProductId = product.ProductId,
					Code = GenerateUniqueProductCode(product.ProductId, qualityPecentage), // Tạo mã sản phẩm
					IsAvailable = true,
					QualityPercentage = qualityPecentage, // Tính phần trăm chất lượng
				};
				//_vPSContext.Add(productCode);
				//_vPSContext.SaveChanges();
				repositoryProductCode.AddProductCode(productCode);
			}

			// Trả về phản hồi thành công
			return Ok(product);
		}
		private string GenerateUniqueProductCode(int productId, decimal qualityPercentage)
		{
			using (SHA256 sha256 = SHA256.Create())
			{
				string input = $"{productId}-{qualityPercentage}"; // Kết hợp thông tin liên quan đến sản phẩm
				byte[] hashBytes = sha256.ComputeHash(Encoding.UTF8.GetBytes(input));
				string hash = BitConverter.ToString(hashBytes).Replace("-", "").Substring(0, 5); // Lấy 10 ký tự đầu của mã băm

				string additionalText = "VPS"; // Chuỗi chữ bạn muốn thêm
				string uniqueCode = $"{additionalText}{hash}";

				return uniqueCode;
			}
		}
		private async Task<string> UploadImageToS3(IFormFile imageFile)
		{
			// Tạo tên tệp duy nhất
			string extension = Path.GetExtension(imageFile.FileName);
			string filename = "thumbnail-" + Guid.NewGuid().ToString() + extension;

			// Tải ảnh lên Amazon S3
			var s3Client = new AmazonS3Client(accessKey, secretKey, RegionEndpoint.APSoutheast1);
			using (var imageStream = imageFile.OpenReadStream())
			{
				var putRequest = new PutObjectRequest
				{
					BucketName = bucketName,
					Key = "product/" + filename,
					InputStream = imageStream,
					ContentType = imageFile.ContentType
				};
				await s3Client.PutObjectAsync(putRequest);
			}

			// Trả về URL S3 của ảnh đã tải lên
			return "product/" + filename;
		}

		[HttpGet("{productcodeid}")]
		public async Task<ActionResult<List<ProductCode>>> GetProductCodeByProductId(int productcodeid)
		{
			var product = repository.GetProductById(productcodeid);
			List<ProductCode> productCodes = repositoryProductCodeRepository.GetProductCodeByProductId(product.ProductId);
			foreach(var code in productCodes)
			{
				code.ProductId = product.ProductId;
				code.Code = code.Code;
				code.QualityPercentage = code.QualityPercentage;
				code.IsAvailable = code.IsAvailable;
			}
			return productCodes;
		}
	}
}

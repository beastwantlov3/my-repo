﻿using BusinessObject.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace APIConnect.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PStatusController : ControllerBase
    {
        [HttpGet]
        [Route("list")]
        public async Task<ActionResult<IEnumerable<Pstatus>>> GetPStatus()
        {
            VPSContext context = new VPSContext();
            return context.Pstatuses.ToList();
        }
    }
}

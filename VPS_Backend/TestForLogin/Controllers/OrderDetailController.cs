﻿using Amazon.S3.Model;
using Amazon.S3;
using BusinessObject.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Repositories;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using Amazon;

namespace APIConnect.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	public class OrderDetailController : ControllerBase
	{
        private readonly string bucketName = "system-viewpoint";
        private readonly string accessKey = "AKIAX3J7OK3364RPYPEB";
        private readonly string secretKey = "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";
        private IOrderDetailRepository repository = new OrderDetailRepository();
		[HttpGet]
		[Route("list")]
		public async Task<ActionResult<IEnumerable<OrderDetail>>> GetOrderDetail()
		{
			return repository.GetOrderDetail();
		}
        private string GetImageUrlForProduct(string avatarKey)
        {
            try
            {
                // Tạo đối tượng Amazon S3
                using (var client = new AmazonS3Client(accessKey, secretKey, RegionEndpoint.APSoutheast1))
                {
                    // Tạo request để lấy đường dẫn URL của tệp ảnh
                    var request = new GetPreSignedUrlRequest
                    {
                        BucketName = bucketName,
                        Key = avatarKey,
                        Expires = DateTime.Now.AddMinutes(60) // Thời gian URL hết hạn (tùy chỉnh)
                    };

                    // Lấy đường dẫn URL đã ký hợp lệ của tệp ảnh
                    string imageUrl = client.GetPreSignedURL(request);
                    return imageUrl;
                }
            }
            catch (Exception ex)
            {
                // Xử lý ngoại lệ và ghi log
                // Thay vì in ra message, bạn có thể ghi log chi tiết về lỗi để dễ dàng gỡ lỗi sau này
                Console.WriteLine($"Error getting avatar URL from S3: {ex.Message}");
                return null;
            }
        }
        [HttpGet]
		[Route("listbyod")]
		public async Task<ActionResult<IEnumerable<OrderDetail>>> GetOrderDetailByOrderId(int id)
		{
			var od = repository.GetOrderDetailByOrderId(id);
			foreach(var item in od)
			{
				item.Product.Image = GetImageUrlForProduct(item.Product.Image);
            }
            return od;
		}
		[HttpGet]
		[Route("totalprice")]
		public async Task<double> GetTotalOrder()
		{
			return repository.GetTotalOrderDetail();
		}
		[HttpGet("{id}")]
		public async Task<ActionResult<OrderDetail>> GetOrderById(int id)
		{
			return repository.GetOrderDetailById(id);
		}
		[HttpPut("{id}")]
		public async Task<IActionResult> PutOrderDetail(int id, OrderDetail ord)
		{
			repository.UpdateOrderDetail(id, ord);
			return NoContent();
		}
		[HttpDelete("{id}")]
		public async Task<IActionResult> DeleteOrderDetail(int id)
		{
			var account = repository.GetOrderDetailById(id);
			if (account == null)
			{
				return NotFound();
			}

			repository.DeleteOrderDetail(id);
			return NoContent();
		}
		[HttpPost]
		public async Task<ActionResult<OrderDetail>> PostAccount(OrderDetail ord)
		{
			repository.AddOrderDetail(ord);
			return NoContent();
		}
	}
}

﻿using Amazon.S3.Model;
using Amazon.S3;
using BusinessObject.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Repositories;
using Amazon;
using Microsoft.AspNetCore.Authorization;

namespace APIConnect.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	public class CustomerController : ControllerBase
	{
		private readonly string bucketName = "system-viewpoint"; // Thay thế bằng tên bucket S3 của bạn
		private readonly string accessKey = "AKIAX3J7OK3364RPYPEB"; // Thay thế bằng AWS Access Key ID của bạn
		private readonly string secretKey = "7Qhj1qCJGDeVvXDdA8QUccKO1dFR66vLjiSH5YfV"; // Thay thế bằng AWS Secret Access Key của bạn
		private IAccountRepository repositoryAccount = new AccountRepository();
		private ICustomerRepository repository = new CustomerRepository();

		[HttpGet]
		[Route("list")]
		public async Task<ActionResult<IEnumerable<Customer>>> GetCustomer()
		{
			return repository.GetCustomer();
		}
		[HttpGet]
		[Route("get8customer")]
		public async Task<ActionResult<IEnumerable<Customer>>> GetCustomerByOrderId()
		{
			return repository.Get8Customer();
		}
		[HttpGet]
		[Route("total")]
		public async Task<int> GetTotalCustomer()
		{
			return repository.GetTotalCustomer();
		}
		[HttpGet("{id}")]
		public async Task<ActionResult<Customer>> GetCustomerById(int id)
		{
			return repository.GetCustomerById(id);
		}

		//private string GetImageUrlForProduct(string avatarKey)
		//{
		//	try
		//	{
		//		// Tạo đối tượng Amazon S3
		//		using (var client = new AmazonS3Client(accessKey, secretKey, RegionEndpoint.APSoutheast1))
		//		{
		//			// Tạo request để lấy đường dẫn URL của tệp ảnh
		//			var request = new GetPreSignedUrlRequest
		//			{
		//				BucketName = bucketName,
		//				Key = avatarKey,
		//				Expires = DateTime.Now.AddMinutes(60) // Thời gian URL hết hạn (tùy chỉnh)
		//			};

		//			// Lấy đường dẫn URL đã ký hợp lệ của tệp ảnh
		//			string imageUrl = client.GetPreSignedURL(request);
		//			return imageUrl;
		//		}
		//	}
		//	catch (Exception ex)
		//	{
		//		// Xử lý ngoại lệ và ghi log
		//		// Thay vì in ra message, bạn có thể ghi log chi tiết về lỗi để dễ dàng gỡ lỗi sau này
		//		Console.WriteLine($"Error getting avatar URL from S3: {ex.Message}");
		//		return null;
		//	}
		//}

		[HttpGet("[action]/{cusid}")]
		public async Task<ActionResult<IdentityCard>> GetIdentityCardByCustomerId(int cusid)
		{
			VPSContext context = new VPSContext();
			var Icard = context.IdentityCards.FirstOrDefault(x => x.CustomerId == cusid);
			var result = new
			{
				IdentityCardImage = GetAvatarUrlFromS3(Icard.IdentityCardImage.Trim()),
				BackIdentityCard = GetAvatarUrlFromS3(Icard.BackIdentityCard.Trim())
			};
			return Ok(result);
		}

		private string GetAvatarUrlFromS3(string avatarKey)
		{
			try
			{
				// Tạo đối tượng Amazon S3
				using (var client = new AmazonS3Client(accessKey, secretKey, RegionEndpoint.APSoutheast1))
				{
					// Tạo request để lấy đường dẫn URL của tệp ảnh
					var request = new GetPreSignedUrlRequest
					{
						BucketName = bucketName,
						Key = avatarKey,
						Expires = DateTime.Now.AddMinutes(60) // Thời gian URL hết hạn (tùy chỉnh)
					};

					// Lấy đường dẫn URL đã ký hợp lệ của tệp ảnh
					string imageUrl = client.GetPreSignedURL(request);
					return imageUrl;
				}
			}
			catch (Exception ex)
			{
				// Xử lý ngoại lệ và ghi log
				// Thay vì in ra message, bạn có thể ghi log chi tiết về lỗi để dễ dàng gỡ lỗi sau này
				Console.WriteLine($"Error getting avatar URL from S3: {ex.Message}");
				return null;
			}
		}

		[HttpGet]
		[Route("get-customer-id")]
		public async Task<ActionResult<IEnumerable<Customer>>> GetSaleById()
		{
			// Trích xuất thông tin người dùng từ mã thông báo
			var username = HttpContext.User.Identity.Name;
			if (!string.IsNullOrEmpty(username))
			{
				//Trả về thông tin người dùng
				var account = repositoryAccount.GetAccountByUsername(username);
				if (account != null)
				{
					var customer = repository.GetCustomerById((int)account.SaleId);
					return Ok(customer);
				}
				else
				{
					// Người dùng không tồn tại trong cơ sở dữ liệu
					return NotFound();
				}
			}
			else
			{
				// Người dùng chưa đăng nhập, trả về lỗi hoặc chuyển hướng đến trang đăng nhập
				return Unauthorized(new { error = "Người dùng chưa đăng nhập." });
			}
		}
		[HttpPut("{id}")]
		public async Task<IActionResult> PutCustomer(int id, Customer cus)
		{
			repository.UpdateCustomer(id, cus);
			return NoContent();
		}
		[HttpDelete("{id}")]
		public async Task<IActionResult> DeleteCus(int id)
		{
			var account = repository.GetCustomerById(id);
			if (account == null)
			{
				return NotFound();
			}

			repository.DeleteCustomer(id);
			return NoContent();
		}
		[HttpPost]
		public async Task<ActionResult<Blog>> PostCustomer(Customer cus)
		{
			repository.AddCustomert(cus);
			return NoContent();
		}
	}
}

﻿using Amazon.S3.Transfer;
using Amazon.S3;
using APIConnect.DTO;
using BusinessObject.Models;
using Microsoft.AspNetCore.Mvc;
using Repositories;
using System.Globalization;
using Amazon;
using Amazon.S3.Model;
using Tesseract;
using System.Drawing;
using System.Text.RegularExpressions;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using OpenCvSharp;
using OpenCvSharp.Face;
using OpenCvSharp.Dnn;
using Size = OpenCvSharp.Size;
using Newtonsoft.Json;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using DataAccess;
using Org.BouncyCastle.Asn1.X509;
using Konscious.Security.Cryptography;
using System.Text;
using System.Security.Claims;
using Castle.Core.Resource;
using SixLabors.ImageSharp.Formats.Png;
using Image = SixLabors.ImageSharp.Image;
using MathNet.Numerics.Distributions;

namespace TestForLogin.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	public class AccountController : ControllerBase
	{
		private readonly string bucketName = "system-viewpoint";
		private readonly string accessKey = "AKIAX3J7OK3364RPYPEB";
		private readonly string secretKey = "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";
		private ICustomerRepository repositoryCustomer = new CustomerRepository();
		private IAccountRepository repository = new AccountRepository();
		private readonly VPSContext _vPSContext;
		public AccountController(VPSContext vPSContext
			)
		{
			_vPSContext = vPSContext;
		}
		[HttpGet]
		//[Authorize(AuthenticationSchemes = CookieAuthenticationDefaults.AuthenticationScheme, Policy = "AdminOnly")]
		[Route("list")]
		public async Task<ActionResult<IEnumerable<Account>>> GetAccount()
		{
			return repository.GetAccount();
		}
		[HttpGet]
		[Route("taikhoangthang1")]
		public IActionResult GetOrder1()
		{
			VPSContext context = new VPSContext();
			var totalAccount = context.Accounts
		.Where(o => o.CreatedDate >= Convert.ToDateTime("2023-01-01") && o.CreatedDate <= Convert.ToDateTime("2023-01-30"))
		.Count();
			return Ok(totalAccount);
		}
		[HttpGet]
		[Route("taikhoangthang2")]
		public IActionResult GetOrder2()
		{
			VPSContext context = new VPSContext();
			var totalAccount = context.Accounts
		.Where(o => o.CreatedDate >= Convert.ToDateTime("2023-02-01") && o.CreatedDate <= Convert.ToDateTime("2023-02-28"))
		.Count();
			return Ok(totalAccount);
		}
		[HttpGet]
		[Route("taikhoangthang3")]
		public IActionResult GetOrder3()
		{
			VPSContext context = new VPSContext();
			var totalAccount = context.Accounts
		.Where(o => o.CreatedDate >= Convert.ToDateTime("2023-03-01") && o.CreatedDate <= Convert.ToDateTime("2023-03-30"))
		.Count();
			return Ok(totalAccount);
		}
		[HttpGet]
		[Route("taikhoangthang4")]
		public IActionResult GetOrder4()
		{
			VPSContext context = new VPSContext();
			var totalAccount = context.Accounts
		.Where(o => o.CreatedDate >= Convert.ToDateTime("2023-04-01") && o.CreatedDate <= Convert.ToDateTime("2023-04-30"))
		.Count();
			return Ok(totalAccount);
		}
		[HttpGet]
		[Route("taikhoangthang5")]
		public IActionResult GetOrder5()
		{
			VPSContext context = new VPSContext();
			var totalAccount = context.Accounts
		.Where(o => o.CreatedDate >= Convert.ToDateTime("2023-05-01") && o.CreatedDate <= Convert.ToDateTime("2023-05-30"))
		.Count();
			return Ok(totalAccount);
		}
		[HttpGet]
		[Route("taikhoangthang6")]
		public IActionResult GetOrder6()
		{
			VPSContext context = new VPSContext();
			var totalAccount = context.Accounts
		.Where(o => o.CreatedDate >= Convert.ToDateTime("2023-06-01") && o.CreatedDate <= Convert.ToDateTime("2023-06-30"))
		.Count();
			return Ok(totalAccount);
		}
		[HttpGet]
		[Route("taikhoangthang7")]
		public IActionResult GetOrder7()
		{
			VPSContext context = new VPSContext();
			var totalAccount = context.Accounts
		.Where(o => o.CreatedDate >= Convert.ToDateTime("2023-07-01") && o.CreatedDate <= Convert.ToDateTime("2023-07-30"))
		.Count();
			return Ok(totalAccount);
		}
		[HttpGet]
		[Route("taikhoangthang8")]
		public IActionResult GetOrder8()
		{
			VPSContext context = new VPSContext();
			var totalAccount = context.Accounts
		.Where(o => o.CreatedDate >= Convert.ToDateTime("2023-08-01") && o.CreatedDate <= Convert.ToDateTime("2023-08-30"))
		.Count();
			return Ok(totalAccount);
		}
		[HttpGet]
		[Route("taikhoangthang9")]
		public IActionResult GetOrder9()
		{
			VPSContext context = new VPSContext();
			var totalAccount = context.Accounts
		.Where(o => o.CreatedDate >= Convert.ToDateTime("2023-09-01") && o.CreatedDate <= Convert.ToDateTime("2023-09-30"))
		.Count();
			return Ok(totalAccount);
		}
		[HttpGet]
		[Route("taikhoangthang10")]
		public IActionResult GetOrder10()
		{
			VPSContext context = new VPSContext();
			var totalAccount = context.Accounts
		.Where(o => o.CreatedDate >= Convert.ToDateTime("2023-10-01") && o.CreatedDate <= Convert.ToDateTime("2023-10-30"))
		.Count();
			return Ok(totalAccount);
		}
		[HttpGet]
		[Route("taikhoangthang11")]
		public IActionResult GetOrder11()
		{
			VPSContext context = new VPSContext();
			var totalAccount = context.Accounts
		.Where(o => o.CreatedDate >= Convert.ToDateTime("2023-11-01") && o.CreatedDate <= Convert.ToDateTime("2023-11-30"))
		.Count();
			return Ok(totalAccount);
		}
		[HttpGet]
		[Route("taikhoangthang12")]
		public IActionResult GetOrder12()
		{
			VPSContext context = new VPSContext();
			var totalAccount = context.Accounts
		.Where(o => o.CreatedDate >= Convert.ToDateTime("2023-12-01") && o.CreatedDate <= Convert.ToDateTime("2023-12-30"))
		.Count();
			return Ok(totalAccount);
		}
		[HttpGet]
		[Route("AccountByDate")]
		public IActionResult GetMoneyByDate(string start, string end)
		{
			VPSContext context = new VPSContext();
			var totalRevenue = context.Accounts
		.Where(o => o.CreatedDate >= Convert.ToDateTime(start) && o.CreatedDate <= Convert.ToDateTime(end))
		.Count();
			return Ok(totalRevenue);
		}
		[HttpGet]
		[Route("GetAllEmployee")]
		public async Task<ActionResult<IEnumerable<Account>>> GetAllEmployee()
		{
			return repository.GetAccountEmployee();
		}
		[HttpGet]
		[Route("total")]
		public async Task<int> GetTotalAccount()
		{
			return repository.GetTotalAccount();
		}
		[HttpGet("{id}")]
		public async Task<ActionResult<Account>> GetAccountById(int id)
		{
			return repository.GetAccountById(id);
		}
		[HttpPut("{id}")]
		public async Task<IActionResult> PutAccount(int id, Account account)
		{
			repository.UpdateAccount(id, account);
			return NoContent();
		}
		[HttpDelete("{id}")]
		public async Task<IActionResult> DeleteAccount(int id)
		{
			var account = repository.GetAccountById(id);
			if (account == null)
			{
				return NotFound();
			}

			repository.DeleteAccount(id);
			return NoContent();
		}
		[HttpPost]
		public async Task<ActionResult<Account>> PostAccount(Account acc)
		{
			acc.Password = HashPassword(acc.Password);
			repository.AddAccount(acc);
			return NoContent();
		}

		private string HashPassword(string password)
		{
			using (var argon2 = new Argon2id(Encoding.UTF8.GetBytes(password)))
			{
				argon2.DegreeOfParallelism = 8; // Độ song song (có thể điều chỉnh)
				argon2.MemorySize = 65536; // Kích thước bộ nhớ (có thể điều chỉnh)
				argon2.Iterations = 4; // Số lượt lặp (có thể điều chỉnh)

				var hashBytes = argon2.GetBytes(32); // Chiều dài của mã hash (có thể điều chỉnh)
				var hashedPassword = Convert.ToBase64String(hashBytes);

				return hashedPassword;
			}
		}

		[Authorize]
		[HttpGet("get-user")]
		//[Authorize(Policy = "CustomersOnly")]
		public IActionResult GetLoggedInUser()
		{
			var userName = HttpContext.User.Identity.Name;
			if (!string.IsNullOrEmpty(userName))
			{
				var roleName = HttpContext.User.Claims.Where(c => c.Type == ClaimTypes.Role).Select(c => c.Value).ToList();
				//Trả về thông tin người dùng
				var account = repository.GetAccountByUsername(userName);
				if (roleName.Contains("Admin"))
				{
					var admin = _vPSContext.Admins.Find((int)account.AdminId);
					if (admin != null)
					{
						string avatarUrl = GetAvatarUrlFromS3(account.Avatar);
						// Khách hàng tồn tại, tạo đối tượng response chứa thông tin người dùng và thông tin khách hàng
						var result = new
						{
							UserName = account.UserName,
							Email = account.Email,
							Phone = admin.Phone,
							Address = admin.Address,
							DOB = admin.Dob.Value.ToString("dd/MM/yyyy"),
							FullName = admin.FullName,
							CreatedDate = account.CreatedDate.Value.ToString("dd/MM/yyyy"),
							profile = account.Profile,
							Avatar = avatarUrl,
							Role = "Admin"
						};

						return Ok(result);
					}
					else
					{
						// Khách hàng không tồn tại trong cơ sở dữ liệu
						return NotFound();
					}
				}
				else if (roleName.Contains("Customers"))
				{
					var customer = repositoryCustomer.GetCustomerById((int)account.CustomerId);
					if (customer != null)
					{
						string avatarUrl = GetAvatarUrlFromS3(account.Avatar);
						// Khách hàng tồn tại, tạo đối tượng response chứa thông tin người dùng và thông tin khách hàng
						var result = new
						{
							UserName = account.UserName,
							Email = account.Email,
							Phone = customer.Phone,
							Address = customer.Address,
							Balance = customer.Balance,
							DOB = customer.Dob.Value.ToString("dd/MM/yyyy"),
							FullName = customer.FullName,
							CreatedDate = account.CreatedDate.Value.ToString("dd/MM/yyyy"),
							profile = account.Profile,
							Avatar = avatarUrl,
						};

						return Ok(result);
					}
					else
					{
						// Khách hàng không tồn tại trong cơ sở dữ liệu
						return NotFound();
					}
				}
				else if (roleName.Contains("Blogger"))
				{
					var blogger = _vPSContext.Bloggers.Find((int)account.BloggerId);
					if (blogger != null)
					{
						string avatarUrl = GetAvatarUrlFromS3(account.Avatar);
						// Khách hàng tồn tại, tạo đối tượng response chứa thông tin người dùng và thông tin khách hàng
						var result = new
						{
							UserName = account.UserName,
							Email = account.Email,
							Phone = blogger.Phone,
							Address = blogger.Address,
							DOB = blogger.Dob.Value.ToString("dd/MM/yyyy"),
							FullName = blogger.FullName,
							CreatedDate = account.CreatedDate.Value.ToString("dd/MM/yyyy"),
							profile = account.Profile,
							Avatar = avatarUrl,
							Role = "Nhân viên viết blog"
						};

						return Ok(result);
					}
					else
					{
						// Khách hàng không tồn tại trong cơ sở dữ liệu
						return NotFound();
					}
				}
				else if (roleName.Contains("Manager"))
				{
					var manager = _vPSContext.Managers.Find((int)account.ManagerId);
					if (manager != null)
					{
						string avatarUrl = GetAvatarUrlFromS3(account.Avatar);
						// Khách hàng tồn tại, tạo đối tượng response chứa thông tin người dùng và thông tin khách hàng
						var result = new
						{
							UserName = account.UserName,
							Email = account.Email,
							Phone = manager.Phone,
							Address = manager.Address,
							DOB = manager.Dob.Value.ToString("dd/MM/yyyy"),
							FullName = manager.FullName,
							CreatedDate = account.CreatedDate.Value.ToString("dd/MM/yyyy"),
							profile = account.Profile,
							Avatar = avatarUrl,
							Role = "Quản trị viên"
						};

						return Ok(result);
					}
					else
					{
						// Khách hàng không tồn tại trong cơ sở dữ liệu
						return NotFound();
					}
				}
				else if (roleName.Contains("Sale"))
				{
					var sale = _vPSContext.Sales.Find((int)account.SaleId);
					if (sale != null)
					{
						string avatarUrl = GetAvatarUrlFromS3(account.Avatar);
						// Khách hàng tồn tại, tạo đối tượng response chứa thông tin người dùng và thông tin khách hàng
						var result = new
						{
							UserName = account.UserName,
							Email = account.Email,
							Phone = sale.Phone,
							Address = sale.Address,
							DOB = sale.Dob.Value.ToString("dd/MM/yyyy"),
							FullName = sale.FullName,
							CreatedDate = account.CreatedDate.Value.ToString("dd/MM/yyyy"),
							profile = account.Profile,
							Avatar = avatarUrl,
							Role = "Nhân viên bán hàng"
						};

						return Ok(result);
					}
					else
					{
						// Khách hàng không tồn tại trong cơ sở dữ liệu
						return NotFound();
					}
				}
				else if (roleName.Contains("Shipper"))
				{
					var shipper = _vPSContext.Shippers.Find((int)account.ShipperId);
					if (shipper != null)
					{
						string avatarUrl = GetAvatarUrlFromS3(account.Avatar);
						// Khách hàng tồn tại, tạo đối tượng response chứa thông tin người dùng và thông tin khách hàng
						var result = new
						{
							UserName = account.UserName,
							Email = account.Email,
							Phone = shipper.Phone,
							Address = shipper.Address,
							DOB = shipper.Dob.Value.ToString("dd/MM/yyyy"),
							FullName = shipper.FullName,
							CreatedDate = account.CreatedDate.Value.ToString("dd/MM/yyyy"),
							profile = account.Profile,
							Avatar = avatarUrl,
							Role = "Nhân viên giao hàng"
						};

						return Ok(result);
					}
					else
					{
						// Khách hàng không tồn tại trong cơ sở dữ liệu
						return NotFound();
					}
				}
				else if (roleName.Contains("Supplier"))
				{
					var supplier = _vPSContext.Suppliers.Find((int)account.SupplierId);
					if (supplier != null)
					{
						string avatarUrl = GetAvatarUrlFromS3(account.Avatar);
						// Khách hàng tồn tại, tạo đối tượng response chứa thông tin người dùng và thông tin khách hàng
						var result = new
						{
							UserName = account.UserName,
							Email = account.Email,
							Phone = supplier.Phone,
							Address = supplier.Address,
							DOB = supplier.Dob.Value.ToString("dd/MM/yyyy"),
							FullName = supplier.SupplierName,
							CreatedDate = account.CreatedDate.Value.ToString("dd/MM/yyyy"),
							profile = account.Profile,
							Avatar = avatarUrl,
							Role = "Nhà cung cấp"
						};

						return Ok(result);
					}
					else
					{
						// Khách hàng không tồn tại trong cơ sở dữ liệu
						return NotFound();
					}
				}

				else
				{
					// Unrecognized role, return BadRequest with an error message
					return BadRequest(new { error = "Unrecognized role." });
				}
			}
			else
			{
				// Người dùng chưa đăng nhập, trả về lỗi hoặc chuyển hướng đến trang đăng nhập
				return Unauthorized(new { error = "Người dùng chưa đăng nhập." });
			}
		}

		private string GetAvatarUrlFromS3(string avatarKey)
		{
			try
			{
				// Tạo đối tượng Amazon S3
				using (var client = new AmazonS3Client(accessKey, secretKey, RegionEndpoint.APSoutheast1))
				{
					// Tạo request để lấy đường dẫn URL của tệp ảnh
					var request = new GetPreSignedUrlRequest
					{
						BucketName = bucketName,
						Key = avatarKey,
						Expires = DateTime.Now.AddMinutes(60) // Thời gian URL hết hạn (tùy chỉnh)
					};

					// Lấy đường dẫn URL đã ký hợp lệ của tệp ảnh
					string imageUrl = client.GetPreSignedURL(request);
					return imageUrl;
				}
			}
			catch (Exception ex)
			{
				// Xử lý ngoại lệ và ghi log
				// Thay vì in ra message, bạn có thể ghi log chi tiết về lỗi để dễ dàng gỡ lỗi sau này
				Console.WriteLine($"Error getting avatar URL from S3: {ex.Message}");
				return null;
			}
		}

		[HttpPost("update-avatar")]
		public async Task<IActionResult> UploadAvatar(IFormFile file)
		{
			try
			{
				var username = HttpContext.User.Identity.Name;

				// Kiểm tra xem cookie có tồn tại và có thông tin người dùng không
				if (string.IsNullOrEmpty(username))
				{
					// Người dùng chưa đăng nhập, trả về lỗi hoặc chuyển hướng đến trang đăng nhập
					return Unauthorized(new { error = "Người dùng chưa đăng nhập." });
				}

				// Xoá ảnh cũ nếu có
				var account = repository.GetAccountByUsername(username);
				if (!string.IsNullOrEmpty(account.Avatar))
				{
					string existingFileName = account.Avatar.Split('/').Last();
					using (var client = new AmazonS3Client(accessKey, secretKey, RegionEndpoint.APSoutheast1)) // Thay thế RegionEndpoint nếu bạn sử dụng vùng khác
					{
						var deleteRequest = new DeleteObjectRequest
						{
							BucketName = bucketName,
							Key = existingFileName
						};
						await client.DeleteObjectAsync(deleteRequest);
					}
				}

				// Tải lên ảnh gốc lên Amazon S3
				string originalFileName = "avatar-" + Guid.NewGuid() + Path.GetExtension(file.FileName);
				using (var client = new AmazonS3Client(accessKey, secretKey, RegionEndpoint.APSoutheast1)) // Thay thế RegionEndpoint nếu bạn sử dụng vùng khác
				{
					using (var fileTransferUtility = new TransferUtility(client))
					{
						var uploadRequest = new TransferUtilityUploadRequest
						{
							BucketName = bucketName,
							Key = originalFileName,
							InputStream = file.OpenReadStream(),
							ContentType = file.ContentType
						};

						await fileTransferUtility.UploadAsync(uploadRequest);
					}
				}

				// Lưu đường dẫn ảnh vào cơ sở dữ liệu
				account.Avatar = $"{originalFileName}";
				repository.UpdateAccount(account);

				return Ok("Avatar uploaded successfully");
			}
			catch (Exception ex)
			{
				// Xử lý ngoại lệ và ghi log
				// Thay vì in ra message, bạn có thể ghi log chi tiết về lỗi để dễ dàng gỡ lỗi sau này
				Console.WriteLine($"Error: {ex.Message}");
				return StatusCode(500, "Internal Server Error");
			}
		}

		[HttpPut("update-profile")]
		public async Task<IActionResult> UpdateUser(UserProfile acc)
		{
			var username = HttpContext.User.Identity.Name;
			//string roleName = HttpContext.Request.Cookies["roleName"];
			if (!string.IsNullOrEmpty(username))
			{
				//Trả về thông tin người dùng
				var account = repository.GetAccountByUsername(username);
				if (account != null)
				{
					var customer = repositoryCustomer.GetCustomerById((int)account.CustomerId);
					if (customer != null)
					{
						// Khách hàng tồn tại, tạo đối tượng response chứa thông tin người dùng và thông tin khách hàng
						customer.FullName = acc.FullName;
						customer.Phone = acc.Phone;
						customer.Address = acc.Address;
						customer.Dob = Convert.ToDateTime(DateTime.ParseExact(acc.DOB, "dd/MM/yyyy", CultureInfo.InvariantCulture));
						account.Profile = acc.Profile;
						repository.UpdateAccount(account);
						repositoryCustomer.UpdateCustomer(customer);
						return Ok();
					}
					else
					{
						// Khách hàng không tồn tại trong cơ sở dữ liệu
						return NotFound();
					}
				}
				else
				{
					// Người dùng không tồn tại trong cơ sở dữ liệu
					return NotFound();
				}
			}
			else
			{
				// Người dùng chưa đăng nhập, trả về lỗi hoặc chuyển hướng đến trang đăng nhập
				return Unauthorized(new { error = "Người dùng chưa đăng nhập." });
			}
		}

		private readonly string tessdataPath = "E:\\viewpoint-system\\VPS_Backend\\TestForLogin\\tessdata";

		private byte[] ReadImageFile(IFormFile imageFile)
		{
			using (var memoryStream = new MemoryStream())
			{
				imageFile.CopyTo(memoryStream);
				return memoryStream.ToArray();
			}
		}

		private async Task<string> UploadImageToS3(IFormFile imageFile)
		{
			// Tạo tên tệp duy nhất
			string extension = Path.GetExtension(imageFile.FileName);
			string filename = "recognize-" + Guid.NewGuid().ToString() + extension;

			// Tải ảnh lên Amazon S3
			// (Thay thế "your-bucket-name" bằng tên thật của bucket S3 của bạn)
			var s3Client = new AmazonS3Client(accessKey, secretKey, RegionEndpoint.APSoutheast1);
			using (var imageStream = imageFile.OpenReadStream())
			{
				var putRequest = new PutObjectRequest
				{
					BucketName = bucketName,
					Key = "RecognizeIdentityCard/" + filename,
					InputStream = imageStream,
					ContentType = imageFile.ContentType
				};
				await s3Client.PutObjectAsync(putRequest);
			}

			// Trả về URL S3 của ảnh đã tải lên
			return "RecognizeIdentityCard/" + filename;
		}

		[HttpGet("get-identity")]
		public IActionResult IdentityCardImage()
		{
			var userName = HttpContext.User.Identity.Name;
			if (!string.IsNullOrEmpty(userName))
			{
				var account = repository.GetAccountByUsername(userName);
				if (account != null)
				{
					var customer = repositoryCustomer.GetCustomerById((int)account.CustomerId);
					if (customer != null)
					{
						var identityCard = _vPSContext.IdentityCards.FirstOrDefault(x => x.CustomerId == customer.CustomerId);
						string identityCardImage = GetAvatarUrlFromS3(identityCard.IdentityCardImage.Trim());
						string backIdentityCard = GetAvatarUrlFromS3(identityCard.BackIdentityCard.Trim());
						// Khách hàng tồn tại, tạo đối tượng response chứa thông tin người dùng và thông tin khách hàng
						var result = new
						{

							IdentityCardImage = identityCardImage,
							BackIdentityCard = backIdentityCard,
							IsValid = customer.IsValid,
							CardNumber = identityCard.CardNumber,
							Fullname = identityCard.FullName,
							DateOfBirth = identityCard.DateOfBirth,
							Gender = identityCard.Gender,
							Nationality = identityCard.Nationality,
							PlaceOfOrigin = identityCard.PlaceOfOrigin,
						};

						return Ok(result);
					}
					else
					{
						// Khách hàng không tồn tại trong cơ sở dữ liệu
						return NotFound();
					}
				}
				else
				{
					// Người dùng không tồn tại trong cơ sở dữ liệu
					return NotFound();
				}
			}
			else
			{
				// Người dùng chưa đăng nhập, trả về lỗi hoặc chuyển hướng đến trang đăng nhập
				return Unauthorized(new { error = "Người dùng chưa đăng nhập." });
			}
		}

		[Authorize]
		[HttpPost("recognize")]
		public async Task<IActionResult> RecognizeIdentityCard([FromForm] IdentityCardDTO identityCardDTO)
		{
			var username = HttpContext.User.Identity.Name;
			if (!string.IsNullOrEmpty(username))
			{
				// Tạo và lưu thumbnail vào vị trí tạm thời
				string tempThumbnailPath = Path.GetTempFileName();
				string tempThumbnailPath1 = Path.GetTempFileName();
				int thumbnailWidth = 200; // Điều chỉnh kích thước thumbnail tùy ý
				int thumbnailHeight = 200; // Điều chỉnh kích thước thumbnail tùy ý

				using (var stream = new FileStream(tempThumbnailPath, FileMode.Create))
				{
					using (var image = Image.Load(identityCardDTO.IdentityCardImage.OpenReadStream()))
					{
						image.Mutate(x => x.Resize(new ResizeOptions
						{
							Mode = ResizeMode.Max,
							Size = new SixLabors.ImageSharp.Size(thumbnailWidth, thumbnailHeight)
						}));

						image.Save(stream, new PngEncoder()); // Lưu thumbnail dưới dạng PNG
					}
				}
				using (var stream = new FileStream(tempThumbnailPath1, FileMode.Create))
				{
					using (var image = Image.Load(identityCardDTO.BackIdentityCard.OpenReadStream()))
					{
						image.Mutate(x => x.Resize(new ResizeOptions
						{
							Mode = ResizeMode.Max,
							Size = new SixLabors.ImageSharp.Size(thumbnailWidth, thumbnailHeight)
						}));

						image.Save(stream, new PngEncoder()); // Lưu thumbnail dưới dạng PNG
					}
				}
				// Tải thumbnail lên Amazon S3
				string identityCardImage = await UploadImageToS3(identityCardDTO.IdentityCardImage);
				string backIdentityCard = await UploadImageToS3(identityCardDTO.BackIdentityCard);

				// Xóa tệp tạm thời
				System.IO.File.Delete(tempThumbnailPath);
				System.IO.File.Delete(tempThumbnailPath1);

				//Trả về thông tin người dùng
				var account = repository.GetAccountByUsername(username);
				var customer = repositoryCustomer.GetCustomerById((int)account.CustomerId);
				if (identityCardDTO.IdentityCardImage == null || identityCardDTO.IdentityCardImage.Length <= 0)
				{
					return BadRequest("Vui lòng tải lên ảnh căn cước công dân.");
				}
				// Nhận dạng văn bản trong ảnh
				string recognizedText = RecognizeText(identityCardDTO.IdentityCardImage);
				// Kiểm tra nếu không phải là căn cước công dân
				//if (!recognizedText.Contains("Citizen ldentity Card"))
				//{
				//	return BadRequest("Không phải căn cước công dân.");
				//}
				string recognizedText2 = RecognizeText(identityCardDTO.BackIdentityCard);
				// Kiểm tra nếu không phải là căn cước công dân
				//if (!recognizedText.Contains("Personal identification"))
				//{
				//	return BadRequest("Không phải căn cước công dân mặt sau.");
				//}
				string citizenIdentityCardNumber = GetCitizenIdentityCardNumber(recognizedText);
				string fullName = GetFullNameFromRecognizedText(recognizedText);
				DateTime dateOfBirth = GetDateOfBirthFromRecognizedText(recognizedText); // Change the type to DateTime?
				bool? gender = GetGenderFromRecognizedText(recognizedText);
				string nationality = GetNationalityFromRecognizedText(recognizedText);
				string placeOfOrigin = GetPlaceOfOriginFromRecognizedText(recognizedText);
				var identityCard = new IdentityCard
				{
					CardNumber = citizenIdentityCardNumber,
					FullName = fullName,
					DateOfBirth = dateOfBirth,
					Gender = gender,
					Nationality = nationality,
					PlaceOfOrigin = placeOfOrigin,
					IdentityCardImage = identityCardImage,
					BackIdentityCard = backIdentityCard,
				};
				identityCard.CustomerId = customer.CustomerId;
				_vPSContext.IdentityCards.Add(identityCard);
				_vPSContext.SaveChanges();

				return Ok(new
				{
					RecognizedText = recognizedText,
					RecognizedText2 = recognizedText2,
					CitizenIdentityCardNumber = citizenIdentityCardNumber,
					FullName = fullName,
					DateOfBirth = dateOfBirth,
					Gender = gender,
					Nationality = nationality,
					PlaceOfOrigin = placeOfOrigin,
				});
			}
			else
			{
				// Người dùng chưa đăng nhập, trả về lỗi hoặc chuyển hướng đến trang đăng nhập
				return Unauthorized(new { error = "Người dùng chưa đăng nhập." });
			}
		}

		private string RecognizeText(IFormFile imageFile)
		{
			using (var engine = new TesseractEngine(tessdataPath, "vie", EngineMode.Default))
			{
				using (var image = Pix.LoadFromMemory(ReadImageFile(imageFile)))
				{
					using (var page = engine.Process(image))
					{
						return page.GetText();
					}
				}
			}
		}

		private string GetCitizenIdentityCardNumber(string recognizedText)
		{
			// Use regex to find the Citizen Identity Card number in the recognizedText string
			string pattern = @"\b\d{12}\b";

			Match match = Regex.Match(recognizedText, pattern);
			if (match.Success)
			{
				// If the card number is found in the string, extract and return it
				return match.Value;
			}

			// If the card number is not found, return null or an empty string
			return null;
		}

		//lấy tên từ recognizedText
		private string GetFullNameFromRecognizedText(string recognizedText)
		{
			// Biểu thức Regular Expression để tìm kiếm thông tin tên đầy đủ
			string fullNamePattern = @"Họ và tên \/ Full name:\s*(.+)";
			Match match = Regex.Match(recognizedText, fullNamePattern);

			if (match.Success)
			{
				// Lấy thông tin tên đầy đủ từ nhóm kết quả
				string fullName = match.Groups[1].Value;

				// Loại bỏ khoảng trắng và ký tự không cần thiết
				fullName = fullName.Trim();

				// Nếu thông tin tên đầy đủ không rỗng, trả về nó
				if (!string.IsNullOrEmpty(fullName))
				{
					return fullName;
				}
			}

			// Nếu không tìm thấy thông tin tên đầy đủ hoặc nó rỗng, trả về null
			return null;
		}

		//// Helper method để trích xuất ngày sinh từ thông tin nhận dạng
		private DateTime GetDateOfBirthFromRecognizedText(string recognizedText)
		{
			// Sử dụng regex để tìm kiếm ngày sinh trong chuỗi recognizedText
			// Ví dụ: Ngày sinh có định dạng "dd/MM/yyyy"
			string pattern = @"\d{2}/\d{2}/\d{4}";

			Match match = Regex.Match(recognizedText, pattern);
			if (match.Success)
			{
				// Nếu tìm thấy ngày sinh trong chuỗi, chuyển đổi sang kiểu DateTime
				string dateOfBirthText = match.Value;
				if (DateTime.TryParseExact(dateOfBirthText, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out DateTime dateOfBirth))
				{
					return dateOfBirth;
				}
			}

			// Nếu không tìm thấy ngày sinh hoặc không thể chuyển đổi sang kiểu DateTime, trả về null
			return DateTime.MinValue;
		}
		public static bool? GetGenderFromRecognizedText(string recognizedText)
		{
			// Sử dụng regex để tìm kiếm thông tin giới tính trong recognizedText
			// Ví dụ: Tìm kiếm các từ "Nam" hoặc "Nữ"
			string malePattern = @"\bNam\b";
			string femalePattern = @"\bNữ\b";

			bool isMale = Regex.IsMatch(recognizedText, malePattern, RegexOptions.IgnoreCase);
			bool isFemale = Regex.IsMatch(recognizedText, femalePattern, RegexOptions.IgnoreCase);

			if (isMale)
			{
				return true; // Nam
			}
			else if (isFemale)
			{
				return false; // Nữ
			}
			else
			{
				return null; // Không tìm thấy thông tin giới tính
			}
		}

		private string GetNationalityFromRecognizedText(string recognizedText)
		{
			// Biểu thức Regular Expression để tìm kiếm thông tin quốc tịch
			string nationalityPattern = @"Quóc tịch \/ Nationality-\s*(.+)";

			Match match = Regex.Match(recognizedText, nationalityPattern);

			if (match.Success)
			{
				// Lấy thông tin quốc tịch từ nhóm kết quả
				string nationality = match.Groups[1].Value;

				// Loại bỏ khoảng trắng và ký tự không cần thiết
				nationality = nationality.Trim();

				// Nếu thông tin quốc tịch không rỗng, trả về nó
				if (!string.IsNullOrEmpty(nationality))
				{
					return nationality;
				}
			}

			// Nếu không tìm thấy thông tin quốc tịch hoặc nó rỗng, trả về null
			return null;
		}


		private string GetPlaceOfOriginFromRecognizedText(string recognizedText)
		{
			// Biểu thức Regular Expression để tìm kiếm thông tin quê quán
			string placeOfOriginPattern = @"Quê quán \/ Place of origin:\s*(.+)";

			Match match = Regex.Match(recognizedText, placeOfOriginPattern);

			if (match.Success)
			{
				// Lấy thông tin quê quán từ nhóm kết quả
				string placeOfOrigin = match.Groups[1].Value;

				// Loại bỏ khoảng trắng và ký tự không cần thiết
				placeOfOrigin = placeOfOrigin.Trim();

				// Nếu thông tin quê quán không rỗng, trả về nó
				if (!string.IsNullOrEmpty(placeOfOrigin))
				{
					return placeOfOrigin;
				}
			}

			// Nếu không tìm thấy thông tin quê quán hoặc nó rỗng, trả về null
			return null;
		}
	}
}

﻿using BusinessObject.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Repositories;

namespace APIConnect.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	public class ShipperController : ControllerBase
	{
        private IAccountRepository repositoryAccount = new AccountRepository();
        private IShipperRepository repository = new ShipperRepository();
		[HttpGet]
		[Route("list")]
		public async Task<ActionResult<IEnumerable<Shipper>>> GetShipper()
		{
			return repository.GetShipper();
		}
		//[HttpGet]
		[HttpGet("{id}")]
		public async Task<ActionResult<Shipper>> GetShipperById(int id)
		{
			return repository.GetShipperbyId(id);
		}
        [HttpGet]
        [Route("get-shipper-id")]
        public async Task<ActionResult<IEnumerable<Shipper>>> GetShipperById()
        {
            // Trích xuất thông tin người dùng từ mã thông báo
            var username = HttpContext.User.Identity.Name;
            if (!string.IsNullOrEmpty(username))
            {
                //Trả về thông tin người dùng
                var account = repositoryAccount.GetAccountByUsername(username);
                if (account != null)
                {
                    var customer = repository.GetShipperbyId((int)account.ShipperId);
                    return Ok(customer);
                }
                else
                {
                    // Người dùng không tồn tại trong cơ sở dữ liệu
                    return NotFound();
                }
            }
            else
            {
                // Người dùng chưa đăng nhập, trả về lỗi hoặc chuyển hướng đến trang đăng nhập
                return Unauthorized(new { error = "Người dùng chưa đăng nhập." });
            }
        }
        [HttpPut("{id}")]
		public async Task<IActionResult> PutShipper(int id, Shipper shipper)
		{
			repository.UpdateShipper(id, shipper);
			return NoContent();
		}
		[HttpDelete("{id}")]
		public async Task<IActionResult> DeleteShipper(int id)
		{
			var account = repository.GetShipperbyId(id);
			if (account == null)
			{
				return NotFound();
			}

			repository.DeleteShipper(id);
			return NoContent();
		}
		[HttpPost]
		public async Task<ActionResult<Sale>> PostShipper(Shipper shipper)
		{
			repository.AddShipper(shipper);
			return NoContent();
		}
	}
}

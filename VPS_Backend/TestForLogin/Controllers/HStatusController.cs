﻿using BusinessObject.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace APIConnect.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class HStatusController : ControllerBase
    {
        [HttpGet]
        [Route("list")]
        public async Task<ActionResult<IEnumerable<Hstatus>>> GetHStatus()
        {
            VPSContext context = new VPSContext();
            return context.Hstatuses.ToList();
        }
    }
}

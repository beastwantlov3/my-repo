﻿using Amazon.S3.Model;
using Amazon.S3;
using BusinessObject.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Repositories;
using Amazon;

namespace APIConnect.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	public class ProductDetailController : ControllerBase
	{
        private readonly string bucketName = "system-viewpoint";
        private readonly string accessKey = "AKIAX3J7OK3364RPYPEB";
        private readonly string secretKey = "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";
        private IProductDetailRepository repository = new ProductDetailRepository();
		[HttpGet]
		[Route("list")]
		public async Task<ActionResult<IEnumerable<ProductDetail>>> GetProductDetail()
		{
			return repository.GetProductDetail();
		}
		//[HttpGet]
		[HttpGet("{id}")]
		public async Task<ActionResult<ProductDetail>> GetProductDetailById(int id)
		{
			return repository.GetProductDetailbyId(id);
		}
        private string GetImageUrlForProduct(string avatarKey)
        {
            try
            {
                // Tạo đối tượng Amazon S3
                using (var client = new AmazonS3Client(accessKey, secretKey, RegionEndpoint.APSoutheast1))
                {
                    // Tạo request để lấy đường dẫn URL của tệp ảnh
                    var request = new GetPreSignedUrlRequest
                    {
                        BucketName = bucketName,
                        Key = avatarKey,
                        Expires = DateTime.Now.AddMinutes(60) // Thời gian URL hết hạn (tùy chỉnh)
                    };

                    // Lấy đường dẫn URL đã ký hợp lệ của tệp ảnh
                    string imageUrl = client.GetPreSignedURL(request);
                    return imageUrl;
                }
            }
            catch (Exception ex)
            {
                // Xử lý ngoại lệ và ghi log
                // Thay vì in ra message, bạn có thể ghi log chi tiết về lỗi để dễ dàng gỡ lỗi sau này
                Console.WriteLine($"Error getting avatar URL from S3: {ex.Message}");
                return null;
            }
        }
        [HttpGet("[action]/{id}")]
        public IActionResult GetProductDetailByProductId(int id)
        {
            VPSContext context = new VPSContext();
            var PDetail = context.ProductDetails.Where(x => x.ProductId == id).ToArray().Take(3);
			foreach(var item in PDetail)
			{
				item.CoreImage = GetImageUrlForProduct(item.CoreImage);
			}
			return Ok(PDetail);
        }
        [HttpPut("{id}")]
		public async Task<IActionResult> PutProductDetail(int id, ProductDetail productDetail)
		{
			repository.UpdateProductDetail(id, productDetail);
			return NoContent();
		}
		[HttpDelete("{id}")]
		public async Task<IActionResult> DeleteProductDetail(int id)
		{
			var account = repository.GetProductDetailbyId(id);
			if (account == null)
			{
				return NotFound();
			}

			repository.DeleteProductDetail(id);
			return NoContent();
		}
		[HttpPost]
		public async Task<ActionResult<ProductDetail>> PostProductDetail(ProductDetail productDetail)
		{
			repository.AddProductDetail(productDetail);
			return NoContent();
		}
	}
}

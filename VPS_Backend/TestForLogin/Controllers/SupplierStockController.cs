﻿using Amazon.S3.Model;
using Amazon.S3;
using BusinessObject.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Repositories;
using Amazon;

namespace APIConnect.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	public class SupplierStockController : ControllerBase
	{
		private readonly string bucketName = "system-viewpoint";
		private readonly string accessKey = "AKIAX3J7OK3364RPYPEB";
		private readonly string secretKey = "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";
		private IAccountRepository repositoryAccount = new AccountRepository();
        private ISupplierStockRepository repository = new SupplierStockRepository();
		[HttpGet]
		[Route("list")]
		public async Task<ActionResult<IEnumerable<SupplierStock>>> GetSupplierStock()
		{
			return repository.GetSupplierStock();
		}
		//[HttpGet]
		[HttpGet("{id}")]
		public async Task<ActionResult<SupplierStock>> GetSupplierStockById(int id)
		{
			return repository.GetSupplierStockbyId(id);
		}

		[HttpGet("[action]/{id}")]
		public async Task<ActionResult<IEnumerable<SupplierStock>>> GetCurrentProductBySupplierId(int id)
        {
            return repository.GetProductBySupplierId(id);
        }

        [HttpGet("[action]")]
		public async Task<ActionResult<IEnumerable<SupplierStock>>> GetProductBySupplierId()
		{
            var username = HttpContext.User.Identity.Name;
            if (!string.IsNullOrEmpty(username))
            {
                //Trả về thông tin người dùng
                var account = repositoryAccount.GetAccountByUsername(username);
                if (account != null)
                {
                    var supplier = repository.GetProductBySupplierId((int)account.SupplierId);
					foreach (var supplierStock in supplier)
					{
						var product = supplierStock.Product;

						// Kiểm tra xem sản phẩm có hình ảnh không
						if (!string.IsNullOrEmpty(product.Image))
						{
							product.Image = GetImageUrlForProduct(product.Image);
						}
					}
					return Ok(supplier);
                }
                else
                {
                    // Người dùng không tồn tại trong cơ sở dữ liệu
                    return NotFound();
                }
            }
            else
            {
                // Người dùng chưa đăng nhập, trả về lỗi hoặc chuyển hướng đến trang đăng nhập
                return Unauthorized(new { error = "Người dùng chưa đăng nhập." });
            }
		}
		[HttpPut("{id}")]
		public async Task<IActionResult> PutSupplier(int id, SupplierStock supplierStock)
		{
			repository.UpdateSupplierStaock(id, supplierStock);
			return NoContent();
		}
		[HttpDelete("{id}")]
		public async Task<IActionResult> DeleteSupplierStock(int id)
		{
			var account = repository.GetSupplierStockbyId(id);
			if (account == null)
			{
				return NotFound();
			}

			repository.DeleteSupplierStock(id);
			return NoContent();
		}
		[HttpPost]
		public async Task<ActionResult<SupplierStock>> PostSupplierStock(SupplierStock supplierStock)
		{
			repository.AddSupplierStock(supplierStock);
			return NoContent();
		}

		private string GetImageUrlForProduct(string avatarKey)
		{
			try
			{
				// Tạo đối tượng Amazon S3
				using (var client = new AmazonS3Client(accessKey, secretKey, RegionEndpoint.APSoutheast1))
				{
					// Tạo request để lấy đường dẫn URL của tệp ảnh
					var request = new GetPreSignedUrlRequest
					{
						BucketName = bucketName,
						Key = avatarKey,
						Expires = DateTime.Now.AddMinutes(60) // Thời gian URL hết hạn (tùy chỉnh)
					};

					// Lấy đường dẫn URL đã ký hợp lệ của tệp ảnh
					string imageUrl = client.GetPreSignedURL(request);
					return imageUrl;
				}
			}
			catch (Exception ex)
			{
				// Xử lý ngoại lệ và ghi log
				// Thay vì in ra message, bạn có thể ghi log chi tiết về lỗi để dễ dàng gỡ lỗi sau này
				Console.WriteLine($"Error getting avatar URL from S3: {ex.Message}");
				return null;
			}
		}
	}
}

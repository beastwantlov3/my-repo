﻿using APIConnect.DTO;
using BusinessObject.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Net.Mail;
using System.Globalization;
using System.Text.RegularExpressions;
using MailKit.Security;
using MimeKit;
using MailKit.Net.Smtp;
using System.Security.Claims;
using System.Text;
using Repositories;
using DataAccess;
using Konscious.Security.Cryptography;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using SixLabors.ImageSharp;

namespace APIConnect.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class AuthController : Controller
    {
        //private static Dictionary<string, string> registrationDataCode = new Dictionary<string, string>();
		private static Dictionary<string, RegistrationData> registrationDataCode = new Dictionary<string, RegistrationData>();

		private readonly VPSContext _vPSContext;
        private IAccountRepository _accountRepository = new AccountRepository();
        private readonly IConfiguration _configuration;
        public AuthController(VPSContext vPSContext,
            IConfiguration configuration
            )
        {
            _vPSContext = vPSContext;
            _configuration = configuration;
        }

		[HttpPost("register")]
		public async Task<IActionResult> Register([FromBody] AccountDTO accountDTO)
		{
			// Kiểm tra xem tên người dùng đã tồn tại hay chưa
			if (await _vPSContext.Accounts.AnyAsync(x => x.UserName == accountDTO.Username))
			{
				return BadRequest(new { error = "Tên người dùng đã tồn tại" });
			}

			// Kiểm tra xem email đã tồn tại hay chưa
			if (await _vPSContext.Accounts.AnyAsync(x => x.Email == accountDTO.Email))
			{
				return BadRequest(new { error = "Email đã tồn tại" });
			}

			// Tạo một tài khoản mới
			var account = new Account
			{
				UserName = accountDTO.Username,
				Email = accountDTO.Email,
				Password = HashPassword(accountDTO.Password), // Hãy đảm bảo mã hóa mật khẩu trước khi lưu vào cơ sở dữ liệu
				RoleId = await GetDefaultRoleId(), // Lấy ID của vai trò mặc định ("customer")
				CreatedDate = DateTime.Now,
                Status = 1
			};

			string verificationCode = GenerateVerificationCode();
			// Send verification email
			bool isEmailSent = await SendVerificationEmail(account.UserName, account.Email, verificationCode);

			if (!isEmailSent)
			{
				// Handle email sending failure
				return BadRequest("Failed to send verification email.");
			}

			// Lưu thông tin vào danh sách đăng ký
			registrationDataCode[account.Email] = new RegistrationData
			{
				Account = account,
				AccountDTO = accountDTO,
				VerificationCode = verificationCode
			};

			return Ok();
		}

		[HttpPost("verify")]
		public async Task<IActionResult> Verify(string email, string verificationCode)
		{
			if (registrationDataCode.TryGetValue(email, out RegistrationData registrationData))
			{
				if (registrationData.VerificationCode == verificationCode)
				{
					// Xác thực thành công
					registrationDataCode.Remove(email);

					// Lưu dữ liệu vào cơ sở dữ liệu sau khi xác thực thành công
					var savedSuccessfully = await SaveDataToDatabase(registrationData);

					if (savedSuccessfully)
					{
						return Ok(); // Trả về thành công nếu dữ liệu đã được lưu vào cơ sở dữ liệu
					}
					else
					{
						return BadRequest("An error occurred while processing the verification.");
					}
				}
			}

			// Xác thực thất bại
			return BadRequest("Invalid verification code.");
		}

		private async Task<bool> SaveDataToDatabase(RegistrationData registrationData)
		{
			try
			{
				var account = registrationData.Account;
				var accountDTO = registrationData.AccountDTO;

				var customer = new Customer
				{
					FullName = accountDTO.FullName,
					Phone = accountDTO.Phone,
					Dob = Convert.ToDateTime(DateTime.ParseExact(accountDTO.DOB, "dd/MM/yyyy", CultureInfo.InvariantCulture)),
                    StatusId = 1,
                    Balance = 0
				};
				_vPSContext.Customers.Add(customer);
				await _vPSContext.SaveChangesAsync();

				account.CustomerId = customer.CustomerId;
				_vPSContext.Accounts.Update(account);
				await _vPSContext.SaveChangesAsync();

				return true; // Trả về true nếu dữ liệu đã được lưu thành công
			}
			catch (Exception)
			{
				return false; // Trả về false nếu có lỗi xảy ra trong quá trình lưu dữ liệu
			}
		}



		private string HashPassword(string password)
        {
            using (var argon2 = new Argon2id(Encoding.UTF8.GetBytes(password)))
            {
                argon2.DegreeOfParallelism = 8; // Độ song song (có thể điều chỉnh)
                argon2.MemorySize = 65536; // Kích thước bộ nhớ (có thể điều chỉnh)
                argon2.Iterations = 4; // Số lượt lặp (có thể điều chỉnh)

                var hashBytes = argon2.GetBytes(32); // Chiều dài của mã hash (có thể điều chỉnh)
                var hashedPassword = Convert.ToBase64String(hashBytes);

                return hashedPassword;
            }
        }

        private async Task<int> GetDefaultRoleId()
        {
            // Lấy ID của vai trò mặc định ("customer") từ cơ sở dữ liệu
            var defaultRole = await _vPSContext.Roles.FirstOrDefaultAsync(r => r.RoleName == "Customers");
            return defaultRole?.RoleId ?? 0;
        }

        [HttpPost("resendverification")]
        public IActionResult ResendVerificationCode(string username, string email)
        {
			//// Kiểm tra xem đã tồn tại yêu cầu gửi lại mã xác thực cho email này chưa
			//if (registrationDataCode.ContainsKey(email))
			//{
			//    DateTime lastRequestTime = verificationCodeRequests[email];
			//    TimeSpan elapsedTime = DateTime.Now - lastRequestTime;

			//    // Kiểm tra xem đã đủ thời gian chờ chưa
			//    if (elapsedTime < resendTimeout)
			//    {
			//        TimeSpan remainingTime = resendTimeout - elapsedTime;
			//        return BadRequest($"Vui lòng đợi {remainingTime.TotalMinutes} phút trước khi yêu cầu gửi lại mã xác thực.");
			//    }
			//}

			var registrationData = registrationDataCode[email];

			// Gửi lại mã xác thực
			SendVerificationEmail(username, email, registrationData.VerificationCode);

			// Lưu thời gian yêu cầu gửi lại mã xác thực
			//verificationCodeRequests[email] = DateTime.Now;

			return Ok("Đã gửi lại mã xác thực.");
        }

        [AllowAnonymous]
        [HttpPost("login")]
        public IActionResult Login([FromBody] LoginDTO account)
        {
            try
            {
                // Xác thực người dùng
                // Tìm người dùng theo username và mật khẩu
                var acc = _vPSContext.Accounts.Include(a => a.Role).FirstOrDefault(a =>
                    a.UserName == account.Username);
                if (acc == null)
                {
                    return BadRequest(new { error = "Tài khoản không tồn tại. Vui lòng nhập lại tài khoản!" });
                }
                // So sánh mật khẩu
                string hashedPassword = HashPassword(account.Password);
                if (hashedPassword != acc.Password)
                {
                    return BadRequest(new { error = "Mật Khẩu sai. Vui lòng nhập lại mật khẩu!" });
                }
                // Tạo JWT token nếu xác thực thành công
                if (acc != null)
                {
                    var role = acc.Role;
                    var roleName = role.RoleName;

                    // Kiểm tra giá trị RememberMe từ đối tượng account
                    var rememberMe = account.RememberMe;

                    // Tạo JWT token nếu xác thực thành công
                    var token = GenerateJwtToken(acc, rememberMe);

                    // Trả về thông tin người dùng và token
                    return Ok(new
                    {
                        loggedIn = true,
                        username = acc.UserName,
                        roleName = roleName,
                        email = acc.Email,
                        token // Trả về token trong response
                    });
                }

                return BadRequest(new { error = "Sai tên đăng nhập hoặc mật khẩu! Xin vui lòng nhập lại" });
            }
            catch (Exception ex)
            {
                // Handle email sending exception
                return BadRequest(new { error = "Sai tên đăng nhập hoặc mật khẩu! Xin vui lòng nhập lại" });
            }
        }

        private string GenerateJwtToken(Account account, bool rememberMe)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var secretKey = _configuration["Jwt:Key"]; // Use the correct key from configuration
            var key = Encoding.ASCII.GetBytes(secretKey);
            var issuer = _configuration["Jwt:Issuer"]; // Use the correct issuer from configuration
            var audience = _configuration["Jwt:Audience"]; // Use the correct audience from configuration

            // Tạo claims chứa thông tin người dùng và vai trò
            var claims = new List<Claim>
        {
                new Claim(JwtRegisteredClaimNames.Sub, _configuration["Jwt:Subject"]),
                        new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                        new Claim(JwtRegisteredClaimNames.Iat, DateTime.UtcNow.ToString()),
            new Claim(ClaimTypes.Name, account.UserName),
            new Claim(ClaimTypes.Role, account.Role.RoleName),
            new Claim("UserName", account.UserName),
            new Claim("RoleName", account.Role.RoleName)
        };

            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(claims),
                Expires = rememberMe ? DateTime.UtcNow.AddDays(7) : DateTime.UtcNow.AddHours(1), // Đặt thời gian hết hạn theo giá trị RememberMe
                Issuer = issuer,
                Audience = audience,
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };

            var token = tokenHandler.CreateToken(tokenDescriptor);
            return tokenHandler.WriteToken(token);
        }


        // Phương thức đăng xuất
        [HttpPost("logout")]
        public IActionResult Logout()
        {
            // Xóa cookie đăng nhập
            Response.Cookies.Delete("loggedIn");
            Response.Cookies.Delete("username");
            Response.Cookies.Delete("role");

            return Ok("Đăng xuất thành công!");
        }

        private string GenerateVerificationCode()
        {
            string digits = "0123456789";
            Random random = new Random();
            string verificationCode = new string(Enumerable.Repeat(digits, 6)
                .Select(s => s[random.Next(s.Length)]).ToArray());
            return verificationCode;
        }
        private string GetHTMLTemplate(string verifictionCode)
        {
            string htmlTemplate = "<!DOCTYPE html>\r\n<html>\r\n    <head>\r\n        <title>Verification Code</title>\r\n        <style>\r\n        body {\r\n            font-family: Arial, sans-serif;\r\n            background-color: #f4f4f4;\r\n            padding: 20px;\r\n        }\r\n        \r\n        .container {\r\n            max-width: 600px;\r\n            margin: 0 auto;\r\n            background-color: #ffffff;\r\n            padding: 30px;\r\n            border-radius: 5px;\r\n            box-shadow: 0 2px 5px rgba(0, 0, 0, 0.1);\r\n        }\r\n        \r\n        h1 {\r\n            color: #333333;\r\n            text-align: center;\r\n        }\r\n        \r\n        p {\r\n            color: #555555;\r\n            line-height: 1.5;\r\n        }\r\n        \r\n        .verification-code {\r\n            font-size: 28px;\r\n            text-align: center;\r\n            margin-bottom: 20px;\r\n        }\r\n        \r\n        .cta-button {\r\n            display: inline-block;\r\n            background-color: #007bff;\r\n            color: #ffffff;\r\n            text-decoration: none;\r\n            padding: 10px 20px;\r\n            border-radius: 4px;\r\n            margin-top: 20px;\r\n        }\r\n        \r\n        .cta-button:hover {\r\n            background-color: #0056b3;\r\n        }\r\n    </style>\r\n    </head>\r\n    <body>\r\n        <div class=\"container\">\r\n            <h1>Verification Code</h1>\r\n            <p>Thank you for signing up! Please use the following verification\r\n                code to complete your registration:</p>\r\n            <p class=\"verification-code\">"+ verifictionCode + "</p>\r\n            <a href=\"http://localhost:3000/login-register\" class=\"cta-button\">Verify Now</a>\r\n        </div>\r\n    </body>\r\n</html>";
            return htmlTemplate;

        }
        private async Task<bool> SendVerificationEmail(string username, string email, string verificationCode)
        {
            try
            {
                var message = new MimeMessage();
                message.From.Add(new MailboxAddress("ViewPoint System", "systemviewpoint@gmail.com")); // Replace with your email address
                message.To.Add(new MailboxAddress(email, email));
                message.Subject = "Xác thực email của bạn";
                string HtmlCode = GetHTMLTemplate(verificationCode);
                var body = new TextPart("html")
                {
                    Text = HtmlCode
                };
                var multipart = new Multipart("alternative");
                multipart.Add(body);
                message.Body = multipart;
                using (var client = new MailKit.Net.Smtp.SmtpClient())
                {
                    await client.ConnectAsync("smtp.gmail.com", 587, SecureSocketOptions.StartTls);
                    await client.AuthenticateAsync("systemviewpoint@gmail.com", "cwgmaisruuwanuld");

                    await client.SendAsync(message);
                    await client.DisconnectAsync(true);
                }

                return true;
            }
            catch (Exception ex)
            {
                // Handle email sending exception
                return false;
            }
        }

        [HttpPost("forgot-password")]
        public IActionResult ForgotPassword(string email)
        {
            var account = _vPSContext.Accounts.FirstOrDefault(a => a.Email == email);
            if (account == null)
            {
                return BadRequest(new { error = "Email không tồn tại. Vui lòng nhập lại email!" });
            }

            // Tạo mật khẩu mới ngẫu nhiên
            string newPassword = GenerateRandomPassword();

            // Gửi mật khẩu mới đến email người dùng
            SendPasswordResetEmail(account.UserName, account.Email, newPassword);

            // Cập nhật mật khẩu mới trong cơ sở dữ liệu
            account.Password = HashPassword(newPassword);
            _vPSContext.SaveChanges();


            return Ok("Password reset instructions have been sent to your email.");
        }
		[HttpPut("change-password")]
		public async Task<IActionResult> ChangePassword([FromForm] ChangePassword changePassword)
		{
			var username = HttpContext.User.Identity.Name;
			if (!string.IsNullOrEmpty(username))
			{
				//Trả về thông tin người dùng
				var account = _accountRepository.GetAccountByUsername(username);
				if (account != null)
				{
					// Kiểm tra mật khẩu hiện tại
					if (account.Password != HashPassword(changePassword.currentPassword))
					{
						return BadRequest(new { error = "Mật khẩu hiện tại không đúng. Vui lòng nhập lại mật khẩu!" });
					}
					// Xác thực và hạn chế trường Password
					if (changePassword.newPassword.Length > 14 ||
						!Regex.IsMatch(changePassword.newPassword, @"^(?=.*[A-Z])(?=.*[!@#$%^&*])(?=.*[0-9]).{8,14}$"))
					{
						return BadRequest(new { error = "Mật khẩu không hợp lệ. Mật khẩu yêu cầu ít nhất 1 kí tự viết hoa và 1 kí tự đặc biệt và 1 kí tự số và không được quá 14 kí tự" });
					}

					if (changePassword.newPassword != changePassword.confirmPassword)
					{
						return BadRequest(new { error = "Xác nhận mật khẩu không khớp với mật khẩu." });
					}
					// Cập nhật mật khẩu mới
					account.Password = HashPassword(changePassword.newPassword);
					_accountRepository.UpdateAccount(account);
                    return Ok("Password changed successfully");
                }
                else
                {
                    // Người dùng không tồn tại trong cơ sở dữ liệu
                    return NotFound("User not found");
                }
            }
            else
            {
                // Người dùng không tồn tại trong cơ sở dữ liệu
                return NotFound();
            }
        }

        private async Task<bool> SendPasswordResetEmail(string username, string email, string newPassword)
        {
            try
            {
                var message = new MimeMessage();
                message.From.Add(new MailboxAddress("ViewPoint System", "systemviewpoint@gmail.com")); // Replace with your email address
                message.To.Add(new MailboxAddress(email, email));
                message.Subject = "Xác thực email của bạn";
                message.Body = new TextPart("plain")
                {
                    Text = $"Xin chào {username}! Mật khẩu mới của bạn là: {newPassword}"
                };

                using (var client = new MailKit.Net.Smtp.SmtpClient())
                {
                    await client.ConnectAsync("smtp.gmail.com", 587, SecureSocketOptions.StartTls);
                    await client.AuthenticateAsync("systemviewpoint@gmail.com", "cwgmaisruuwanuld");

                    await client.SendAsync(message);
                    await client.DisconnectAsync(true);
                }

                return true;
            }
            catch (Exception ex)
            {
                // Handle email sending exception
                return false;
            }
        }

        private string GenerateRandomPassword()
        {
            const string validChars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
            int passwordLength = 8; // Độ dài mật khẩu ngẫu nhiên

            StringBuilder password = new StringBuilder();
            Random random = new Random();

            for (int i = 0; i < passwordLength; i++)
            {
                int randomIndex = random.Next(0, validChars.Length);
                password.Append(validChars[randomIndex]);
            }

            return password.ToString();
        }


    }
}

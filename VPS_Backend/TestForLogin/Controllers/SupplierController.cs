﻿using BusinessObject.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Repositories;

namespace APIConnect.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	public class SupplierController : ControllerBase
	{
        private IAccountRepository repositoryAccount = new AccountRepository();
        private ISupplierRepository repository = new SupplierRepository();
		[HttpGet]
		[Route("list")]
		public async Task<ActionResult<IEnumerable<Supplier>>> GetSupplier()
		{
			return repository.GetSupplier();
		}
		//[HttpGet]
		[HttpGet("{id}")]
		public async Task<ActionResult<Supplier>> GetCurrentSupplierById(int id)
		{
			return repository.GetSupplierbyId(id);
		}
        [HttpGet]
        [Route("get-supplier-id")]
        public async Task<ActionResult<IEnumerable<Supplier>>> GetSupplierById()
        {
            // Trích xuất thông tin người dùng từ mã thông báo
            var username = HttpContext.User.Identity.Name;
            if (!string.IsNullOrEmpty(username))
            {
                //Trả về thông tin người dùng
                var account = repositoryAccount.GetAccountByUsername(username);
                if (account != null)
                {
                    var customer = repository.GetSupplierbyId((int)account.SupplierId);
                    return Ok(customer);
                }
                else
                {
                    // Người dùng không tồn tại trong cơ sở dữ liệu
                    return NotFound();
                }
            }
            else
            {
                // Người dùng chưa đăng nhập, trả về lỗi hoặc chuyển hướng đến trang đăng nhập
                return Unauthorized(new { error = "Người dùng chưa đăng nhập." });
            }
        }
        [HttpPut("{id}")]
		public async Task<IActionResult> PutSupplier(int id, Supplier supplier)
		{
			repository.UpdateSupplier(id, supplier);
			return NoContent();
		}
		[HttpDelete("{id}")]
		public async Task<IActionResult> DeleteSupplier(int id)
		{
			var account = repository.GetSupplierbyId(id);
			if (account == null)
			{
				return NotFound();
			}

			repository.DeleteSupplier(id);
			return NoContent();
		}
		[HttpPost]
		public async Task<ActionResult<Supplier>> PostSupplier(Supplier supplier)
		{
			repository.AddSupplier(supplier);
			return NoContent();
		}
	}
}

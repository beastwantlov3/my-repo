﻿using BusinessObject.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Repositories;

namespace APIConnect.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductCodeController : ControllerBase
    {
        private IProductCodeRepository repository = new ProductCodeRepository();
        [HttpGet]
        [Route("list")]
        public async Task<ActionResult<IEnumerable<ProductCode>>> GetProductCode()
        {
            return repository.GetProductCode();
        }
        //[HttpGet]
        [HttpGet("{id}")]
        public async Task<ActionResult<ProductCode>> GetProductCodeById(int id)
        {
            return repository.GetProductCodeById(id);
        }
        [HttpPut("{id}")]
        public async Task<IActionResult> PutProductCode(int id, ProductCode feedBack)
        {
            repository.UpdateProductCode(id, feedBack);
            return NoContent();
        }
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteProductCode(int id)
        {
            var account = repository.GetProductCodeById(id);
            if (account == null)
            {
                return NotFound();
            }

            repository.DeleteProductCode(id);
            return NoContent();
        }
        [HttpPost]
        public async Task<ActionResult<Conversation>> PostProductCode(ProductCode feedBack)
        {
            repository.AddProductCode(feedBack);
            return NoContent();
        }
    }
}

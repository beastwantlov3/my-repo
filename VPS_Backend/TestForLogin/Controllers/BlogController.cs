﻿using Amazon.S3.Model;
using Amazon.S3;
using BusinessObject.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Repositories;
using Amazon;
using APIConnect.DTO;
using SixLabors.ImageSharp.Formats.Png;
using Image = SixLabors.ImageSharp.Image;
using DataAccess;

namespace APIConnect.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	public class BlogController : ControllerBase
	{
		private readonly string bucketName = "system-viewpoint"; 
		private readonly string accessKey = "AKIAX3J7OK3364RPYPEB";
		private readonly string secretKey = "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"; 
		private IBlogRepository repository = new BlogRepository();
		[HttpGet]
		[Route("list")]
		public async Task<ActionResult<IEnumerable<Blog>>> GetBlog()
		{
			var blogs = repository.GetBlog();
			foreach (var blog in blogs)
			{
				blog.Image = GetImageUrlForBlog(blog.Image);
			}

			return blogs;
		}

		private string GetImageUrlForBlog(string avatarKey)
		{
			try
			{
				// Tạo đối tượng Amazon S3
				using (var client = new AmazonS3Client(accessKey, secretKey, RegionEndpoint.APSoutheast1))
				{
					// Tạo request để lấy đường dẫn URL của tệp ảnh
					var request = new GetPreSignedUrlRequest
					{
						BucketName = bucketName,
						Key = avatarKey,
						Expires = DateTime.Now.AddMinutes(60) // Thời gian URL hết hạn (tùy chỉnh)
					};

					// Lấy đường dẫn URL đã ký hợp lệ của tệp ảnh
					string imageUrl = client.GetPreSignedURL(request);
					return imageUrl;
				}
			}
			catch (Exception ex)
			{
				// Xử lý ngoại lệ và ghi log
				// Thay vì in ra message, bạn có thể ghi log chi tiết về lỗi để dễ dàng gỡ lỗi sau này
				Console.WriteLine($"Error getting avatar URL from S3: {ex.Message}");
				return null;
			}
		}
		//[HttpGet]
		[HttpGet]
		[Route("total")]
		public async Task<int> GetTotalBlog()
		{
			return repository.GetTotalBlog();
		}
        [HttpGet]
        [Route("GetTop3")]
        public async Task<ActionResult<IEnumerable<Blog>>> GetTop3Blog()
        {
			VPSContext context = new VPSContext();
			var blogs = context.Blogs.OrderByDescending(x=>x.Id).Take(3).ToList();
			foreach (var blog in blogs)
			{
				blog.Image = GetImageUrlForBlog(blog.Image);
			}

			return blogs;
		}
        [HttpGet]
        [Route("BlogByDate")]
        public IActionResult GetBlogByDate(string start, string end)
        {
            VPSContext context = new VPSContext();
            var totalRevenue = context.Blogs
        .Where(o => o.PublishDate >= Convert.ToDateTime(start) && o.PublishDate <= Convert.ToDateTime(end))
        .Count();
            return Ok(totalRevenue);
        }
        [HttpGet("{id}")]
		public async Task<ActionResult<Blog>> GetBlogById(int id)
		{
			return repository.GetBlogById(id);
		}
		[HttpPut("{id}")]
		public async Task<IActionResult> PutBlog(int id, Blog blog)
		{
			repository.UpdateBlog(id, blog);
			return NoContent();
		}
		[HttpDelete("{id}")]
		public async Task<IActionResult> DeleteBlog(int id)
		{
			var account = repository.GetBlogById(id);
			if (account == null)
			{
				return NotFound();
			}

			repository.DeleteBlog(id);
			return NoContent();
		}
		[HttpPost]
		public async Task<ActionResult<Blog>> PostBlog(Blog blog)
		{
			repository.AddBlog(blog);
			return NoContent();
		}

		[HttpPost("post-product")]
		public async Task<IActionResult> UploadProduct([FromForm] BlogDTO blogDTO)
		{
			if (blogDTO.Image == null || blogDTO.Image.Length <= 0)
			{
				return BadRequest("Không tìm thấy tệp ảnh.");
			}

			// Tạo và lưu thumbnail vào vị trí tạm thời
			string tempThumbnailPath = Path.GetTempFileName();
			int thumbnailWidth = 200; // Điều chỉnh kích thước thumbnail tùy ý
			int thumbnailHeight = 200; // Điều chỉnh kích thước thumbnail tùy ý

			using (var stream = new FileStream(tempThumbnailPath, FileMode.Create))
			{
				using (var image = Image.Load(blogDTO.Image.OpenReadStream()))
				{
					image.Mutate(x => x.Resize(new ResizeOptions
					{
						Mode = ResizeMode.Max,
						Size = new SixLabors.ImageSharp.Size(thumbnailWidth, thumbnailHeight)
					}));

					image.Save(stream, new PngEncoder()); // Lưu thumbnail dưới dạng PNG
				}
			}

			// Tải thumbnail lên Amazon S3
			string s3ThumbnailUrl = await UploadImageToS3(blogDTO.Image);

			// Xóa tệp tạm thời
			System.IO.File.Delete(tempThumbnailPath);

			// Tạo một đối tượng sản phẩm mới
			var blog = new Blog
			{
				BloggerId = blogDTO.BloggerId,
				Title = blogDTO.Title,
				Description = blogDTO.Description,
				PublishDate = DateTime.Now,
				Image = s3ThumbnailUrl,
				CateId = blogDTO.CateId,
			};
			repository.AddBlog(blog);

			// Trả về phản hồi thành công
			return Ok(blog);
		}

		private async Task<string> UploadImageToS3(IFormFile imageFile)
		{
			// Tạo tên tệp duy nhất
			string extension = Path.GetExtension(imageFile.FileName);
			string filename = "blog-" + Guid.NewGuid().ToString() + extension;

			// Tải ảnh lên Amazon S3
			// (Thay thế "your-bucket-name" bằng tên thật của bucket S3 của bạn)
			var s3Client = new AmazonS3Client(accessKey, secretKey, RegionEndpoint.APSoutheast1);
			using (var imageStream = imageFile.OpenReadStream())
			{
				var putRequest = new PutObjectRequest
				{
					BucketName = bucketName,
					Key = "blog/" + filename,
					InputStream = imageStream,
					ContentType = imageFile.ContentType
				};
				await s3Client.PutObjectAsync(putRequest);
			}

			// Trả về URL S3 của ảnh đã tải lên
			return "blog/" + filename;
		}

	}
}

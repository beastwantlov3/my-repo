﻿using BusinessObject.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Repositories;

namespace APIConnect.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OrderDetailCodeController : ControllerBase
    {
        private IOrderDetailCodeRepository repository = new OrderDetailCodeRepository();
        [HttpGet]
        [Route("list")]
        public async Task<ActionResult<IEnumerable<OrderDetailCode>>> GetOrderDetailCode()
        {
            return repository.GetOrderDetailCode();
        }
        //[HttpGet]
        [HttpGet("{id}")]
        public async Task<ActionResult<OrderDetailCode>> GetOrderDetailCodeById(int id)
        {
            return repository.GetOrderDetailCodebyId(id);
        }
        [HttpPut("{id}")]
        public async Task<IActionResult> PutOrderDetailCode(int id, OrderDetailCode OrderDetailCode)
        {
            repository.UpdateOrderDetailCode(id, OrderDetailCode);
            return NoContent();
        }
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteOrderDetailCode(int id)
        {
            var account = repository.GetOrderDetailCodebyId(id);
            if (account == null)
            {
                return NotFound();
            }

            repository.DeleteOrderDetailCode(id);
            return NoContent();
        }
        [HttpPost]
        public async Task<ActionResult<OrderDetailCode>> PostOrderDetailCode(OrderDetailCode OrderDetailCode)
        {
            repository.AddOrderDetailCode(OrderDetailCode);
            return NoContent();
        }
    }
}

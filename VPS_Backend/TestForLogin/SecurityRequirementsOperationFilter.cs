﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.Filters;
using Swashbuckle.AspNetCore.SwaggerGen;
using System.Collections.Generic;

public class SecurityRequirementsOperationFilter : IOperationFilter
{
	public void Apply(OpenApiOperation operation, OperationFilterContext context)
	{
		var attributes = context.MethodInfo.DeclaringType.GetCustomAttributes(true)
			.Union(context.MethodInfo.GetCustomAttributes(true));

		var allowAnonymous = attributes.OfType<AllowAnonymousAttribute>().Any();
		if (allowAnonymous)
			return;

		operation.Security ??= new List<OpenApiSecurityRequirement>();

		var scheme = new OpenApiSecurityScheme { Reference = new OpenApiReference { Type = ReferenceType.SecurityScheme, Id = "Bearer" } };
		operation.Security.Add(new OpenApiSecurityRequirement
		{
			[scheme] = new List<string>()
		});
	}
}

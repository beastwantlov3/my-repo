﻿using APIConnect.Helper;
using BusinessObject.Models;
using MathNet.Numerics;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using Repositories;
using Swashbuckle.AspNetCore.Filters;
using System.Text;

var builder = WebApplication.CreateBuilder(args);


var configuration = builder.Services.BuildServiceProvider().GetRequiredService<IConfiguration>();
var jwtSettings = configuration.GetSection("Jwt");
// Đọc giá trị từ appsettings.json
var secretKey = jwtSettings.GetValue<string>("Key");
var key = Encoding.ASCII.GetBytes(secretKey);
var issuer = configuration["Issuer"];
var audience = configuration["Audience"];
var subject = configuration["Subject"];

// Add services to the container.

builder.Services.AddControllers();
builder.Services.AddControllersWithViews().AddJsonOptions(options => options.JsonSerializerOptions.ReferenceHandler = System.Text.Json.Serialization.ReferenceHandler.IgnoreCycles);
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddDbContext<VPSContext>(opt =>
{
	opt.UseSqlServer(builder.Configuration.GetConnectionString("VPS"));
});

builder.Services.AddSwaggerGen(c =>
{
	c.SwaggerDoc("v1", new OpenApiInfo { Title = "My API", Version = "v1" });

	// Thêm xác thực JWT vào Swagger UI
	c.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
	{
		Description = "JWT Authorization header using the Bearer scheme. Example: \"Bearer {token}\"",
		Type = SecuritySchemeType.Http,
		Scheme = "bearer"
	});

	c.OperationFilter<SecurityRequirementsOperationFilter>();
});
builder.Services.AddHostedService<EmailWorker>();
// Cấu hình xác thực JWT
builder.Services.AddAuthentication(options =>
{
	options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
	options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
})
.AddJwtBearer(options =>
{
	options.RequireHttpsMetadata = false;
	options.SaveToken = true;
	options.TokenValidationParameters = new TokenValidationParameters
	{
		ValidateIssuerSigningKey = true,
		IssuerSigningKey = new SymmetricSecurityKey(key),
		ValidateIssuer = false,
		//ValidIssuer = issuer,
		ValidateAudience = false
		//ValidAudience = audience
	};
});

builder.Services.AddCors(options =>
{
	options.AddPolicy("AllowOrigin", builder =>
	{
		builder.WithOrigins("http://localhost:3000", "http://localhost:3001", "http://localhost:3002")
			   .AllowAnyHeader()
			   .AllowAnyMethod()
			   .AllowCredentials();
	});
});



//// Cấu hình Role-based Authorization
//builder.Services.AddAuthorization(options =>
//{
//	options.AddPolicy("AdminOnly", policy => policy.RequireRole("Admin"));
//	options.AddPolicy("BloggerOnly", policy => policy.RequireRole("Blogger"));
//	options.AddPolicy("ConsultantOnly", policy => policy.RequireRole("Consultant"));
//	options.AddPolicy("CustomersOnly", policy => policy.RequireRole("Customers"));
//	options.AddPolicy("ManagerOnly", policy => policy.RequireRole("Manager"));
//	options.AddPolicy("SaleOnly", policy => policy.RequireRole("Sale"));
//	options.AddPolicy("ShipperOnly", policy => policy.RequireRole("Shipper"));
//	options.AddPolicy("SupplierOnly", policy => policy.RequireRole("Supplier"));
//	options.AddPolicy("TechnicalOnly", policy => policy.RequireRole("Technical"));
//	// Thêm các policy khác nếu cần
//});
builder.Services.AddScoped<IOrderRepository, OrderRepository>();

builder.Services.AddSession(options =>
{
	// Set session timeout
	options.IdleTimeout = TimeSpan.FromMinutes(30);
	options.Cookie.HttpOnly = true;
	options.Cookie.IsEssential = true;
});
var app = builder.Build();

// Configure the HTTP request pipeline.
    app.UseSwagger();
	app.UseSwaggerUI(c =>
	{
		c.SwaggerEndpoint("/swagger/v1/swagger.json", "My API V1");
	});
app.UseHttpsRedirection();
app.UseRouting();
app.UseSession();
app.UseCors("AllowOrigin");
app.UseAuthentication();

//app.UseCookiePolicy();

app.UseAuthorization();

//app.UseCors(
//    x => x.AllowAnyHeader()
//    .AllowAnyMethod()
//    .AllowCredentials()
//    .SetIsOriginAllowed(origin => true));

app.MapControllers();

app.Run();


﻿namespace APIConnect.DTO
{
	public class BlogDTO
	{
		public int? BloggerId { get; set; }
		public string? Title { get; set; }
		public string? Description { get; set; }
		public DateTime? PublishDate { get; set; }
		public IFormFile Image { get; set; }
		public int? CateId { get; set; }
	}
}

﻿using BusinessObject.Models;
using System.ComponentModel.DataAnnotations;

namespace APIConnect.DTO
{
	public class RegistrationData
	{
		public Account Account { get; set; }
		public AccountDTO AccountDTO { get; set; }
		public string VerificationCode { get; set; }
	}
	public class AccountDTO
    {
        //public int AccountId { get; set; }
        [Required]
        public string Username { get; set; }

        public string Email { get; set; }

        [Required]
        public string Password { get; set; }
        public string ConfirmPassword { get; set; }
        public string FullName { get; set; }
        public string Phone { get; set; }
        public string DOB { get; set; }
        //public int RoleId { get; set; }
    }

    public class ChangePasswordDTO
    {
        public string CurrentPassword { get; set; }
        public string NewPassword { get; set; }
        public string ConfirmPassword { get; set; }
    }

    public class UserProfile
    {
        public string FullName { get; set; }
        public string Phone { get; set; }
        public string Address { get; set; }
        public string DOB { get; set; }

        public string Profile { get; set; }
    }

	public class IdentityCardDTO
	{
		public IFormFile IdentityCardImage { get; set; }
		public IFormFile BackIdentityCard { get; set; }
	}
}

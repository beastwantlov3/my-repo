﻿namespace APIConnect.DTO
{
	public class SupplierProductDTO
	{
		public string? ProductName { get; set; }
		public string? Description { get; set; }
		public int? CategoryId { get; set; }
		public IFormFile Image { get; set; }
		public string? Brand { get; set; }
		public int? StatusId { get; set; }
		public int? UnitInStock { get; set; }
		public double? UnitPrice { get; set; }
		//public int? NumberOfUse { get; set; }
		public int? Quality { get; set; }
	}
}

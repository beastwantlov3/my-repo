﻿using DataAccess;

namespace APIConnect.DTO
{
	public class OrderRequestModel
	{
		public int CustomerId { get; set; }
		public IFormFile CurrentImage { get; set; }
		public double Price { get; set; }
		public decimal ShipCost { get; set; }
		public List<CartItemRequestModel> CartItems { get; set; }
	}

	public class CartItemRequestModel
	{
		public int ProductId { get; set; }
		public int Quantity { get; set; }
		public double UnitPrice { get; set; }
		public double? Discount { get; set; }
		public bool? ThreeDays { get; set; }
		public bool? OneWeeks { get; set; }
		public string? ProductCode { get; set; }
	}
}
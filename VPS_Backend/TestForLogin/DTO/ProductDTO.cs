﻿namespace APIConnect.DTO
{
	public class ProductDTO
	{
		public string ProductName { get; set; }
		public int CategoryId { get; set; }
		public int? UnitInStock { get; set; }
		public double UnitPrice { get; set; }
		public IFormFile Image { get; set; }
		public List<IFormFile> CoreImage { get; set; }
		public int StatusId { get; set; }
		public string Brand { get; set; }
		public string Description { get; set; }
		public int Quality { get; set; }
		public List<int> QualityPercentage { get; set; }
		public int? Discount { get; set; }
	}

	public class Top4SellingProductsDTO
	{
		public int ProductId { get; set; }
		public string ProductName { get; set; }
		public double? UnitPrice { get; set; }
		public int? StatusId { get; set; }
		public string Brand { get; set; }
		public string Description { get; set; }
		public int? Discount { get; set; }
		public DateTime? DiscountDate { get; set; }
		public int? Quality { get; set; }
		public int? UnitInStock { get; set; }
		public string Image { get; set; }
		public int? TotalOrder { get; set; }
	}
}

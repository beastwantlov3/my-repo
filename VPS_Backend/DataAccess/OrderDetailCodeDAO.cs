﻿using BusinessObject.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess
{
    public class OrderDetailCodeDAO
    {
        public static List<OrderDetailCode> GetOrderDetailCode()
        {
            var listOrderDetailCode = new List<OrderDetailCode>();
            try
            {
                using (var context = new VPSContext())
                {
                    listOrderDetailCode = context.OrderDetailCodes.ToList();
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            return listOrderDetailCode;
        }
        public static OrderDetailCode GetOrderDetailCodesById(int id)
        {
            var OrderDetailCode = new OrderDetailCode();
            try
            {
                using (var context = new VPSContext())
                {
                    OrderDetailCode = context.OrderDetailCodes.Find(id);
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            return OrderDetailCode;
        }
        public static void UpdateOrderDetailCode(int id, OrderDetailCode OrderDetailCode)
        {
            OrderDetailCode current;
            try
            {
                using (var context = new VPSContext())
                {
                    current = context.OrderDetailCodes.Find(id);
                    current.OrderDetailId = OrderDetailCode.OrderDetailId;
                    current.ProductCodeId = OrderDetailCode.ProductCodeId;
                    context.OrderDetailCodes.Update(current);
                    context.SaveChanges();
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }
        public static void AddOrderDetailCode(OrderDetailCode OrderDetailCode)
        {
            try
            {
                using (var context = new VPSContext())
                {
                    context.OrderDetailCodes.Add(OrderDetailCode);
                    context.SaveChanges();
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }
        public static void DeleteOrderDetailCode(int id)
        {
            OrderDetailCode current;
            try
            {
                using (var context = new VPSContext())
                {
                    current = context.OrderDetailCodes.Find(id);
                    context.OrderDetailCodes.Remove(current);
                    context.SaveChanges();
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }
    }
}

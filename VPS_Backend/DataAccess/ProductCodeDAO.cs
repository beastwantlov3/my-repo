﻿using BusinessObject.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess
{
    public class ProductCodeDAO
    {
        public static List<ProductCode> GetProductCode()
        {
            var productCode = new List<ProductCode>();
            try
            {
                using (var context = new VPSContext())
                {
                    productCode = context.ProductCodes.ToList();
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            return productCode;
        }
        public static ProductCode GetProductById(int id)
        {
            var productCode = new ProductCode();
            try
            {
                using (var context = new VPSContext())
                {
                    productCode = context.ProductCodes.Find(id);
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            return productCode;
        }

        public static List<ProductCode> GetProductCodeByProductId (int id)
        {
			var productCode = new List<ProductCode>();
			try
			{
				using (var context = new VPSContext())
				{
					productCode = context.ProductCodes.Where(p => p.ProductId == id).ToList();
				}
			}
			catch (Exception e)
			{
				throw new Exception(e.Message);
			}
			return productCode;
		}

        public static void UpdateProductCode(int id, ProductCode productCode)
        {
            ProductCode current;
            try
            {
                using (var context = new VPSContext())
                {
                    current = context.ProductCodes.Find(id);
                    current.ProductId = productCode.ProductId;
                    current.Code = productCode.Code;
                    current.IsAvailable = productCode.IsAvailable;
                    current.QualityPercentage = productCode.QualityPercentage;
                    context.ProductCodes.Update(current);
                    context.SaveChanges();
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }
        public static void AddProductCode(ProductCode productCode)
        {
            try
            {
                using (var context = new VPSContext())
                {
                    context.ProductCodes.Add(productCode);
                    context.SaveChanges();
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }
        public static void DeleteProductCode(int id)
        {
            ProductCode current;
            try
            {
                using (var context = new VPSContext())
                {
                    current = context.ProductCodes.Find(id);
                    context.ProductCodes.Remove(current);
                    context.SaveChanges();
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }
    }
}

﻿using BusinessObject.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess
{
    public class AlertDAO
    {
        public static List<Alert> GetAlert()
        {
            var listBlogs = new List<Alert>();
            try
            {
                using (var context = new VPSContext())
                {
                    listBlogs = context.Alerts.Include(x=>x.Customer).Include(y=>y.Sale).Include(m=>m.Shipper).ToList();
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            return listBlogs;
        }
        public static Alert GetAlertById(int id)
        {
            var blog = new Alert();
            try
            {
                using (var context = new VPSContext())
                {
                    blog = context.Alerts.Include(x => x.Customer).Include(y => y.Sale).Include(m => m.Shipper).FirstOrDefault(x=>x.AlertId == id);
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            return blog;
        }
        public static void UpdateAlert(int id, Alert alert)
        {
            Alert current;
            try
            {
                using (var context = new VPSContext())
                {
                    current = context.Alerts.Find(id);
                    current.CustomerId = alert.CustomerId;
                    current.SaleId = alert.SaleId;
                    current.ShipperId = alert.ShipperId;
                    current.Message = alert.Message;
                    current.StatusId = alert.StatusId;
                    current.OrderDetailId = alert.OrderDetailId;
                    context.Alerts.Update(current);
                    context.SaveChanges();
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }
        public static void AddAlert(Alert blog)
        {
            try
            {
                using (var context = new VPSContext())
                {
                    context.Alerts.Add(blog);
                    context.SaveChanges();
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }
        public static void DeleteAlert(int id)
        {
            Alert current;
            try
            {
                using (var context = new VPSContext())
                {
                    current = context.Alerts.Find(id);
                    context.Alerts.Remove(current);
                    context.SaveChanges();
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }
    }
}

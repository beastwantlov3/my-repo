﻿using BusinessObject.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess
{
    public class SupplierAlertDAO
    {
        public static List<SupplierAlert> GetSupplierAlert()
        {
            var listSupplierAlert = new List<SupplierAlert>();
            try
            {
                using (var context = new VPSContext())
                {
                    listSupplierAlert = context.SupplierAlerts.Include(x=>x.Supplier).Include(Y=>Y.Sproduct).ToList();
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            return listSupplierAlert;
        }
        public static SupplierAlert GetSupplierAlertById(int id)
        {
            var supplierAlert = new SupplierAlert();
            try
            {
                using (var context = new VPSContext())
                {
                    supplierAlert = context.SupplierAlerts.Include(x => x.Supplier).Include(Y => Y.Sproduct).FirstOrDefault(m=>m.Sid == id);
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            return supplierAlert;
        }

		public static List<SupplierAlert> GetListSupplierAlertById(int supplierId)
		{
			using (var context = new VPSContext())
			{
				var supplierAlerts = context.SupplierAlerts
					.Where(salert => salert.SupplierId == supplierId)
					.ToList();

				return supplierAlerts;
			}
		}
		public static void UpdateSupplierAlert(int id, SupplierAlert supplierAlert)
        {
            SupplierAlert current;
            try
            {
                using (var context = new VPSContext())
                {
                    current = context.SupplierAlerts.Find(id);
                    current.SproductId = supplierAlert.SproductId;
                    current.Quantity = supplierAlert.Quantity;
                    current.SupplierId = supplierAlert.SupplierId;
                    current.Message = supplierAlert.Message;
                    current.StatusId = supplierAlert.StatusId;
                    current.CurrentDate = supplierAlert.CurrentDate;
                    current.ThreeDays = supplierAlert.ThreeDays;
                    current.OneWeeks = supplierAlert.OneWeeks;
                    current.Price = supplierAlert.Price;
                    context.SupplierAlerts.Update(current);
                    context.SaveChanges();
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }
        public static void AddSupplierAlert(SupplierAlert feedBack)
        {
            try
            {
                using (var context = new VPSContext())
                {
                    context.SupplierAlerts.Add(feedBack);
                    context.SaveChanges();
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }
        public static void DeleteSupplierAlert(int id)
        {
            SupplierAlert current;
            try
            {
                using (var context = new VPSContext())
                {
                    current = context.SupplierAlerts.Find(id);
                    context.SupplierAlerts.Remove(current);
                    context.SaveChanges();
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }
    }
}

USE [VPS]
GO
/****** Object:  Table [dbo].[Account]    Script Date: 8/12/2023 9:09:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Account](
	[AccountId] [int] IDENTITY(1,1) NOT NULL,
	[UserName] [nvarchar](50) NOT NULL,
	[Password] [nvarchar](50) NOT NULL,
	[Email] [nvarchar](200) NOT NULL,
	[Avatar] [nvarchar](max) NULL,
	[Status] [int] NULL,
	[Profile] [nvarchar](500) NULL,
	[RoleId] [int] NULL,
	[BloggerId] [int] NULL,
	[ConsultantId] [int] NULL,
	[CustomerId] [int] NULL,
	[ManagerId] [int] NULL,
	[SupplierId] [int] NULL,
	[TechnicalId] [int] NULL,
	[AdminId] [int] NULL,
	[SaleId] [int] NULL,
	[ShipperId] [int] NULL,
	[CreatedDate] [datetime] NULL,
 CONSTRAINT [PK_Account] PRIMARY KEY CLUSTERED 
(
	[AccountId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Admin]    Script Date: 8/12/2023 9:09:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Admin](
	[AdminId] [int] IDENTITY(1,1) NOT NULL,
	[FullName] [nvarchar](250) NULL,
	[Phone] [nvarchar](250) NULL,
	[Address] [nvarchar](250) NULL,
	[DOB] [datetime] NULL,
	[StatusID] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[AdminId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Alert]    Script Date: 8/12/2023 9:09:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Alert](
	[AlertID] [int] IDENTITY(1,1) NOT NULL,
	[CustomerID] [int] NULL,
	[SaleID] [int] NULL,
	[ShipperID] [int] NULL,
	[Message] [nvarchar](30) NULL,
	[StatusID] [int] NULL,
	[OrderID] [int] NULL,
	[CurrentDate] [datetime] NULL,
	[OrderDetailID] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[AlertID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Blog]    Script Date: 8/12/2023 9:09:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Blog](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[BloggerId] [int] NULL,
	[Title] [nvarchar](200) NULL,
	[Description] [nvarchar](max) NULL,
	[PublishDate] [date] NULL,
	[Image] [nvarchar](150) NULL,
	[StatusId] [int] NULL,
	[CateId] [int] NULL,
 CONSTRAINT [PK_Blog] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[BlogCategory]    Script Date: 8/12/2023 9:09:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BlogCategory](
	[BCategory] [int] IDENTITY(1,1) NOT NULL,
	[BName] [nvarchar](200) NULL,
PRIMARY KEY CLUSTERED 
(
	[BCategory] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Blogger]    Script Date: 8/12/2023 9:09:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Blogger](
	[BloggerId] [int] IDENTITY(1,1) NOT NULL,
	[FullName] [nvarchar](200) NULL,
	[Phone] [nvarchar](50) NULL,
	[Address] [nvarchar](200) NULL,
	[DOB] [datetime] NULL,
	[StatusId] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[BloggerId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Category]    Script Date: 8/12/2023 9:09:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Category](
	[CategoryId] [int] IDENTITY(1,1) NOT NULL,
	[CategoryName] [nvarchar](100) NOT NULL,
	[Description] [ntext] NULL,
PRIMARY KEY CLUSTERED 
(
	[CategoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Comment]    Script Date: 8/12/2023 9:09:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Comment](
	[CommentId] [int] IDENTITY(1,1) NOT NULL,
	[BlogId] [int] NULL,
	[BloggerId] [int] NULL,
	[CustomerId] [int] NULL,
	[Content] [nvarchar](500) NULL,
	[CommentDate] [date] NULL,
PRIMARY KEY CLUSTERED 
(
	[CommentId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Consultant]    Script Date: 8/12/2023 9:09:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Consultant](
	[ConsultantId] [int] IDENTITY(1,1) NOT NULL,
	[FullName] [nvarchar](200) NULL,
	[Phone] [nvarchar](50) NULL,
	[Address] [nvarchar](200) NULL,
	[DOB] [datetime] NULL,
	[StatusId] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[ConsultantId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Contract]    Script Date: 8/12/2023 9:09:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Contract](
	[ContractId] [int] IDENTITY(1,1) NOT NULL,
	[TechnicalId] [int] NULL,
	[SupplierId] [int] NULL,
	[Content] [nvarchar](max) NULL,
PRIMARY KEY CLUSTERED 
(
	[ContractId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Conversation]    Script Date: 8/12/2023 9:09:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Conversation](
	[ConversationId] [int] IDENTITY(1,1) NOT NULL,
	[ConversationName] [nvarchar](100) NULL,
PRIMARY KEY CLUSTERED 
(
	[ConversationId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Customers]    Script Date: 8/12/2023 9:09:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Customers](
	[CustomerId] [int] IDENTITY(1,1) NOT NULL,
	[FullName] [nvarchar](200) NULL,
	[Phone] [nvarchar](50) NULL,
	[Address] [nvarchar](200) NULL,
	[Balance] [int] NULL,
	[DOB] [datetime] NULL,
	[StatusId] [int] NULL,
	[IsValid] [bit] NULL,
	[Voucher] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[CustomerId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[FeedBack]    Script Date: 8/12/2023 9:09:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FeedBack](
	[FeedBackId] [int] IDENTITY(1,1) NOT NULL,
	[CustomerId] [int] NULL,
	[OrderId] [int] NULL,
	[OrderDetailId] [int] NULL,
	[Star] [int] NULL,
	[Content] [nvarchar](300) NULL,
PRIMARY KEY CLUSTERED 
(
	[FeedBackId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[HStatus]    Script Date: 8/12/2023 9:09:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[HStatus](
	[StatusId] [int] IDENTITY(1,1) NOT NULL,
	[StatusNum] [int] NULL,
	[StatusValue] [nvarchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[StatusId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[IdentityCard]    Script Date: 8/12/2023 9:09:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[IdentityCard](
	[CardId] [int] IDENTITY(1,1) NOT NULL,
	[CardNumber] [char](14) NULL,
	[FullName] [nvarchar](200) NULL,
	[DateOfBirth] [date] NULL,
	[Gender] [bit] NULL,
	[Nationality] [nvarchar](250) NULL,
	[PlaceOfOrigin] [nvarchar](250) NULL,
	[IdentityCardImage] [nchar](250) NULL,
	[BackIdentityCard] [nchar](250) NULL,
	[CustomerId] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[CardId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Manager]    Script Date: 8/12/2023 9:09:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Manager](
	[ManagerId] [int] NOT NULL,
	[FullName] [nvarchar](200) NULL,
	[Phone] [nvarchar](50) NULL,
	[Address] [nvarchar](200) NULL,
	[DOB] [datetime] NULL,
	[StatusId] [int] NULL,
 CONSTRAINT [PK_Manager] PRIMARY KEY CLUSTERED 
(
	[ManagerId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Messages]    Script Date: 8/12/2023 9:09:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Messages](
	[MessageId] [int] IDENTITY(1,1) NOT NULL,
	[ConversationId] [int] NULL,
	[CustomerId] [int] NULL,
	[Description] [nvarchar](500) NULL,
	[Time] [datetime] NULL,
	[StatusId] [int] NULL,
	[ConsultantId] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[MessageId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Order]    Script Date: 8/12/2023 9:09:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Order](
	[OrderId] [int] IDENTITY(1,1) NOT NULL,
	[SaleId] [int] NULL,
	[CustomerId] [int] NULL,
	[OrderDate] [datetime] NULL,
	[ShippedDate] [datetime] NULL,
	[ShipId] [int] NULL,
	[Price] [float] NOT NULL,
	[StatusId] [int] NOT NULL,
	[ShipCost] [money] NULL,
	[ManagerConfirm] [datetime] NULL,
	[SaleConfirm] [datetime] NULL,
	[ShipConfirm] [datetime] NULL,
	[CustomerConfirm] [datetime] NULL,
	[ShipSuccessConfirm] [datetime] NULL,
	[ManagerViewed] [datetime] NULL,
	[IsValid] [bit] NULL,
	[ManagerCancelled] [datetime] NULL,
	[SalerCancelled] [datetime] NULL,
	[ShipperCancelled] [datetime] NULL,
	[IdentityCardImage] [nvarchar](200) NULL,
	[CurrentImage] [nvarchar](200) NULL,
	[CustomerCancelled] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[OrderId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OrderDetail]    Script Date: 8/12/2023 9:09:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OrderDetail](
	[OrderDetailId] [int] IDENTITY(1,1) NOT NULL,
	[OrderId] [int] NULL,
	[ProductId] [int] NULL,
	[UnitPrice] [float] NULL,
	[Quantity] [int] NULL,
	[Discount] [float] NULL,
	[ThreeDays] [bit] NULL,
	[OneWeeks] [bit] NULL,
	[ProductCode] [nchar](10) NULL,
PRIMARY KEY CLUSTERED 
(
	[OrderDetailId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OrderDetailCode]    Script Date: 8/12/2023 9:09:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OrderDetailCode](
	[OrderDetailCodeId] [int] IDENTITY(1,1) NOT NULL,
	[OrderDetailId] [int] NULL,
	[ProductCodeId] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[OrderDetailCodeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Product]    Script Date: 8/12/2023 9:09:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Product](
	[ProductId] [int] IDENTITY(1,1) NOT NULL,
	[ProductName] [nvarchar](250) NULL,
	[CategoryId] [int] NULL,
	[UnitInStock] [int] NULL,
	[UnitPrice] [float] NULL,
	[Image] [nvarchar](max) NULL,
	[StatusId] [int] NULL,
	[Brand] [nvarchar](250) NULL,
	[Description] [nvarchar](max) NULL,
	[DiscountDate] [datetime] NULL,
	[Type] [nvarchar](100) NULL,
	[Moneytype] [nvarchar](50) NULL,
	[Quality] [int] NULL,
	[Discount] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[ProductId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ProductCode]    Script Date: 8/12/2023 9:09:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ProductCode](
	[CodeId] [int] IDENTITY(1,1) NOT NULL,
	[ProductId] [int] NULL,
	[Code] [nchar](20) NULL,
	[IsAvailable] [bit] NULL,
	[qualityPercentage] [int] NULL,
 CONSTRAINT [PK_ProductCode] PRIMARY KEY CLUSTERED 
(
	[CodeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ProductDetail]    Script Date: 8/12/2023 9:09:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ProductDetail](
	[DetailId] [int] IDENTITY(1,1) NOT NULL,
	[ProductId] [int] NULL,
	[CoreImage] [nvarchar](200) NULL,
PRIMARY KEY CLUSTERED 
(
	[DetailId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PStatus]    Script Date: 8/12/2023 9:09:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PStatus](
	[StatusId] [int] IDENTITY(1,1) NOT NULL,
	[StatusNum] [int] NULL,
	[StatusValue] [nvarchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[StatusId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[QRCode]    Script Date: 8/12/2023 9:09:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[QRCode](
	[QRID] [int] IDENTITY(1,1) NOT NULL,
	[Code] [nvarchar](250) NULL,
	[Description] [nvarchar](250) NULL,
	[DayCreate] [datetime] NULL,
	[ProductId] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[QRID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Rent]    Script Date: 8/12/2023 9:09:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Rent](
	[BuyId] [int] IDENTITY(1,1) NOT NULL,
	[OrderId] [int] NOT NULL,
	[CustomerId] [int] NOT NULL,
 CONSTRAINT [PK_Buy] PRIMARY KEY CLUSTERED 
(
	[BuyId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Role]    Script Date: 8/12/2023 9:09:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Role](
	[RoleId] [int] IDENTITY(1,1) NOT NULL,
	[RoleName] [nvarchar](200) NULL,
PRIMARY KEY CLUSTERED 
(
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Sale]    Script Date: 8/12/2023 9:09:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Sale](
	[SaleId] [int] IDENTITY(1,1) NOT NULL,
	[FullName] [nvarchar](200) NULL,
	[Phone] [nvarchar](50) NULL,
	[Address] [nvarchar](200) NULL,
	[DOB] [datetime] NULL,
	[StatusId] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[SaleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Shipper]    Script Date: 8/12/2023 9:09:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Shipper](
	[ShipperID] [int] IDENTITY(1,1) NOT NULL,
	[FullName] [nvarchar](200) NULL,
	[Phone] [nvarchar](50) NULL,
	[Address] [nvarchar](200) NULL,
	[DOB] [datetime] NULL,
	[StatusId] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[ShipperID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Status]    Script Date: 8/12/2023 9:09:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Status](
	[StatusId] [int] IDENTITY(1,1) NOT NULL,
	[StatusNum] [int] NULL,
	[StatusValue] [nvarchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[StatusId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Stock]    Script Date: 8/12/2023 9:09:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Stock](
	[StockId] [int] IDENTITY(1,1) NOT NULL,
	[ProductId] [int] NULL,
	[TechnicalId] [int] NULL,
	[SupplierId] [int] NULL,
	[CategoryId] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[StockId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Supplier]    Script Date: 8/12/2023 9:09:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Supplier](
	[SupplierId] [int] IDENTITY(1,1) NOT NULL,
	[SupplierName] [nvarchar](100) NULL,
	[Phone] [nvarchar](50) NULL,
	[Address] [nvarchar](200) NULL,
	[DOB] [datetime] NULL,
	[Avatar] [nvarchar](200) NULL,
	[StatusId] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[SupplierId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SupplierAlert]    Script Date: 8/12/2023 9:09:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SupplierAlert](
	[SId] [int] IDENTITY(1,1) NOT NULL,
	[SProductId] [int] NULL,
	[Quantity] [int] NULL,
	[SupplierId] [int] NULL,
	[Message] [nvarchar](200) NULL,
	[StatusId] [bit] NULL,
	[CurrentDate] [datetime] NULL,
	[ThreeDays] [bit] NULL,
	[OneWeeks] [bit] NULL,
	[Price] [money] NULL,
PRIMARY KEY CLUSTERED 
(
	[SId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SupplierProduct]    Script Date: 8/12/2023 9:09:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SupplierProduct](
	[ProductId] [int] IDENTITY(1,1) NOT NULL,
	[ProductName] [nvarchar](250) NULL,
	[Description] [nvarchar](250) NULL,
	[CategoryId] [int] NULL,
	[Image] [nvarchar](250) NULL,
	[Brand] [nvarchar](250) NULL,
	[StatusId] [int] NULL,
	[UnitInStock] [int] NULL,
	[UnitPrice] [float] NULL,
	[NumberOfUse] [int] NULL,
	[Quality] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[ProductId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SupplierStock]    Script Date: 8/12/2023 9:09:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SupplierStock](
	[StockId] [int] IDENTITY(1,1) NOT NULL,
	[ProductId] [int] NULL,
	[SupplierId] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[StockId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Technical]    Script Date: 8/12/2023 9:09:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Technical](
	[TechnicalId] [int] IDENTITY(1,1) NOT NULL,
	[FullName] [nvarchar](200) NULL,
	[Phone] [nvarchar](50) NULL,
	[Address] [nvarchar](200) NULL,
	[DOB] [datetime] NULL,
	[Avatar] [nvarchar](250) NULL,
	[StatusId] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[TechnicalId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Account] ON 

INSERT [dbo].[Account] ([AccountId], [UserName], [Password], [Email], [Avatar], [Status], [Profile], [RoleId], [BloggerId], [ConsultantId], [CustomerId], [ManagerId], [SupplierId], [TechnicalId], [AdminId], [SaleId], [ShipperId], [CreatedDate]) VALUES (17, N'nguyenhoanglong', N'E10I0MEotAjGhoVCMELgfW+g6JedjRA+8VReVjefkjo=', N'nguyenhoanglong@email.com', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, CAST(N'2023-05-07T00:00:00.000' AS DateTime))
INSERT [dbo].[Account] ([AccountId], [UserName], [Password], [Email], [Avatar], [Status], [Profile], [RoleId], [BloggerId], [ConsultantId], [CustomerId], [ManagerId], [SupplierId], [TechnicalId], [AdminId], [SaleId], [ShipperId], [CreatedDate]) VALUES (19, N'nguyenvananh', N'E10I0MEotAjGhoVCMELgfW+g6JedjRA+8VReVjefkjo=', N'nguyenhoainam@email.com', N'avatar-51914b55-ae76-429b-a338-25ddc8ab4131', 1, NULL, 4, 1, NULL, 16, NULL, NULL, NULL, NULL, NULL, NULL, CAST(N'2023-01-07T00:00:00.000' AS DateTime))
INSERT [dbo].[Account] ([AccountId], [UserName], [Password], [Email], [Avatar], [Status], [Profile], [RoleId], [BloggerId], [ConsultantId], [CustomerId], [ManagerId], [SupplierId], [TechnicalId], [AdminId], [SaleId], [ShipperId], [CreatedDate]) VALUES (20, N'lequangcuong', N'E10I0MEotAjGhoVCMELgfW+g6JedjRA+8VReVjefkjo=', N'lehoanganh@email.com', NULL, 1, NULL, 4, NULL, NULL, 18, NULL, NULL, NULL, NULL, NULL, 4, CAST(N'2023-03-04T00:00:00.000' AS DateTime))
INSERT [dbo].[Account] ([AccountId], [UserName], [Password], [Email], [Avatar], [Status], [Profile], [RoleId], [BloggerId], [ConsultantId], [CustomerId], [ManagerId], [SupplierId], [TechnicalId], [AdminId], [SaleId], [ShipperId], [CreatedDate]) VALUES (21, N'phamthanhhung', N'E10I0MEotAjGhoVCMELgfW+g6JedjRA+8VReVjefkjo=', N'nguyenvananh@email.com', NULL, 1, NULL, 4, NULL, NULL, 19, NULL, NULL, 1, NULL, NULL, NULL, CAST(N'2023-01-05T00:00:00.000' AS DateTime))
INSERT [dbo].[Account] ([AccountId], [UserName], [Password], [Email], [Avatar], [Status], [Profile], [RoleId], [BloggerId], [ConsultantId], [CustomerId], [ManagerId], [SupplierId], [TechnicalId], [AdminId], [SaleId], [ShipperId], [CreatedDate]) VALUES (1020, N'nguyendangdung', N'E10I0MEotAjGhoVCMELgfW+g6JedjRA+8VReVjefkjo=', N'nguyenvanminh@gmail.com', N'avatar-5cd4e368-c464-43ae-b135-a596cd7eb3d7', 1, NULL, 4, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, CAST(N'2023-03-02T00:00:00.000' AS DateTime))
INSERT [dbo].[Account] ([AccountId], [UserName], [Password], [Email], [Avatar], [Status], [Profile], [RoleId], [BloggerId], [ConsultantId], [CustomerId], [ManagerId], [SupplierId], [TechnicalId], [AdminId], [SaleId], [ShipperId], [CreatedDate]) VALUES (1021, N'nguyenvana', N'E10I0MEotAjGhoVCMELgfW+g6JedjRA+8VReVjefkjo=', N'nguyenvana@gmail.com', NULL, 1, NULL, 4, NULL, NULL, 1002, NULL, NULL, NULL, NULL, NULL, NULL, CAST(N'2023-07-16T10:42:05.283' AS DateTime))
INSERT [dbo].[Account] ([AccountId], [UserName], [Password], [Email], [Avatar], [Status], [Profile], [RoleId], [BloggerId], [ConsultantId], [CustomerId], [ManagerId], [SupplierId], [TechnicalId], [AdminId], [SaleId], [ShipperId], [CreatedDate]) VALUES (1022, N'maithitramy', N'E10I0MEotAjGhoVCMELgfW+g6JedjRA+8VReVjefkjo=', N'nguyenvanb@gmail.com', NULL, 1, NULL, 6, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, CAST(N'2023-07-16T10:42:05.283' AS DateTime))
INSERT [dbo].[Account] ([AccountId], [UserName], [Password], [Email], [Avatar], [Status], [Profile], [RoleId], [BloggerId], [ConsultantId], [CustomerId], [ManagerId], [SupplierId], [TechnicalId], [AdminId], [SaleId], [ShipperId], [CreatedDate]) VALUES (1023, N'tranvanbinh', N'E10I0MEotAjGhoVCMELgfW+g6JedjRA+8VReVjefkjo=', N'nguyenvanc@gmail.com', NULL, 1, NULL, 6, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, CAST(N'2023-07-16T10:42:05.283' AS DateTime))
INSERT [dbo].[Account] ([AccountId], [UserName], [Password], [Email], [Avatar], [Status], [Profile], [RoleId], [BloggerId], [ConsultantId], [CustomerId], [ManagerId], [SupplierId], [TechnicalId], [AdminId], [SaleId], [ShipperId], [CreatedDate]) VALUES (1024, N'leminhcung', N'E10I0MEotAjGhoVCMELgfW+g6JedjRA+8VReVjefkjo=', N'nguyenvand@gmail.com', NULL, 1, NULL, 6, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 3, NULL, CAST(N'2023-07-16T10:42:05.283' AS DateTime))
INSERT [dbo].[Account] ([AccountId], [UserName], [Password], [Email], [Avatar], [Status], [Profile], [RoleId], [BloggerId], [ConsultantId], [CustomerId], [ManagerId], [SupplierId], [TechnicalId], [AdminId], [SaleId], [ShipperId], [CreatedDate]) VALUES (1025, N'phamthanhdanh', N'E10I0MEotAjGhoVCMELgfW+g6JedjRA+8VReVjefkjo=', N'nguyenvane@gmail.com', NULL, 1, NULL, 6, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, NULL, CAST(N'2023-07-16T10:42:05.283' AS DateTime))
INSERT [dbo].[Account] ([AccountId], [UserName], [Password], [Email], [Avatar], [Status], [Profile], [RoleId], [BloggerId], [ConsultantId], [CustomerId], [ManagerId], [SupplierId], [TechnicalId], [AdminId], [SaleId], [ShipperId], [CreatedDate]) VALUES (1026, N'danghoangchung', N'E10I0MEotAjGhoVCMELgfW+g6JedjRA+8VReVjefkjo=', N'nguyenvanf@gmail.com', NULL, 1, NULL, 6, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 5, NULL, CAST(N'2023-07-16T10:42:05.283' AS DateTime))
INSERT [dbo].[Account] ([AccountId], [UserName], [Password], [Email], [Avatar], [Status], [Profile], [RoleId], [BloggerId], [ConsultantId], [CustomerId], [ManagerId], [SupplierId], [TechnicalId], [AdminId], [SaleId], [ShipperId], [CreatedDate]) VALUES (1027, N'nguyenvana', N'E10I0MEotAjGhoVCMELgfW+g6JedjRA+8VReVjefkjo=', N'nguyenvang@gmail.com', NULL, 1, NULL, 5, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, CAST(N'2023-07-16T10:42:05.283' AS DateTime))
INSERT [dbo].[Account] ([AccountId], [UserName], [Password], [Email], [Avatar], [Status], [Profile], [RoleId], [BloggerId], [ConsultantId], [CustomerId], [ManagerId], [SupplierId], [TechnicalId], [AdminId], [SaleId], [ShipperId], [CreatedDate]) VALUES (1028, N'phamthib', N'E10I0MEotAjGhoVCMELgfW+g6JedjRA+8VReVjefkjo=', N'nguyenvanh@gmail.com', NULL, 1, NULL, 5, NULL, NULL, NULL, 2, NULL, NULL, NULL, NULL, NULL, CAST(N'2023-07-16T10:42:05.283' AS DateTime))
INSERT [dbo].[Account] ([AccountId], [UserName], [Password], [Email], [Avatar], [Status], [Profile], [RoleId], [BloggerId], [ConsultantId], [CustomerId], [ManagerId], [SupplierId], [TechnicalId], [AdminId], [SaleId], [ShipperId], [CreatedDate]) VALUES (1029, N'tranvanc', N'E10I0MEotAjGhoVCMELgfW+g6JedjRA+8VReVjefkjo=', N'nguyenvani@gmail.com', NULL, 1, NULL, 5, NULL, NULL, NULL, 3, NULL, NULL, NULL, NULL, NULL, CAST(N'2023-07-16T10:42:05.283' AS DateTime))
INSERT [dbo].[Account] ([AccountId], [UserName], [Password], [Email], [Avatar], [Status], [Profile], [RoleId], [BloggerId], [ConsultantId], [CustomerId], [ManagerId], [SupplierId], [TechnicalId], [AdminId], [SaleId], [ShipperId], [CreatedDate]) VALUES (1030, N'lethid', N'E10I0MEotAjGhoVCMELgfW+g6JedjRA+8VReVjefkjo=', N'nguyenvanj@gmail.com', NULL, 1, NULL, 5, NULL, NULL, NULL, 4, NULL, NULL, NULL, NULL, NULL, CAST(N'2023-07-16T10:42:05.283' AS DateTime))
INSERT [dbo].[Account] ([AccountId], [UserName], [Password], [Email], [Avatar], [Status], [Profile], [RoleId], [BloggerId], [ConsultantId], [CustomerId], [ManagerId], [SupplierId], [TechnicalId], [AdminId], [SaleId], [ShipperId], [CreatedDate]) VALUES (1031, N'ngodinhe', N'E10I0MEotAjGhoVCMELgfW+g6JedjRA+8VReVjefkjo=', N'nguyenvank@gmail.com', NULL, 1, NULL, 5, NULL, NULL, NULL, 5, NULL, NULL, NULL, NULL, NULL, CAST(N'2023-07-16T10:42:05.283' AS DateTime))
INSERT [dbo].[Account] ([AccountId], [UserName], [Password], [Email], [Avatar], [Status], [Profile], [RoleId], [BloggerId], [ConsultantId], [CustomerId], [ManagerId], [SupplierId], [TechnicalId], [AdminId], [SaleId], [ShipperId], [CreatedDate]) VALUES (1032, N'trinhthif', N'E10I0MEotAjGhoVCMELgfW+g6JedjRA+8VReVjefkjo=', N'nguyenvanl@gmail.com', NULL, 1, NULL, 5, NULL, NULL, NULL, 6, NULL, NULL, NULL, NULL, NULL, CAST(N'2023-07-16T10:42:05.283' AS DateTime))
INSERT [dbo].[Account] ([AccountId], [UserName], [Password], [Email], [Avatar], [Status], [Profile], [RoleId], [BloggerId], [ConsultantId], [CustomerId], [ManagerId], [SupplierId], [TechnicalId], [AdminId], [SaleId], [ShipperId], [CreatedDate]) VALUES (1033, N'hoangvang', N'E10I0MEotAjGhoVCMELgfW+g6JedjRA+8VReVjefkjo=', N'nguyenvanm@gmail.com', NULL, 1, NULL, 5, NULL, NULL, NULL, 7, NULL, NULL, NULL, NULL, NULL, CAST(N'2023-07-16T10:42:05.283' AS DateTime))
INSERT [dbo].[Account] ([AccountId], [UserName], [Password], [Email], [Avatar], [Status], [Profile], [RoleId], [BloggerId], [ConsultantId], [CustomerId], [ManagerId], [SupplierId], [TechnicalId], [AdminId], [SaleId], [ShipperId], [CreatedDate]) VALUES (1034, N'vuthih', N'E10I0MEotAjGhoVCMELgfW+g6JedjRA+8VReVjefkjo=', N'nguyenvann@gmail.com', NULL, 1, NULL, 5, NULL, NULL, NULL, 8, NULL, NULL, NULL, NULL, NULL, CAST(N'2023-07-16T10:42:05.283' AS DateTime))
INSERT [dbo].[Account] ([AccountId], [UserName], [Password], [Email], [Avatar], [Status], [Profile], [RoleId], [BloggerId], [ConsultantId], [CustomerId], [ManagerId], [SupplierId], [TechnicalId], [AdminId], [SaleId], [ShipperId], [CreatedDate]) VALUES (1035, N'dangvani', N'E10I0MEotAjGhoVCMELgfW+g6JedjRA+8VReVjefkjo=', N'nguyenvano@gmail.com', NULL, 1, NULL, 5, NULL, NULL, NULL, 9, NULL, NULL, NULL, NULL, NULL, CAST(N'2023-07-16T10:42:05.283' AS DateTime))
INSERT [dbo].[Account] ([AccountId], [UserName], [Password], [Email], [Avatar], [Status], [Profile], [RoleId], [BloggerId], [ConsultantId], [CustomerId], [ManagerId], [SupplierId], [TechnicalId], [AdminId], [SaleId], [ShipperId], [CreatedDate]) VALUES (1036, N'nguyenthanhlong', N'E10I0MEotAjGhoVCMELgfW+g6JedjRA+8VReVjefkjo=', N'nguyenvanp@gmail.com', NULL, 1, NULL, 7, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, CAST(N'2023-07-16T10:42:05.283' AS DateTime))
INSERT [dbo].[Account] ([AccountId], [UserName], [Password], [Email], [Avatar], [Status], [Profile], [RoleId], [BloggerId], [ConsultantId], [CustomerId], [ManagerId], [SupplierId], [TechnicalId], [AdminId], [SaleId], [ShipperId], [CreatedDate]) VALUES (1037, N'tranbinhdang', N'E10I0MEotAjGhoVCMELgfW+g6JedjRA+8VReVjefkjo=', N'nguyenvanq@gmail.com', NULL, 1, NULL, 7, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, CAST(N'2023-07-16T10:42:05.283' AS DateTime))
INSERT [dbo].[Account] ([AccountId], [UserName], [Password], [Email], [Avatar], [Status], [Profile], [RoleId], [BloggerId], [ConsultantId], [CustomerId], [ManagerId], [SupplierId], [TechnicalId], [AdminId], [SaleId], [ShipperId], [CreatedDate]) VALUES (1038, N'nguyentritue', N'E10I0MEotAjGhoVCMELgfW+g6JedjRA+8VReVjefkjo=', N'nguyenvanr@gmail.com', NULL, 1, NULL, 8, NULL, NULL, NULL, NULL, 5, NULL, NULL, NULL, NULL, CAST(N'2023-07-16T10:42:05.283' AS DateTime))
INSERT [dbo].[Account] ([AccountId], [UserName], [Password], [Email], [Avatar], [Status], [Profile], [RoleId], [BloggerId], [ConsultantId], [CustomerId], [ManagerId], [SupplierId], [TechnicalId], [AdminId], [SaleId], [ShipperId], [CreatedDate]) VALUES (1039, N'hoangcongquyet', N'E10I0MEotAjGhoVCMELgfW+g6JedjRA+8VReVjefkjo=', N'hoangcongquyet6@gmail.com', N'avatar-27dc6c15-2fff-48b9-8e4f-43c42e1bec27', 1, NULL, 2, 6, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, CAST(N'2023-08-11T07:43:12.933' AS DateTime))
SET IDENTITY_INSERT [dbo].[Account] OFF
GO
SET IDENTITY_INSERT [dbo].[Admin] ON 

INSERT [dbo].[Admin] ([AdminId], [FullName], [Phone], [Address], [DOB], [StatusID]) VALUES (1, N'Nguyễn Hoàng Long', N'0985551234', N'Hà Nội', CAST(N'1990-01-01T00:00:00.000' AS DateTime), 1)
INSERT [dbo].[Admin] ([AdminId], [FullName], [Phone], [Address], [DOB], [StatusID]) VALUES (2, N'Trần Đình Nam', N'0965355678', N'Hồ Chí Minh', CAST(N'1995-05-05T00:00:00.000' AS DateTime), 2)
INSERT [dbo].[Admin] ([AdminId], [FullName], [Phone], [Address], [DOB], [StatusID]) VALUES (3, N'Nguyễn Huy Hoàng', N'0963452468', N'Đà Nẵng', CAST(N'1985-12-31T00:00:00.000' AS DateTime), 1)
INSERT [dbo].[Admin] ([AdminId], [FullName], [Phone], [Address], [DOB], [StatusID]) VALUES (4, N'Kim Ngọc Sinh', N'0985463698', N'Hải Phòng', CAST(N'1995-06-15T00:00:00.000' AS DateTime), 1)
INSERT [dbo].[Admin] ([AdminId], [FullName], [Phone], [Address], [DOB], [StatusID]) VALUES (5, N'Trần Quốc Bảo', N'0946789876', N'Hà Nội', CAST(N'1992-09-30T00:00:00.000' AS DateTime), 2)
SET IDENTITY_INSERT [dbo].[Admin] OFF
GO
SET IDENTITY_INSERT [dbo].[Alert] ON 

INSERT [dbo].[Alert] ([AlertID], [CustomerID], [SaleID], [ShipperID], [Message], [StatusID], [OrderID], [CurrentDate], [OrderDetailID]) VALUES (1, 20, NULL, NULL, N'xau vai', NULL, NULL, CAST(N'2023-08-11T13:17:34.547' AS DateTime), NULL)
INSERT [dbo].[Alert] ([AlertID], [CustomerID], [SaleID], [ShipperID], [Message], [StatusID], [OrderID], [CurrentDate], [OrderDetailID]) VALUES (2, NULL, NULL, NULL, N'haha', NULL, NULL, CAST(N'2023-08-12T11:55:14.550' AS DateTime), NULL)
SET IDENTITY_INSERT [dbo].[Alert] OFF
GO
SET IDENTITY_INSERT [dbo].[Blog] ON 

INSERT [dbo].[Blog] ([Id], [BloggerId], [Title], [Description], [PublishDate], [Image], [StatusId], [CateId]) VALUES (8, 1, N'Loa này rất to', N'Loa này rất to', CAST(N'2023-08-11' AS Date), N'blog/blog-3c84d533-4c98-4bfe-b3a9-3236d4000c04.jpg', NULL, 1)
INSERT [dbo].[Blog] ([Id], [BloggerId], [Title], [Description], [PublishDate], [Image], [StatusId], [CateId]) VALUES (9, 6, N'Thiết Bị Âm Thanh Ánh Sáng Trong Sự Kiện', N'<p>Tất cả các thiết bị âm thanh, ánh sáng đều được sử dụng cho một mục đích nào đó của chương trình. Việc kết hợp các thiết bị này với nhau một cách hợp lý sẽ giúp chương trình trở nên hoàn hảo hơn.</p><h2><strong style="color: rgb(0, 0, 255);">I. Thiết bị âm thanh trong tổ chức sự kiện:</strong></h2><h3><strong>1) Hệ thống phát âm thanh:</strong></h3><p>Loa Full: là loa chính trong hệ thống âm thanh nó có đủ các dải tần từ thấp (low), trung (mid), cao (high). Loa này có tác dụng thể hiện âm thanh đến tai người nghe một cách trung thực nhất và cũng là loại thiết bị quan trọng nhất.</p><p>Loa/Speakers: Là thiết bị dễ nhận ra nhất của hệ thống âm thanh, ánh sáng. Loa có tác dụng truyền tải âm thanh đã qua xử lý đến tai người nghe. Loa chia làm 03 loại cơ bản (ngoài ra còn có nhiều loại khác theo mục đích khác nhau chúng ta sẽ đề cập đến trong các bài viết sau)</p><p>Loa Subwoofer (Subbass): loa siêu trầm , loa này được cắt toàn bộ tần số trung và cao chỉ để tần số thấp có tác dụng hỗ trợ loa Full. Loa trầm giúp tăng hiệu ứng âm thanh làm âm thanh có độ chắc, độ hoà quyện tốt hơn.</p><p>Loa Monitor: là loại loa đặc thù hướng về phía người đứng trên sân khấu. Loa này giúp họ nghe được nhạc của bài hát, giọng của bản thân… để điều chỉnh việc trình diễn của mình một cách phù hợp nhất.</p><h3><strong>2)&nbsp;Thiết bị đầu vào và đầu ra của âm thanh:</strong></h3><p>Tất cả các thiết bị đầu vào và đầu ra của âm thanh đều qua bàn Mixer để xử lý tín hiệu.</p><p>Các thiết bị đầu vào (input): bao gồm microphone, nhạc 3,5mm, laptop, DVD…</p><p>Các thiết bị đầu ra (output): ra amply, công suất power, tai nghe, loa….</p><h3><strong>3)&nbsp;Thiết bị hỗ trợ xử lý tín hiệu: </strong></h3><p>Bàn điều chỉnh/Mixer - trung tâm của hệ thống âm thanh.<strong>&nbsp;</strong>Bàn Mixer nói không quá thì chiểm 30% chất lượng của thiết bị. Bạn chỉ cần nhìn vào bàn Mixer có thể đánh giá được hệ thống âm thanh, ánh sáng đạt được level nào.&nbsp;Bàn Mixer có 02 loại chính:</p><p>Analog Mixer: bàn cơ công nghệ cũ. Tuy nhiên nếu biết khai thác vẫn vô cùng hiệu quả. Nó đặc biệt ở sự đơn giản và cơ động.</p><p>Digital Mixer: bàn số với tín hiệu Digital. Xử lý âm thanh bằng tín hiệu số cho chất lượng vượt trội. Tuy nhiên có đòi kĩ thuật viên phải có tay nghề nhất định mới khai thác được hết khả năng của mình. Hiện nay bàn số đang chiếm ưu thế so với bàn analog.</p><p>Trong âm thanh chuyên nghiệp bàn mixer luôn cần các thiết bị đi kèm để hỗ trợ việc xử lý tín hiệu. Tuy nhiên, từ khi có sự xuất hiện của bàn Mixer Digital việc cần thiết có các thiết bị này cũng trở nên ít hơn.</p><h5>Effect: dân dã hơn thường gọi là Echo hay Bộ vang. Bao gồm Revert và Delay cái này giúp âm thanh vang vọng hơn và tăng thêm hiệu ứng cho âm thanh.</h5><h5>Com: khống chế tín hiệu và tần số âm thanh ra</h5><h5>Crossover: chia tần số</h5><h5>Equazlizer: điều chỉnh dải tần âm thanh.</h5><h4><strong>5) Các thiết bị chuyên dụng khác:</strong></h4><p>Microphone: Đây cũng là một phần vô cùng quan trọng quyết định đến việc tốt hay dở của một hệ thống âm thanh, ánh sáng.</p><p>Microphone có tác dụng thu âm thanh của người nói đến bộ nhận và truyền qua dây dẫn đến Mixer để xử lý tín hiệu.</p><p>Microphone quan trọng nhất là 2 yếu tố, chất âm thể hiện và sóng của Microphone.</p><p>Hiện nay đa phần sử dụng Microphone không dây (Wireless Microphone) trong tất cả các chương trình.</p><p>Tủ điện và hệ thống điện: đây cũng là yếu tố mà nhiều người thường xem nhẹ. </p><p>Hệ thống điện cần phải thật an toàn và đảm bảo đủ công suất mới giúp thiết bị âm thanh có thể phát huy được hết công suất.</p>', CAST(N'2023-08-11' AS Date), N'blog/blog-e31b9fe7-a647-4126-a029-e020e06dbce5.jpg', 1, 1)
INSERT [dbo].[Blog] ([Id], [BloggerId], [Title], [Description], [PublishDate], [Image], [StatusId], [CateId]) VALUES (10, 1, N'Cung Cấp Cho Thuê Bàn Ghế Sự Kiện', N'<p>Trong một <strong style="color: rgb(0, 0, 255);">sự kiện</strong><span style="color: rgb(0, 0, 255);"> </span>một điều không thể bỏ qua đó là chuẩn bị&nbsp;<em>bàn ghế</em>&nbsp;cho khách mời tham dự. Kiểu dáng&nbsp;bàn ghế&nbsp;đơn giản hay sang trọng một phần dựa vào tính chất của sự kiện và một phần do yêu cầu của quý công ty. Theo xu hướng hiện nay thường dùng 2 loại chính&nbsp;bàn ghế&nbsp;thường và bàn ghế&nbsp;quây với các tông tím trắng, đỏ trắng…</p><p>Với sự đầu tư 100%&nbsp;<em>bàn ghế</em>&nbsp;mới phục vụ sự kiện, cùng với kinh nghiệm lâu năm trong lĩnh vực <strong style="color: rgb(0, 0, 255);">tổ chức sự kiện</strong> trọn gói và <strong style="color: rgb(0, 0, 255);">tổ chức sự kiện</strong>, đã tạo được uy tín với khách hàng quanh khu vực Hoà Lạc. Hiện nay chúng tôi đang <strong style="color: rgb(0, 0, 255);"><em>cung cấp, cho thuê&nbsp;bàn ghế&nbsp;</em></strong>sự&nbsp;kiện&nbsp;với 4 tông màu chính cùng với rất nhiều kiểu dáng khác nhau đáp ứng mọi nhu cầu của quý khác.</p><p><strong style="color: rgb(0, 0, 255);">Cung cấp,&nbsp;cho thuê bàn ghế</strong><strong>&nbsp;</strong>phục vụ sự kiện,&nbsp;<strong style="color: rgb(0, 0, 255);">cung cấp,&nbsp;cho thuê bàn ghế&nbsp;</strong>hội nghị hội thảo;&nbsp;<strong style="color: rgb(0, 0, 255);">cung cấp,&nbsp;cho thuê bàn ghế</strong>&nbsp;vip,&nbsp;<em>bàn ghế</em>&nbsp;thường;&nbsp;<strong style="color: rgb(0, 0, 255);"><em>cung cấp,&nbsp;cho thuê bàn ghế&nbsp;</em></strong>cho lễ khai trương khánh thành;&nbsp;<strong style="color: rgb(0, 0, 255);">cung cấp,&nbsp;cho thuê bàn ghế</strong>&nbsp;lễ khởi công, lễ động thổ.&nbsp;<strong style="color: rgb(0, 0, 255);">Cung cấp, cho thuê bàn ghế&nbsp;</strong>có khăn phủ nơ; <strong style="color: rgb(0, 0, 255);">cung cấp,&nbsp;cho thuê bàn ghế</strong>&nbsp;phục vụ khai mạc hội chợ&nbsp;triển lãm;&nbsp;<strong style="color: rgb(0, 0, 255);">cung cấp, cho thuê</strong><span style="color: rgb(0, 0, 255);"> </span><strong style="color: rgb(0, 0, 255);">bàn ghế</strong> số lượng lớn, chất lượng cao; <strong style="color: rgb(0, 0, 255);">cung cấp,cho thuê bàn ghế</strong>&nbsp;chuyên nghiệp.</p><p>Ngoài<em>&nbsp;</em><strong style="color: rgb(0, 0, 255);">cung cấp, cho thuê bàn ghế</strong>&nbsp;phục vụ sự kiện <strong>VIEWPOINT SYSTEM </strong>còn cung cấp tất cả thiết bị<strong style="color: rgb(0, 0, 255);"> tổ chức sự kiện</strong> như: âm thanh ánh sáng, bạt dù cổng hơi, nhà bạt nhà không gian, dụng cụ nghi thức…</p>', CAST(N'2023-08-11' AS Date), N'blog/blog-7ea5327b-8691-4dae-8088-1133642aee57.jpg', NULL, 3)
INSERT [dbo].[Blog] ([Id], [BloggerId], [Title], [Description], [PublishDate], [Image], [StatusId], [CateId]) VALUES (11, 2, N'Cho Thuê Cổng Hơi - Rối Hơi', N'<p><strong style="color: rgb(51, 153, 204);">VIEWPOINT SYSTEM </strong>chuyên <strong style="color: rgb(0, 0, 255);">cung cấp, cho thuê thiết bị sự kiện</strong> như:&nbsp;<strong style="color: rgb(0, 0, 255);"><em>cổng hơi</em></strong>,&nbsp;<strong style="color: rgb(0, 0, 255);"><em>cho thuê cổng chào bằng hơi</em></strong>, <strong style="color: rgb(0, 0, 255);">cung cấp cổng chào bằng hơi</strong><span style="color: rgb(0, 0, 255);"> </span>theo thiết kế có sẵn hoặc theo đơn đặt hàng khách hàng. Chuyên<strong style="color: rgb(0, 0, 255);"> cung cấp, cho thuê</strong><span style="color: rgb(0, 0, 255);"> </span>các mặt hàng bằng hơi như&nbsp;<strong style="color: rgb(0, 0, 255);"><em>cổng – rối hơi – nhà hơi</em></strong>, các chai lọ hơi in hình logo công ty.</p><p>Nhận thi công lắp đặt tư vấn hướng dẫn thi công lắp đặt các loại <strong style="color: rgb(0, 0, 255);">cổng hơi</strong><strong> </strong><strong style="color: rgb(0, 0, 255);">rối hơi</strong> Nhà hơi Nhà phao<strong style="color: rgb(0, 0, 255);"> Rối hơi</strong><strong> </strong>Hoạt hình bơm hơi Thú hơi Lâu đài hơi Phao thể thao trên nước Khinh khí cầu Bóng hơi trên nước Lều hơi Thuyền bơm hơi Bể phao bơm hơi Mô hình bơm hơi Bóng bay khổng lồ Trò chơi bơm hơi khổng lồ Cầu trượt bơm hơi Núi bơm hơi Trụ đồng đèn bơm hơi Sản phẩm giáng sinh bơm hơi.</p>', CAST(N'2023-08-11' AS Date), N'blog/blog-fcf9b56b-91bf-442d-be3e-772de3d65850.jpg', NULL, 2)
INSERT [dbo].[Blog] ([Id], [BloggerId], [Title], [Description], [PublishDate], [Image], [StatusId], [CateId]) VALUES (12, 2, N'111111111', N'<p>111111111111111111111</p>', CAST(N'2023-08-11' AS Date), N'blog/blog-e0cb551f-b862-4b14-a972-2628757f49f1.jpg', NULL, 4)
SET IDENTITY_INSERT [dbo].[Blog] OFF
GO
SET IDENTITY_INSERT [dbo].[BlogCategory] ON 

INSERT [dbo].[BlogCategory] ([BCategory], [BName]) VALUES (1, N'Các trang thiết bị âm thanh và ánh sáng cho sự kiện')
INSERT [dbo].[BlogCategory] ([BCategory], [BName]) VALUES (2, N'Các chiến lược trang trí sự kiện')
INSERT [dbo].[BlogCategory] ([BCategory], [BName]) VALUES (3, N'Các xu hướng mới nhất trong ngành cho thuê thiết bị sự kiện')
INSERT [dbo].[BlogCategory] ([BCategory], [BName]) VALUES (4, N'Các mẹo và kinh nghiệm cho việc tổ chức sự kiện')
INSERT [dbo].[BlogCategory] ([BCategory], [BName]) VALUES (5, N'Các ví dụ về các sự kiện thành công đã từng được tổ chức')
SET IDENTITY_INSERT [dbo].[BlogCategory] OFF
GO
SET IDENTITY_INSERT [dbo].[Blogger] ON 

INSERT [dbo].[Blogger] ([BloggerId], [FullName], [Phone], [Address], [DOB], [StatusId]) VALUES (1, N'Nguyễn Hoài Nam', N'0985551234', N'Hà Nội', CAST(N'1985-06-15T00:00:00.000' AS DateTime), 1)
INSERT [dbo].[Blogger] ([BloggerId], [FullName], [Phone], [Address], [DOB], [StatusId]) VALUES (2, N'Phí Thị Quỳnh', N'0955555678', N'Lai Châu', CAST(N'1990-01-01T00:00:00.000' AS DateTime), 2)
INSERT [dbo].[Blogger] ([BloggerId], [FullName], [Phone], [Address], [DOB], [StatusId]) VALUES (3, N'Trần Nam Anh', N'0935559876', N'Hải Phòng', CAST(N'1978-12-31T00:00:00.000' AS DateTime), 1)
INSERT [dbo].[Blogger] ([BloggerId], [FullName], [Phone], [Address], [DOB], [StatusId]) VALUES (4, N'Trần Tiến Đạt', N'0965555555', N'Đà Nẵng', CAST(N'1987-05-20T00:00:00.000' AS DateTime), 2)
INSERT [dbo].[Blogger] ([BloggerId], [FullName], [Phone], [Address], [DOB], [StatusId]) VALUES (5, N'Nguyễn Quang Linh', N'0925554321', N'Nghệ An', CAST(N'1995-11-11T00:00:00.000' AS DateTime), 1)
INSERT [dbo].[Blogger] ([BloggerId], [FullName], [Phone], [Address], [DOB], [StatusId]) VALUES (6, N'Hoàng Công Quyết', N'0976507864', N'Yên Bái', CAST(N'2001-08-19T00:00:00.000' AS DateTime), 1)
SET IDENTITY_INSERT [dbo].[Blogger] OFF
GO
SET IDENTITY_INSERT [dbo].[Category] ON 

INSERT [dbo].[Category] ([CategoryId], [CategoryName], [Description]) VALUES (1, N'Thiết Bị Âm Thanh – Ánh Sáng', N'Thiết bị kết hợp âm thanh và ánh sáng để tạo ra hiệu ứng âm nhạc và ánh sáng đặc biệt.')
INSERT [dbo].[Category] ([CategoryId], [CategoryName], [Description]) VALUES (2, N'Màn Hình LED', N'Thiết bị hiển thị hình ảnh và video sử dụng công nghệ LED.')
INSERT [dbo].[Category] ([CategoryId], [CategoryName], [Description]) VALUES (3, N'Hệ Thống Sân Khấu', N'Thiết bị âm thanh, ánh sáng và hiệu ứng trên sân khấu.')
INSERT [dbo].[Category] ([CategoryId], [CategoryName], [Description]) VALUES (4, N'Trang Thiết Bị Khu Vực Tiệc', N'Thiết bị âm thanh, ánh sáng và trang trí dùng cho khu vực tiệc.')
INSERT [dbo].[Category] ([CategoryId], [CategoryName], [Description]) VALUES (5, N'Thiết Bị Nhà Bạt Không Gian', N'Thiết bị âm thanh, ánh sáng và trang trí dùng cho không gian nhà bạt. ')
SET IDENTITY_INSERT [dbo].[Category] OFF
GO
SET IDENTITY_INSERT [dbo].[Consultant] ON 

INSERT [dbo].[Consultant] ([ConsultantId], [FullName], [Phone], [Address], [DOB], [StatusId]) VALUES (1, N'Nguyễn Văn An', N'0987654321', N'Hà Nội', CAST(N'1990-01-01T00:00:00.000' AS DateTime), 1)
INSERT [dbo].[Consultant] ([ConsultantId], [FullName], [Phone], [Address], [DOB], [StatusId]) VALUES (2, N'Trần Thị Bích', N'0954645456', N'Hồ Chí Minh', CAST(N'1985-02-15T00:00:00.000' AS DateTime), 2)
INSERT [dbo].[Consultant] ([ConsultantId], [FullName], [Phone], [Address], [DOB], [StatusId]) VALUES (3, N'Lê Quang Cao', N'0912345678', N'Đà Nẵng', CAST(N'1995-07-30T00:00:00.000' AS DateTime), 1)
INSERT [dbo].[Consultant] ([ConsultantId], [FullName], [Phone], [Address], [DOB], [StatusId]) VALUES (4, N'Phạm Thanh Dung', N'0994834848', N'Hà Tĩnh', CAST(N'1993-04-21T00:00:00.000' AS DateTime), 2)
INSERT [dbo].[Consultant] ([ConsultantId], [FullName], [Phone], [Address], [DOB], [StatusId]) VALUES (5, N'Võ Hoàng Yến', N'0994383483', N'Hải Phòng', CAST(N'1988-11-03T00:00:00.000' AS DateTime), 1)
SET IDENTITY_INSERT [dbo].[Consultant] OFF
GO
SET IDENTITY_INSERT [dbo].[Customers] ON 

INSERT [dbo].[Customers] ([CustomerId], [FullName], [Phone], [Address], [Balance], [DOB], [StatusId], [IsValid], [Voucher]) VALUES (16, N'Nguyễn Văn Anh', N'0987654321', N'Hà Nội', 5000000, CAST(N'1990-01-01T00:00:00.000' AS DateTime), 1, NULL, NULL)
INSERT [dbo].[Customers] ([CustomerId], [FullName], [Phone], [Address], [Balance], [DOB], [StatusId], [IsValid], [Voucher]) VALUES (18, N'Lê Quang Cường', N'0912345678', N'Đà Nẵng', 7500000, CAST(N'1995-07-30T00:00:00.000' AS DateTime), 3, NULL, NULL)
INSERT [dbo].[Customers] ([CustomerId], [FullName], [Phone], [Address], [Balance], [DOB], [StatusId], [IsValid], [Voucher]) VALUES (19, N'Phạm Thanh Hùng', N'099897635', N'Nghệ An', 2000000, CAST(N'1993-04-21T00:00:00.000' AS DateTime), 1, NULL, NULL)
INSERT [dbo].[Customers] ([CustomerId], [FullName], [Phone], [Address], [Balance], [DOB], [StatusId], [IsValid], [Voucher]) VALUES (20, N'Nguyễn Đăng Dũng', N'0349175696', N'Hải Dương', 100000000, CAST(N'2023-07-02T15:09:32.367' AS DateTime), 1, 1, NULL)
INSERT [dbo].[Customers] ([CustomerId], [FullName], [Phone], [Address], [Balance], [DOB], [StatusId], [IsValid], [Voucher]) VALUES (1002, N'Hồ Hởi', N'0394834734', N'Hà Nội', 138438750, CAST(N'2023-07-02T15:38:50.340' AS DateTime), 1, NULL, NULL)
SET IDENTITY_INSERT [dbo].[Customers] OFF
GO
SET IDENTITY_INSERT [dbo].[HStatus] ON 

INSERT [dbo].[HStatus] ([StatusId], [StatusNum], [StatusValue]) VALUES (1, 1, N'Đang hoạt động')
INSERT [dbo].[HStatus] ([StatusId], [StatusNum], [StatusValue]) VALUES (2, 2, N'Ngừng hoạt động')
INSERT [dbo].[HStatus] ([StatusId], [StatusNum], [StatusValue]) VALUES (3, 3, N'Đã xác nhận')
SET IDENTITY_INSERT [dbo].[HStatus] OFF
GO
SET IDENTITY_INSERT [dbo].[IdentityCard] ON 

INSERT [dbo].[IdentityCard] ([CardId], [CardNumber], [FullName], [DateOfBirth], [Gender], [Nationality], [PlaceOfOrigin], [IdentityCardImage], [BackIdentityCard], [CustomerId]) VALUES (1, N'12345         ', N'Dung', CAST(N'2023-03-02' AS Date), 1, N'VietNam', N'VietNam', N'12                                                                                                                                                                                                                                                        ', N'12                                                                                                                                                                                                                                                        ', 16)
INSERT [dbo].[IdentityCard] ([CardId], [CardNumber], [FullName], [DateOfBirth], [Gender], [Nationality], [PlaceOfOrigin], [IdentityCardImage], [BackIdentityCard], [CustomerId]) VALUES (2, N'43546         ', N'Nam', CAST(N'2023-05-07' AS Date), 0, N'VietNam', N'VietName', N'345                                                                                                                                                                                                                                                       ', N'34532                                                                                                                                                                                                                                                     ', 20)
INSERT [dbo].[IdentityCard] ([CardId], [CardNumber], [FullName], [DateOfBirth], [Gender], [Nationality], [PlaceOfOrigin], [IdentityCardImage], [BackIdentityCard], [CustomerId]) VALUES (3, N'015201004401  ', N'HOÀNG CÔNG QUYẾT', CAST(N'0001-01-01' AS Date), 1, N'Việt Nam', N'Đại Lịch, Văn Chân, Yên Bái', N'RecognizeIdentityCard/recognize-07d6faff-446f-49eb-a4df-2b6eab04d2fd.jpg                                                                                                                                                                                  ', N'RecognizeIdentityCard/recognize-d5cd27b9-555b-4321-a89f-5f2785253f66.jpg                                                                                                                                                                                  ', 20)
INSERT [dbo].[IdentityCard] ([CardId], [CardNumber], [FullName], [DateOfBirth], [Gender], [Nationality], [PlaceOfOrigin], [IdentityCardImage], [BackIdentityCard], [CustomerId]) VALUES (4, N'015201004401  ', N'HOÀNG CÔNG QUYẾT', CAST(N'0001-01-01' AS Date), 1, N'Việt Nam', N'Đại Lịch, Văn Chân, Yên Bái', N'RecognizeIdentityCard/recognize-a128ad64-01f0-43b0-be7a-c6e89a982326.jpg                                                                                                                                                                                  ', N'RecognizeIdentityCard/recognize-312f1339-0480-474c-b10f-8d5e37d59f67.jpg                                                                                                                                                                                  ', 20)
SET IDENTITY_INSERT [dbo].[IdentityCard] OFF
GO
INSERT [dbo].[Manager] ([ManagerId], [FullName], [Phone], [Address], [DOB], [StatusId]) VALUES (1, N'Nguyễn Văn A', N'0987654321', N'Hà Nội', CAST(N'1990-01-01T00:00:00.000' AS DateTime), 1)
INSERT [dbo].[Manager] ([ManagerId], [FullName], [Phone], [Address], [DOB], [StatusId]) VALUES (2, N'Phạm Thị B', N'0123456789', N'Thành phố Hồ Chí Minh', CAST(N'1985-05-05T00:00:00.000' AS DateTime), 1)
INSERT [dbo].[Manager] ([ManagerId], [FullName], [Phone], [Address], [DOB], [StatusId]) VALUES (3, N'Trần Văn C', N'0912345678', N'Đà Nẵng', CAST(N'1995-10-10T00:00:00.000' AS DateTime), 1)
INSERT [dbo].[Manager] ([ManagerId], [FullName], [Phone], [Address], [DOB], [StatusId]) VALUES (4, N'Lê Thị D', N'0901234567', N'Hải Phòng', CAST(N'1980-03-15T00:00:00.000' AS DateTime), 2)
INSERT [dbo].[Manager] ([ManagerId], [FullName], [Phone], [Address], [DOB], [StatusId]) VALUES (5, N'Ngô Đình E', N'0987123456', N'Bình Dương', CAST(N'1978-12-25T00:00:00.000' AS DateTime), 2)
INSERT [dbo].[Manager] ([ManagerId], [FullName], [Phone], [Address], [DOB], [StatusId]) VALUES (6, N'Trịnh Thị F', N'0918765432', N'Thanh Hóa', CAST(N'1987-07-07T00:00:00.000' AS DateTime), 1)
INSERT [dbo].[Manager] ([ManagerId], [FullName], [Phone], [Address], [DOB], [StatusId]) VALUES (7, N'Hoàng Văn G', N'0987654321', N'Hà Tĩnh', CAST(N'1992-02-02T00:00:00.000' AS DateTime), 1)
INSERT [dbo].[Manager] ([ManagerId], [FullName], [Phone], [Address], [DOB], [StatusId]) VALUES (8, N'Vũ Thị H', N'0912345678', N'Thừa Thiên Huế', CAST(N'1998-08-08T00:00:00.000' AS DateTime), 2)
INSERT [dbo].[Manager] ([ManagerId], [FullName], [Phone], [Address], [DOB], [StatusId]) VALUES (9, N'Đặng Văn I', N'0901234567', N'Quảng Ninh', CAST(N'1983-04-01T00:00:00.000' AS DateTime), 1)
INSERT [dbo].[Manager] ([ManagerId], [FullName], [Phone], [Address], [DOB], [StatusId]) VALUES (10, N'Âu Văn K', N'0987123456', N'Hưng Yên', CAST(N'1991-11-11T00:00:00.000' AS DateTime), 2)
GO
SET IDENTITY_INSERT [dbo].[Order] ON 

INSERT [dbo].[Order] ([OrderId], [SaleId], [CustomerId], [OrderDate], [ShippedDate], [ShipId], [Price], [StatusId], [ShipCost], [ManagerConfirm], [SaleConfirm], [ShipConfirm], [CustomerConfirm], [ShipSuccessConfirm], [ManagerViewed], [IsValid], [ManagerCancelled], [SalerCancelled], [ShipperCancelled], [IdentityCardImage], [CurrentImage], [CustomerCancelled]) VALUES (18, 1, 18, CAST(N'2023-07-26T14:11:43.413' AS DateTime), CAST(N'2023-07-26T14:11:43.413' AS DateTime), 4, 500000, 1, 30000.0000, CAST(N'2023-07-26T14:11:43.413' AS DateTime), CAST(N'2023-07-26T14:11:43.413' AS DateTime), CAST(N'2023-07-26T14:11:43.413' AS DateTime), CAST(N'2023-07-26T14:11:43.413' AS DateTime), CAST(N'2023-07-26T14:11:43.413' AS DateTime), CAST(N'2023-07-26T14:11:43.413' AS DateTime), 1, CAST(N'2023-07-26T14:11:43.413' AS DateTime), CAST(N'2023-07-26T14:11:43.413' AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[Order] ([OrderId], [SaleId], [CustomerId], [OrderDate], [ShippedDate], [ShipId], [Price], [StatusId], [ShipCost], [ManagerConfirm], [SaleConfirm], [ShipConfirm], [CustomerConfirm], [ShipSuccessConfirm], [ManagerViewed], [IsValid], [ManagerCancelled], [SalerCancelled], [ShipperCancelled], [IdentityCardImage], [CurrentImage], [CustomerCancelled]) VALUES (19, 2, 19, CAST(N'2023-06-04T00:00:00.000' AS DateTime), CAST(N'2023-06-08T00:00:00.000' AS DateTime), 3, 200000, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Order] ([OrderId], [SaleId], [CustomerId], [OrderDate], [ShippedDate], [ShipId], [Price], [StatusId], [ShipCost], [ManagerConfirm], [SaleConfirm], [ShipConfirm], [CustomerConfirm], [ShipSuccessConfirm], [ManagerViewed], [IsValid], [ManagerCancelled], [SalerCancelled], [ShipperCancelled], [IdentityCardImage], [CurrentImage], [CustomerCancelled]) VALUES (1012, 2, 16, CAST(N'2023-01-23T00:00:00.000' AS DateTime), CAST(N'2023-01-29T00:00:00.000' AS DateTime), 2, 190000, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Order] ([OrderId], [SaleId], [CustomerId], [OrderDate], [ShippedDate], [ShipId], [Price], [StatusId], [ShipCost], [ManagerConfirm], [SaleConfirm], [ShipConfirm], [CustomerConfirm], [ShipSuccessConfirm], [ManagerViewed], [IsValid], [ManagerCancelled], [SalerCancelled], [ShipperCancelled], [IdentityCardImage], [CurrentImage], [CustomerCancelled]) VALUES (1013, 3, 16, CAST(N'2023-01-22T00:00:00.000' AS DateTime), CAST(N'2023-01-30T00:00:00.000' AS DateTime), 3, 500000, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Order] ([OrderId], [SaleId], [CustomerId], [OrderDate], [ShippedDate], [ShipId], [Price], [StatusId], [ShipCost], [ManagerConfirm], [SaleConfirm], [ShipConfirm], [CustomerConfirm], [ShipSuccessConfirm], [ManagerViewed], [IsValid], [ManagerCancelled], [SalerCancelled], [ShipperCancelled], [IdentityCardImage], [CurrentImage], [CustomerCancelled]) VALUES (1015, 5, 16, CAST(N'2023-01-12T00:00:00.000' AS DateTime), CAST(N'2023-01-28T00:00:00.000' AS DateTime), 2, 433000, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Order] ([OrderId], [SaleId], [CustomerId], [OrderDate], [ShippedDate], [ShipId], [Price], [StatusId], [ShipCost], [ManagerConfirm], [SaleConfirm], [ShipConfirm], [CustomerConfirm], [ShipSuccessConfirm], [ManagerViewed], [IsValid], [ManagerCancelled], [SalerCancelled], [ShipperCancelled], [IdentityCardImage], [CurrentImage], [CustomerCancelled]) VALUES (1021, 1, 18, CAST(N'2023-02-21T00:00:00.000' AS DateTime), CAST(N'2023-02-22T00:00:00.000' AS DateTime), 2, 130000, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Order] ([OrderId], [SaleId], [CustomerId], [OrderDate], [ShippedDate], [ShipId], [Price], [StatusId], [ShipCost], [ManagerConfirm], [SaleConfirm], [ShipConfirm], [CustomerConfirm], [ShipSuccessConfirm], [ManagerViewed], [IsValid], [ManagerCancelled], [SalerCancelled], [ShipperCancelled], [IdentityCardImage], [CurrentImage], [CustomerCancelled]) VALUES (1022, 2, 19, CAST(N'2023-02-20T00:00:00.000' AS DateTime), CAST(N'2023-02-21T00:00:00.000' AS DateTime), 3, 200000, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Order] ([OrderId], [SaleId], [CustomerId], [OrderDate], [ShippedDate], [ShipId], [Price], [StatusId], [ShipCost], [ManagerConfirm], [SaleConfirm], [ShipConfirm], [CustomerConfirm], [ShipSuccessConfirm], [ManagerViewed], [IsValid], [ManagerCancelled], [SalerCancelled], [ShipperCancelled], [IdentityCardImage], [CurrentImage], [CustomerCancelled]) VALUES (1024, 4, 20, CAST(N'2023-02-18T00:00:00.000' AS DateTime), CAST(N'2023-02-23T00:00:00.000' AS DateTime), 2, 120000, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Order] ([OrderId], [SaleId], [CustomerId], [OrderDate], [ShippedDate], [ShipId], [Price], [StatusId], [ShipCost], [ManagerConfirm], [SaleConfirm], [ShipConfirm], [CustomerConfirm], [ShipSuccessConfirm], [ManagerViewed], [IsValid], [ManagerCancelled], [SalerCancelled], [ShipperCancelled], [IdentityCardImage], [CurrentImage], [CustomerCancelled]) VALUES (1025, 5, 20, CAST(N'2023-02-17T00:00:00.000' AS DateTime), CAST(N'2023-02-28T00:00:00.000' AS DateTime), 3, 130000, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Order] ([OrderId], [SaleId], [CustomerId], [OrderDate], [ShippedDate], [ShipId], [Price], [StatusId], [ShipCost], [ManagerConfirm], [SaleConfirm], [ShipConfirm], [CustomerConfirm], [ShipSuccessConfirm], [ManagerViewed], [IsValid], [ManagerCancelled], [SalerCancelled], [ShipperCancelled], [IdentityCardImage], [CurrentImage], [CustomerCancelled]) VALUES (1026, 1, 18, CAST(N'2023-02-16T00:00:00.000' AS DateTime), CAST(N'2023-02-27T00:00:00.000' AS DateTime), 1, 110000, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Order] ([OrderId], [SaleId], [CustomerId], [OrderDate], [ShippedDate], [ShipId], [Price], [StatusId], [ShipCost], [ManagerConfirm], [SaleConfirm], [ShipConfirm], [CustomerConfirm], [ShipSuccessConfirm], [ManagerViewed], [IsValid], [ManagerCancelled], [SalerCancelled], [ShipperCancelled], [IdentityCardImage], [CurrentImage], [CustomerCancelled]) VALUES (1027, 2, 19, CAST(N'2023-02-15T00:00:00.000' AS DateTime), CAST(N'2023-02-26T00:00:00.000' AS DateTime), 2, 140000, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Order] ([OrderId], [SaleId], [CustomerId], [OrderDate], [ShippedDate], [ShipId], [Price], [StatusId], [ShipCost], [ManagerConfirm], [SaleConfirm], [ShipConfirm], [CustomerConfirm], [ShipSuccessConfirm], [ManagerViewed], [IsValid], [ManagerCancelled], [SalerCancelled], [ShipperCancelled], [IdentityCardImage], [CurrentImage], [CustomerCancelled]) VALUES (1029, 4, 20, CAST(N'2023-02-13T00:00:00.000' AS DateTime), CAST(N'2023-02-24T00:00:00.000' AS DateTime), 1, 340000, 7, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, CAST(N'2023-08-11T13:17:34.547' AS DateTime))
INSERT [dbo].[Order] ([OrderId], [SaleId], [CustomerId], [OrderDate], [ShippedDate], [ShipId], [Price], [StatusId], [ShipCost], [ManagerConfirm], [SaleConfirm], [ShipConfirm], [CustomerConfirm], [ShipSuccessConfirm], [ManagerViewed], [IsValid], [ManagerCancelled], [SalerCancelled], [ShipperCancelled], [IdentityCardImage], [CurrentImage], [CustomerCancelled]) VALUES (1030, 5, 20, CAST(N'2023-02-12T00:00:00.000' AS DateTime), CAST(N'2023-02-23T00:00:00.000' AS DateTime), 2, 560000, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Order] ([OrderId], [SaleId], [CustomerId], [OrderDate], [ShippedDate], [ShipId], [Price], [StatusId], [ShipCost], [ManagerConfirm], [SaleConfirm], [ShipConfirm], [CustomerConfirm], [ShipSuccessConfirm], [ManagerViewed], [IsValid], [ManagerCancelled], [SalerCancelled], [ShipperCancelled], [IdentityCardImage], [CurrentImage], [CustomerCancelled]) VALUES (1031, 2, 18, CAST(N'2023-03-11T00:00:00.000' AS DateTime), CAST(N'2023-03-22T00:00:00.000' AS DateTime), 3, 730000, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Order] ([OrderId], [SaleId], [CustomerId], [OrderDate], [ShippedDate], [ShipId], [Price], [StatusId], [ShipCost], [ManagerConfirm], [SaleConfirm], [ShipConfirm], [CustomerConfirm], [ShipSuccessConfirm], [ManagerViewed], [IsValid], [ManagerCancelled], [SalerCancelled], [ShipperCancelled], [IdentityCardImage], [CurrentImage], [CustomerCancelled]) VALUES (1032, 2, 19, CAST(N'2023-03-10T00:00:00.000' AS DateTime), CAST(N'2023-03-21T00:00:00.000' AS DateTime), 1, 100000, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Order] ([OrderId], [SaleId], [CustomerId], [OrderDate], [ShippedDate], [ShipId], [Price], [StatusId], [ShipCost], [ManagerConfirm], [SaleConfirm], [ShipConfirm], [CustomerConfirm], [ShipSuccessConfirm], [ManagerViewed], [IsValid], [ManagerCancelled], [SalerCancelled], [ShipperCancelled], [IdentityCardImage], [CurrentImage], [CustomerCancelled]) VALUES (1034, 4, 20, CAST(N'2023-03-08T00:00:00.000' AS DateTime), CAST(N'2023-03-19T00:00:00.000' AS DateTime), 3, 350000, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Order] ([OrderId], [SaleId], [CustomerId], [OrderDate], [ShippedDate], [ShipId], [Price], [StatusId], [ShipCost], [ManagerConfirm], [SaleConfirm], [ShipConfirm], [CustomerConfirm], [ShipSuccessConfirm], [ManagerViewed], [IsValid], [ManagerCancelled], [SalerCancelled], [ShipperCancelled], [IdentityCardImage], [CurrentImage], [CustomerCancelled]) VALUES (1035, 5, 20, CAST(N'2023-03-07T00:00:00.000' AS DateTime), CAST(N'2023-03-18T00:00:00.000' AS DateTime), 1, 720000, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Order] ([OrderId], [SaleId], [CustomerId], [OrderDate], [ShippedDate], [ShipId], [Price], [StatusId], [ShipCost], [ManagerConfirm], [SaleConfirm], [ShipConfirm], [CustomerConfirm], [ShipSuccessConfirm], [ManagerViewed], [IsValid], [ManagerCancelled], [SalerCancelled], [ShipperCancelled], [IdentityCardImage], [CurrentImage], [CustomerCancelled]) VALUES (1036, 1, 18, CAST(N'2023-03-06T00:00:00.000' AS DateTime), CAST(N'2023-03-17T00:00:00.000' AS DateTime), 2, 490000, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Order] ([OrderId], [SaleId], [CustomerId], [OrderDate], [ShippedDate], [ShipId], [Price], [StatusId], [ShipCost], [ManagerConfirm], [SaleConfirm], [ShipConfirm], [CustomerConfirm], [ShipSuccessConfirm], [ManagerViewed], [IsValid], [ManagerCancelled], [SalerCancelled], [ShipperCancelled], [IdentityCardImage], [CurrentImage], [CustomerCancelled]) VALUES (1037, 2, 19, CAST(N'2023-03-05T00:00:00.000' AS DateTime), CAST(N'2023-03-16T00:00:00.000' AS DateTime), 3, 872000, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Order] ([OrderId], [SaleId], [CustomerId], [OrderDate], [ShippedDate], [ShipId], [Price], [StatusId], [ShipCost], [ManagerConfirm], [SaleConfirm], [ShipConfirm], [CustomerConfirm], [ShipSuccessConfirm], [ManagerViewed], [IsValid], [ManagerCancelled], [SalerCancelled], [ShipperCancelled], [IdentityCardImage], [CurrentImage], [CustomerCancelled]) VALUES (1039, 4, 20, CAST(N'2023-03-03T00:00:00.000' AS DateTime), CAST(N'2023-03-14T00:00:00.000' AS DateTime), 2, 130000, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Order] ([OrderId], [SaleId], [CustomerId], [OrderDate], [ShippedDate], [ShipId], [Price], [StatusId], [ShipCost], [ManagerConfirm], [SaleConfirm], [ShipConfirm], [CustomerConfirm], [ShipSuccessConfirm], [ManagerViewed], [IsValid], [ManagerCancelled], [SalerCancelled], [ShipperCancelled], [IdentityCardImage], [CurrentImage], [CustomerCancelled]) VALUES (1040, 5, 20, CAST(N'2023-03-02T00:00:00.000' AS DateTime), CAST(N'2023-03-13T00:00:00.000' AS DateTime), 3, 400000, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Order] ([OrderId], [SaleId], [CustomerId], [OrderDate], [ShippedDate], [ShipId], [Price], [StatusId], [ShipCost], [ManagerConfirm], [SaleConfirm], [ShipConfirm], [CustomerConfirm], [ShipSuccessConfirm], [ManagerViewed], [IsValid], [ManagerCancelled], [SalerCancelled], [ShipperCancelled], [IdentityCardImage], [CurrentImage], [CustomerCancelled]) VALUES (1041, 1, 19, CAST(N'2023-04-01T00:00:00.000' AS DateTime), CAST(N'2023-04-11T00:00:00.000' AS DateTime), 1, 200000, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Order] ([OrderId], [SaleId], [CustomerId], [OrderDate], [ShippedDate], [ShipId], [Price], [StatusId], [ShipCost], [ManagerConfirm], [SaleConfirm], [ShipConfirm], [CustomerConfirm], [ShipSuccessConfirm], [ManagerViewed], [IsValid], [ManagerCancelled], [SalerCancelled], [ShipperCancelled], [IdentityCardImage], [CurrentImage], [CustomerCancelled]) VALUES (1042, 2, 19, CAST(N'2023-04-12T00:00:00.000' AS DateTime), CAST(N'2023-04-14T00:00:00.000' AS DateTime), 2, 430000, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Order] ([OrderId], [SaleId], [CustomerId], [OrderDate], [ShippedDate], [ShipId], [Price], [StatusId], [ShipCost], [ManagerConfirm], [SaleConfirm], [ShipConfirm], [CustomerConfirm], [ShipSuccessConfirm], [ManagerViewed], [IsValid], [ManagerCancelled], [SalerCancelled], [ShipperCancelled], [IdentityCardImage], [CurrentImage], [CustomerCancelled]) VALUES (1044, 4, 20, CAST(N'2023-04-09T00:00:00.000' AS DateTime), CAST(N'2023-04-22T00:00:00.000' AS DateTime), 1, 190000, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Order] ([OrderId], [SaleId], [CustomerId], [OrderDate], [ShippedDate], [ShipId], [Price], [StatusId], [ShipCost], [ManagerConfirm], [SaleConfirm], [ShipConfirm], [CustomerConfirm], [ShipSuccessConfirm], [ManagerViewed], [IsValid], [ManagerCancelled], [SalerCancelled], [ShipperCancelled], [IdentityCardImage], [CurrentImage], [CustomerCancelled]) VALUES (1045, 5, 20, CAST(N'2023-04-08T00:00:00.000' AS DateTime), CAST(N'2023-04-29T00:00:00.000' AS DateTime), 2, 145000, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Order] ([OrderId], [SaleId], [CustomerId], [OrderDate], [ShippedDate], [ShipId], [Price], [StatusId], [ShipCost], [ManagerConfirm], [SaleConfirm], [ShipConfirm], [CustomerConfirm], [ShipSuccessConfirm], [ManagerViewed], [IsValid], [ManagerCancelled], [SalerCancelled], [ShipperCancelled], [IdentityCardImage], [CurrentImage], [CustomerCancelled]) VALUES (1046, 1, 18, CAST(N'2023-04-07T00:00:00.000' AS DateTime), CAST(N'2023-04-28T00:00:00.000' AS DateTime), 3, 210000, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Order] ([OrderId], [SaleId], [CustomerId], [OrderDate], [ShippedDate], [ShipId], [Price], [StatusId], [ShipCost], [ManagerConfirm], [SaleConfirm], [ShipConfirm], [CustomerConfirm], [ShipSuccessConfirm], [ManagerViewed], [IsValid], [ManagerCancelled], [SalerCancelled], [ShipperCancelled], [IdentityCardImage], [CurrentImage], [CustomerCancelled]) VALUES (1047, 2, 19, CAST(N'2023-04-06T00:00:00.000' AS DateTime), CAST(N'2023-04-27T00:00:00.000' AS DateTime), 1, 320000, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Order] ([OrderId], [SaleId], [CustomerId], [OrderDate], [ShippedDate], [ShipId], [Price], [StatusId], [ShipCost], [ManagerConfirm], [SaleConfirm], [ShipConfirm], [CustomerConfirm], [ShipSuccessConfirm], [ManagerViewed], [IsValid], [ManagerCancelled], [SalerCancelled], [ShipperCancelled], [IdentityCardImage], [CurrentImage], [CustomerCancelled]) VALUES (1049, 4, 20, CAST(N'2023-04-04T00:00:00.000' AS DateTime), CAST(N'2023-04-25T00:00:00.000' AS DateTime), 3, 420000, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Order] ([OrderId], [SaleId], [CustomerId], [OrderDate], [ShippedDate], [ShipId], [Price], [StatusId], [ShipCost], [ManagerConfirm], [SaleConfirm], [ShipConfirm], [CustomerConfirm], [ShipSuccessConfirm], [ManagerViewed], [IsValid], [ManagerCancelled], [SalerCancelled], [ShipperCancelled], [IdentityCardImage], [CurrentImage], [CustomerCancelled]) VALUES (1050, 5, 20, CAST(N'2023-04-03T00:00:00.000' AS DateTime), CAST(N'2023-04-24T00:00:00.000' AS DateTime), 1, 680000, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Order] ([OrderId], [SaleId], [CustomerId], [OrderDate], [ShippedDate], [ShipId], [Price], [StatusId], [ShipCost], [ManagerConfirm], [SaleConfirm], [ShipConfirm], [CustomerConfirm], [ShipSuccessConfirm], [ManagerViewed], [IsValid], [ManagerCancelled], [SalerCancelled], [ShipperCancelled], [IdentityCardImage], [CurrentImage], [CustomerCancelled]) VALUES (1051, 1, 18, CAST(N'2023-04-02T00:00:00.000' AS DateTime), CAST(N'2023-04-23T00:00:00.000' AS DateTime), 2, 530000, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Order] ([OrderId], [SaleId], [CustomerId], [OrderDate], [ShippedDate], [ShipId], [Price], [StatusId], [ShipCost], [ManagerConfirm], [SaleConfirm], [ShipConfirm], [CustomerConfirm], [ShipSuccessConfirm], [ManagerViewed], [IsValid], [ManagerCancelled], [SalerCancelled], [ShipperCancelled], [IdentityCardImage], [CurrentImage], [CustomerCancelled]) VALUES (1052, 2, 19, CAST(N'2023-04-01T00:00:00.000' AS DateTime), CAST(N'2023-04-22T00:00:00.000' AS DateTime), 3, 290000, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Order] ([OrderId], [SaleId], [CustomerId], [OrderDate], [ShippedDate], [ShipId], [Price], [StatusId], [ShipCost], [ManagerConfirm], [SaleConfirm], [ShipConfirm], [CustomerConfirm], [ShipSuccessConfirm], [ManagerViewed], [IsValid], [ManagerCancelled], [SalerCancelled], [ShipperCancelled], [IdentityCardImage], [CurrentImage], [CustomerCancelled]) VALUES (1054, 4, 20, CAST(N'2023-04-09T00:00:00.000' AS DateTime), CAST(N'2023-04-20T00:00:00.000' AS DateTime), 2, 410000, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Order] ([OrderId], [SaleId], [CustomerId], [OrderDate], [ShippedDate], [ShipId], [Price], [StatusId], [ShipCost], [ManagerConfirm], [SaleConfirm], [ShipConfirm], [CustomerConfirm], [ShipSuccessConfirm], [ManagerViewed], [IsValid], [ManagerCancelled], [SalerCancelled], [ShipperCancelled], [IdentityCardImage], [CurrentImage], [CustomerCancelled]) VALUES (1055, 5, 20, CAST(N'2023-04-08T00:00:00.000' AS DateTime), CAST(N'2023-04-19T00:00:00.000' AS DateTime), 3, 890000, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Order] ([OrderId], [SaleId], [CustomerId], [OrderDate], [ShippedDate], [ShipId], [Price], [StatusId], [ShipCost], [ManagerConfirm], [SaleConfirm], [ShipConfirm], [CustomerConfirm], [ShipSuccessConfirm], [ManagerViewed], [IsValid], [ManagerCancelled], [SalerCancelled], [ShipperCancelled], [IdentityCardImage], [CurrentImage], [CustomerCancelled]) VALUES (1056, 1, 18, CAST(N'2023-05-17T00:00:00.000' AS DateTime), CAST(N'2023-05-28T00:00:00.000' AS DateTime), 1, 120000, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Order] ([OrderId], [SaleId], [CustomerId], [OrderDate], [ShippedDate], [ShipId], [Price], [StatusId], [ShipCost], [ManagerConfirm], [SaleConfirm], [ShipConfirm], [CustomerConfirm], [ShipSuccessConfirm], [ManagerViewed], [IsValid], [ManagerCancelled], [SalerCancelled], [ShipperCancelled], [IdentityCardImage], [CurrentImage], [CustomerCancelled]) VALUES (1057, 2, 19, CAST(N'2023-05-16T00:00:00.000' AS DateTime), CAST(N'2023-05-27T00:00:00.000' AS DateTime), 2, 123000, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Order] ([OrderId], [SaleId], [CustomerId], [OrderDate], [ShippedDate], [ShipId], [Price], [StatusId], [ShipCost], [ManagerConfirm], [SaleConfirm], [ShipConfirm], [CustomerConfirm], [ShipSuccessConfirm], [ManagerViewed], [IsValid], [ManagerCancelled], [SalerCancelled], [ShipperCancelled], [IdentityCardImage], [CurrentImage], [CustomerCancelled]) VALUES (1059, 4, 20, CAST(N'2023-05-14T00:00:00.000' AS DateTime), CAST(N'2023-05-25T00:00:00.000' AS DateTime), 1, 490000, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Order] ([OrderId], [SaleId], [CustomerId], [OrderDate], [ShippedDate], [ShipId], [Price], [StatusId], [ShipCost], [ManagerConfirm], [SaleConfirm], [ShipConfirm], [CustomerConfirm], [ShipSuccessConfirm], [ManagerViewed], [IsValid], [ManagerCancelled], [SalerCancelled], [ShipperCancelled], [IdentityCardImage], [CurrentImage], [CustomerCancelled]) VALUES (1060, 5, 20, CAST(N'2023-05-13T00:00:00.000' AS DateTime), CAST(N'2023-05-24T00:00:00.000' AS DateTime), 2, 790000, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Order] ([OrderId], [SaleId], [CustomerId], [OrderDate], [ShippedDate], [ShipId], [Price], [StatusId], [ShipCost], [ManagerConfirm], [SaleConfirm], [ShipConfirm], [CustomerConfirm], [ShipSuccessConfirm], [ManagerViewed], [IsValid], [ManagerCancelled], [SalerCancelled], [ShipperCancelled], [IdentityCardImage], [CurrentImage], [CustomerCancelled]) VALUES (1061, 1, 18, CAST(N'2023-05-12T00:00:00.000' AS DateTime), CAST(N'2023-05-23T00:00:00.000' AS DateTime), 3, 320000, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Order] ([OrderId], [SaleId], [CustomerId], [OrderDate], [ShippedDate], [ShipId], [Price], [StatusId], [ShipCost], [ManagerConfirm], [SaleConfirm], [ShipConfirm], [CustomerConfirm], [ShipSuccessConfirm], [ManagerViewed], [IsValid], [ManagerCancelled], [SalerCancelled], [ShipperCancelled], [IdentityCardImage], [CurrentImage], [CustomerCancelled]) VALUES (1062, 2, 19, CAST(N'2023-05-11T00:00:00.000' AS DateTime), CAST(N'2023-05-22T00:00:00.000' AS DateTime), 1, 189000, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Order] ([OrderId], [SaleId], [CustomerId], [OrderDate], [ShippedDate], [ShipId], [Price], [StatusId], [ShipCost], [ManagerConfirm], [SaleConfirm], [ShipConfirm], [CustomerConfirm], [ShipSuccessConfirm], [ManagerViewed], [IsValid], [ManagerCancelled], [SalerCancelled], [ShipperCancelled], [IdentityCardImage], [CurrentImage], [CustomerCancelled]) VALUES (1064, 4, 20, CAST(N'2023-05-09T00:00:00.000' AS DateTime), CAST(N'2023-05-10T00:00:00.000' AS DateTime), 3, 690000, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Order] ([OrderId], [SaleId], [CustomerId], [OrderDate], [ShippedDate], [ShipId], [Price], [StatusId], [ShipCost], [ManagerConfirm], [SaleConfirm], [ShipConfirm], [CustomerConfirm], [ShipSuccessConfirm], [ManagerViewed], [IsValid], [ManagerCancelled], [SalerCancelled], [ShipperCancelled], [IdentityCardImage], [CurrentImage], [CustomerCancelled]) VALUES (1065, 5, 20, CAST(N'2023-05-08T00:00:00.000' AS DateTime), CAST(N'2023-05-19T00:00:00.000' AS DateTime), 1, 790000, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Order] ([OrderId], [SaleId], [CustomerId], [OrderDate], [ShippedDate], [ShipId], [Price], [StatusId], [ShipCost], [ManagerConfirm], [SaleConfirm], [ShipConfirm], [CustomerConfirm], [ShipSuccessConfirm], [ManagerViewed], [IsValid], [ManagerCancelled], [SalerCancelled], [ShipperCancelled], [IdentityCardImage], [CurrentImage], [CustomerCancelled]) VALUES (1066, 1, 18, CAST(N'2023-06-07T00:00:00.000' AS DateTime), CAST(N'2023-06-08T00:00:00.000' AS DateTime), 2, 320000, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Order] ([OrderId], [SaleId], [CustomerId], [OrderDate], [ShippedDate], [ShipId], [Price], [StatusId], [ShipCost], [ManagerConfirm], [SaleConfirm], [ShipConfirm], [CustomerConfirm], [ShipSuccessConfirm], [ManagerViewed], [IsValid], [ManagerCancelled], [SalerCancelled], [ShipperCancelled], [IdentityCardImage], [CurrentImage], [CustomerCancelled]) VALUES (1067, 2, 19, CAST(N'2023-06-06T00:00:00.000' AS DateTime), CAST(N'2023-06-17T00:00:00.000' AS DateTime), 3, 290000, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Order] ([OrderId], [SaleId], [CustomerId], [OrderDate], [ShippedDate], [ShipId], [Price], [StatusId], [ShipCost], [ManagerConfirm], [SaleConfirm], [ShipConfirm], [CustomerConfirm], [ShipSuccessConfirm], [ManagerViewed], [IsValid], [ManagerCancelled], [SalerCancelled], [ShipperCancelled], [IdentityCardImage], [CurrentImage], [CustomerCancelled]) VALUES (1069, 4, 20, CAST(N'2023-06-04T00:00:00.000' AS DateTime), CAST(N'2023-06-15T00:00:00.000' AS DateTime), 2, 400000, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Order] ([OrderId], [SaleId], [CustomerId], [OrderDate], [ShippedDate], [ShipId], [Price], [StatusId], [ShipCost], [ManagerConfirm], [SaleConfirm], [ShipConfirm], [CustomerConfirm], [ShipSuccessConfirm], [ManagerViewed], [IsValid], [ManagerCancelled], [SalerCancelled], [ShipperCancelled], [IdentityCardImage], [CurrentImage], [CustomerCancelled]) VALUES (1070, 5, 20, CAST(N'2023-06-03T00:00:00.000' AS DateTime), CAST(N'2023-06-14T00:00:00.000' AS DateTime), 3, 700000, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Order] ([OrderId], [SaleId], [CustomerId], [OrderDate], [ShippedDate], [ShipId], [Price], [StatusId], [ShipCost], [ManagerConfirm], [SaleConfirm], [ShipConfirm], [CustomerConfirm], [ShipSuccessConfirm], [ManagerViewed], [IsValid], [ManagerCancelled], [SalerCancelled], [ShipperCancelled], [IdentityCardImage], [CurrentImage], [CustomerCancelled]) VALUES (1071, 1, 18, CAST(N'2023-06-02T00:00:00.000' AS DateTime), CAST(N'2023-06-13T00:00:00.000' AS DateTime), 1, 200000, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Order] ([OrderId], [SaleId], [CustomerId], [OrderDate], [ShippedDate], [ShipId], [Price], [StatusId], [ShipCost], [ManagerConfirm], [SaleConfirm], [ShipConfirm], [CustomerConfirm], [ShipSuccessConfirm], [ManagerViewed], [IsValid], [ManagerCancelled], [SalerCancelled], [ShipperCancelled], [IdentityCardImage], [CurrentImage], [CustomerCancelled]) VALUES (1072, 2, 19, CAST(N'2023-06-01T00:00:00.000' AS DateTime), CAST(N'2023-06-12T00:00:00.000' AS DateTime), 2, 600000, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Order] ([OrderId], [SaleId], [CustomerId], [OrderDate], [ShippedDate], [ShipId], [Price], [StatusId], [ShipCost], [ManagerConfirm], [SaleConfirm], [ShipConfirm], [CustomerConfirm], [ShipSuccessConfirm], [ManagerViewed], [IsValid], [ManagerCancelled], [SalerCancelled], [ShipperCancelled], [IdentityCardImage], [CurrentImage], [CustomerCancelled]) VALUES (1074, 4, 20, CAST(N'2023-06-12T00:00:00.000' AS DateTime), CAST(N'2023-06-30T00:00:00.000' AS DateTime), 1, 320000, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Order] ([OrderId], [SaleId], [CustomerId], [OrderDate], [ShippedDate], [ShipId], [Price], [StatusId], [ShipCost], [ManagerConfirm], [SaleConfirm], [ShipConfirm], [CustomerConfirm], [ShipSuccessConfirm], [ManagerViewed], [IsValid], [ManagerCancelled], [SalerCancelled], [ShipperCancelled], [IdentityCardImage], [CurrentImage], [CustomerCancelled]) VALUES (1075, 5, 20, CAST(N'2023-06-12T00:00:00.000' AS DateTime), CAST(N'2023-06-29T00:00:00.000' AS DateTime), 2, 470000, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Order] ([OrderId], [SaleId], [CustomerId], [OrderDate], [ShippedDate], [ShipId], [Price], [StatusId], [ShipCost], [ManagerConfirm], [SaleConfirm], [ShipConfirm], [CustomerConfirm], [ShipSuccessConfirm], [ManagerViewed], [IsValid], [ManagerCancelled], [SalerCancelled], [ShipperCancelled], [IdentityCardImage], [CurrentImage], [CustomerCancelled]) VALUES (1076, 1, 18, CAST(N'2023-07-17T00:00:00.000' AS DateTime), CAST(N'2023-07-28T00:00:00.000' AS DateTime), 3, 320000, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Order] ([OrderId], [SaleId], [CustomerId], [OrderDate], [ShippedDate], [ShipId], [Price], [StatusId], [ShipCost], [ManagerConfirm], [SaleConfirm], [ShipConfirm], [CustomerConfirm], [ShipSuccessConfirm], [ManagerViewed], [IsValid], [ManagerCancelled], [SalerCancelled], [ShipperCancelled], [IdentityCardImage], [CurrentImage], [CustomerCancelled]) VALUES (1077, 2, 19, CAST(N'2023-07-16T00:00:00.000' AS DateTime), CAST(N'2023-07-27T00:00:00.000' AS DateTime), 1, 370000, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Order] ([OrderId], [SaleId], [CustomerId], [OrderDate], [ShippedDate], [ShipId], [Price], [StatusId], [ShipCost], [ManagerConfirm], [SaleConfirm], [ShipConfirm], [CustomerConfirm], [ShipSuccessConfirm], [ManagerViewed], [IsValid], [ManagerCancelled], [SalerCancelled], [ShipperCancelled], [IdentityCardImage], [CurrentImage], [CustomerCancelled]) VALUES (1079, 4, 20, CAST(N'2023-07-14T00:00:00.000' AS DateTime), CAST(N'2023-07-25T00:00:00.000' AS DateTime), 3, 300000, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Order] ([OrderId], [SaleId], [CustomerId], [OrderDate], [ShippedDate], [ShipId], [Price], [StatusId], [ShipCost], [ManagerConfirm], [SaleConfirm], [ShipConfirm], [CustomerConfirm], [ShipSuccessConfirm], [ManagerViewed], [IsValid], [ManagerCancelled], [SalerCancelled], [ShipperCancelled], [IdentityCardImage], [CurrentImage], [CustomerCancelled]) VALUES (1080, 5, 20, CAST(N'2023-07-13T00:00:00.000' AS DateTime), CAST(N'2023-07-24T00:00:00.000' AS DateTime), 1, 600000, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Order] ([OrderId], [SaleId], [CustomerId], [OrderDate], [ShippedDate], [ShipId], [Price], [StatusId], [ShipCost], [ManagerConfirm], [SaleConfirm], [ShipConfirm], [CustomerConfirm], [ShipSuccessConfirm], [ManagerViewed], [IsValid], [ManagerCancelled], [SalerCancelled], [ShipperCancelled], [IdentityCardImage], [CurrentImage], [CustomerCancelled]) VALUES (1081, 1, 18, CAST(N'2023-07-12T00:00:00.000' AS DateTime), CAST(N'2023-07-23T00:00:00.000' AS DateTime), 2, 550000, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Order] ([OrderId], [SaleId], [CustomerId], [OrderDate], [ShippedDate], [ShipId], [Price], [StatusId], [ShipCost], [ManagerConfirm], [SaleConfirm], [ShipConfirm], [CustomerConfirm], [ShipSuccessConfirm], [ManagerViewed], [IsValid], [ManagerCancelled], [SalerCancelled], [ShipperCancelled], [IdentityCardImage], [CurrentImage], [CustomerCancelled]) VALUES (1082, 2, 19, CAST(N'2023-07-11T00:00:00.000' AS DateTime), CAST(N'2023-07-22T00:00:00.000' AS DateTime), 3, 320000, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Order] ([OrderId], [SaleId], [CustomerId], [OrderDate], [ShippedDate], [ShipId], [Price], [StatusId], [ShipCost], [ManagerConfirm], [SaleConfirm], [ShipConfirm], [CustomerConfirm], [ShipSuccessConfirm], [ManagerViewed], [IsValid], [ManagerCancelled], [SalerCancelled], [ShipperCancelled], [IdentityCardImage], [CurrentImage], [CustomerCancelled]) VALUES (1084, 4, 20, CAST(N'2023-07-09T00:00:00.000' AS DateTime), CAST(N'2023-07-20T00:00:00.000' AS DateTime), 2, 720000, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Order] ([OrderId], [SaleId], [CustomerId], [OrderDate], [ShippedDate], [ShipId], [Price], [StatusId], [ShipCost], [ManagerConfirm], [SaleConfirm], [ShipConfirm], [CustomerConfirm], [ShipSuccessConfirm], [ManagerViewed], [IsValid], [ManagerCancelled], [SalerCancelled], [ShipperCancelled], [IdentityCardImage], [CurrentImage], [CustomerCancelled]) VALUES (1085, 5, 20, CAST(N'2023-07-08T00:00:00.000' AS DateTime), CAST(N'2023-07-19T00:00:00.000' AS DateTime), 3, 320000, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Order] ([OrderId], [SaleId], [CustomerId], [OrderDate], [ShippedDate], [ShipId], [Price], [StatusId], [ShipCost], [ManagerConfirm], [SaleConfirm], [ShipConfirm], [CustomerConfirm], [ShipSuccessConfirm], [ManagerViewed], [IsValid], [ManagerCancelled], [SalerCancelled], [ShipperCancelled], [IdentityCardImage], [CurrentImage], [CustomerCancelled]) VALUES (1086, 1, 18, CAST(N'2023-08-07T00:00:00.000' AS DateTime), CAST(N'2023-08-18T00:00:00.000' AS DateTime), 1, 420000, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Order] ([OrderId], [SaleId], [CustomerId], [OrderDate], [ShippedDate], [ShipId], [Price], [StatusId], [ShipCost], [ManagerConfirm], [SaleConfirm], [ShipConfirm], [CustomerConfirm], [ShipSuccessConfirm], [ManagerViewed], [IsValid], [ManagerCancelled], [SalerCancelled], [ShipperCancelled], [IdentityCardImage], [CurrentImage], [CustomerCancelled]) VALUES (1087, 2, 19, CAST(N'2023-08-06T00:00:00.000' AS DateTime), CAST(N'2023-08-17T00:00:00.000' AS DateTime), 2, 350000, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Order] ([OrderId], [SaleId], [CustomerId], [OrderDate], [ShippedDate], [ShipId], [Price], [StatusId], [ShipCost], [ManagerConfirm], [SaleConfirm], [ShipConfirm], [CustomerConfirm], [ShipSuccessConfirm], [ManagerViewed], [IsValid], [ManagerCancelled], [SalerCancelled], [ShipperCancelled], [IdentityCardImage], [CurrentImage], [CustomerCancelled]) VALUES (1089, 4, 20, CAST(N'2023-08-04T00:00:00.000' AS DateTime), CAST(N'2023-08-15T00:00:00.000' AS DateTime), 1, 550000, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Order] ([OrderId], [SaleId], [CustomerId], [OrderDate], [ShippedDate], [ShipId], [Price], [StatusId], [ShipCost], [ManagerConfirm], [SaleConfirm], [ShipConfirm], [CustomerConfirm], [ShipSuccessConfirm], [ManagerViewed], [IsValid], [ManagerCancelled], [SalerCancelled], [ShipperCancelled], [IdentityCardImage], [CurrentImage], [CustomerCancelled]) VALUES (1090, 5, 20, CAST(N'2023-08-03T00:00:00.000' AS DateTime), CAST(N'2023-08-14T00:00:00.000' AS DateTime), 2, 720000, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Order] ([OrderId], [SaleId], [CustomerId], [OrderDate], [ShippedDate], [ShipId], [Price], [StatusId], [ShipCost], [ManagerConfirm], [SaleConfirm], [ShipConfirm], [CustomerConfirm], [ShipSuccessConfirm], [ManagerViewed], [IsValid], [ManagerCancelled], [SalerCancelled], [ShipperCancelled], [IdentityCardImage], [CurrentImage], [CustomerCancelled]) VALUES (1091, 1, 18, CAST(N'2023-08-02T00:00:00.000' AS DateTime), CAST(N'2023-08-13T00:00:00.000' AS DateTime), 3, 840000, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Order] ([OrderId], [SaleId], [CustomerId], [OrderDate], [ShippedDate], [ShipId], [Price], [StatusId], [ShipCost], [ManagerConfirm], [SaleConfirm], [ShipConfirm], [CustomerConfirm], [ShipSuccessConfirm], [ManagerViewed], [IsValid], [ManagerCancelled], [SalerCancelled], [ShipperCancelled], [IdentityCardImage], [CurrentImage], [CustomerCancelled]) VALUES (1092, 2, 19, CAST(N'2023-08-01T00:00:00.000' AS DateTime), CAST(N'2023-08-12T00:00:00.000' AS DateTime), 1, 190000, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Order] ([OrderId], [SaleId], [CustomerId], [OrderDate], [ShippedDate], [ShipId], [Price], [StatusId], [ShipCost], [ManagerConfirm], [SaleConfirm], [ShipConfirm], [CustomerConfirm], [ShipSuccessConfirm], [ManagerViewed], [IsValid], [ManagerCancelled], [SalerCancelled], [ShipperCancelled], [IdentityCardImage], [CurrentImage], [CustomerCancelled]) VALUES (1094, 4, 20, CAST(N'2023-08-09T00:00:00.000' AS DateTime), CAST(N'2023-08-20T00:00:00.000' AS DateTime), 3, 230000, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Order] ([OrderId], [SaleId], [CustomerId], [OrderDate], [ShippedDate], [ShipId], [Price], [StatusId], [ShipCost], [ManagerConfirm], [SaleConfirm], [ShipConfirm], [CustomerConfirm], [ShipSuccessConfirm], [ManagerViewed], [IsValid], [ManagerCancelled], [SalerCancelled], [ShipperCancelled], [IdentityCardImage], [CurrentImage], [CustomerCancelled]) VALUES (1095, 5, 20, CAST(N'2023-08-08T00:00:00.000' AS DateTime), CAST(N'2023-08-19T00:00:00.000' AS DateTime), 1, 290000, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Order] ([OrderId], [SaleId], [CustomerId], [OrderDate], [ShippedDate], [ShipId], [Price], [StatusId], [ShipCost], [ManagerConfirm], [SaleConfirm], [ShipConfirm], [CustomerConfirm], [ShipSuccessConfirm], [ManagerViewed], [IsValid], [ManagerCancelled], [SalerCancelled], [ShipperCancelled], [IdentityCardImage], [CurrentImage], [CustomerCancelled]) VALUES (1096, 1, 18, CAST(N'2023-09-07T00:00:00.000' AS DateTime), CAST(N'2023-09-18T00:00:00.000' AS DateTime), 2, 480000, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Order] ([OrderId], [SaleId], [CustomerId], [OrderDate], [ShippedDate], [ShipId], [Price], [StatusId], [ShipCost], [ManagerConfirm], [SaleConfirm], [ShipConfirm], [CustomerConfirm], [ShipSuccessConfirm], [ManagerViewed], [IsValid], [ManagerCancelled], [SalerCancelled], [ShipperCancelled], [IdentityCardImage], [CurrentImage], [CustomerCancelled]) VALUES (1097, 2, 19, CAST(N'2023-09-06T00:00:00.000' AS DateTime), CAST(N'2023-09-17T00:00:00.000' AS DateTime), 3, 230000, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Order] ([OrderId], [SaleId], [CustomerId], [OrderDate], [ShippedDate], [ShipId], [Price], [StatusId], [ShipCost], [ManagerConfirm], [SaleConfirm], [ShipConfirm], [CustomerConfirm], [ShipSuccessConfirm], [ManagerViewed], [IsValid], [ManagerCancelled], [SalerCancelled], [ShipperCancelled], [IdentityCardImage], [CurrentImage], [CustomerCancelled]) VALUES (1099, 4, 20, CAST(N'2023-09-04T00:00:00.000' AS DateTime), CAST(N'2023-09-15T00:00:00.000' AS DateTime), 2, 120000, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Order] ([OrderId], [SaleId], [CustomerId], [OrderDate], [ShippedDate], [ShipId], [Price], [StatusId], [ShipCost], [ManagerConfirm], [SaleConfirm], [ShipConfirm], [CustomerConfirm], [ShipSuccessConfirm], [ManagerViewed], [IsValid], [ManagerCancelled], [SalerCancelled], [ShipperCancelled], [IdentityCardImage], [CurrentImage], [CustomerCancelled]) VALUES (1100, 5, 20, CAST(N'2023-09-03T00:00:00.000' AS DateTime), CAST(N'2023-09-14T00:00:00.000' AS DateTime), 3, 140000, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Order] ([OrderId], [SaleId], [CustomerId], [OrderDate], [ShippedDate], [ShipId], [Price], [StatusId], [ShipCost], [ManagerConfirm], [SaleConfirm], [ShipConfirm], [CustomerConfirm], [ShipSuccessConfirm], [ManagerViewed], [IsValid], [ManagerCancelled], [SalerCancelled], [ShipperCancelled], [IdentityCardImage], [CurrentImage], [CustomerCancelled]) VALUES (1101, 1, 18, CAST(N'2023-09-02T00:00:00.000' AS DateTime), CAST(N'2023-09-13T00:00:00.000' AS DateTime), 1, 260000, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Order] ([OrderId], [SaleId], [CustomerId], [OrderDate], [ShippedDate], [ShipId], [Price], [StatusId], [ShipCost], [ManagerConfirm], [SaleConfirm], [ShipConfirm], [CustomerConfirm], [ShipSuccessConfirm], [ManagerViewed], [IsValid], [ManagerCancelled], [SalerCancelled], [ShipperCancelled], [IdentityCardImage], [CurrentImage], [CustomerCancelled]) VALUES (1102, 2, 19, CAST(N'2023-09-10T00:00:00.000' AS DateTime), CAST(N'2023-09-22T00:00:00.000' AS DateTime), 2, 490000, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Order] ([OrderId], [SaleId], [CustomerId], [OrderDate], [ShippedDate], [ShipId], [Price], [StatusId], [ShipCost], [ManagerConfirm], [SaleConfirm], [ShipConfirm], [CustomerConfirm], [ShipSuccessConfirm], [ManagerViewed], [IsValid], [ManagerCancelled], [SalerCancelled], [ShipperCancelled], [IdentityCardImage], [CurrentImage], [CustomerCancelled]) VALUES (1104, 4, 20, CAST(N'2023-09-13T00:00:00.000' AS DateTime), CAST(N'2023-09-30T00:00:00.000' AS DateTime), 1, 320000, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Order] ([OrderId], [SaleId], [CustomerId], [OrderDate], [ShippedDate], [ShipId], [Price], [StatusId], [ShipCost], [ManagerConfirm], [SaleConfirm], [ShipConfirm], [CustomerConfirm], [ShipSuccessConfirm], [ManagerViewed], [IsValid], [ManagerCancelled], [SalerCancelled], [ShipperCancelled], [IdentityCardImage], [CurrentImage], [CustomerCancelled]) VALUES (1105, 5, 20, CAST(N'2023-09-12T00:00:00.000' AS DateTime), CAST(N'2023-09-30T00:00:00.000' AS DateTime), 2, 310000, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Order] ([OrderId], [SaleId], [CustomerId], [OrderDate], [ShippedDate], [ShipId], [Price], [StatusId], [ShipCost], [ManagerConfirm], [SaleConfirm], [ShipConfirm], [CustomerConfirm], [ShipSuccessConfirm], [ManagerViewed], [IsValid], [ManagerCancelled], [SalerCancelled], [ShipperCancelled], [IdentityCardImage], [CurrentImage], [CustomerCancelled]) VALUES (1106, 1, 18, CAST(N'2023-10-18T00:00:00.000' AS DateTime), CAST(N'2023-10-29T00:00:00.000' AS DateTime), 3, 220000, 1, NULL, NULL, NULL, NULL, NULL, NULL, CAST(N'2023-08-11T12:10:46.303' AS DateTime), NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Order] ([OrderId], [SaleId], [CustomerId], [OrderDate], [ShippedDate], [ShipId], [Price], [StatusId], [ShipCost], [ManagerConfirm], [SaleConfirm], [ShipConfirm], [CustomerConfirm], [ShipSuccessConfirm], [ManagerViewed], [IsValid], [ManagerCancelled], [SalerCancelled], [ShipperCancelled], [IdentityCardImage], [CurrentImage], [CustomerCancelled]) VALUES (1168, NULL, 16, CAST(N'2023-08-11T12:33:18.990' AS DateTime), NULL, NULL, 22711500, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'product/thumbnail-3a09f386-e72e-432f-8829-aed61ed2ac86', NULL)
INSERT [dbo].[Order] ([OrderId], [SaleId], [CustomerId], [OrderDate], [ShippedDate], [ShipId], [Price], [StatusId], [ShipCost], [ManagerConfirm], [SaleConfirm], [ShipConfirm], [CustomerConfirm], [ShipSuccessConfirm], [ManagerViewed], [IsValid], [ManagerCancelled], [SalerCancelled], [ShipperCancelled], [IdentityCardImage], [CurrentImage], [CustomerCancelled]) VALUES (1169, NULL, 20, CAST(N'2023-08-11T12:48:45.003' AS DateTime), NULL, NULL, 876015, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'product/thumbnail-ce6c895a-250b-46fb-81e7-4b461d912f9a', NULL)
INSERT [dbo].[Order] ([OrderId], [SaleId], [CustomerId], [OrderDate], [ShippedDate], [ShipId], [Price], [StatusId], [ShipCost], [ManagerConfirm], [SaleConfirm], [ShipConfirm], [CustomerConfirm], [ShipSuccessConfirm], [ManagerViewed], [IsValid], [ManagerCancelled], [SalerCancelled], [ShipperCancelled], [IdentityCardImage], [CurrentImage], [CustomerCancelled]) VALUES (1170, NULL, 20, CAST(N'2023-08-11T13:02:05.843' AS DateTime), NULL, NULL, 1320975, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'product/thumbnail-98ef4607-d13c-49ca-8de1-a15f7f6b7fa5', NULL)
INSERT [dbo].[Order] ([OrderId], [SaleId], [CustomerId], [OrderDate], [ShippedDate], [ShipId], [Price], [StatusId], [ShipCost], [ManagerConfirm], [SaleConfirm], [ShipConfirm], [CustomerConfirm], [ShipSuccessConfirm], [ManagerViewed], [IsValid], [ManagerCancelled], [SalerCancelled], [ShipperCancelled], [IdentityCardImage], [CurrentImage], [CustomerCancelled]) VALUES (1171, NULL, 20, CAST(N'2023-08-11T13:07:07.100' AS DateTime), NULL, NULL, 2920050, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'product/thumbnail-856fea97-2846-4b46-88b8-bf740d75e73f', NULL)
INSERT [dbo].[Order] ([OrderId], [SaleId], [CustomerId], [OrderDate], [ShippedDate], [ShipId], [Price], [StatusId], [ShipCost], [ManagerConfirm], [SaleConfirm], [ShipConfirm], [CustomerConfirm], [ShipSuccessConfirm], [ManagerViewed], [IsValid], [ManagerCancelled], [SalerCancelled], [ShipperCancelled], [IdentityCardImage], [CurrentImage], [CustomerCancelled]) VALUES (1172, NULL, 20, CAST(N'2023-08-11T13:10:44.927' AS DateTime), NULL, NULL, 10274250, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'product/thumbnail-a41be6d6-3a98-4d9d-88fe-1d61d664f680', NULL)
INSERT [dbo].[Order] ([OrderId], [SaleId], [CustomerId], [OrderDate], [ShippedDate], [ShipId], [Price], [StatusId], [ShipCost], [ManagerConfirm], [SaleConfirm], [ShipConfirm], [CustomerConfirm], [ShipSuccessConfirm], [ManagerViewed], [IsValid], [ManagerCancelled], [SalerCancelled], [ShipperCancelled], [IdentityCardImage], [CurrentImage], [CustomerCancelled]) VALUES (1173, NULL, 20, CAST(N'2023-08-11T13:18:06.973' AS DateTime), NULL, NULL, 4403250, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'product/thumbnail-f8cf1da9-e60b-4c94-bba2-28011f2fa933', NULL)
INSERT [dbo].[Order] ([OrderId], [SaleId], [CustomerId], [OrderDate], [ShippedDate], [ShipId], [Price], [StatusId], [ShipCost], [ManagerConfirm], [SaleConfirm], [ShipConfirm], [CustomerConfirm], [ShipSuccessConfirm], [ManagerViewed], [IsValid], [ManagerCancelled], [SalerCancelled], [ShipperCancelled], [IdentityCardImage], [CurrentImage], [CustomerCancelled]) VALUES (1174, NULL, 20, CAST(N'2023-08-11T13:21:00.730' AS DateTime), NULL, NULL, 463500, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'product/thumbnail-5a52f7b6-010d-4744-a45a-9e9f1ce54675', NULL)
INSERT [dbo].[Order] ([OrderId], [SaleId], [CustomerId], [OrderDate], [ShippedDate], [ShipId], [Price], [StatusId], [ShipCost], [ManagerConfirm], [SaleConfirm], [ShipConfirm], [CustomerConfirm], [ShipSuccessConfirm], [ManagerViewed], [IsValid], [ManagerCancelled], [SalerCancelled], [ShipperCancelled], [IdentityCardImage], [CurrentImage], [CustomerCancelled]) VALUES (1175, NULL, 20, CAST(N'2023-08-11T13:24:57.670' AS DateTime), NULL, NULL, 2920050, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'product/thumbnail-09d186f6-92d9-4487-b782-1e44ccac5af5', NULL)
INSERT [dbo].[Order] ([OrderId], [SaleId], [CustomerId], [OrderDate], [ShippedDate], [ShipId], [Price], [StatusId], [ShipCost], [ManagerConfirm], [SaleConfirm], [ShipConfirm], [CustomerConfirm], [ShipSuccessConfirm], [ManagerViewed], [IsValid], [ManagerCancelled], [SalerCancelled], [ShipperCancelled], [IdentityCardImage], [CurrentImage], [CustomerCancelled]) VALUES (1176, NULL, 20, CAST(N'2023-08-11T13:29:03.403' AS DateTime), NULL, NULL, 11170350, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'product/thumbnail-144f5f54-7e62-4a4b-a1ff-8641fc533b85', NULL)
INSERT [dbo].[Order] ([OrderId], [SaleId], [CustomerId], [OrderDate], [ShippedDate], [ShipId], [Price], [StatusId], [ShipCost], [ManagerConfirm], [SaleConfirm], [ShipConfirm], [CustomerConfirm], [ShipSuccessConfirm], [ManagerViewed], [IsValid], [ManagerCancelled], [SalerCancelled], [ShipperCancelled], [IdentityCardImage], [CurrentImage], [CustomerCancelled]) VALUES (1177, NULL, 20, CAST(N'2023-08-11T14:58:15.320' AS DateTime), NULL, NULL, 463500, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'product/thumbnail-18157dc8-4111-4810-bfb2-810ebd137d6a', NULL)
SET IDENTITY_INSERT [dbo].[Order] OFF
GO
SET IDENTITY_INSERT [dbo].[OrderDetail] ON 

INSERT [dbo].[OrderDetail] ([OrderDetailId], [OrderId], [ProductId], [UnitPrice], [Quantity], [Discount], [ThreeDays], [OneWeeks], [ProductCode]) VALUES (1324, 1177, 41, 500000, 1, 10, 0, 0, NULL)
SET IDENTITY_INSERT [dbo].[OrderDetail] OFF
GO
SET IDENTITY_INSERT [dbo].[Product] ON 

INSERT [dbo].[Product] ([ProductId], [ProductName], [CategoryId], [UnitInStock], [UnitPrice], [Image], [StatusId], [Brand], [Description], [DiscountDate], [Type], [Moneytype], [Quality], [Discount]) VALUES (30, N'Bàn ghế hội nghị', 4, 100, 150000, N'product/thumbnail-d30ab872-74fe-4296-8e88-ee49c9e76f16.png', 2, N'Ghế inox lưng nhựa cao cấp', N' Bàn 0,5m x 2m, khăn bàn trắng, vây bàn màu

Ghế nệm vuông có lưng dựa, áo ghế trắng, nơ màu

Ghế chân quày (Ghế bành), áo ghế trắng, ríp màu', NULL, NULL, NULL, 9, 10)
INSERT [dbo].[Product] ([ProductId], [ProductName], [CategoryId], [UnitInStock], [UnitPrice], [Image], [StatusId], [Brand], [Description], [DiscountDate], [Type], [Moneytype], [Quality], [Discount]) VALUES (31, N'Bộ bàn tròn chiêu đãi', 4, 100, 180000, N'product/thumbnail-c6b23675-3d9d-476a-8039-a050b6fdd63b.JPG', 3, N'Bàn ngoại nhập', N' Bàn tròn 1.55m (Bàn ngoại nhập, chất liệu cao cấp, mặt bàn melamin, chân sắt sơn tĩnh điện)

Khăn bàn trắng, decord màu (nhiều màu vải chọn lựa, màu nơ tùy chọn)', NULL, NULL, NULL, 7, 10)
INSERT [dbo].[Product] ([ProductId], [ProductName], [CategoryId], [UnitInStock], [UnitPrice], [Image], [StatusId], [Brand], [Description], [DiscountDate], [Type], [Moneytype], [Quality], [Discount]) VALUES (32, N'Màn hình LED ngoài trời', 2, 2, 2000000, N'product/thumbnail-4e75e6bb-55c4-4f59-b9e2-08bd6445c23c.jpg', 2, N'Màn Hình Led LG', N'Ngày nay, việc sử dụng màn hình LED cho sự kiện là không hiếm, đặc thù là với các sự kiện có quy mô trung bình trở lên. Màn hình Led là một trong những công nghệ mang lại nhiều hiệu ứng về mặt hình ảnh nhất cho các sân khấu, đặc biệt là các sân khấu biểu diễn, nghệ thuật. Vì thế dịch vụ cho thuê màn hình LED càng trở nên phổ biến hơn.

Tuy nhiên tùy vào chất lượng màn hình LED của đơn vị cho thuê sẽ thì chất lượng hình ảnh mang lại khác nhau. ViewPoint System là đơn vị cho thuê màn hình LED có kinh nghiệm lâu năm. Chúng tôi đã bắt gặp nhiều trường hợp khách hàng trước đó vì ham rẻ, thuê màn hình LED kém chất lượng từ những đơn vị cung cấp không rõ ràng, không uy tín…để rồi bỏ ra một khoản kinh phí lớn nhưng lại không đạt được hiệu quả như mình mong muốn.', NULL, NULL, NULL, 10, 0)
INSERT [dbo].[Product] ([ProductId], [ProductName], [CategoryId], [UnitInStock], [UnitPrice], [Image], [StatusId], [Brand], [Description], [DiscountDate], [Type], [Moneytype], [Quality], [Discount]) VALUES (33, N'Flycam DJI MAVIC 3 PRO kèm quay phim', 1, 2, 1500000, N'product/thumbnail-4cd7e6ad-93df-4136-82aa-82a0534f3810.jpeg', 2, N'DJI', N'Mavic 3 Pro được tích hợp hệ thống ba camera hoàn toàn mới với ba cảm biến và ống kính có độ dài tiêu cự khác nhau. Kết hợp Hasselblad camera và cụm camera tele kép, hỗ trợ quay video lên đến 5.1K/50fps, hình ảnh chất lượng 48MP, Mavic 3 Pro sẽ là chiếc máy bay không người lái mở ra những góc quay mới, cho phép bạn tự do sáng tạo và tạo ra những kiệt tác điện ảnh.', NULL, NULL, NULL, 9, 10)
INSERT [dbo].[Product] ([ProductId], [ProductName], [CategoryId], [UnitInStock], [UnitPrice], [Image], [StatusId], [Brand], [Description], [DiscountDate], [Type], [Moneytype], [Quality], [Discount]) VALUES (34, N'Máy quay phim Sony HXR-MC2500', 1, 1, 2600000, N'product/thumbnail-d186f668-c3cd-4f5e-986b-f3ae0ad5b13f.jpg', 2, N'Sony', N'Thông số kỹ thuật:

- Máy quay Sony chuyên nghiệp với ống kính G cao cấp
- Bộ nhớ dung lượng 32GB, hỗ trợ ghi vào thẻ nhớ 
- Quay phim Full-HD 1080p, nén dữ liệu dạng AVCHD
- Cảm biến Exmor 1/4" hiệu quả với mọi nguồn sáng
- Ống kính góc rộng 26,8-360mm chống rung quang học
- Dải Zoom quang 12x, dải zoom kỹ thuật số tới 200x
- Định dạng ghi HD: MPEG 4/AVCHD 2.0 SD: DV ( AVI )
- Kết nối vào ra: Composite, BNC mini, USB2.0, HDMI
- Màn hình cảm ứng 3.0", hỗ trợ Micro ghi âm ngoài ', NULL, NULL, NULL, 10, 10)
INSERT [dbo].[Product] ([ProductId], [ProductName], [CategoryId], [UnitInStock], [UnitPrice], [Image], [StatusId], [Brand], [Description], [DiscountDate], [Type], [Moneytype], [Quality], [Discount]) VALUES (35, N'Mixer Yamaha MGP16X', 1, 2, 500000, N'product/thumbnail-fa9d4be6-9089-4b97-b6ba-adeb5ea3c3b6.jpg', 2, N'Yamaha ', N'Mixer Yamaha MGP16X có tổng cộng 16 kênh đầu vào với 8 đầu vào đơn và 4 đầu vào âm thanh nổi. Đây chắc chắn là sự lựa chọn hoàn hảo nhất cho bạn khi muốn sở hữu một bộ dàn âm thanh mang tính chuyên nghiệp. Thuộc dòng Preamp Mic “D-PRE” Class A Rời Rạc, MGP16X được trang bị hệ thống Musical X-pressive EQ dựa trên Công nghệ VCM nổi tiếng của nhà sản xuất Yamaha. Bên cạnh đó nó còn được sử dụng bộ nén 1 núm vặn chuyên nghiệp với đèn báo LED cao cấp. ', NULL, NULL, NULL, 8, 10)
INSERT [dbo].[Product] ([ProductId], [ProductName], [CategoryId], [UnitInStock], [UnitPrice], [Image], [StatusId], [Brand], [Description], [DiscountDate], [Type], [Moneytype], [Quality], [Discount]) VALUES (36, N'ÂM THANH NGOÀI TRỜI 6 LOA', 1, 1, 2400000, N'product/thumbnail-9e897713-f43b-4f63-ac4c-5c634a0e274c.png', 2, N'JBL', N'Sử dụng trong các sự kiện ngoài trời như đám cưới, khánh thành,biểu diễn ca nhạc ...
Phục vụ từ 300-700 người.

    4-6 loa thùng JBL Công suất 800-1000w/ 2 Loa
    2-3 Cục đẩy công suất CA20
    1 Mixer Yamaha
    3 Bộ Micro không dây Shure UR 12D (6 tay mic)
    Bộ điều khiển và dây tín hiệu (trọn bộ)
    Nhân viên kĩ thuật lắp đặt, tháo dỡ. Nhân viên vận hành trong suốt quá trình diễn ra sự kiện.', NULL, NULL, NULL, 9, 5)
INSERT [dbo].[Product] ([ProductId], [ProductName], [CategoryId], [UnitInStock], [UnitPrice], [Image], [StatusId], [Brand], [Description], [DiscountDate], [Type], [Moneytype], [Quality], [Discount]) VALUES (37, N'ÂM THANH HÁT KARAOKE', 1, 2, 1200000, N'product/thumbnail-e793b6d1-e82c-414e-93d4-06ed67e50599.jpg', 2, N'JBL', N'Sử dụng trong các sự kiện có kèm theo hát karaoke.
Phục vụ từ 50-200 người

    2 loa thùng JBL công suất 800 - 1000W
    1 Cục đẩy công suất
    1 Mixer Yamaha MGP16X
    1 Bộ Micro không dây Shure UR 12D
    Bộ hát karaoke VIỆT KTV (Như quán hát)
    Màn hình hiển thị chọn bài
    Nhân viên kỹ thuật', NULL, NULL, NULL, 10, 8)
INSERT [dbo].[Product] ([ProductId], [ProductName], [CategoryId], [UnitInStock], [UnitPrice], [Image], [StatusId], [Brand], [Description], [DiscountDate], [Type], [Moneytype], [Quality], [Discount]) VALUES (38, N'Ô DÙ-THẢM TRẢI SÀN', 5, 1, 3500000, N'product/thumbnail-e34ff2d0-fc0f-4bd2-947f-17bb0a6b2dcf.jpg', 2, N'No', N'Chúng tôi chuyên cho thuê các loại Ô DÙ -THẢM TRẢI SÀN với các kích thước và chất lượng đảm bảo nhất để thực hiện chương trình.', NULL, NULL, NULL, 9, 0)
INSERT [dbo].[Product] ([ProductId], [ProductName], [CategoryId], [UnitInStock], [UnitPrice], [Image], [StatusId], [Brand], [Description], [DiscountDate], [Type], [Moneytype], [Quality], [Discount]) VALUES (39, N'NHÀ BẠT KHÔNG GIAN', 5, 1, 5000000, N'product/thumbnail-72bda683-621b-4761-9c53-7ac992cc2070.jpg', 2, N'No', N'Các loại nhà bạt không gian, dù che có trụ và không trụ. Nhà bạt không gian tổ chức sự kiện lớn-ngoài trời. Kích cỡ đa dạng. Lắp đặt và trang trí nhanh chóng, chuyên nghiệp.', NULL, NULL, NULL, 9, -4)
INSERT [dbo].[Product] ([ProductId], [ProductName], [CategoryId], [UnitInStock], [UnitPrice], [Image], [StatusId], [Brand], [Description], [DiscountDate], [Type], [Moneytype], [Quality], [Discount]) VALUES (40, N'LẮP ĐẶT SÂN KHẤU', 3, 2, 1500000, N'product/thumbnail-8c1ed7f1-e8fd-4f8e-8dcd-4edba87af389.jpg', 2, N'No', N'Sân khấu khung sắt, mặt gỗ, trải thảm đỏ, viền xung quanh. Chiều cao sân khấu đủ loại: 30 cm, 40 cm, 60 cm, 1m, 1,2m.', NULL, NULL, NULL, 10, 0)
INSERT [dbo].[Product] ([ProductId], [ProductName], [CategoryId], [UnitInStock], [UnitPrice], [Image], [StatusId], [Brand], [Description], [DiscountDate], [Type], [Moneytype], [Quality], [Discount]) VALUES (41, N'Máy chiếu ViewSonic M2 Pro', 3, 1, 500000, N'product/thumbnail-d3334608-e88c-48ca-b499-564af1369ae5.jpg', 2, N'ViewSonic ', N'ViewSonic M2 là dòng máy chiếu full hd tiện dụng để thiết lập ở hầu hết mọi nơi bạn muốn. Và bạn có thể dễ dàng thưởng thức những bộ phim, bài thuyết trình hoặc chương trình giải trí với các ứng dụng tích hợp sẵn. Và việc dễ dàng thiết lập trình chiếu nhanh chóng ở bất cứ vị trí nào. Với tính năng lấy nét và cân chỉnh tự động thì bạn không cần phải tự tay tuỳ chỉnh.', NULL, NULL, NULL, 10, 10)
INSERT [dbo].[Product] ([ProductId], [ProductName], [CategoryId], [UnitInStock], [UnitPrice], [Image], [StatusId], [Brand], [Description], [DiscountDate], [Type], [Moneytype], [Quality], [Discount]) VALUES (42, N'Máy chiếu', 1, 2, 10000000, N'https://example.com/images/projector.jpg', 1, N'MNO', N'Máy chiếu độ phân giải cao', NULL, NULL, NULL, 5, NULL)
SET IDENTITY_INSERT [dbo].[Product] OFF
GO
SET IDENTITY_INSERT [dbo].[ProductCode] ON 

INSERT [dbo].[ProductCode] ([CodeId], [ProductId], [Code], [IsAvailable], [qualityPercentage]) VALUES (1, 32, N'XRG056              ', 1, 70)
INSERT [dbo].[ProductCode] ([CodeId], [ProductId], [Code], [IsAvailable], [qualityPercentage]) VALUES (2, 32, N'MTZ732              ', 1, 60)
SET IDENTITY_INSERT [dbo].[ProductCode] OFF
GO
SET IDENTITY_INSERT [dbo].[ProductDetail] ON 

INSERT [dbo].[ProductDetail] ([DetailId], [ProductId], [CoreImage]) VALUES (1, 30, N'product/thumbnail-329d2e1c-1446-4734-b7f7-6aa250d8abd5.jpg')
INSERT [dbo].[ProductDetail] ([DetailId], [ProductId], [CoreImage]) VALUES (2, 30, N'product/thumbnail-60bc2469-ba2e-4126-ace0-90b9ee7e3c9a.jpg')
INSERT [dbo].[ProductDetail] ([DetailId], [ProductId], [CoreImage]) VALUES (3, 31, N'product/thumbnail-05da52da-4559-487a-8935-37888a1889d7.JPG')
INSERT [dbo].[ProductDetail] ([DetailId], [ProductId], [CoreImage]) VALUES (4, 31, N'product/thumbnail-9414f968-a51e-4f25-a855-e9b02b1291e7.jpg')
INSERT [dbo].[ProductDetail] ([DetailId], [ProductId], [CoreImage]) VALUES (5, 32, N'product/thumbnail-d434d198-d494-4cc0-b2d9-966f25849fb2.jpg')
INSERT [dbo].[ProductDetail] ([DetailId], [ProductId], [CoreImage]) VALUES (6, 33, N'product/thumbnail-50ff066e-5435-4e7d-a69b-480f263415f1.jpg')
INSERT [dbo].[ProductDetail] ([DetailId], [ProductId], [CoreImage]) VALUES (7, 34, N'product/thumbnail-ce488469-03aa-49ab-8b22-8a26b314dfc7.jpg')
INSERT [dbo].[ProductDetail] ([DetailId], [ProductId], [CoreImage]) VALUES (8, 35, N'product/thumbnail-044bf906-8001-4fdd-9eb3-7703ec96efe3.jpg')
INSERT [dbo].[ProductDetail] ([DetailId], [ProductId], [CoreImage]) VALUES (9, 36, N'product/thumbnail-67fa9df4-392f-4106-b212-c02ebc93bb9e.png')
INSERT [dbo].[ProductDetail] ([DetailId], [ProductId], [CoreImage]) VALUES (10, 37, N'product/thumbnail-df02ab09-58da-4f9a-9816-a19be3af2be0.jpg')
INSERT [dbo].[ProductDetail] ([DetailId], [ProductId], [CoreImage]) VALUES (11, 38, N'product/thumbnail-b594b6ab-764f-4c97-b45c-217599949b7e.jpg')
INSERT [dbo].[ProductDetail] ([DetailId], [ProductId], [CoreImage]) VALUES (12, 39, N'product/thumbnail-e33d7ea5-7618-4d88-8334-f571e6451b7a.jpg')
INSERT [dbo].[ProductDetail] ([DetailId], [ProductId], [CoreImage]) VALUES (13, 40, N'product/thumbnail-4563ed89-dabf-45a9-84ac-756e67e4687c.jpg')
INSERT [dbo].[ProductDetail] ([DetailId], [ProductId], [CoreImage]) VALUES (14, 41, N'product/thumbnail-24f8c7c4-2d21-4907-8402-f7386288449f.jpg')
INSERT [dbo].[ProductDetail] ([DetailId], [ProductId], [CoreImage]) VALUES (15, 41, N'product/thumbnail-a9238cd6-3f4e-4d3d-957c-fd8de3b6e704.jpg')
SET IDENTITY_INSERT [dbo].[ProductDetail] OFF
GO
SET IDENTITY_INSERT [dbo].[PStatus] ON 

INSERT [dbo].[PStatus] ([StatusId], [StatusNum], [StatusValue]) VALUES (1, 1, N'Sản phẩm hết hàng')
INSERT [dbo].[PStatus] ([StatusId], [StatusNum], [StatusValue]) VALUES (2, 2, N'Sản phẩm còn trong kho')
INSERT [dbo].[PStatus] ([StatusId], [StatusNum], [StatusValue]) VALUES (3, 3, N'Sản phẩm đang được khuyến mãi')
INSERT [dbo].[PStatus] ([StatusId], [StatusNum], [StatusValue]) VALUES (4, 4, N'Hỏng')
SET IDENTITY_INSERT [dbo].[PStatus] OFF
GO
SET IDENTITY_INSERT [dbo].[Rent] ON 

INSERT [dbo].[Rent] ([BuyId], [OrderId], [CustomerId]) VALUES (2, 18, 16)
SET IDENTITY_INSERT [dbo].[Rent] OFF
GO
SET IDENTITY_INSERT [dbo].[Role] ON 

INSERT [dbo].[Role] ([RoleId], [RoleName]) VALUES (1, N'Admin')
INSERT [dbo].[Role] ([RoleId], [RoleName]) VALUES (2, N'Blogger')
INSERT [dbo].[Role] ([RoleId], [RoleName]) VALUES (3, N'Consultant')
INSERT [dbo].[Role] ([RoleId], [RoleName]) VALUES (4, N'Customers')
INSERT [dbo].[Role] ([RoleId], [RoleName]) VALUES (5, N'Manager')
INSERT [dbo].[Role] ([RoleId], [RoleName]) VALUES (6, N'Sale')
INSERT [dbo].[Role] ([RoleId], [RoleName]) VALUES (7, N'Shipper')
INSERT [dbo].[Role] ([RoleId], [RoleName]) VALUES (8, N'Supplier')
INSERT [dbo].[Role] ([RoleId], [RoleName]) VALUES (9, N'Technical')
SET IDENTITY_INSERT [dbo].[Role] OFF
GO
SET IDENTITY_INSERT [dbo].[Sale] ON 

INSERT [dbo].[Sale] ([SaleId], [FullName], [Phone], [Address], [DOB], [StatusId]) VALUES (1, N'Mai Thị Trà My', N'02948573867', N'Nam Định', CAST(N'2023-07-21T02:14:06.157' AS DateTime), 2)
INSERT [dbo].[Sale] ([SaleId], [FullName], [Phone], [Address], [DOB], [StatusId]) VALUES (2, N'Trần Văn Bình', N'0901234567', N'Tp.HCM', CAST(N'1985-05-20T00:00:00.000' AS DateTime), 1)
INSERT [dbo].[Sale] ([SaleId], [FullName], [Phone], [Address], [DOB], [StatusId]) VALUES (3, N'Lê Minh Cung', N'0912345678', N'Đà Nẵng', CAST(N'2000-12-31T00:00:00.000' AS DateTime), 1)
INSERT [dbo].[Sale] ([SaleId], [FullName], [Phone], [Address], [DOB], [StatusId]) VALUES (4, N'Phạm Thanh Danh', N'0976543210', N'Hải Phòng', CAST(N'1975-06-15T00:00:00.000' AS DateTime), 2)
INSERT [dbo].[Sale] ([SaleId], [FullName], [Phone], [Address], [DOB], [StatusId]) VALUES (5, N'Đặng Hoàng Chung', N'0965432109', N'Nghệ An', CAST(N'1999-03-10T00:00:00.000' AS DateTime), 1)
SET IDENTITY_INSERT [dbo].[Sale] OFF
GO
SET IDENTITY_INSERT [dbo].[Shipper] ON 

INSERT [dbo].[Shipper] ([ShipperID], [FullName], [Phone], [Address], [DOB], [StatusId]) VALUES (1, N'Nguyễn Thành Long', N'0987433564', N'Hà Nội', CAST(N'1992-12-22T00:00:00.000' AS DateTime), 1)
INSERT [dbo].[Shipper] ([ShipperID], [FullName], [Phone], [Address], [DOB], [StatusId]) VALUES (2, N'Trần Bình Dang', N'0987963746', N'Hải Phòng', CAST(N'1994-03-12T00:00:00.000' AS DateTime), 2)
INSERT [dbo].[Shipper] ([ShipperID], [FullName], [Phone], [Address], [DOB], [StatusId]) VALUES (3, N'Nguyễn Trí Tuệ', N'0987673647', N'Hà Nội', CAST(N'1998-03-02T00:00:00.000' AS DateTime), 1)
INSERT [dbo].[Shipper] ([ShipperID], [FullName], [Phone], [Address], [DOB], [StatusId]) VALUES (4, N'Lê Hoàng Anh', N'0937846573', N'Hà Tĩnh', CAST(N'1996-05-27T00:00:00.000' AS DateTime), 2)
INSERT [dbo].[Shipper] ([ShipperID], [FullName], [Phone], [Address], [DOB], [StatusId]) VALUES (5, N'Kim Song Ngưu', N'0926374657', N'Hưng Yên', CAST(N'1991-07-29T00:00:00.000' AS DateTime), 1)
SET IDENTITY_INSERT [dbo].[Shipper] OFF
GO
SET IDENTITY_INSERT [dbo].[Status] ON 

INSERT [dbo].[Status] ([StatusId], [StatusNum], [StatusValue]) VALUES (1, 1, N'Chờ duyệt bởi manager')
INSERT [dbo].[Status] ([StatusId], [StatusNum], [StatusValue]) VALUES (2, 2, N'Chờ duyệt bởi saler')
INSERT [dbo].[Status] ([StatusId], [StatusNum], [StatusValue]) VALUES (3, 3, N'Chờ duyệt bởi shipper')
INSERT [dbo].[Status] ([StatusId], [StatusNum], [StatusValue]) VALUES (4, 4, N'Chờ shipper xác nhận giao hàng thành công')
INSERT [dbo].[Status] ([StatusId], [StatusNum], [StatusValue]) VALUES (5, 5, N'Giao hàng thành công')
INSERT [dbo].[Status] ([StatusId], [StatusNum], [StatusValue]) VALUES (6, 6, N'Đơn hàng đã được nhận')
INSERT [dbo].[Status] ([StatusId], [StatusNum], [StatusValue]) VALUES (7, 0, N'Đơn hàng đã bị hủy')
INSERT [dbo].[Status] ([StatusId], [StatusNum], [StatusValue]) VALUES (8, 7, N'Đơn hàng cần hoàn trả')
INSERT [dbo].[Status] ([StatusId], [StatusNum], [StatusValue]) VALUES (9, 8, N'Đơn hàng đã dùng xong')
INSERT [dbo].[Status] ([StatusId], [StatusNum], [StatusValue]) VALUES (10, NULL, N'Đơn hàng đã về kho')
SET IDENTITY_INSERT [dbo].[Status] OFF
GO
SET IDENTITY_INSERT [dbo].[Supplier] ON 

INSERT [dbo].[Supplier] ([SupplierId], [SupplierName], [Phone], [Address], [DOB], [Avatar], [StatusId]) VALUES (5, N'Omagla', N'0123456789', N'123 Ðuờng, Thành phố', CAST(N'2021-01-01T00:00:00.000' AS DateTime), N'Ajax.jpg', 1)
INSERT [dbo].[Supplier] ([SupplierId], [SupplierName], [Phone], [Address], [DOB], [Avatar], [StatusId]) VALUES (6, N'Kinatra', N'0234567890', N'456 Ðại lộ, Thị trấn', CAST(N'2021-02-02T00:00:00.000' AS DateTime), N'B.jpg', 1)
INSERT [dbo].[Supplier] ([SupplierId], [SupplierName], [Phone], [Address], [DOB], [Avatar], [StatusId]) VALUES (7, N'Hombis 3', N'0345678901', N'789 Ðuờng, Tỉnh', CAST(N'2021-03-03T00:00:00.000' AS DateTime), N'C.jpg', 1)
INSERT [dbo].[Supplier] ([SupplierId], [SupplierName], [Phone], [Address], [DOB], [Avatar], [StatusId]) VALUES (8, N'Koolor 4', N'0456789012', N'147 Boulevard, Quận', CAST(N'2021-04-04T00:00:00.000' AS DateTime), N'D.jpg', 1)
INSERT [dbo].[Supplier] ([SupplierId], [SupplierName], [Phone], [Address], [DOB], [Avatar], [StatusId]) VALUES (9, N'Divida 5', N'0567890123', N'258 Ngõ, Huyện', CAST(N'2021-05-05T00:00:00.000' AS DateTime), N'E.jpg', 1)
SET IDENTITY_INSERT [dbo].[Supplier] OFF
GO
SET IDENTITY_INSERT [dbo].[SupplierAlert] ON 

INSERT [dbo].[SupplierAlert] ([SId], [SProductId], [Quantity], [SupplierId], [Message], [StatusId], [CurrentDate], [ThreeDays], [OneWeeks], [Price]) VALUES (1, 50, 2, 5, N'Chúng tôi muốn nhập với số lượng 2 sản phẩm #50 Máy chiếu từ nhà cung cấp Omagla. Vui lòng xác nhận hoặc liên hệ với shop để biết thêm thông tin chi tiết. Xin cảm ơn! ', 0, CAST(N'2023-08-11T13:42:53.293' AS DateTime), NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[SupplierAlert] OFF
GO
SET IDENTITY_INSERT [dbo].[SupplierProduct] ON 

INSERT [dbo].[SupplierProduct] ([ProductId], [ProductName], [Description], [CategoryId], [Image], [Brand], [StatusId], [UnitInStock], [UnitPrice], [NumberOfUse], [Quality]) VALUES (46, N'Sân khấu di động', N'Sân khấu di động chất lượng cao', 1, N'https://example.com/images/stage.jpg', N'ABC', 1, 10, 5000000, 100, 5)
INSERT [dbo].[SupplierProduct] ([ProductId], [ProductName], [Description], [CategoryId], [Image], [Brand], [StatusId], [UnitInStock], [UnitPrice], [NumberOfUse], [Quality]) VALUES (47, N'Màng chiếu phía trước', N'Màng chiếu phía trước 120 inch', 2, N'https://example.com/images/screen.jpg', N'DEF', 2, 5, 7000000, 50, 5)
INSERT [dbo].[SupplierProduct] ([ProductId], [ProductName], [Description], [CategoryId], [Image], [Brand], [StatusId], [UnitInStock], [UnitPrice], [NumberOfUse], [Quality]) VALUES (48, N'Loa kéo di động', N'Loa kéo di động với chất lượng âm thanh tốt', 3, N'https://example.com/images/speaker.jpg', N'GHI', 1, 8, 4000000, 80, 5)
INSERT [dbo].[SupplierProduct] ([ProductId], [ProductName], [Description], [CategoryId], [Image], [Brand], [StatusId], [UnitInStock], [UnitPrice], [NumberOfUse], [Quality]) VALUES (49, N'Đèn LED', N'Đèn LED có thể điều chỉnh màu sắc', 4, N'https://example.com/images/ledlight.jpg', N'JKL', 2, 20, 2000000, 200, 5)
INSERT [dbo].[SupplierProduct] ([ProductId], [ProductName], [Description], [CategoryId], [Image], [Brand], [StatusId], [UnitInStock], [UnitPrice], [NumberOfUse], [Quality]) VALUES (50, N'Máy chiếu', N'Máy chiếu độ phân giải cao', 1, N'https://example.com/images/projector.jpg', N'MNO', 1, 1, 10000000, 30, 5)
INSERT [dbo].[SupplierProduct] ([ProductId], [ProductName], [Description], [CategoryId], [Image], [Brand], [StatusId], [UnitInStock], [UnitPrice], [NumberOfUse], [Quality]) VALUES (51, N'Máy tính bảng', N'Máy tính bảng Samsung Galaxy Tab A7', 1, N'https://example.com/images/tablet.jpg', N'PQR', 2, 15, 8000000, 150, 5)
INSERT [dbo].[SupplierProduct] ([ProductId], [ProductName], [Description], [CategoryId], [Image], [Brand], [StatusId], [UnitInStock], [UnitPrice], [NumberOfUse], [Quality]) VALUES (52, N'Cổng chuyển đổi', N'Cổng chuyển đổi HDMI sang VGA', 4, N'https://example.com/images/adapter.jpg', N'STU', 1, 40, 500000, 400, 5)
INSERT [dbo].[SupplierProduct] ([ProductId], [ProductName], [Description], [CategoryId], [Image], [Brand], [StatusId], [UnitInStock], [UnitPrice], [NumberOfUse], [Quality]) VALUES (53, N'Máy lạnh di động', N'Máy lạnh di động cho sự kiện ngoài trời', 2, N'https://example.com/images/airconditioner.jpg', N'VWX', 2, 2, 15000000, 20, 5)
INSERT [dbo].[SupplierProduct] ([ProductId], [ProductName], [Description], [CategoryId], [Image], [Brand], [StatusId], [UnitInStock], [UnitPrice], [NumberOfUse], [Quality]) VALUES (54, N'Bàn ghế', N'Bàn ghế phục vụ khách hàng', 3, N'https://example.com/images/tableandchair.jpg', N'YZA', 1, 30, 1000000, 300, 5)
INSERT [dbo].[SupplierProduct] ([ProductId], [ProductName], [Description], [CategoryId], [Image], [Brand], [StatusId], [UnitInStock], [UnitPrice], [NumberOfUse], [Quality]) VALUES (55, N'Màn hình LED', N'Màn hình LED kích thước lớn', 3, N'https://example.com/images/ledscreen.jpg', N'BCD', 2, 5, 20000000, 50, 5)
INSERT [dbo].[SupplierProduct] ([ProductId], [ProductName], [Description], [CategoryId], [Image], [Brand], [StatusId], [UnitInStock], [UnitPrice], [NumberOfUse], [Quality]) VALUES (56, N'Âm ly', N'Âm ly cho hệ thống âm thanh', 3, N'https://example.com/images/amplifier.jpg', N'EFG', 1, 6, 6000000, 60, 5)
INSERT [dbo].[SupplierProduct] ([ProductId], [ProductName], [Description], [CategoryId], [Image], [Brand], [StatusId], [UnitInStock], [UnitPrice], [NumberOfUse], [Quality]) VALUES (57, N'Máy phát điện', N'Máy phát điện cho sự kiện ngoài trời', 2, N'https://example.com/images/generator.jpg', N'HIJ', 2, 1, 50000000, 10, 5)
INSERT [dbo].[SupplierProduct] ([ProductId], [ProductName], [Description], [CategoryId], [Image], [Brand], [StatusId], [UnitInStock], [UnitPrice], [NumberOfUse], [Quality]) VALUES (58, N'Bộ micro không dây', N'Bộ micro không dây chất lượng cao', 2, N'https://example.com/images/microphone.jpg', N'KLM', 1, 7, 3000000, 70, 5)
INSERT [dbo].[SupplierProduct] ([ProductId], [ProductName], [Description], [CategoryId], [Image], [Brand], [StatusId], [UnitInStock], [UnitPrice], [NumberOfUse], [Quality]) VALUES (59, N'Màn hình cảm ứng', N'Màn hình cảm ứng kích thước lớn', 1, N'https://example.com/images/touchscreen.jpg', N'NOP', 2, 3, 15000000, 30, 5)
INSERT [dbo].[SupplierProduct] ([ProductId], [ProductName], [Description], [CategoryId], [Image], [Brand], [StatusId], [UnitInStock], [UnitPrice], [NumberOfUse], [Quality]) VALUES (60, N'Cục phát wifi', N'Cục phát wifi cho sự kiện', 1, N'https://example.com/images/wifirouter.jpg', N'QRS', 1, 5, 2000000, 50, 5)
INSERT [dbo].[SupplierProduct] ([ProductId], [ProductName], [Description], [CategoryId], [Image], [Brand], [StatusId], [UnitInStock], [UnitPrice], [NumberOfUse], [Quality]) VALUES (61, N'Máy quay phim', N'Máy quay phim chuyên nghiệp', 3, N'https://example.com/images/camera.jpg', N'TUV', 2, 4, 2000000, 50, 5)
INSERT [dbo].[SupplierProduct] ([ProductId], [ProductName], [Description], [CategoryId], [Image], [Brand], [StatusId], [UnitInStock], [UnitPrice], [NumberOfUse], [Quality]) VALUES (62, N'Đèn 7 màu', N'Thiết bị chiếu sáng ngoài trời', 2, N'https://example.com/images/lighting.jpg', N'ABC', 1, 40, 750000, 20, 5)
INSERT [dbo].[SupplierProduct] ([ProductId], [ProductName], [Description], [CategoryId], [Image], [Brand], [StatusId], [UnitInStock], [UnitPrice], [NumberOfUse], [Quality]) VALUES (63, N'Bàn trà', N'Bàn trà cho phục vụ khách hàng', 2, N'https://example.com/images/coffeetable.jpg', N'DEF', 1, 20, 1500000, 200, 5)
INSERT [dbo].[SupplierProduct] ([ProductId], [ProductName], [Description], [CategoryId], [Image], [Brand], [StatusId], [UnitInStock], [UnitPrice], [NumberOfUse], [Quality]) VALUES (64, N'Máy cắt giấy', N'Máy cắt giấy tự động', 3, N'https://example.com/images/papercutter.jpg', N'GHI', 2, 2, 5000000, 30, 5)
INSERT [dbo].[SupplierProduct] ([ProductId], [ProductName], [Description], [CategoryId], [Image], [Brand], [StatusId], [UnitInStock], [UnitPrice], [NumberOfUse], [Quality]) VALUES (65, N'Thiết bị phun sương', N'Thiết bị phun sương cho sự kiện ngoài trời', 1, N'https://example.com/images/mistmachine.jpg', N'JKL', 1, 3, 2000000, 20, 5)
INSERT [dbo].[SupplierProduct] ([ProductId], [ProductName], [Description], [CategoryId], [Image], [Brand], [StatusId], [UnitInStock], [UnitPrice], [NumberOfUse], [Quality]) VALUES (66, N'Máy in', N'Máy in laser đen trắng', 2, N'https://example.com/images/printer.jpg', N'MNO', 2, 8, 4000000, 80, 5)
INSERT [dbo].[SupplierProduct] ([ProductId], [ProductName], [Description], [CategoryId], [Image], [Brand], [StatusId], [UnitInStock], [UnitPrice], [NumberOfUse], [Quality]) VALUES (67, N'Thiết bị chiếu hình', N'Thiết bị chiếu hình cho sự kiện ngoài trời', 5, N'https://example.com/images/imageprojector.jpg', N'PQR', 1, 3, 10000000, 30, 5)
INSERT [dbo].[SupplierProduct] ([ProductId], [ProductName], [Description], [CategoryId], [Image], [Brand], [StatusId], [UnitInStock], [UnitPrice], [NumberOfUse], [Quality]) VALUES (68, N'Bộ chia mạng', N'Bộ chia mạng 24 cổng', 3, N'https://example.com/images/networkswitch.jpg', N'STU', 2, 6, 8000000, 60, 5)
INSERT [dbo].[SupplierProduct] ([ProductId], [ProductName], [Description], [CategoryId], [Image], [Brand], [StatusId], [UnitInStock], [UnitPrice], [NumberOfUse], [Quality]) VALUES (69, N'Bộ định tuyến', N'Bộ định tuyến wifi cho sự kiện', 1, N'https://example.com/images/routersetup.jpg', N'VWX', 1, 4, 10000000, 40, 5)
INSERT [dbo].[SupplierProduct] ([ProductId], [ProductName], [Description], [CategoryId], [Image], [Brand], [StatusId], [UnitInStock], [UnitPrice], [NumberOfUse], [Quality]) VALUES (70, N'Máy hút bụi', N'Máy hút bụi công nghiệp', 1, N'https://example.com/images/vacuumcleaner.jpg', N'YZA', 2, 2, 12000000, 20, 5)
INSERT [dbo].[SupplierProduct] ([ProductId], [ProductName], [Description], [CategoryId], [Image], [Brand], [StatusId], [UnitInStock], [UnitPrice], [NumberOfUse], [Quality]) VALUES (71, N'Thiết bị chiếu video', N'Thiết bị chiếu video kích thước lớn', 4, N'https://example.com/images/videoprojector.jpg', N'BCD', 1, 4, 15000000, 40, 5)
INSERT [dbo].[SupplierProduct] ([ProductId], [ProductName], [Description], [CategoryId], [Image], [Brand], [StatusId], [UnitInStock], [UnitPrice], [NumberOfUse], [Quality]) VALUES (72, N'Máy phun sơn', N'Máy phun sơn điện tử', 1, N'https://example.com/images/paintsprayer.jpg', N'EFG', 2, 3, 5000000, 30, 5)
INSERT [dbo].[SupplierProduct] ([ProductId], [ProductName], [Description], [CategoryId], [Image], [Brand], [StatusId], [UnitInStock], [UnitPrice], [NumberOfUse], [Quality]) VALUES (73, N'Thiết bị trình chiếu', N'Thiết bị trình chiếu bằng laser', 2, N'https://example.com/images/laserpointer.jpg', N'HIJ', 1, 2, 2000000, 20, 5)
INSERT [dbo].[SupplierProduct] ([ProductId], [ProductName], [Description], [CategoryId], [Image], [Brand], [StatusId], [UnitInStock], [UnitPrice], [NumberOfUse], [Quality]) VALUES (74, N'Máy ảnh', N'Máy ảnh chuyên nghiệp', 3, N'https://example.com/images/camera.jpg', N'KLM', 2, 4, 10000000, 40, 5)
INSERT [dbo].[SupplierProduct] ([ProductId], [ProductName], [Description], [CategoryId], [Image], [Brand], [StatusId], [UnitInStock], [UnitPrice], [NumberOfUse], [Quality]) VALUES (75, N'Thiết bị chiếu ánh sáng', N'Thiết bị chiếu ánh sáng cho sự kiện ngoài trời', 4, N'https://example.com/images/lightprojector.jpg', N'NOP', 1, 3, 12000000, 30, 5)
INSERT [dbo].[SupplierProduct] ([ProductId], [ProductName], [Description], [CategoryId], [Image], [Brand], [StatusId], [UnitInStock], [UnitPrice], [NumberOfUse], [Quality]) VALUES (76, N'Đèn huỳnh quanh', N'Đèn giúp tổ chức sự kiện ban dêm', 4, N'https://example.com/images/coffeetable.jpg', N'MDT', 2, 120, 4000000, 5, 5)
SET IDENTITY_INSERT [dbo].[SupplierProduct] OFF
GO
SET IDENTITY_INSERT [dbo].[SupplierStock] ON 

INSERT [dbo].[SupplierStock] ([StockId], [ProductId], [SupplierId]) VALUES (1, 46, 1)
INSERT [dbo].[SupplierStock] ([StockId], [ProductId], [SupplierId]) VALUES (2, 47, 2)
INSERT [dbo].[SupplierStock] ([StockId], [ProductId], [SupplierId]) VALUES (3, 48, 3)
INSERT [dbo].[SupplierStock] ([StockId], [ProductId], [SupplierId]) VALUES (4, 49, 4)
INSERT [dbo].[SupplierStock] ([StockId], [ProductId], [SupplierId]) VALUES (5, 50, 5)
INSERT [dbo].[SupplierStock] ([StockId], [ProductId], [SupplierId]) VALUES (6, 51, 1)
INSERT [dbo].[SupplierStock] ([StockId], [ProductId], [SupplierId]) VALUES (7, 52, 2)
INSERT [dbo].[SupplierStock] ([StockId], [ProductId], [SupplierId]) VALUES (8, 53, 3)
INSERT [dbo].[SupplierStock] ([StockId], [ProductId], [SupplierId]) VALUES (9, 54, 4)
INSERT [dbo].[SupplierStock] ([StockId], [ProductId], [SupplierId]) VALUES (10, 55, 5)
INSERT [dbo].[SupplierStock] ([StockId], [ProductId], [SupplierId]) VALUES (11, 56, 1)
INSERT [dbo].[SupplierStock] ([StockId], [ProductId], [SupplierId]) VALUES (12, 57, 2)
INSERT [dbo].[SupplierStock] ([StockId], [ProductId], [SupplierId]) VALUES (13, 58, 3)
INSERT [dbo].[SupplierStock] ([StockId], [ProductId], [SupplierId]) VALUES (14, 59, 4)
INSERT [dbo].[SupplierStock] ([StockId], [ProductId], [SupplierId]) VALUES (15, 60, 5)
INSERT [dbo].[SupplierStock] ([StockId], [ProductId], [SupplierId]) VALUES (16, 61, 1)
INSERT [dbo].[SupplierStock] ([StockId], [ProductId], [SupplierId]) VALUES (17, 62, 2)
INSERT [dbo].[SupplierStock] ([StockId], [ProductId], [SupplierId]) VALUES (18, 63, 3)
INSERT [dbo].[SupplierStock] ([StockId], [ProductId], [SupplierId]) VALUES (19, 64, 4)
INSERT [dbo].[SupplierStock] ([StockId], [ProductId], [SupplierId]) VALUES (20, 65, 5)
INSERT [dbo].[SupplierStock] ([StockId], [ProductId], [SupplierId]) VALUES (21, 66, 1)
INSERT [dbo].[SupplierStock] ([StockId], [ProductId], [SupplierId]) VALUES (22, 67, 2)
INSERT [dbo].[SupplierStock] ([StockId], [ProductId], [SupplierId]) VALUES (23, 68, 3)
INSERT [dbo].[SupplierStock] ([StockId], [ProductId], [SupplierId]) VALUES (24, 69, 4)
INSERT [dbo].[SupplierStock] ([StockId], [ProductId], [SupplierId]) VALUES (25, 70, 5)
INSERT [dbo].[SupplierStock] ([StockId], [ProductId], [SupplierId]) VALUES (26, 71, 1)
INSERT [dbo].[SupplierStock] ([StockId], [ProductId], [SupplierId]) VALUES (27, 72, 2)
INSERT [dbo].[SupplierStock] ([StockId], [ProductId], [SupplierId]) VALUES (28, 73, 3)
INSERT [dbo].[SupplierStock] ([StockId], [ProductId], [SupplierId]) VALUES (29, 74, 4)
INSERT [dbo].[SupplierStock] ([StockId], [ProductId], [SupplierId]) VALUES (30, 75, 5)
INSERT [dbo].[SupplierStock] ([StockId], [ProductId], [SupplierId]) VALUES (31, 46, 1)
INSERT [dbo].[SupplierStock] ([StockId], [ProductId], [SupplierId]) VALUES (32, 47, 2)
INSERT [dbo].[SupplierStock] ([StockId], [ProductId], [SupplierId]) VALUES (33, 48, 3)
INSERT [dbo].[SupplierStock] ([StockId], [ProductId], [SupplierId]) VALUES (34, 49, 4)
INSERT [dbo].[SupplierStock] ([StockId], [ProductId], [SupplierId]) VALUES (35, 50, 5)
INSERT [dbo].[SupplierStock] ([StockId], [ProductId], [SupplierId]) VALUES (36, 51, 1)
INSERT [dbo].[SupplierStock] ([StockId], [ProductId], [SupplierId]) VALUES (37, 52, 2)
INSERT [dbo].[SupplierStock] ([StockId], [ProductId], [SupplierId]) VALUES (38, 53, 3)
INSERT [dbo].[SupplierStock] ([StockId], [ProductId], [SupplierId]) VALUES (39, 54, 4)
INSERT [dbo].[SupplierStock] ([StockId], [ProductId], [SupplierId]) VALUES (40, 55, 5)
INSERT [dbo].[SupplierStock] ([StockId], [ProductId], [SupplierId]) VALUES (41, 56, 1)
INSERT [dbo].[SupplierStock] ([StockId], [ProductId], [SupplierId]) VALUES (42, 57, 2)
INSERT [dbo].[SupplierStock] ([StockId], [ProductId], [SupplierId]) VALUES (43, 58, 3)
INSERT [dbo].[SupplierStock] ([StockId], [ProductId], [SupplierId]) VALUES (44, 59, 4)
INSERT [dbo].[SupplierStock] ([StockId], [ProductId], [SupplierId]) VALUES (45, 60, 5)
INSERT [dbo].[SupplierStock] ([StockId], [ProductId], [SupplierId]) VALUES (46, 61, 1)
INSERT [dbo].[SupplierStock] ([StockId], [ProductId], [SupplierId]) VALUES (47, 62, 2)
INSERT [dbo].[SupplierStock] ([StockId], [ProductId], [SupplierId]) VALUES (48, 63, 3)
INSERT [dbo].[SupplierStock] ([StockId], [ProductId], [SupplierId]) VALUES (49, 64, 4)
INSERT [dbo].[SupplierStock] ([StockId], [ProductId], [SupplierId]) VALUES (50, 65, 5)
INSERT [dbo].[SupplierStock] ([StockId], [ProductId], [SupplierId]) VALUES (51, 66, 1)
INSERT [dbo].[SupplierStock] ([StockId], [ProductId], [SupplierId]) VALUES (52, 67, 2)
INSERT [dbo].[SupplierStock] ([StockId], [ProductId], [SupplierId]) VALUES (53, 68, 3)
INSERT [dbo].[SupplierStock] ([StockId], [ProductId], [SupplierId]) VALUES (54, 69, 4)
INSERT [dbo].[SupplierStock] ([StockId], [ProductId], [SupplierId]) VALUES (55, 70, 5)
INSERT [dbo].[SupplierStock] ([StockId], [ProductId], [SupplierId]) VALUES (56, 71, 1)
INSERT [dbo].[SupplierStock] ([StockId], [ProductId], [SupplierId]) VALUES (57, 72, 2)
INSERT [dbo].[SupplierStock] ([StockId], [ProductId], [SupplierId]) VALUES (58, 73, 3)
INSERT [dbo].[SupplierStock] ([StockId], [ProductId], [SupplierId]) VALUES (59, 74, 4)
INSERT [dbo].[SupplierStock] ([StockId], [ProductId], [SupplierId]) VALUES (60, 75, 5)
INSERT [dbo].[SupplierStock] ([StockId], [ProductId], [SupplierId]) VALUES (61, 46, 1)
INSERT [dbo].[SupplierStock] ([StockId], [ProductId], [SupplierId]) VALUES (62, 47, 2)
INSERT [dbo].[SupplierStock] ([StockId], [ProductId], [SupplierId]) VALUES (63, 48, 3)
INSERT [dbo].[SupplierStock] ([StockId], [ProductId], [SupplierId]) VALUES (64, 49, 4)
INSERT [dbo].[SupplierStock] ([StockId], [ProductId], [SupplierId]) VALUES (65, 50, 5)
INSERT [dbo].[SupplierStock] ([StockId], [ProductId], [SupplierId]) VALUES (66, 51, 1)
INSERT [dbo].[SupplierStock] ([StockId], [ProductId], [SupplierId]) VALUES (67, 52, 2)
INSERT [dbo].[SupplierStock] ([StockId], [ProductId], [SupplierId]) VALUES (68, 53, 3)
INSERT [dbo].[SupplierStock] ([StockId], [ProductId], [SupplierId]) VALUES (69, 54, 4)
INSERT [dbo].[SupplierStock] ([StockId], [ProductId], [SupplierId]) VALUES (70, 55, 5)
INSERT [dbo].[SupplierStock] ([StockId], [ProductId], [SupplierId]) VALUES (71, 56, 1)
INSERT [dbo].[SupplierStock] ([StockId], [ProductId], [SupplierId]) VALUES (72, 57, 2)
INSERT [dbo].[SupplierStock] ([StockId], [ProductId], [SupplierId]) VALUES (73, 58, 3)
INSERT [dbo].[SupplierStock] ([StockId], [ProductId], [SupplierId]) VALUES (74, 59, 4)
INSERT [dbo].[SupplierStock] ([StockId], [ProductId], [SupplierId]) VALUES (75, 60, 5)
INSERT [dbo].[SupplierStock] ([StockId], [ProductId], [SupplierId]) VALUES (76, 61, 1)
INSERT [dbo].[SupplierStock] ([StockId], [ProductId], [SupplierId]) VALUES (77, 62, 2)
INSERT [dbo].[SupplierStock] ([StockId], [ProductId], [SupplierId]) VALUES (78, 63, 3)
INSERT [dbo].[SupplierStock] ([StockId], [ProductId], [SupplierId]) VALUES (79, 64, 4)
INSERT [dbo].[SupplierStock] ([StockId], [ProductId], [SupplierId]) VALUES (80, 65, 5)
INSERT [dbo].[SupplierStock] ([StockId], [ProductId], [SupplierId]) VALUES (81, 66, 1)
INSERT [dbo].[SupplierStock] ([StockId], [ProductId], [SupplierId]) VALUES (82, 67, 2)
INSERT [dbo].[SupplierStock] ([StockId], [ProductId], [SupplierId]) VALUES (83, 68, 3)
INSERT [dbo].[SupplierStock] ([StockId], [ProductId], [SupplierId]) VALUES (84, 69, 4)
INSERT [dbo].[SupplierStock] ([StockId], [ProductId], [SupplierId]) VALUES (85, 70, 5)
INSERT [dbo].[SupplierStock] ([StockId], [ProductId], [SupplierId]) VALUES (86, 71, 1)
INSERT [dbo].[SupplierStock] ([StockId], [ProductId], [SupplierId]) VALUES (87, 72, 2)
INSERT [dbo].[SupplierStock] ([StockId], [ProductId], [SupplierId]) VALUES (88, 73, 3)
INSERT [dbo].[SupplierStock] ([StockId], [ProductId], [SupplierId]) VALUES (89, 74, 4)
INSERT [dbo].[SupplierStock] ([StockId], [ProductId], [SupplierId]) VALUES (90, 75, 5)
INSERT [dbo].[SupplierStock] ([StockId], [ProductId], [SupplierId]) VALUES (91, 46, 1)
INSERT [dbo].[SupplierStock] ([StockId], [ProductId], [SupplierId]) VALUES (92, 47, 2)
INSERT [dbo].[SupplierStock] ([StockId], [ProductId], [SupplierId]) VALUES (93, 48, 3)
INSERT [dbo].[SupplierStock] ([StockId], [ProductId], [SupplierId]) VALUES (94, 49, 4)
INSERT [dbo].[SupplierStock] ([StockId], [ProductId], [SupplierId]) VALUES (95, 50, 5)
INSERT [dbo].[SupplierStock] ([StockId], [ProductId], [SupplierId]) VALUES (96, 51, 1)
INSERT [dbo].[SupplierStock] ([StockId], [ProductId], [SupplierId]) VALUES (97, 52, 2)
INSERT [dbo].[SupplierStock] ([StockId], [ProductId], [SupplierId]) VALUES (98, 53, 3)
INSERT [dbo].[SupplierStock] ([StockId], [ProductId], [SupplierId]) VALUES (99, 54, 4)
GO
INSERT [dbo].[SupplierStock] ([StockId], [ProductId], [SupplierId]) VALUES (100, 55, 5)
INSERT [dbo].[SupplierStock] ([StockId], [ProductId], [SupplierId]) VALUES (101, 56, 1)
INSERT [dbo].[SupplierStock] ([StockId], [ProductId], [SupplierId]) VALUES (102, 57, 2)
INSERT [dbo].[SupplierStock] ([StockId], [ProductId], [SupplierId]) VALUES (103, 58, 3)
INSERT [dbo].[SupplierStock] ([StockId], [ProductId], [SupplierId]) VALUES (104, 59, 4)
INSERT [dbo].[SupplierStock] ([StockId], [ProductId], [SupplierId]) VALUES (105, 60, 5)
INSERT [dbo].[SupplierStock] ([StockId], [ProductId], [SupplierId]) VALUES (106, 61, 1)
INSERT [dbo].[SupplierStock] ([StockId], [ProductId], [SupplierId]) VALUES (107, 62, 2)
INSERT [dbo].[SupplierStock] ([StockId], [ProductId], [SupplierId]) VALUES (108, 63, 3)
INSERT [dbo].[SupplierStock] ([StockId], [ProductId], [SupplierId]) VALUES (109, 64, 4)
INSERT [dbo].[SupplierStock] ([StockId], [ProductId], [SupplierId]) VALUES (110, 65, 5)
INSERT [dbo].[SupplierStock] ([StockId], [ProductId], [SupplierId]) VALUES (111, 66, 1)
INSERT [dbo].[SupplierStock] ([StockId], [ProductId], [SupplierId]) VALUES (112, 67, 2)
INSERT [dbo].[SupplierStock] ([StockId], [ProductId], [SupplierId]) VALUES (113, 68, 3)
INSERT [dbo].[SupplierStock] ([StockId], [ProductId], [SupplierId]) VALUES (114, 69, 4)
INSERT [dbo].[SupplierStock] ([StockId], [ProductId], [SupplierId]) VALUES (115, 70, 5)
INSERT [dbo].[SupplierStock] ([StockId], [ProductId], [SupplierId]) VALUES (116, 71, 1)
INSERT [dbo].[SupplierStock] ([StockId], [ProductId], [SupplierId]) VALUES (117, 72, 2)
INSERT [dbo].[SupplierStock] ([StockId], [ProductId], [SupplierId]) VALUES (118, 73, 3)
INSERT [dbo].[SupplierStock] ([StockId], [ProductId], [SupplierId]) VALUES (119, 74, 4)
INSERT [dbo].[SupplierStock] ([StockId], [ProductId], [SupplierId]) VALUES (120, 75, 5)
INSERT [dbo].[SupplierStock] ([StockId], [ProductId], [SupplierId]) VALUES (121, 46, 1)
INSERT [dbo].[SupplierStock] ([StockId], [ProductId], [SupplierId]) VALUES (122, 47, 2)
INSERT [dbo].[SupplierStock] ([StockId], [ProductId], [SupplierId]) VALUES (123, 48, 3)
INSERT [dbo].[SupplierStock] ([StockId], [ProductId], [SupplierId]) VALUES (124, 49, 4)
INSERT [dbo].[SupplierStock] ([StockId], [ProductId], [SupplierId]) VALUES (125, 50, 5)
INSERT [dbo].[SupplierStock] ([StockId], [ProductId], [SupplierId]) VALUES (126, 51, 1)
INSERT [dbo].[SupplierStock] ([StockId], [ProductId], [SupplierId]) VALUES (127, 52, 2)
INSERT [dbo].[SupplierStock] ([StockId], [ProductId], [SupplierId]) VALUES (128, 53, 3)
INSERT [dbo].[SupplierStock] ([StockId], [ProductId], [SupplierId]) VALUES (129, 54, 4)
INSERT [dbo].[SupplierStock] ([StockId], [ProductId], [SupplierId]) VALUES (130, 55, 5)
INSERT [dbo].[SupplierStock] ([StockId], [ProductId], [SupplierId]) VALUES (131, 56, 1)
INSERT [dbo].[SupplierStock] ([StockId], [ProductId], [SupplierId]) VALUES (132, 57, 2)
INSERT [dbo].[SupplierStock] ([StockId], [ProductId], [SupplierId]) VALUES (133, 58, 3)
INSERT [dbo].[SupplierStock] ([StockId], [ProductId], [SupplierId]) VALUES (134, 59, 4)
INSERT [dbo].[SupplierStock] ([StockId], [ProductId], [SupplierId]) VALUES (135, 60, 5)
INSERT [dbo].[SupplierStock] ([StockId], [ProductId], [SupplierId]) VALUES (136, 61, 1)
INSERT [dbo].[SupplierStock] ([StockId], [ProductId], [SupplierId]) VALUES (137, 62, 2)
INSERT [dbo].[SupplierStock] ([StockId], [ProductId], [SupplierId]) VALUES (138, 63, 3)
INSERT [dbo].[SupplierStock] ([StockId], [ProductId], [SupplierId]) VALUES (139, 64, 4)
INSERT [dbo].[SupplierStock] ([StockId], [ProductId], [SupplierId]) VALUES (140, 65, 5)
INSERT [dbo].[SupplierStock] ([StockId], [ProductId], [SupplierId]) VALUES (141, 66, 1)
INSERT [dbo].[SupplierStock] ([StockId], [ProductId], [SupplierId]) VALUES (142, 67, 2)
INSERT [dbo].[SupplierStock] ([StockId], [ProductId], [SupplierId]) VALUES (143, 68, 3)
INSERT [dbo].[SupplierStock] ([StockId], [ProductId], [SupplierId]) VALUES (144, 69, 4)
INSERT [dbo].[SupplierStock] ([StockId], [ProductId], [SupplierId]) VALUES (145, 70, 5)
INSERT [dbo].[SupplierStock] ([StockId], [ProductId], [SupplierId]) VALUES (146, 71, 1)
INSERT [dbo].[SupplierStock] ([StockId], [ProductId], [SupplierId]) VALUES (147, 72, 2)
INSERT [dbo].[SupplierStock] ([StockId], [ProductId], [SupplierId]) VALUES (148, 73, 3)
INSERT [dbo].[SupplierStock] ([StockId], [ProductId], [SupplierId]) VALUES (149, 74, 4)
INSERT [dbo].[SupplierStock] ([StockId], [ProductId], [SupplierId]) VALUES (150, 75, 5)
INSERT [dbo].[SupplierStock] ([StockId], [ProductId], [SupplierId]) VALUES (151, 46, 1)
INSERT [dbo].[SupplierStock] ([StockId], [ProductId], [SupplierId]) VALUES (152, 47, 2)
INSERT [dbo].[SupplierStock] ([StockId], [ProductId], [SupplierId]) VALUES (153, 48, 3)
INSERT [dbo].[SupplierStock] ([StockId], [ProductId], [SupplierId]) VALUES (154, 49, 4)
INSERT [dbo].[SupplierStock] ([StockId], [ProductId], [SupplierId]) VALUES (155, 50, 5)
INSERT [dbo].[SupplierStock] ([StockId], [ProductId], [SupplierId]) VALUES (156, 51, 1)
INSERT [dbo].[SupplierStock] ([StockId], [ProductId], [SupplierId]) VALUES (157, 52, 2)
INSERT [dbo].[SupplierStock] ([StockId], [ProductId], [SupplierId]) VALUES (158, 53, 3)
INSERT [dbo].[SupplierStock] ([StockId], [ProductId], [SupplierId]) VALUES (159, 54, 4)
INSERT [dbo].[SupplierStock] ([StockId], [ProductId], [SupplierId]) VALUES (160, 55, 5)
INSERT [dbo].[SupplierStock] ([StockId], [ProductId], [SupplierId]) VALUES (161, 56, 1)
INSERT [dbo].[SupplierStock] ([StockId], [ProductId], [SupplierId]) VALUES (162, 57, 2)
INSERT [dbo].[SupplierStock] ([StockId], [ProductId], [SupplierId]) VALUES (163, 58, 3)
INSERT [dbo].[SupplierStock] ([StockId], [ProductId], [SupplierId]) VALUES (164, 59, 4)
INSERT [dbo].[SupplierStock] ([StockId], [ProductId], [SupplierId]) VALUES (165, 60, 5)
INSERT [dbo].[SupplierStock] ([StockId], [ProductId], [SupplierId]) VALUES (166, 61, 1)
INSERT [dbo].[SupplierStock] ([StockId], [ProductId], [SupplierId]) VALUES (167, 62, 2)
INSERT [dbo].[SupplierStock] ([StockId], [ProductId], [SupplierId]) VALUES (168, 63, 3)
INSERT [dbo].[SupplierStock] ([StockId], [ProductId], [SupplierId]) VALUES (169, 64, 4)
INSERT [dbo].[SupplierStock] ([StockId], [ProductId], [SupplierId]) VALUES (170, 65, 5)
INSERT [dbo].[SupplierStock] ([StockId], [ProductId], [SupplierId]) VALUES (171, 66, 1)
INSERT [dbo].[SupplierStock] ([StockId], [ProductId], [SupplierId]) VALUES (172, 67, 2)
INSERT [dbo].[SupplierStock] ([StockId], [ProductId], [SupplierId]) VALUES (173, 68, 3)
INSERT [dbo].[SupplierStock] ([StockId], [ProductId], [SupplierId]) VALUES (174, 69, 4)
INSERT [dbo].[SupplierStock] ([StockId], [ProductId], [SupplierId]) VALUES (175, 70, 5)
INSERT [dbo].[SupplierStock] ([StockId], [ProductId], [SupplierId]) VALUES (176, 71, 1)
INSERT [dbo].[SupplierStock] ([StockId], [ProductId], [SupplierId]) VALUES (177, 72, 2)
INSERT [dbo].[SupplierStock] ([StockId], [ProductId], [SupplierId]) VALUES (178, 73, 3)
INSERT [dbo].[SupplierStock] ([StockId], [ProductId], [SupplierId]) VALUES (179, 74, 4)
INSERT [dbo].[SupplierStock] ([StockId], [ProductId], [SupplierId]) VALUES (180, 75, 5)
SET IDENTITY_INSERT [dbo].[SupplierStock] OFF
GO
SET IDENTITY_INSERT [dbo].[Technical] ON 

INSERT [dbo].[Technical] ([TechnicalId], [FullName], [Phone], [Address], [DOB], [Avatar], [StatusId]) VALUES (1, N'Nguyễn Văn Anh', N'0987654321', N'Hà Nội', CAST(N'1980-01-01T00:00:00.000' AS DateTime), NULL, 1)
INSERT [dbo].[Technical] ([TechnicalId], [FullName], [Phone], [Address], [DOB], [Avatar], [StatusId]) VALUES (2, N'Trần Thị Bắc', N'0901234567', N'Tp.HCM', CAST(N'1995-05-20T00:00:00.000' AS DateTime), NULL, 1)
INSERT [dbo].[Technical] ([TechnicalId], [FullName], [Phone], [Address], [DOB], [Avatar], [StatusId]) VALUES (3, N'Lê Minh Canh', N'0912345678', N'Đà Nẵng', CAST(N'1990-12-31T00:00:00.000' AS DateTime), NULL, 2)
INSERT [dbo].[Technical] ([TechnicalId], [FullName], [Phone], [Address], [DOB], [Avatar], [StatusId]) VALUES (4, N'Phạm Thanh Dũng', N'0976543210', N'Hải Phòng', CAST(N'1985-06-15T00:00:00.000' AS DateTime), NULL, 1)
INSERT [dbo].[Technical] ([TechnicalId], [FullName], [Phone], [Address], [DOB], [Avatar], [StatusId]) VALUES (5, N'Đặng Hoàng Nam', N'0965432109', N'Nghệ An', CAST(N'2000-03-10T00:00:00.000' AS DateTime), NULL, 1)
SET IDENTITY_INSERT [dbo].[Technical] OFF
GO
ALTER TABLE [dbo].[Account]  WITH CHECK ADD  CONSTRAINT [FK_Account_Admin] FOREIGN KEY([AdminId])
REFERENCES [dbo].[Admin] ([AdminId])
GO
ALTER TABLE [dbo].[Account] CHECK CONSTRAINT [FK_Account_Admin]
GO
ALTER TABLE [dbo].[Account]  WITH CHECK ADD  CONSTRAINT [FK_Account_Blogger] FOREIGN KEY([BloggerId])
REFERENCES [dbo].[Blogger] ([BloggerId])
GO
ALTER TABLE [dbo].[Account] CHECK CONSTRAINT [FK_Account_Blogger]
GO
ALTER TABLE [dbo].[Account]  WITH CHECK ADD  CONSTRAINT [FK_Account_Consultant] FOREIGN KEY([ConsultantId])
REFERENCES [dbo].[Consultant] ([ConsultantId])
GO
ALTER TABLE [dbo].[Account] CHECK CONSTRAINT [FK_Account_Consultant]
GO
ALTER TABLE [dbo].[Account]  WITH CHECK ADD  CONSTRAINT [FK_Account_Customers] FOREIGN KEY([CustomerId])
REFERENCES [dbo].[Customers] ([CustomerId])
GO
ALTER TABLE [dbo].[Account] CHECK CONSTRAINT [FK_Account_Customers]
GO
ALTER TABLE [dbo].[Account]  WITH CHECK ADD  CONSTRAINT [FK_Account_HStatus] FOREIGN KEY([Status])
REFERENCES [dbo].[HStatus] ([StatusId])
GO
ALTER TABLE [dbo].[Account] CHECK CONSTRAINT [FK_Account_HStatus]
GO
ALTER TABLE [dbo].[Account]  WITH CHECK ADD  CONSTRAINT [FK_Account_Manager] FOREIGN KEY([ManagerId])
REFERENCES [dbo].[Manager] ([ManagerId])
GO
ALTER TABLE [dbo].[Account] CHECK CONSTRAINT [FK_Account_Manager]
GO
ALTER TABLE [dbo].[Account]  WITH CHECK ADD  CONSTRAINT [FK_Account_Role] FOREIGN KEY([RoleId])
REFERENCES [dbo].[Role] ([RoleId])
GO
ALTER TABLE [dbo].[Account] CHECK CONSTRAINT [FK_Account_Role]
GO
ALTER TABLE [dbo].[Account]  WITH CHECK ADD  CONSTRAINT [FK_Account_Sale] FOREIGN KEY([SaleId])
REFERENCES [dbo].[Sale] ([SaleId])
GO
ALTER TABLE [dbo].[Account] CHECK CONSTRAINT [FK_Account_Sale]
GO
ALTER TABLE [dbo].[Account]  WITH CHECK ADD  CONSTRAINT [FK_Account_Shipper] FOREIGN KEY([ShipperId])
REFERENCES [dbo].[Shipper] ([ShipperID])
GO
ALTER TABLE [dbo].[Account] CHECK CONSTRAINT [FK_Account_Shipper]
GO
ALTER TABLE [dbo].[Account]  WITH CHECK ADD  CONSTRAINT [FK_Account_Supplier] FOREIGN KEY([SupplierId])
REFERENCES [dbo].[Supplier] ([SupplierId])
GO
ALTER TABLE [dbo].[Account] CHECK CONSTRAINT [FK_Account_Supplier]
GO
ALTER TABLE [dbo].[Account]  WITH CHECK ADD  CONSTRAINT [FK_Account_Technical] FOREIGN KEY([TechnicalId])
REFERENCES [dbo].[Technical] ([TechnicalId])
GO
ALTER TABLE [dbo].[Account] CHECK CONSTRAINT [FK_Account_Technical]
GO
ALTER TABLE [dbo].[Admin]  WITH CHECK ADD  CONSTRAINT [FK_Admin_HStatus] FOREIGN KEY([StatusID])
REFERENCES [dbo].[HStatus] ([StatusId])
GO
ALTER TABLE [dbo].[Admin] CHECK CONSTRAINT [FK_Admin_HStatus]
GO
ALTER TABLE [dbo].[Alert]  WITH CHECK ADD  CONSTRAINT [FK_Alert_Customers] FOREIGN KEY([CustomerID])
REFERENCES [dbo].[Customers] ([CustomerId])
GO
ALTER TABLE [dbo].[Alert] CHECK CONSTRAINT [FK_Alert_Customers]
GO
ALTER TABLE [dbo].[Alert]  WITH CHECK ADD  CONSTRAINT [FK_Alert_Order] FOREIGN KEY([OrderID])
REFERENCES [dbo].[Order] ([OrderId])
GO
ALTER TABLE [dbo].[Alert] CHECK CONSTRAINT [FK_Alert_Order]
GO
ALTER TABLE [dbo].[Alert]  WITH CHECK ADD  CONSTRAINT [FK_Alert_Sale] FOREIGN KEY([SaleID])
REFERENCES [dbo].[Sale] ([SaleId])
GO
ALTER TABLE [dbo].[Alert] CHECK CONSTRAINT [FK_Alert_Sale]
GO
ALTER TABLE [dbo].[Alert]  WITH CHECK ADD  CONSTRAINT [FK_Alert_Shipper] FOREIGN KEY([ShipperID])
REFERENCES [dbo].[Shipper] ([ShipperID])
GO
ALTER TABLE [dbo].[Alert] CHECK CONSTRAINT [FK_Alert_Shipper]
GO
ALTER TABLE [dbo].[Blog]  WITH CHECK ADD  CONSTRAINT [FK_Blog_BlogCategory] FOREIGN KEY([CateId])
REFERENCES [dbo].[BlogCategory] ([BCategory])
GO
ALTER TABLE [dbo].[Blog] CHECK CONSTRAINT [FK_Blog_BlogCategory]
GO
ALTER TABLE [dbo].[Blog]  WITH CHECK ADD  CONSTRAINT [FK_Blog_Blogger] FOREIGN KEY([BloggerId])
REFERENCES [dbo].[Blogger] ([BloggerId])
GO
ALTER TABLE [dbo].[Blog] CHECK CONSTRAINT [FK_Blog_Blogger]
GO
ALTER TABLE [dbo].[Comment]  WITH CHECK ADD  CONSTRAINT [FK_Comment_Blogger] FOREIGN KEY([BloggerId])
REFERENCES [dbo].[Blogger] ([BloggerId])
GO
ALTER TABLE [dbo].[Comment] CHECK CONSTRAINT [FK_Comment_Blogger]
GO
ALTER TABLE [dbo].[Comment]  WITH CHECK ADD  CONSTRAINT [FK_Comment_Customers] FOREIGN KEY([CustomerId])
REFERENCES [dbo].[Customers] ([CustomerId])
GO
ALTER TABLE [dbo].[Comment] CHECK CONSTRAINT [FK_Comment_Customers]
GO
ALTER TABLE [dbo].[Consultant]  WITH CHECK ADD  CONSTRAINT [FK_Consultant_HStatus] FOREIGN KEY([StatusId])
REFERENCES [dbo].[HStatus] ([StatusId])
GO
ALTER TABLE [dbo].[Consultant] CHECK CONSTRAINT [FK_Consultant_HStatus]
GO
ALTER TABLE [dbo].[Contract]  WITH CHECK ADD  CONSTRAINT [FK_Contract_Supplier] FOREIGN KEY([SupplierId])
REFERENCES [dbo].[Supplier] ([SupplierId])
GO
ALTER TABLE [dbo].[Contract] CHECK CONSTRAINT [FK_Contract_Supplier]
GO
ALTER TABLE [dbo].[Contract]  WITH CHECK ADD  CONSTRAINT [FK_Contract_Technical] FOREIGN KEY([TechnicalId])
REFERENCES [dbo].[Technical] ([TechnicalId])
GO
ALTER TABLE [dbo].[Contract] CHECK CONSTRAINT [FK_Contract_Technical]
GO
ALTER TABLE [dbo].[Customers]  WITH CHECK ADD  CONSTRAINT [FK_Customers_HStatus] FOREIGN KEY([StatusId])
REFERENCES [dbo].[HStatus] ([StatusId])
GO
ALTER TABLE [dbo].[Customers] CHECK CONSTRAINT [FK_Customers_HStatus]
GO
ALTER TABLE [dbo].[IdentityCard]  WITH CHECK ADD  CONSTRAINT [FK_IdentityCard_Customers] FOREIGN KEY([CustomerId])
REFERENCES [dbo].[Customers] ([CustomerId])
GO
ALTER TABLE [dbo].[IdentityCard] CHECK CONSTRAINT [FK_IdentityCard_Customers]
GO
ALTER TABLE [dbo].[Manager]  WITH CHECK ADD  CONSTRAINT [FK_Manager_HStatus] FOREIGN KEY([StatusId])
REFERENCES [dbo].[HStatus] ([StatusId])
GO
ALTER TABLE [dbo].[Manager] CHECK CONSTRAINT [FK_Manager_HStatus]
GO
ALTER TABLE [dbo].[Messages]  WITH CHECK ADD  CONSTRAINT [FK_Messages_Consultant] FOREIGN KEY([ConsultantId])
REFERENCES [dbo].[Consultant] ([ConsultantId])
GO
ALTER TABLE [dbo].[Messages] CHECK CONSTRAINT [FK_Messages_Consultant]
GO
ALTER TABLE [dbo].[Messages]  WITH CHECK ADD  CONSTRAINT [FK_Messages_Conversation] FOREIGN KEY([ConversationId])
REFERENCES [dbo].[Conversation] ([ConversationId])
GO
ALTER TABLE [dbo].[Messages] CHECK CONSTRAINT [FK_Messages_Conversation]
GO
ALTER TABLE [dbo].[Messages]  WITH CHECK ADD  CONSTRAINT [FK_Messages_Customers] FOREIGN KEY([CustomerId])
REFERENCES [dbo].[Customers] ([CustomerId])
GO
ALTER TABLE [dbo].[Messages] CHECK CONSTRAINT [FK_Messages_Customers]
GO
ALTER TABLE [dbo].[Order]  WITH CHECK ADD  CONSTRAINT [FK_Order_Customers] FOREIGN KEY([CustomerId])
REFERENCES [dbo].[Customers] ([CustomerId])
GO
ALTER TABLE [dbo].[Order] CHECK CONSTRAINT [FK_Order_Customers]
GO
ALTER TABLE [dbo].[Order]  WITH CHECK ADD  CONSTRAINT [FK_Order_Sale] FOREIGN KEY([SaleId])
REFERENCES [dbo].[Sale] ([SaleId])
GO
ALTER TABLE [dbo].[Order] CHECK CONSTRAINT [FK_Order_Sale]
GO
ALTER TABLE [dbo].[Order]  WITH CHECK ADD  CONSTRAINT [FK_Order_Shipper] FOREIGN KEY([ShipId])
REFERENCES [dbo].[Shipper] ([ShipperID])
GO
ALTER TABLE [dbo].[Order] CHECK CONSTRAINT [FK_Order_Shipper]
GO
ALTER TABLE [dbo].[OrderDetail]  WITH CHECK ADD  CONSTRAINT [FK_OrderDetail_Order] FOREIGN KEY([OrderId])
REFERENCES [dbo].[Order] ([OrderId])
GO
ALTER TABLE [dbo].[OrderDetail] CHECK CONSTRAINT [FK_OrderDetail_Order]
GO
ALTER TABLE [dbo].[OrderDetail]  WITH CHECK ADD  CONSTRAINT [FK_OrderDetail_Product] FOREIGN KEY([ProductId])
REFERENCES [dbo].[Product] ([ProductId])
GO
ALTER TABLE [dbo].[OrderDetail] CHECK CONSTRAINT [FK_OrderDetail_Product]
GO
ALTER TABLE [dbo].[OrderDetailCode]  WITH CHECK ADD  CONSTRAINT [FK_OrderDetailCode_OrderDetail] FOREIGN KEY([OrderDetailId])
REFERENCES [dbo].[OrderDetail] ([OrderDetailId])
GO
ALTER TABLE [dbo].[OrderDetailCode] CHECK CONSTRAINT [FK_OrderDetailCode_OrderDetail]
GO
ALTER TABLE [dbo].[OrderDetailCode]  WITH CHECK ADD  CONSTRAINT [FK_OrderDetailCode_ProductCode] FOREIGN KEY([ProductCodeId])
REFERENCES [dbo].[ProductCode] ([CodeId])
GO
ALTER TABLE [dbo].[OrderDetailCode] CHECK CONSTRAINT [FK_OrderDetailCode_ProductCode]
GO
ALTER TABLE [dbo].[Product]  WITH CHECK ADD  CONSTRAINT [FK_Product_Category] FOREIGN KEY([CategoryId])
REFERENCES [dbo].[Category] ([CategoryId])
GO
ALTER TABLE [dbo].[Product] CHECK CONSTRAINT [FK_Product_Category]
GO
ALTER TABLE [dbo].[Product]  WITH CHECK ADD  CONSTRAINT [FK_Product_PStatus] FOREIGN KEY([StatusId])
REFERENCES [dbo].[PStatus] ([StatusId])
GO
ALTER TABLE [dbo].[Product] CHECK CONSTRAINT [FK_Product_PStatus]
GO
ALTER TABLE [dbo].[ProductCode]  WITH CHECK ADD  CONSTRAINT [FK_ProductCode_Product] FOREIGN KEY([ProductId])
REFERENCES [dbo].[Product] ([ProductId])
GO
ALTER TABLE [dbo].[ProductCode] CHECK CONSTRAINT [FK_ProductCode_Product]
GO
ALTER TABLE [dbo].[ProductDetail]  WITH CHECK ADD  CONSTRAINT [FK_ProductDetail_Product] FOREIGN KEY([ProductId])
REFERENCES [dbo].[Product] ([ProductId])
GO
ALTER TABLE [dbo].[ProductDetail] CHECK CONSTRAINT [FK_ProductDetail_Product]
GO
ALTER TABLE [dbo].[Rent]  WITH CHECK ADD  CONSTRAINT [FK_Buy_Customers] FOREIGN KEY([CustomerId])
REFERENCES [dbo].[Customers] ([CustomerId])
GO
ALTER TABLE [dbo].[Rent] CHECK CONSTRAINT [FK_Buy_Customers]
GO
ALTER TABLE [dbo].[Rent]  WITH CHECK ADD  CONSTRAINT [FK_Buy_Order] FOREIGN KEY([OrderId])
REFERENCES [dbo].[Order] ([OrderId])
GO
ALTER TABLE [dbo].[Rent] CHECK CONSTRAINT [FK_Buy_Order]
GO
ALTER TABLE [dbo].[Sale]  WITH CHECK ADD  CONSTRAINT [FK_Sale_HStatus] FOREIGN KEY([StatusId])
REFERENCES [dbo].[HStatus] ([StatusId])
GO
ALTER TABLE [dbo].[Sale] CHECK CONSTRAINT [FK_Sale_HStatus]
GO
ALTER TABLE [dbo].[Shipper]  WITH CHECK ADD  CONSTRAINT [FK_Shipper_HStatus] FOREIGN KEY([StatusId])
REFERENCES [dbo].[HStatus] ([StatusId])
GO
ALTER TABLE [dbo].[Shipper] CHECK CONSTRAINT [FK_Shipper_HStatus]
GO
ALTER TABLE [dbo].[Stock]  WITH CHECK ADD  CONSTRAINT [FK_Stock_Supplier] FOREIGN KEY([SupplierId])
REFERENCES [dbo].[Supplier] ([SupplierId])
GO
ALTER TABLE [dbo].[Stock] CHECK CONSTRAINT [FK_Stock_Supplier]
GO
ALTER TABLE [dbo].[Stock]  WITH CHECK ADD  CONSTRAINT [FK_Stock_SupplierProduct] FOREIGN KEY([ProductId])
REFERENCES [dbo].[SupplierProduct] ([ProductId])
GO
ALTER TABLE [dbo].[Stock] CHECK CONSTRAINT [FK_Stock_SupplierProduct]
GO
ALTER TABLE [dbo].[Stock]  WITH CHECK ADD  CONSTRAINT [FK_Stock_Technical] FOREIGN KEY([TechnicalId])
REFERENCES [dbo].[Technical] ([TechnicalId])
GO
ALTER TABLE [dbo].[Stock] CHECK CONSTRAINT [FK_Stock_Technical]
GO
ALTER TABLE [dbo].[Supplier]  WITH CHECK ADD  CONSTRAINT [FK_Supplier_HStatus] FOREIGN KEY([StatusId])
REFERENCES [dbo].[HStatus] ([StatusId])
GO
ALTER TABLE [dbo].[Supplier] CHECK CONSTRAINT [FK_Supplier_HStatus]
GO
ALTER TABLE [dbo].[SupplierAlert]  WITH CHECK ADD  CONSTRAINT [FK_SupplierAlert_Supplier] FOREIGN KEY([SupplierId])
REFERENCES [dbo].[Supplier] ([SupplierId])
GO
ALTER TABLE [dbo].[SupplierAlert] CHECK CONSTRAINT [FK_SupplierAlert_Supplier]
GO
ALTER TABLE [dbo].[SupplierAlert]  WITH CHECK ADD  CONSTRAINT [FK_SupplierAlert_SupplierProduct] FOREIGN KEY([SProductId])
REFERENCES [dbo].[SupplierProduct] ([ProductId])
GO
ALTER TABLE [dbo].[SupplierAlert] CHECK CONSTRAINT [FK_SupplierAlert_SupplierProduct]
GO
ALTER TABLE [dbo].[SupplierProduct]  WITH CHECK ADD  CONSTRAINT [FK_SupplierProduct_Category] FOREIGN KEY([CategoryId])
REFERENCES [dbo].[Category] ([CategoryId])
GO
ALTER TABLE [dbo].[SupplierProduct] CHECK CONSTRAINT [FK_SupplierProduct_Category]
GO
ALTER TABLE [dbo].[SupplierProduct]  WITH CHECK ADD  CONSTRAINT [FK_SupplierProduct_PStatus] FOREIGN KEY([StatusId])
REFERENCES [dbo].[PStatus] ([StatusId])
GO
ALTER TABLE [dbo].[SupplierProduct] CHECK CONSTRAINT [FK_SupplierProduct_PStatus]
GO
ALTER TABLE [dbo].[SupplierStock]  WITH CHECK ADD  CONSTRAINT [FK_SupplierStock_SupplierProduct] FOREIGN KEY([ProductId])
REFERENCES [dbo].[SupplierProduct] ([ProductId])
GO
ALTER TABLE [dbo].[SupplierStock] CHECK CONSTRAINT [FK_SupplierStock_SupplierProduct]
GO
ALTER TABLE [dbo].[Technical]  WITH CHECK ADD  CONSTRAINT [FK_Technical_HStatus] FOREIGN KEY([StatusId])
REFERENCES [dbo].[HStatus] ([StatusId])
GO
ALTER TABLE [dbo].[Technical] CHECK CONSTRAINT [FK_Technical_HStatus]
GO

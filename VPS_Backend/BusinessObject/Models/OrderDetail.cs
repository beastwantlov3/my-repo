﻿using System;
using System.Collections.Generic;

namespace BusinessObject.Models
{
    public partial class OrderDetail
    {
        public OrderDetail()
        {
            OrderDetailCodes = new HashSet<OrderDetailCode>();
        }

        public int OrderDetailId { get; set; }
        public int? OrderId { get; set; }
        public int? ProductId { get; set; }
        public double? UnitPrice { get; set; }
        public int? Quantity { get; set; }
        public double? Discount { get; set; }
        public bool? ThreeDays { get; set; }
        public bool? OneWeeks { get; set; }
        public string? ProductCode { get; set; }
        public int? Status { get; set; }

        public virtual Order? Order { get; set; }
        public virtual Product? Product { get; set; }
        public virtual ICollection<OrderDetailCode> OrderDetailCodes { get; set; }
    }
}

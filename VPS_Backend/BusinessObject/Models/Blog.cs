﻿using System;
using System.Collections.Generic;

namespace BusinessObject.Models
{
    public partial class Blog
    {
        public int Id { get; set; }
        public int? BloggerId { get; set; }
        public string? Title { get; set; }
        public string? Description { get; set; }
        public DateTime? PublishDate { get; set; }
        public string? Image { get; set; }
        public int? StatusId { get; set; }
        public int? CateId { get; set; }

        public virtual Blogger? Blogger { get; set; }
        public virtual BlogCategory? Cate { get; set; }
    }
}

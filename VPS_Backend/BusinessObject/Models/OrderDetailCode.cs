﻿using System;
using System.Collections.Generic;

namespace BusinessObject.Models
{
    public partial class OrderDetailCode
    {
        public int OrderDetailCodeId { get; set; }
        public int? OrderDetailId { get; set; }
        public int? ProductCodeId { get; set; }

        public virtual OrderDetail? OrderDetail { get; set; }
        public virtual ProductCode? ProductCode { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;

namespace BusinessObject.Models
{
    public partial class SupplierAlert
    {
        public int Sid { get; set; }
        public int? SproductId { get; set; }
        public int? Quantity { get; set; }
        public int? SupplierId { get; set; }
        public string? Message { get; set; }
        public bool? StatusId { get; set; }
        public DateTime? CurrentDate { get; set; }
        public bool? ThreeDays { get; set; }
        public bool? OneWeeks { get; set; }
        public decimal? Price { get; set; }

        public virtual SupplierProduct? Sproduct { get; set; }
        public virtual Supplier? Supplier { get; set; }
    }
}

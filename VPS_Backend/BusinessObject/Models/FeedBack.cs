﻿using System;
using System.Collections.Generic;

namespace BusinessObject.Models
{
    public partial class FeedBack
    {
        public int FeedBackId { get; set; }
        public int? CustomerId { get; set; }
        public int? OrderId { get; set; }
        public int? OrderDetailId { get; set; }
        public int? Star { get; set; }
        public string? Content { get; set; }
    }
}

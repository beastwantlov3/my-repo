﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace BusinessObject.Models
{
    public partial class VPSContext : DbContext
    {
        public VPSContext()
        {
        }

        public VPSContext(DbContextOptions<VPSContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Account> Accounts { get; set; } = null!;
        public virtual DbSet<Admin> Admins { get; set; } = null!;
        public virtual DbSet<Alert> Alerts { get; set; } = null!;
        public virtual DbSet<Blog> Blogs { get; set; } = null!;
        public virtual DbSet<BlogCategory> BlogCategories { get; set; } = null!;
        public virtual DbSet<Blogger> Bloggers { get; set; } = null!;
        public virtual DbSet<Category> Categories { get; set; } = null!;
        public virtual DbSet<Comment> Comments { get; set; } = null!;
        public virtual DbSet<Consultant> Consultants { get; set; } = null!;
        public virtual DbSet<Contract> Contracts { get; set; } = null!;
        public virtual DbSet<Conversation> Conversations { get; set; } = null!;
        public virtual DbSet<Customer> Customers { get; set; } = null!;
        public virtual DbSet<FeedBack> FeedBacks { get; set; } = null!;
        public virtual DbSet<Hstatus> Hstatuses { get; set; } = null!;
        public virtual DbSet<IdentityCard> IdentityCards { get; set; } = null!;
        public virtual DbSet<Manager> Managers { get; set; } = null!;
        public virtual DbSet<Message> Messages { get; set; } = null!;
        public virtual DbSet<Order> Orders { get; set; } = null!;
        public virtual DbSet<OrderDetail> OrderDetails { get; set; } = null!;
        public virtual DbSet<OrderDetailCode> OrderDetailCodes { get; set; } = null!;
        public virtual DbSet<Product> Products { get; set; } = null!;
        public virtual DbSet<ProductCode> ProductCodes { get; set; } = null!;
        public virtual DbSet<ProductDetail> ProductDetails { get; set; } = null!;
        public virtual DbSet<Pstatus> Pstatuses { get; set; } = null!;
        public virtual DbSet<Qrcode> Qrcodes { get; set; } = null!;
        public virtual DbSet<Rent> Rents { get; set; } = null!;
        public virtual DbSet<Role> Roles { get; set; } = null!;
        public virtual DbSet<Sale> Sales { get; set; } = null!;
        public virtual DbSet<Shipper> Shippers { get; set; } = null!;
        public virtual DbSet<Status> Statuses { get; set; } = null!;
        public virtual DbSet<Stock> Stocks { get; set; } = null!;
        public virtual DbSet<Supplier> Suppliers { get; set; } = null!;
        public virtual DbSet<SupplierAlert> SupplierAlerts { get; set; } = null!;
        public virtual DbSet<SupplierProduct> SupplierProducts { get; set; } = null!;
        public virtual DbSet<SupplierStock> SupplierStocks { get; set; } = null!;
        public virtual DbSet<Technical> Technicals { get; set; } = null!;

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
                optionsBuilder.UseSqlServer("server=THANHPHONG\\THANHPHONG;database=VPS;uid=sa;pwd=123;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Account>(entity =>
            {
                entity.ToTable("Account");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.Email).HasMaxLength(200);

                entity.Property(e => e.Password).HasMaxLength(50);

                entity.Property(e => e.Profile).HasMaxLength(500);

                entity.Property(e => e.UserName).HasMaxLength(50);

                entity.HasOne(d => d.Admin)
                    .WithMany(p => p.Accounts)
                    .HasForeignKey(d => d.AdminId)
                    .HasConstraintName("FK_Account_Admin");

                entity.HasOne(d => d.Blogger)
                    .WithMany(p => p.Accounts)
                    .HasForeignKey(d => d.BloggerId)
                    .HasConstraintName("FK_Account_Blogger");

                entity.HasOne(d => d.Consultant)
                    .WithMany(p => p.Accounts)
                    .HasForeignKey(d => d.ConsultantId)
                    .HasConstraintName("FK_Account_Consultant");

                entity.HasOne(d => d.Customer)
                    .WithMany(p => p.Accounts)
                    .HasForeignKey(d => d.CustomerId)
                    .HasConstraintName("FK_Account_Customers");

                entity.HasOne(d => d.Manager)
                    .WithMany(p => p.Accounts)
                    .HasForeignKey(d => d.ManagerId)
                    .HasConstraintName("FK_Account_Manager");

                entity.HasOne(d => d.Role)
                    .WithMany(p => p.Accounts)
                    .HasForeignKey(d => d.RoleId)
                    .HasConstraintName("FK_Account_Role");

                entity.HasOne(d => d.Sale)
                    .WithMany(p => p.Accounts)
                    .HasForeignKey(d => d.SaleId)
                    .HasConstraintName("FK_Account_Sale");

                entity.HasOne(d => d.Shipper)
                    .WithMany(p => p.Accounts)
                    .HasForeignKey(d => d.ShipperId)
                    .HasConstraintName("FK_Account_Shipper");

                entity.HasOne(d => d.StatusNavigation)
                    .WithMany(p => p.Accounts)
                    .HasForeignKey(d => d.Status)
                    .HasConstraintName("FK_Account_HStatus");

                entity.HasOne(d => d.Supplier)
                    .WithMany(p => p.Accounts)
                    .HasForeignKey(d => d.SupplierId)
                    .HasConstraintName("FK_Account_Supplier");

                entity.HasOne(d => d.Technical)
                    .WithMany(p => p.Accounts)
                    .HasForeignKey(d => d.TechnicalId)
                    .HasConstraintName("FK_Account_Technical");
            });

            modelBuilder.Entity<Admin>(entity =>
            {
                entity.ToTable("Admin");

                entity.Property(e => e.Address).HasMaxLength(250);

                entity.Property(e => e.Dob)
                    .HasColumnType("datetime")
                    .HasColumnName("DOB");

                entity.Property(e => e.FullName).HasMaxLength(250);

                entity.Property(e => e.Phone).HasMaxLength(250);

                entity.Property(e => e.StatusId).HasColumnName("StatusID");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.Admins)
                    .HasForeignKey(d => d.StatusId)
                    .HasConstraintName("FK_Admin_HStatus");
            });

            modelBuilder.Entity<Alert>(entity =>
            {
                entity.ToTable("Alert");

                entity.Property(e => e.AlertId).HasColumnName("AlertID");

                entity.Property(e => e.CurrentDate).HasColumnType("datetime");

                entity.Property(e => e.CustomerId).HasColumnName("CustomerID");

                entity.Property(e => e.Message).HasMaxLength(3000);

                entity.Property(e => e.OrderDetailId).HasColumnName("OrderDetailID");

                entity.Property(e => e.OrderId).HasColumnName("OrderID");

                entity.Property(e => e.SaleId).HasColumnName("SaleID");

                entity.Property(e => e.ShipperId).HasColumnName("ShipperID");

                entity.Property(e => e.StatusId).HasColumnName("StatusID");

                entity.HasOne(d => d.Customer)
                    .WithMany(p => p.Alerts)
                    .HasForeignKey(d => d.CustomerId)
                    .HasConstraintName("FK_Alert_Customers");

                entity.HasOne(d => d.Order)
                    .WithMany(p => p.Alerts)
                    .HasForeignKey(d => d.OrderId)
                    .HasConstraintName("FK_Alert_Order");

                entity.HasOne(d => d.Sale)
                    .WithMany(p => p.Alerts)
                    .HasForeignKey(d => d.SaleId)
                    .HasConstraintName("FK_Alert_Sale");

                entity.HasOne(d => d.Shipper)
                    .WithMany(p => p.Alerts)
                    .HasForeignKey(d => d.ShipperId)
                    .HasConstraintName("FK_Alert_Shipper");
            });

            modelBuilder.Entity<Blog>(entity =>
            {
                entity.ToTable("Blog");

                entity.Property(e => e.Image).HasMaxLength(150);

                entity.Property(e => e.PublishDate).HasColumnType("date");

                entity.Property(e => e.Title).HasMaxLength(200);

                entity.HasOne(d => d.Blogger)
                    .WithMany(p => p.Blogs)
                    .HasForeignKey(d => d.BloggerId)
                    .HasConstraintName("FK_Blog_Blogger");

                entity.HasOne(d => d.Cate)
                    .WithMany(p => p.Blogs)
                    .HasForeignKey(d => d.CateId)
                    .HasConstraintName("FK_Blog_BlogCategory");
            });

            modelBuilder.Entity<BlogCategory>(entity =>
            {
                entity.HasKey(e => e.Bcategory)
                    .HasName("PK__BlogCate__CFA4E5EEEBA18921");

                entity.ToTable("BlogCategory");

                entity.Property(e => e.Bcategory).HasColumnName("BCategory");

                entity.Property(e => e.Bname)
                    .HasMaxLength(200)
                    .HasColumnName("BName");
            });

            modelBuilder.Entity<Blogger>(entity =>
            {
                entity.ToTable("Blogger");

                entity.Property(e => e.Address).HasMaxLength(200);

                entity.Property(e => e.Dob)
                    .HasColumnType("datetime")
                    .HasColumnName("DOB");

                entity.Property(e => e.FullName).HasMaxLength(200);

                entity.Property(e => e.Phone).HasMaxLength(50);
            });

            modelBuilder.Entity<Category>(entity =>
            {
                entity.ToTable("Category");

                entity.Property(e => e.CategoryName).HasMaxLength(100);

                entity.Property(e => e.Description).HasColumnType("ntext");
            });

            modelBuilder.Entity<Comment>(entity =>
            {
                entity.ToTable("Comment");

                entity.Property(e => e.CommentDate).HasColumnType("date");

                entity.Property(e => e.Content).HasMaxLength(500);

                entity.HasOne(d => d.Blogger)
                    .WithMany(p => p.Comments)
                    .HasForeignKey(d => d.BloggerId)
                    .HasConstraintName("FK_Comment_Blogger");

                entity.HasOne(d => d.Customer)
                    .WithMany(p => p.Comments)
                    .HasForeignKey(d => d.CustomerId)
                    .HasConstraintName("FK_Comment_Customers");
            });

            modelBuilder.Entity<Consultant>(entity =>
            {
                entity.ToTable("Consultant");

                entity.Property(e => e.Address).HasMaxLength(200);

                entity.Property(e => e.Dob)
                    .HasColumnType("datetime")
                    .HasColumnName("DOB");

                entity.Property(e => e.FullName).HasMaxLength(200);

                entity.Property(e => e.Phone).HasMaxLength(50);

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.Consultants)
                    .HasForeignKey(d => d.StatusId)
                    .HasConstraintName("FK_Consultant_HStatus");
            });

            modelBuilder.Entity<Contract>(entity =>
            {
                entity.ToTable("Contract");

                entity.HasOne(d => d.Supplier)
                    .WithMany(p => p.Contracts)
                    .HasForeignKey(d => d.SupplierId)
                    .HasConstraintName("FK_Contract_Supplier");

                entity.HasOne(d => d.Technical)
                    .WithMany(p => p.Contracts)
                    .HasForeignKey(d => d.TechnicalId)
                    .HasConstraintName("FK_Contract_Technical");
            });

            modelBuilder.Entity<Conversation>(entity =>
            {
                entity.ToTable("Conversation");

                entity.Property(e => e.ConversationName).HasMaxLength(100);
            });

            modelBuilder.Entity<Customer>(entity =>
            {
                entity.Property(e => e.Address).HasMaxLength(200);

                entity.Property(e => e.Dob)
                    .HasColumnType("datetime")
                    .HasColumnName("DOB");

                entity.Property(e => e.FullName).HasMaxLength(200);

                entity.Property(e => e.Phone).HasMaxLength(50);

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.Customers)
                    .HasForeignKey(d => d.StatusId)
                    .HasConstraintName("FK_Customers_HStatus");
            });

            modelBuilder.Entity<FeedBack>(entity =>
            {
                entity.ToTable("FeedBack");

                entity.Property(e => e.Content).HasMaxLength(300);
            });

            modelBuilder.Entity<Hstatus>(entity =>
            {
                entity.HasKey(e => e.StatusId)
                    .HasName("PK__HStatus__C8EE20630553AF64");

                entity.ToTable("HStatus");

                entity.Property(e => e.StatusValue).HasMaxLength(50);
            });

            modelBuilder.Entity<IdentityCard>(entity =>
            {
                entity.HasKey(e => e.CardId)
                    .HasName("PK__Identity__55FECDAED0D5810B");

                entity.ToTable("IdentityCard");

                entity.Property(e => e.BackIdentityCard)
                    .HasMaxLength(250)
                    .IsFixedLength();

                entity.Property(e => e.CardNumber)
                    .HasMaxLength(14)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.DateOfBirth).HasColumnType("date");

                entity.Property(e => e.FullName).HasMaxLength(200);

                entity.Property(e => e.IdentityCardImage)
                    .HasMaxLength(250)
                    .IsFixedLength();

                entity.Property(e => e.Nationality).HasMaxLength(250);

                entity.Property(e => e.PlaceOfOrigin).HasMaxLength(250);

                entity.HasOne(d => d.Customer)
                    .WithMany(p => p.IdentityCards)
                    .HasForeignKey(d => d.CustomerId)
                    .HasConstraintName("FK_IdentityCard_Customers");
            });

            modelBuilder.Entity<Manager>(entity =>
            {
                entity.ToTable("Manager");

                entity.Property(e => e.ManagerId).ValueGeneratedNever();

                entity.Property(e => e.Address).HasMaxLength(200);

                entity.Property(e => e.Dob)
                    .HasColumnType("datetime")
                    .HasColumnName("DOB");

                entity.Property(e => e.FullName).HasMaxLength(200);

                entity.Property(e => e.Phone).HasMaxLength(50);

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.Managers)
                    .HasForeignKey(d => d.StatusId)
                    .HasConstraintName("FK_Manager_HStatus");
            });

            modelBuilder.Entity<Message>(entity =>
            {
                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.Time).HasColumnType("datetime");

                entity.HasOne(d => d.Consultant)
                    .WithMany(p => p.Messages)
                    .HasForeignKey(d => d.ConsultantId)
                    .HasConstraintName("FK_Messages_Consultant");

                entity.HasOne(d => d.Conversation)
                    .WithMany(p => p.Messages)
                    .HasForeignKey(d => d.ConversationId)
                    .HasConstraintName("FK_Messages_Conversation");

                entity.HasOne(d => d.Customer)
                    .WithMany(p => p.Messages)
                    .HasForeignKey(d => d.CustomerId)
                    .HasConstraintName("FK_Messages_Customers");
            });

            modelBuilder.Entity<Order>(entity =>
            {
                entity.ToTable("Order");

                entity.Property(e => e.CurrentImage).HasMaxLength(200);

                entity.Property(e => e.CustomerCancelled).HasColumnType("datetime");

                entity.Property(e => e.CustomerConfirm).HasColumnType("datetime");

                entity.Property(e => e.IdentityCardImage).HasMaxLength(200);

                entity.Property(e => e.ManagerCancelled).HasColumnType("datetime");

                entity.Property(e => e.ManagerConfirm).HasColumnType("datetime");

                entity.Property(e => e.ManagerViewed).HasColumnType("datetime");

                entity.Property(e => e.OrderDate).HasColumnType("datetime");

                entity.Property(e => e.SaleConfirm).HasColumnType("datetime");

                entity.Property(e => e.SalerCancelled).HasColumnType("datetime");

                entity.Property(e => e.ShipConfirm).HasColumnType("datetime");

                entity.Property(e => e.ShipCost).HasColumnType("money");

                entity.Property(e => e.ShipSuccessConfirm).HasColumnType("datetime");

                entity.Property(e => e.ShippedDate).HasColumnType("datetime");

                entity.Property(e => e.ShipperCancelled).HasColumnType("datetime");

                entity.HasOne(d => d.Customer)
                    .WithMany(p => p.Orders)
                    .HasForeignKey(d => d.CustomerId)
                    .HasConstraintName("FK_Order_Customers");

                entity.HasOne(d => d.Sale)
                    .WithMany(p => p.Orders)
                    .HasForeignKey(d => d.SaleId)
                    .HasConstraintName("FK_Order_Sale");

                entity.HasOne(d => d.Ship)
                    .WithMany(p => p.Orders)
                    .HasForeignKey(d => d.ShipId)
                    .HasConstraintName("FK_Order_Shipper");
            });

            modelBuilder.Entity<OrderDetail>(entity =>
            {
                entity.ToTable("OrderDetail");

                entity.Property(e => e.ProductCode)
                    .HasMaxLength(10)
                    .IsFixedLength();

                entity.HasOne(d => d.Order)
                    .WithMany(p => p.OrderDetails)
                    .HasForeignKey(d => d.OrderId)
                    .HasConstraintName("FK_OrderDetail_Order");

                entity.HasOne(d => d.Product)
                    .WithMany(p => p.OrderDetails)
                    .HasForeignKey(d => d.ProductId)
                    .HasConstraintName("FK_OrderDetail_Product");
            });

            modelBuilder.Entity<OrderDetailCode>(entity =>
            {
                entity.ToTable("OrderDetailCode");

                entity.HasOne(d => d.OrderDetail)
                    .WithMany(p => p.OrderDetailCodes)
                    .HasForeignKey(d => d.OrderDetailId)
                    .HasConstraintName("FK_OrderDetailCode_OrderDetail");

                entity.HasOne(d => d.ProductCode)
                    .WithMany(p => p.OrderDetailCodes)
                    .HasForeignKey(d => d.ProductCodeId)
                    .HasConstraintName("FK_OrderDetailCode_ProductCode");
            });

            modelBuilder.Entity<Product>(entity =>
            {
                entity.ToTable("Product");

                entity.Property(e => e.Brand).HasMaxLength(250);

                entity.Property(e => e.DiscountDate).HasColumnType("datetime");

                entity.Property(e => e.Moneytype).HasMaxLength(50);

                entity.Property(e => e.ProductName).HasMaxLength(250);

                entity.Property(e => e.Type).HasMaxLength(100);

                entity.HasOne(d => d.Category)
                    .WithMany(p => p.Products)
                    .HasForeignKey(d => d.CategoryId)
                    .HasConstraintName("FK_Product_Category");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.Products)
                    .HasForeignKey(d => d.StatusId)
                    .HasConstraintName("FK_Product_PStatus");
            });

            modelBuilder.Entity<ProductCode>(entity =>
            {
                entity.HasKey(e => e.CodeId);

                entity.ToTable("ProductCode");

                entity.Property(e => e.Code)
                    .HasMaxLength(20)
                    .IsFixedLength();

                entity.Property(e => e.QualityPercentage).HasColumnName("qualityPercentage");

                entity.HasOne(d => d.Product)
                    .WithMany(p => p.ProductCodes)
                    .HasForeignKey(d => d.ProductId)
                    .HasConstraintName("FK_ProductCode_Product");
            });

            modelBuilder.Entity<ProductDetail>(entity =>
            {
                entity.HasKey(e => e.DetailId)
                    .HasName("PK__ProductD__135C316D170C7915");

                entity.ToTable("ProductDetail");

                entity.Property(e => e.CoreImage).HasMaxLength(200);

                entity.HasOne(d => d.Product)
                    .WithMany(p => p.ProductDetails)
                    .HasForeignKey(d => d.ProductId)
                    .HasConstraintName("FK_ProductDetail_Product");
            });

            modelBuilder.Entity<Pstatus>(entity =>
            {
                entity.HasKey(e => e.StatusId)
                    .HasName("PK__PStatus__C8EE206371D3B24E");

                entity.ToTable("PStatus");

                entity.Property(e => e.StatusValue).HasMaxLength(50);
            });

            modelBuilder.Entity<Qrcode>(entity =>
            {
                entity.HasKey(e => e.Qrid)
                    .HasName("PK__QRCode__D8E9E6F859D8CDA7");

                entity.ToTable("QRCode");

                entity.Property(e => e.Qrid).HasColumnName("QRID");

                entity.Property(e => e.Code).HasMaxLength(250);

                entity.Property(e => e.DayCreate).HasColumnType("datetime");

                entity.Property(e => e.Description).HasMaxLength(250);
            });

            modelBuilder.Entity<Rent>(entity =>
            {
                entity.HasKey(e => e.BuyId)
                    .HasName("PK_Buy");

                entity.ToTable("Rent");

                entity.HasOne(d => d.Customer)
                    .WithMany(p => p.Rents)
                    .HasForeignKey(d => d.CustomerId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Buy_Customers");

                entity.HasOne(d => d.Order)
                    .WithMany(p => p.Rents)
                    .HasForeignKey(d => d.OrderId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Buy_Order");
            });

            modelBuilder.Entity<Role>(entity =>
            {
                entity.ToTable("Role");

                entity.Property(e => e.RoleName).HasMaxLength(200);
            });

            modelBuilder.Entity<Sale>(entity =>
            {
                entity.ToTable("Sale");

                entity.Property(e => e.Address).HasMaxLength(200);

                entity.Property(e => e.Dob)
                    .HasColumnType("datetime")
                    .HasColumnName("DOB");

                entity.Property(e => e.FullName).HasMaxLength(200);

                entity.Property(e => e.Phone).HasMaxLength(50);

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.Sales)
                    .HasForeignKey(d => d.StatusId)
                    .HasConstraintName("FK_Sale_HStatus");
            });

            modelBuilder.Entity<Shipper>(entity =>
            {
                entity.ToTable("Shipper");

                entity.Property(e => e.ShipperId).HasColumnName("ShipperID");

                entity.Property(e => e.Address).HasMaxLength(200);

                entity.Property(e => e.Dob)
                    .HasColumnType("datetime")
                    .HasColumnName("DOB");

                entity.Property(e => e.FullName).HasMaxLength(200);

                entity.Property(e => e.Phone).HasMaxLength(50);

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.Shippers)
                    .HasForeignKey(d => d.StatusId)
                    .HasConstraintName("FK_Shipper_HStatus");
            });

            modelBuilder.Entity<Status>(entity =>
            {
                entity.ToTable("Status");

                entity.Property(e => e.StatusValue).HasMaxLength(50);
            });

            modelBuilder.Entity<Stock>(entity =>
            {
                entity.ToTable("Stock");

                entity.HasOne(d => d.Product)
                    .WithMany(p => p.Stocks)
                    .HasForeignKey(d => d.ProductId)
                    .HasConstraintName("FK_Stock_SupplierProduct");

                entity.HasOne(d => d.Supplier)
                    .WithMany(p => p.Stocks)
                    .HasForeignKey(d => d.SupplierId)
                    .HasConstraintName("FK_Stock_Supplier");

                entity.HasOne(d => d.Technical)
                    .WithMany(p => p.Stocks)
                    .HasForeignKey(d => d.TechnicalId)
                    .HasConstraintName("FK_Stock_Technical");
            });

            modelBuilder.Entity<Supplier>(entity =>
            {
                entity.ToTable("Supplier");

                entity.Property(e => e.Address).HasMaxLength(200);

                entity.Property(e => e.Avatar).HasMaxLength(200);

                entity.Property(e => e.Dob)
                    .HasColumnType("datetime")
                    .HasColumnName("DOB");

                entity.Property(e => e.Phone).HasMaxLength(50);

                entity.Property(e => e.SupplierName).HasMaxLength(100);

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.Suppliers)
                    .HasForeignKey(d => d.StatusId)
                    .HasConstraintName("FK_Supplier_HStatus");
            });

            modelBuilder.Entity<SupplierAlert>(entity =>
            {
                entity.HasKey(e => e.Sid)
                    .HasName("PK__Supplier__CA195950EB7AD53D");

                entity.ToTable("SupplierAlert");

                entity.Property(e => e.Sid).HasColumnName("SId");

                entity.Property(e => e.CurrentDate).HasColumnType("datetime");

                entity.Property(e => e.Message).HasMaxLength(200);

                entity.Property(e => e.Price).HasColumnType("money");

                entity.Property(e => e.SproductId).HasColumnName("SProductId");

                entity.HasOne(d => d.Sproduct)
                    .WithMany(p => p.SupplierAlerts)
                    .HasForeignKey(d => d.SproductId)
                    .HasConstraintName("FK_SupplierAlert_SupplierProduct");

                entity.HasOne(d => d.Supplier)
                    .WithMany(p => p.SupplierAlerts)
                    .HasForeignKey(d => d.SupplierId)
                    .HasConstraintName("FK_SupplierAlert_Supplier");
            });

            modelBuilder.Entity<SupplierProduct>(entity =>
            {
                entity.HasKey(e => e.ProductId)
                    .HasName("PK__Supplier__B40CC6CD39EBF7B7");

                entity.ToTable("SupplierProduct");

                entity.Property(e => e.Brand).HasMaxLength(250);

                entity.Property(e => e.Description).HasMaxLength(250);

                entity.Property(e => e.Image).HasMaxLength(250);

                entity.Property(e => e.ProductName).HasMaxLength(250);

                entity.HasOne(d => d.Category)
                    .WithMany(p => p.SupplierProducts)
                    .HasForeignKey(d => d.CategoryId)
                    .HasConstraintName("FK_SupplierProduct_Category");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.SupplierProducts)
                    .HasForeignKey(d => d.StatusId)
                    .HasConstraintName("FK_SupplierProduct_PStatus");
            });

            modelBuilder.Entity<SupplierStock>(entity =>
            {
                entity.HasKey(e => e.StockId)
                    .HasName("PK__Supplier__2C83A9C2BABD531C");

                entity.ToTable("SupplierStock");

                entity.HasOne(d => d.Product)
                    .WithMany(p => p.SupplierStocks)
                    .HasForeignKey(d => d.ProductId)
                    .HasConstraintName("FK_SupplierStock_SupplierProduct");
            });

            modelBuilder.Entity<Technical>(entity =>
            {
                entity.ToTable("Technical");

                entity.Property(e => e.Address).HasMaxLength(200);

                entity.Property(e => e.Avatar).HasMaxLength(250);

                entity.Property(e => e.Dob)
                    .HasColumnType("datetime")
                    .HasColumnName("DOB");

                entity.Property(e => e.FullName).HasMaxLength(200);

                entity.Property(e => e.Phone).HasMaxLength(50);

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.Technicals)
                    .HasForeignKey(d => d.StatusId)
                    .HasConstraintName("FK_Technical_HStatus");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}

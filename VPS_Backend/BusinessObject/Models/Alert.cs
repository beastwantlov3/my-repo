﻿using System;
using System.Collections.Generic;

namespace BusinessObject.Models
{
    public partial class Alert
    {
        public int AlertId { get; set; }
        public int? CustomerId { get; set; }
        public int? SaleId { get; set; }
        public int? ShipperId { get; set; }
        public string? Message { get; set; }
        public int? StatusId { get; set; }
        public int? OrderId { get; set; }
        public DateTime? CurrentDate { get; set; }
        public int? OrderDetailId { get; set; }

        public virtual Customer? Customer { get; set; }
        public virtual Order? Order { get; set; }
        public virtual Sale? Sale { get; set; }
        public virtual Shipper? Shipper { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;

namespace BusinessObject.Models
{
    public partial class ProductCode
    {
        public ProductCode()
        {
            OrderDetailCodes = new HashSet<OrderDetailCode>();
        }

        public int CodeId { get; set; }
        public int? ProductId { get; set; }
        public string? Code { get; set; }
        public bool? IsAvailable { get; set; }
        public int? QualityPercentage { get; set; }

        public virtual Product? Product { get; set; }
        public virtual ICollection<OrderDetailCode> OrderDetailCodes { get; set; }
    }
}

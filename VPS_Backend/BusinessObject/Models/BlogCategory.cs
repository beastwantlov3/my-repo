﻿using System;
using System.Collections.Generic;

namespace BusinessObject.Models
{
    public partial class BlogCategory
    {
        public BlogCategory()
        {
            Blogs = new HashSet<Blog>();
        }

        public int Bcategory { get; set; }
        public string? Bname { get; set; }

        public virtual ICollection<Blog> Blogs { get; set; }
    }
}

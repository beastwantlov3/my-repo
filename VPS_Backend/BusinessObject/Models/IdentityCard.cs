﻿using System;
using System.Collections.Generic;

namespace BusinessObject.Models
{
    public partial class IdentityCard
    {
        public int CardId { get; set; }
        public string? CardNumber { get; set; }
        public string? FullName { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public bool? Gender { get; set; }
        public string? Nationality { get; set; }
        public string? PlaceOfOrigin { get; set; }
        public string? IdentityCardImage { get; set; }
        public string? BackIdentityCard { get; set; }
        public int? CustomerId { get; set; }

        public virtual Customer? Customer { get; set; }
    }
}

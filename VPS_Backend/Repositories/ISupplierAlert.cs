﻿using BusinessObject.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repositories
{
    public interface ISupplierAlert
    {
        List<SupplierAlert> GetSupplierAlert();

        List<SupplierAlert> GetListSupplierAlertById(int id);

        SupplierAlert GetSupplierAlertById(int id);


        void UpdateSupplierAlert(int id, SupplierAlert supplierAlert);

        void AddSupplierAlert(SupplierAlert supplierAlert);

        void DeleteSupplierAlert(int id);
    }
}

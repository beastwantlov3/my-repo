﻿using BusinessObject.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repositories
{
    public interface IAlertRepository
    {
        List<Alert> GetAlert();

        Alert GetAlertById(int id);

        void UpdateAlert(int id, Alert alert);

        void AddAlert(Alert alert);

        void DeleteAlert(int id);
    }
}

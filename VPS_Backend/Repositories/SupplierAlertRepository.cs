﻿using BusinessObject.Models;
using DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repositories
{
    public class SupplierAlertRepository : ISupplierAlert
    {

        public void AddSupplierAlert(SupplierAlert supplierAlert)
        {
            SupplierAlertDAO.AddSupplierAlert(supplierAlert);
        }

        public void DeleteSupplierAlert(int id)
        {
            SupplierAlertDAO.DeleteSupplierAlert(id);
        }

        public List<SupplierAlert> GetSupplierAlert()
        {
           return SupplierAlertDAO.GetSupplierAlert();
        }

        public SupplierAlert GetSupplierAlertById(int id)
        {
            return SupplierAlertDAO.GetSupplierAlertById(id);
        }

        public void UpdateSupplierAlert(int id, SupplierAlert supplierAlert)
        {
            SupplierAlertDAO.UpdateSupplierAlert(id, supplierAlert);
        }

        List<SupplierAlert> ISupplierAlert.GetSupplierAlert()
        {
            return SupplierAlertDAO.GetSupplierAlert();
        }

		List<SupplierAlert> ISupplierAlert.GetListSupplierAlertById(int id)
		{
			return SupplierAlertDAO.GetListSupplierAlertById(id);
		}
	}
}

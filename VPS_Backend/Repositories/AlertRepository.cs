﻿using BusinessObject.Models;
using DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repositories
{
    public class AlertRepository : IAlertRepository
    {
        public void AddAlert(Alert alert)
        {
            AlertDAO.AddAlert(alert);
        }

        public void DeleteAlert(int id)
        {
            AlertDAO.DeleteAlert(id);
        }

        public List<Alert> GetAlert()
        {
            return AlertDAO.GetAlert();
        }

        public Alert GetAlertById(int id)
        {
            return AlertDAO.GetAlertById(id);
        }

        public void UpdateAlert(int id, Alert alert)
        {
            AlertDAO.UpdateAlert(id, alert);
        }
    }
}

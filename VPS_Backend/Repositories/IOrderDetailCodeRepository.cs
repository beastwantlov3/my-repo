﻿using BusinessObject.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repositories
{
    public interface IOrderDetailCodeRepository
    {
        List<OrderDetailCode> GetOrderDetailCode();

        OrderDetailCode GetOrderDetailCodebyId(int id);

        void UpdateOrderDetailCode(int id, OrderDetailCode OrderDetailCode);

        void AddOrderDetailCode(OrderDetailCode OrderDetailCode);

        void DeleteOrderDetailCode(int id);
    }
}

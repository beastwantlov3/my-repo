﻿using BusinessObject.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repositories
{
    public interface IProductCodeRepository
    {
        List<ProductCode> GetProductCode();
        List<ProductCode> GetProductCodeByProductId(int id);

        ProductCode GetProductCodeById(int id);

        void UpdateProductCode(int id, ProductCode productCode);

        void AddProductCode(ProductCode productCode);

        void DeleteProductCode(int id);
    }
}

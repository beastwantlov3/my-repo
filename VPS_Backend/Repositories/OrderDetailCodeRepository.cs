﻿using BusinessObject.Models;
using DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repositories
{
    public class OrderDetailCodeRepository : IOrderDetailCodeRepository
    {
        public void AddOrderDetailCode(OrderDetailCode OrderDetailCode)
        {
            OrderDetailCodeDAO.AddOrderDetailCode(OrderDetailCode);
        }

        public void DeleteOrderDetailCode(int id)
        {
            OrderDetailCodeDAO.DeleteOrderDetailCode(id);
        }

        public List<OrderDetailCode> GetOrderDetailCode()
        {
            return OrderDetailCodeDAO.GetOrderDetailCode();
        }

        public OrderDetailCode GetOrderDetailCodebyId(int id)
        {
            return OrderDetailCodeDAO.GetOrderDetailCodesById(id);
        }

        public void UpdateOrderDetailCode(int id, OrderDetailCode OrderDetailCode)
        {
            OrderDetailCodeDAO.UpdateOrderDetailCode(id, OrderDetailCode);
        }
    }
}

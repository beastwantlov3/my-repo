﻿using BusinessObject.Models;
using DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repositories
{
    public class ProductCodeRepository : IProductCodeRepository
    {
        public void AddProductCode(ProductCode productCode)
        {
            ProductCodeDAO.AddProductCode(productCode);
        }

        public void DeleteProductCode(int id)
        {
            ProductCodeDAO.DeleteProductCode(id);
        }

        public List<ProductCode> GetProductCode()
        {
            return ProductCodeDAO.GetProductCode();
        }

        public List<ProductCode> GetProductCodeByProductId(int id)
        {
            return ProductCodeDAO.GetProductCodeByProductId(id);
        }


		public ProductCode GetProductCodeById(int id)
        {
            return ProductCodeDAO.GetProductById(id);
        }

        public void UpdateProductCode(int id, ProductCode productCode)
        {
            ProductCodeDAO.UpdateProductCode(id, productCode);
        }
    }
}
